# Notices for Eclipse StatET

This content is produced and maintained by the Eclipse StatET project.

  * Project home: https://projects.eclipse.org/projects/science.statet


## Trademarks

Eclipse StatET, and StatET are trademarks of the Eclipse Foundation.


## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.


## Declared Project Licenses

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
which is available at https://www.apache.org/licenses/LICENSE-2.0.

SPDX-License-Identifier: EPL-2.0 OR Apache-2.0

### Different Licenses

Particular contents are licensed under different terms than the declared
project licenses; see the about file of each plug-in.


## Source Code

The project maintains the following source code repositories:

  * https://git.eclipse.org/c/statet/org.eclipse.statet.git
  * https://git.eclipse.org/c/statet/org.eclipse.statet-commons.git
  * https://git.eclipse.org/c/statet/org.eclipse.statet-docmlet.git
  * https://git.eclipse.org/c/statet/org.eclipse.statet-eutils.git
  * https://git.eclipse.org/c/statet/org.eclipse.statet-ltk.git
  * https://git.eclipse.org/c/statet/org.eclipse.statet-r.git
  * https://git.eclipse.org/c/statet/org.eclipse.statet-rj.git


## Third-party Content

This project leverages the following third party content:

ICU4J 67.1
  * Project: http://site.icu-project.org
  * License: ICU AND Unicode-TOU AND BSD-3-Clause AND BSD-2-Clause AND
    LicenseRef-ipadic-license AND LicenseRef-Public-Domain

Apache Commons Logging 1.2
  * Project: https://commons.apache.org/logging
  * License: Apache-2.0

Apache Commons DBCP 2.9
  * Project: https://commons.apache.org/dbcp
  * License: Apache-2.0

Apache Commons Pool 2.11
  * Project: https://commons.apache.org/pool
  * License: Apache-2.0

Apache Derby 10.11
  * Project: https://db.apache.org/derby
  * License: Apache-2.0

Apache Lucene Core 7.5
  * Project: https://lucene.apache.org/core
  * License: Apache-2.0 AND MIT AND BSD-3-Clause

Apache Lucene Analyzers Common 7.5
  * Project: https://lucene.apache.org/core
  * License: Apache-2.0 AND BSD-3-Clause AND BSD-2-Clause

Apache Lucene Queries 7.5
  * Project: https://lucene.apache.org/core
  * License: Apache-2.0

Apache Lucene QueryParsers 7.5
  * Project: https://lucene.apache.org/core
  * License: Apache-2.0

Apache Lucene Highlighter 7.5
  * Project: https://lucene.apache.org/core
  * License: Apache-2.0

Apache Lucene Join 7.5
  * Project: https://lucene.apache.org/core
  * License: Apache-2.0

SnakeYAML 2.3
  * Project: https://bitbucket.org/asomov/snakeyaml-engine
  * License: Apache-2.0

SnakeYAML 1.28
  * Project: https://bitbucket.org/asomov/snakeyaml
  * License: Apache-2.0

JSch 0.1.55
  * Project: https://www.jcraft.com/jsch
  * License: New BSD license

Spring Core 5.3
  * Project: https://spring.io/projects/spring-framework
  * License: Apache-2.0 AND BSD-3-Clause

Spring Web 5.3
  * Project: https://spring.io/projects/spring-framework
  * License: Apache-2.0

Spring AOP 5.3
  * Project: https://spring.io/projects/spring-framework
  * License: Apache-2.0

Spring Beans 5.3
  * Project: https://spring.io/projects/spring-framework
  * License: Apache-2.0

Spring Context 5.3
  * Project: https://spring.io/projects/spring-framework
  * License: Apache-2.0

Spring Expression Language 5.3
  * Project: https://spring.io/projects/spring-framework
  * License: Apache-2.0

Spring Commons Logging Bridge (jcl) 5.3
  * Project: https://spring.io/projects/spring-framework
  * License: Apache-2.0

Spring Boot 2.5
  * Project: https://spring.io/projects/spring-boot
  * License: Apache-2.0

Spring Boot AutoConfigure 2.5
  * Project: https://spring.io/projects/spring-boot
  * License: Apache-2.0

SLF4J API 1.7
  * Project: https://www.slf4j.org
  * License: MIT

JUL to SLF4J Bridge 1.7
  * Project: https://www.slf4j.org
  * License: MIT

Logback Core 1.2
  * Project: https://logback.qos.ch
  * License: EPL-1.0

Logback Classic 1.2
  * Project: https://logback.qos.ch
  * License: EPL-1.0


## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
