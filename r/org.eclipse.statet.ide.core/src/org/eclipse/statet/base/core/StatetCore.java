/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.base.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.ecommons.net.ssh.core.ISshSessionService;

import org.eclipse.statet.internal.ide.core.BaseCorePlugin;


public class StatetCore {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ide.core"; //$NON-NLS-1$
	
	
	public static ISshSessionService getSshSessionManager() {
		return BaseCorePlugin.getInstance().getSshSessionManager();
	}
	
	private static void logError(final CoreException e) {
		BaseCorePlugin.getInstance().getLog().log(new Status(IStatus.ERROR, BUNDLE_ID, -1, "Error catched", e)); //$NON-NLS-1$
	}
	
	
	private StatetCore() {
	}
	
}
