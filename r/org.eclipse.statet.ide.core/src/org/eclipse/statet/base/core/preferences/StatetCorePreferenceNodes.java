/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.base.core.preferences;

import org.eclipse.statet.base.core.StatetCore;


public class StatetCorePreferenceNodes {
	
	
	public static final String CORE_QUALIFIER= StatetCore.BUNDLE_ID;
	
	public static final String CAT_MANAGMENT_QUALIFIER= CORE_QUALIFIER + "/managment"; //$NON-NLS-1$
	
	
}
