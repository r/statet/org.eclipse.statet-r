<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<!--
 #=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<plugin>

   <extension-point id="org.eclipse.statet.base.ui.codeGenerationTemplatesCategory"
         name="(Deprecated) Adds a new category of templates to the StatET dialog for editing code-templates"
         schema="schema/codeGenerationTemplatesCategory.exsd"/>
   
   <extension
         point="org.eclipse.core.runtime.preferences">
      <initializer class="org.eclipse.statet.internal.ide.ui.StatetUIPreferenceInitializer"/>
   </extension>
   <extension
         point="org.eclipse.ui.perspectives">
      <perspective
            class="org.eclipse.statet.internal.ide.ui.StatetPerspectiveFactory"
            icon="icons/view_16/statet_perspective.gif"
            id="org.eclipse.statet.base.perspectives.StatetPerspective"
            name="%perspectives_StatetPerspective_name"/>
   </extension>
   
   <extension
         point="org.eclipse.ui.preferencePages">
      <page
            id="org.eclipse.statet.base.preferencePages.CodeGenerationTemplates"
            category="org.eclipse.statet.r.preferencePages.R"
            class="org.eclipse.statet.internal.ide.ui.preferences.CodeGenerationTemplatesPreferencePage"
            name="%preferencePages_CodeGenerationTemplates_name">
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceTemplates"/>
         <keywordReference id="org.eclipse.statet.base.keywords.ProjectSpecific"/>
      </page>
      <page
            id="org.eclipse.statet.base.ui.preferencePages.TaskTags"
            category="org.eclipse.statet.r.preferencePages.R"
            class="org.eclipse.statet.internal.ide.ui.preferences.TaskTagsPreferencePage"
            name="%preferencePages_TaskTags_name">
         <keywordReference id="org.eclipse.statet.ltk.keywords.TaskTags"/>
         <keywordReference id="org.eclipse.statet.base.keywords.ProjectSpecific"/>
      </page>
   </extension>
   <extension
         point="org.eclipse.ui.propertyPages">
      <page
            class="org.eclipse.statet.internal.ide.ui.preferences.CodeGenerationTemplatesPreferencePage"
            id="org.eclipse.statet.base.propertyPages.CodeGenerationTemplates"
            name="%propertyPages_CodeGenerationTemplates_name"
            >
         <enabledWhen>
            <instanceof value="org.eclipse.core.resources.IProject"/>
         </enabledWhen>
         <filter
               name="nature"
               value="org.eclipse.statet.ide.resourceProjects.Statet"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceTemplates"/>
         <keywordReference id="org.eclipse.statet.base.keywords.ProjectSpecific"/>
      </page>
      <page
            class="org.eclipse.statet.internal.ide.ui.preferences.TaskTagsPreferencePage"
            id="org.eclipse.statet.base.propertyPages.TaskTags"
            name="%propertyPages_TaskTags_name"
            >
         <enabledWhen>
            <instanceof value="org.eclipse.core.resources.IProject"/>
         </enabledWhen>
         <filter
               name="nature"
               value="org.eclipse.statet.ide.resourceProjects.Statet"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.TaskTags"/>
         <keywordReference id="org.eclipse.statet.base.keywords.ProjectSpecific"/>
      </page>
   </extension>
   <extension
         point="org.eclipse.ui.keywords">
      <keyword
            id="org.eclipse.statet.base.keywords.StatetGeneral"
            label="%keywords_StatetGeneral"/>
      <keyword
            id="org.eclipse.statet.base.keywords.ProjectSpecific"
            label="%keywords_ProjectSpecific"/>
   </extension>
   <extension
         point="org.eclipse.ui.views">
      <category
            id="org.eclipse.statet.workbench.views.StatetCategory"
            name="StatET"/>
   </extension>
   <extension
         point="org.eclipse.ui.menus">
      <menuContribution
            locationURI="toolbar:org.eclipse.ui.main.toolbar">
         <toolbar
               id="org.eclipse.ui.edit.text.actionSet.presentation">
            <separator
                  name="Presentation">
            </separator>
         </toolbar>
      </menuContribution>
      <menuContribution
            locationURI="toolbar:org.eclipse.ui.edit.text.actionSet.presentation?after=Presentation">
         <command
               commandId="org.eclipse.jdt.ui.edit.text.java.toggleMarkOccurrences"
               style="toggle">
            <visibleWhen
                  checkEnabled="true">
               <and>
                  <with
                        variable="activeEditor">
                     <instanceof
                           value="org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1">
                     </instanceof>
                  </with>
                  <with
                        variable="activeContexts">
                     <iterate
                           ifEmpty="false"
                           operator="or">
                        <equals
                              value="org.eclipse.ui.edit.text.actionSet.presentation">
                        </equals>
                     </iterate>
                  </with>
               </and>
            </visibleWhen>
         </command>
      </menuContribution>
   </extension>
   
   <extension
         point="org.eclipse.ui.views">
      <view
            id="org.eclipse.statet.workbench.views.ContentFilter"
            category="org.eclipse.statet.workbench.views.StatetCategory"
            icon="icons/view_16/view-filter.png"
            name="%views_ContentFilterView_name"
            class="org.eclipse.statet.base.ui.contentfilter.FilterView"
            restorable="true">
      </view>
   </extension>

</plugin>
