/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.base.ui.contentfilter;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.IContributedContentsView;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.MessagePage;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.PageBookView;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class FilterView extends PageBookView
		implements IContributedContentsView {
	
	
	public static final String VIEW_ID= "org.eclipse.statet.workbench.views.ContentFilter"; //$NON-NLS-1$
	
	
	public FilterView() {
		super();
	}
	
	
	@Override
	protected IPage createDefaultPage(final PageBook book) {
		final MessagePage page= new MessagePage();
		initPage(page);
		page.createControl(book);
		page.setMessage("No data source available");
		
		initPage(page);
		page.createControl(book);
		
		return page;
	}
	
	@Override
	protected @Nullable PageRec doCreatePage(final IWorkbenchPart part) {
		final FilterPage page= part.getAdapter(FilterPage.class);
		if (page != null) {
			initPage(page);
			page.createControl(nonNullAssert(getPageBook()));
			return new PageRec(part, page);
		}
		return null;
	}
	
	@Override
	protected void doDestroyPage(final IWorkbenchPart part, final PageRec rec) {
		final FilterPage page= (FilterPage)rec.page;
		page.dispose();
		rec.dispose();
	}
	
	@Override
	protected @Nullable IWorkbenchPart getBootstrapPart() {
		final IWorkbenchPage page= getSite().getPage();
		if (page != null) {
			return page.getActiveEditor();
		}
		return null;
	}
	
	private @Nullable IWorkbenchPart getContributingEditor() {
		return getCurrentContributingPart();
	}
	
	@Override
	public @Nullable IWorkbenchPart getContributingPart() {
		return getContributingEditor();
	}
	
	@Override
	protected boolean isImportant(final IWorkbenchPart part) {
		return (part instanceof IEditorPart);
	}
	
	@Override
	public void partBroughtToTop(final IWorkbenchPart part) {
		partActivated(part);
	}
	
}
