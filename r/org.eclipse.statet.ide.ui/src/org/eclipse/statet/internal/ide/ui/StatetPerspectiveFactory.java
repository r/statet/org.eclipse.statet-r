/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.ui;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import org.eclipse.statet.ecommons.ui.workbench.IWorkbenchPerspectiveElements;


public class StatetPerspectiveFactory implements IPerspectiveFactory, IWorkbenchPerspectiveElements {
	
	
	/**
	 * Constructs a new Default layout engine.
	 */
	public StatetPerspectiveFactory() {
		super();
	}
	
	@Override
	public void createInitialLayout(final IPageLayout layout) {
		final String editorArea = layout.getEditorArea();
		
		final IFolderLayout leftFolder = layout.createFolder(
				"left", IPageLayout.LEFT, 0.25f, editorArea); //$NON-NLS-1$
		leftFolder.addView(PROJECT_EXPLORER_VIEW);
		
		final IFolderLayout outputfolder = layout.createFolder(
				"bottom", IPageLayout.BOTTOM, 0.70f, editorArea); //$NON-NLS-1$
		outputfolder.addView(CONSOLE_VIEW);
		outputfolder.addView(TASKS_VIEW);
		outputfolder.addPlaceholder(PROBLEM_VIEW);
		outputfolder.addPlaceholder(SEARCH_VIEW);
		outputfolder.addPlaceholder(BOOKMARKS_VIEW);
		outputfolder.addPlaceholder(PROGRESS_VIEW);
		
		final IFolderLayout editorAddFolder = layout.createFolder(
				"editor-additions", IPageLayout.RIGHT, 0.75f, editorArea); //$NON-NLS-1$
		editorAddFolder.addView(OUTLINE_VIEW);
		editorAddFolder.addPlaceholder(TEMPLATES_VIEW);
		editorAddFolder.addPlaceholder(FILTERS_VIEW);
		
		final IFolderLayout consoleAddFolder = layout.createFolder(
				"console-additions", IPageLayout.BOTTOM, 0.60f, "left"); //$NON-NLS-1$ //$NON-NLS-2$
		consoleAddFolder.addView(NICO_OBJECTBROWSER_VIEW);
		consoleAddFolder.addView(NICO_CMDHISTORY_VIEW);
		consoleAddFolder.addPlaceholder("org.eclipse.statet.r.apps.views.VariableViewer"); //$NON-NLS-1$
		
		layout.addActionSet(LAUNCH_ACTION_SET);
		layout.addActionSet(BREAKPOINT_ACTION_SET);
		layout.addActionSet(NAVIGATE_ACTION_SET);
		
		// views - search
		layout.addShowViewShortcut(SEARCH_VIEW);
		
		// views - standard workbench
		layout.addShowViewShortcut(PROJECT_EXPLORER_VIEW);
		layout.addShowViewShortcut(OUTLINE_VIEW);
		layout.addShowViewShortcut(TEMPLATES_VIEW);
		layout.addShowViewShortcut(TASKS_VIEW);
		layout.addShowViewShortcut(PROBLEM_VIEW);
		
		// views - debugging
		layout.addShowViewShortcut(CONSOLE_VIEW);
		layout.addShowViewShortcut(NICO_OBJECTBROWSER_VIEW);
		layout.addShowViewShortcut(NICO_CMDHISTORY_VIEW);
		
		// new actions
		layout.addNewWizardShortcut(NEW_FOLDER_WIZARD);
		layout.addNewWizardShortcut(NEW_TEXTFILE_WIZARD);
		layout.addNewWizardShortcut(NEW_UNTITLED_TEXTFILE_WIZARD);
		
		layout.addPerspectiveShortcut(DEBUG_PERSPECTIVE);
	}
	
}
