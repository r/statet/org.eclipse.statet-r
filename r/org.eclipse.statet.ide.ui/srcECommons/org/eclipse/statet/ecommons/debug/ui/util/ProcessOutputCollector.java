/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.util;

import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.ecommons.debug.core.util.LaunchUtils;

import org.eclipse.statet.internal.ecommons.debug.ui.Messages;
import org.eclipse.statet.internal.ide.ui.StatetUIPlugin;


/**
 * Utility class collecting the output of a process.
 */
public class ProcessOutputCollector extends Thread {
	
	
	private final String fName;
	
	private final ProcessBuilder fProcessBuilder;
	private Process fProcess;
	
	private boolean fIsRunning;
	private final IProgressMonitor fMonitor;
	
	private InputStreamReader fOutputInput;
	private final StringBuilder fBuffer;
	private Exception fReadException;
	
	
	public ProcessOutputCollector(final ProcessBuilder processBuilder, final String name, final IProgressMonitor monitor) {
		super(name+"-Output Monitor"); //$NON-NLS-1$
		this.fProcessBuilder = processBuilder;
		this.fName = name;
		this.fMonitor = monitor;
		this.fBuffer = new StringBuilder();
	}
	
	
	public String collect() throws CoreException {
		try {
			this.fProcessBuilder.redirectErrorStream(true);
			this.fProcess = this.fProcessBuilder.start();
			this.fOutputInput = new InputStreamReader(this.fProcess.getInputStream());
		}
		catch (final IOException e) {
			final String cmdInfo = LaunchUtils.generateCommandLine(this.fProcessBuilder.command());
			throw new CoreException(new Status(IStatus.ERROR, StatetUIPlugin.BUNDLE_ID, 0,
					NLS.bind(Messages.HelpRequestor_error_WhenRunProcess_message, this.fName, cmdInfo), e));
		}
		this.fIsRunning = true;
		this.fMonitor.worked(2);
		
		start();
		this.fMonitor.worked(1);
		
		while (this.fIsRunning) {
			try {
				this.fProcess.waitFor();
				this.fIsRunning = false;
			}
			catch (final InterruptedException e) {
				// forward to reader
				interrupt();
			}
		}
		this.fMonitor.worked(2);
		
		while (true) {
			try {
				join();
				if (this.fReadException != null) {
					throw new CoreException(new Status(IStatus.ERROR, StatetUIPlugin.BUNDLE_ID, -1,
							NLS.bind(Messages.HelpRequestor_error_WhenReadOutput_message, this.fName), this.fReadException));
				}
				this.fMonitor.worked(2);
				return this.fBuffer.toString();
			}
			catch (final InterruptedException e) {
				// forward to reader
				interrupt();
			}
		}
	}
	
	@Override
	public void run() {
		try {
			boolean canRead;
			final char[] b = new char[512];
			while (this.fIsRunning | (canRead = this.fOutputInput.ready())) {
				if (this.fMonitor.isCanceled()) {
					this.fProcess.destroy();
					return;
				}
				if (canRead) {
					final int n = this.fOutputInput.read(b);
					if (n > 0) {
						this.fBuffer.append(b, 0, n);
						continue;
					}
					if (n < 0) {
						return;
					}
				}
				try {
					Thread.sleep(50);
				}
				catch (final InterruptedException e) {
					// continue loop, monitor is checked
				}
			}
		}
		catch (final IOException e) {
			this.fReadException = e;
		}
		finally {
			try {
				this.fOutputInput.close();
			} catch (final IOException e1) {}
		}
	}
	
}
