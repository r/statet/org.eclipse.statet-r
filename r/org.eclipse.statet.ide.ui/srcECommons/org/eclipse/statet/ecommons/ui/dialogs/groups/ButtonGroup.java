/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs.groups;

import java.util.Arrays;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.ui.util.UIAccess;


public class ButtonGroup {

	
	public static interface ButtonListener {
		
		void handleButtonPressed(int buttonIdx);
	}
	
	
	private Composite fComposite;
	
	private Button[] fButtons;
	private final String[] fButtonsLabel;
	private final boolean[] fButtonsEnabled;
	
	private final ButtonListener fListener;
	
	
	public ButtonGroup(final String[] buttonsLabel, final ButtonListener listener) {
		
		this.fButtonsLabel = buttonsLabel;
		this.fButtonsEnabled = new boolean[this.fButtonsLabel.length]; // default disabled
		Arrays.fill(this.fButtonsEnabled, true);
		
		this.fListener = listener;
	}

	public void createGroup(final Layouter layouter) {

		this.fComposite = layouter.composite;
		
		final SelectionListener listener = new SelectionListener() {
			
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				doButtonSelected(e);
			}
			@Override
			public void widgetSelected(final SelectionEvent e) {
				doButtonSelected(e);
			}
		};
		
		this.fButtons = new Button[this.fButtonsLabel.length];
		for (int i = 0; i < this.fButtonsLabel.length; i++) {
			if (this.fButtonsLabel[i] != null) {
				this.fButtons[i] = layouter.addButton(this.fButtonsLabel[i], listener);
				this.fButtons[i].setEnabled(this.fButtonsEnabled[i]);
			}
			else {
				layouter.addFiller();
			}
		}
	}
	
	
	public void doButtonSelected(final SelectionEvent event) {
		
		for (int i = 0; i < this.fButtons.length; i++) {
			if (event.widget == this.fButtons[i]) {
				this.fListener.handleButtonPressed(i);
				return;
			}
		}
	}
	
	public void enableButton(final int buttonIdx, final boolean enable) {
		
		this.fButtonsEnabled[buttonIdx] = enable;
		if (this.fComposite.isEnabled()) {
			this.fButtons[buttonIdx].setEnabled(enable);
		}
	}
	
	public void updateButtonStatet() {
		
		if (this.fButtons == null || !UIAccess.isOkToUse(this.fComposite)) {
			return;
		}
		
		final boolean global = this.fComposite.isEnabled();
		for (int i = 0; i < this.fButtons.length; i++) {
			if (UIAccess.isOkToUse(this.fButtons[i])) {
				this.fButtons[i].setEnabled(global && this.fButtonsEnabled[i]);
			}
		}
	}

	public boolean isButtonEnabled(final int idx) {

		return this.fButtonsEnabled[idx];
	}
	
}
