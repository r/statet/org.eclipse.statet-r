/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs.groups;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.ecommons.ui.dialogs.groups.ButtonGroup.ButtonListener;
import org.eclipse.statet.ecommons.ui.dialogs.groups.CategorizedOptionsGroup.CategorizedItem;


public abstract class CategorizedOptionButtonsGroup<ItemT extends CategorizedItem> 
		extends CategorizedOptionsGroup<ItemT> {

	
	protected ButtonGroup fButtonGroup;
	
	
	public CategorizedOptionButtonsGroup(final String[] buttonLabels) {
		super(true, false);
	
		this.fButtonGroup = new ButtonGroup(buttonLabels, new ButtonListener() {
			@Override
			public void handleButtonPressed(final int buttonIdx) {
				final IStructuredSelection selection = getSelectedItems();
				CategorizedOptionButtonsGroup.this.handleButtonPressed(buttonIdx, getSingleItem(selection), selection);
			}
		});

	}

	@Override
	protected Control createOptionsControl(final Composite parent) {
		final Layouter options = new Layouter(new Composite(parent, SWT.NONE), 1);
		this.fButtonGroup.createGroup(options);
		
		return options.composite;
	}

	public abstract void handleButtonPressed(int buttonIdx, ItemT item, IStructuredSelection rawSelection);
	
}
