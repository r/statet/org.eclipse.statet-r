/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs.groups;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;



public abstract class SelectionOptionsGroup<ItemT extends Object> implements OptionsGroup {

	private final boolean fGrabSelectionHorizontal;
	private final boolean fGrabVertical;
	
	private final List<ItemT> fSelectionModel= new ArrayList<>();
	private Composite fComposite;
	
	
	public SelectionOptionsGroup(final boolean grabSelectionHorizontal, final boolean grabVertical) {
		this.fGrabSelectionHorizontal = grabSelectionHorizontal;
		this.fGrabVertical = grabVertical;
	}
	

	public List<ItemT> getListModel() {
		return this.fSelectionModel;
	}


	@Override
	public void createGroup(final Composite parent, final int hSpan) {
		final Layouter layouter = new Layouter(new Composite(parent, SWT.NONE), 2);
		this.fComposite = layouter.composite;
		this.fComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, this.fGrabVertical, hSpan, 1));
				
		final Control selectionControl = createSelectionControl(this.fComposite);
		selectionControl.setLayoutData(createSelectionGridData());

		final Control optionControl = createOptionsControl(this.fComposite);
		optionControl.setLayoutData(createOptionsGridData());
	}

	protected abstract Control createSelectionControl(Composite parent);
	
	protected GridData createSelectionGridData() {
		return new GridData(SWT.FILL, SWT.FILL, this.fGrabSelectionHorizontal, true);
	}

	protected abstract Control createOptionsControl(Composite parent);
	
	protected GridData createOptionsGridData() {
		return new GridData(SWT.FILL, SWT.FILL, !this.fGrabSelectionHorizontal, true);
	}
	
	/**
	 * Standard-Implementierung macht nichts.
	 */
	@Override
	public void initFields() {
	}

}
