/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.core;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.net.resourcemapping.core.IResourceMapping;


public final class ResourceMapping implements IResourceMapping {
	
	
	private static String cleanDirectory(final String path) {
		int idx= path.length() - 1;
		while (idx > 0) {
			final char c= path.charAt(idx);
			if (c == '/' || c == '\\') {
				idx--;
			}
			else {
				break;
			}
		}
		return path.substring(0, idx + 1);
	}
	
	
	private String id;
	
	private final String localText;
	private final IFileStore fileStore;
	private final IPath remotePath;
	
	private final String hostName;
	private InetAddress[] hostAddress;
	
	
	/**
	 * 
	 * @param id internal id
	 * @param localPath local path used to create IFileStore
	 * @param hostname hostname
	 * @param remotePath remote path
	 * @throws CoreException if a path is invalid
	 */
	public ResourceMapping(final String id, final String localPath, final String hostname, final String remotePath) throws CoreException {
		this.id= id;
		this.localText= localPath;
		this.fileStore= FileUtil.getFileStore(this.localText);
		this.hostName= hostname;
		this.remotePath= new Path(cleanDirectory(remotePath));
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public void setId(final String id) {
		assert (this.id == null);
		this.id= id;
	}
	
	public InetAddress[] getHostAddresses() {
		return this.hostAddress;
	}
	
	public void resolve() throws UnknownHostException {
		this.hostAddress= InetAddress.getAllByName(this.hostName);
	}
	
	public String getLocalText() {
		return this.localText;
	}
	
	@Override
	public IFileStore getFileStore() {
		return this.fileStore;
	}
	
	@Override
	public String getHost() {
		return this.hostName;
	}
	
	@Override
	public IPath getRemotePath() {
		return this.remotePath;
	}
	
}
