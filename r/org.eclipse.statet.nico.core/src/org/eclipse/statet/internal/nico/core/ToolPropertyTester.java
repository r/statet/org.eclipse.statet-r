/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.core;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IProcess;

import org.eclipse.statet.jcommons.ts.core.Tool;


public class ToolPropertyTester extends PropertyTester {
	
	
	public static final String IS_MAIN_TYPE = "isMainType"; //$NON-NLS-1$
	public static final String IS_PROVIDING_FEATURE = "isProvidingFeatureSet"; //$NON-NLS-1$
	public static final String IS_TERMINATED = "isTerminated"; //$NON-NLS-1$
	
	
	public ToolPropertyTester() {
	}
	
	
	@Override
	public boolean test(final Object receiver, final String property, final Object[] args, final Object expectedValue) {
		Tool tool = null;
		if (receiver instanceof Tool) {
			tool = (Tool) receiver;
		}
		else if (receiver instanceof IAdaptable) {
			tool = ((IAdaptable) receiver).getAdapter(Tool.class);
		}
		
		if (property.equals(IS_PROVIDING_FEATURE)) {
			return (tool != null &&
					tool.isProvidingFeatureSet((String) expectedValue));
		}
		if (property.equals(IS_MAIN_TYPE)) {
			return (tool != null &&
					tool.getMainType().equals(expectedValue));
		}
		if (property.equals(IS_TERMINATED)) {
			if (Boolean.FALSE.equals(expectedValue)) {
				return (tool != null && !tool.isTerminated());
			}
			else if (tool != null && tool.isTerminated()) {
				if (args == null || args.length == 0) {
					return true;
				}
				if (tool instanceof IProcess) {
					final IProcess process = (IProcess) tool;
					try {
						final int exitValue = process.getExitValue();
						for (final Object arg : args) {
							if (arg instanceof Integer && ((Integer) arg).intValue() == exitValue) {
								return true;
							}
						}
					}
					catch (final DebugException e) {
					}
				}
			}
			return false;
		}
		return false;
	}
	
}
