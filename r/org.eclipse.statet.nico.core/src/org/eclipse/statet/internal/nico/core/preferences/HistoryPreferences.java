/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.core.preferences;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.IntPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.nico.core.NicoPreferenceNodes;


/**
 * 
 */
public class HistoryPreferences {
	
	
	private static final String KEY_LIMIT_COUNT = "limit.count"; //$NON-NLS-1$
	
	private static final IntPref PREF_LIMIT_COUNT = new IntPref(
			NicoPreferenceNodes.CAT_HISTORY_QUALIFIER, KEY_LIMIT_COUNT);
	
	
	private int fLimitCount;
	
	
	/**
	 * Creates preferences with default values.
	 */
	public HistoryPreferences() {
		setup(10000);
	}
	
	public HistoryPreferences(final PreferenceAccess prefs) {
		load(prefs);
	}
	
	protected void setup(final int limitCount) {
		fLimitCount = limitCount; 
	}
	
	protected void load(final PreferenceAccess prefs) {
		final int limitCount = prefs.getPreferenceValue(PREF_LIMIT_COUNT);
		
		setup(limitCount);
	}
	
	
	/**
	 * Allows to save the preferences. 
	 * 
	 * <p>Note: Intended to usage in preference/property page only.</p>
	 */
	public Map<Preference<?>, Object> addPreferencesToMap(final Map<Preference<?>, Object> map) {
		map.put(PREF_LIMIT_COUNT, fLimitCount);
		return map;
	}
	
	/**
	 * Allows to save the preferences. 
	 * 
	 * <p>Note: Intended to usage in preference/property page only.</p>
	 */
	public Map<Preference<?>, Object> getPreferencesMap() {
		return addPreferencesToMap(new HashMap<Preference<?>, Object>(2));
	}
	
	
	public int getLimitCount() {
		return fLimitCount;
	}
	
	
	@Override
	public int hashCode() {
		return fLimitCount;
	}
	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(obj instanceof HistoryPreferences)) {
			return false;
		}
		
		final HistoryPreferences other = (HistoryPreferences) obj;
		return (fLimitCount == other.fLimitCount);
	}
	
}
