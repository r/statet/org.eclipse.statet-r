/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;


/**
 * Dummy scope to overlay instance preferences with special values for consoles.
 */
public final class ConsoleInstanceScope implements IScopeContext {
	
	
	public static final String SCOPE= "nico.instance"; //$NON-NLS-1$
	
	
	private final IScopeContext fBaseScope;
	
	
	public ConsoleInstanceScope() {
		fBaseScope= InstanceScope.INSTANCE;
	}
	
	
	@Override
	public IPath getLocation() {
		return fBaseScope.getLocation();
	}
	
	@Override
	public String getName() {
		return SCOPE;
	}
	
	@Override
	public IEclipsePreferences getNode(final String qualifier) {
		final int idx = qualifier.indexOf('/');
		if (idx < 0) {
			return (IEclipsePreferences) fBaseScope
					.getNode(NicoPreferenceNodes.SCOPE_QUALIFIER)
					.node(qualifier);
		}
		else {
			return (IEclipsePreferences) fBaseScope
					.getNode(qualifier.substring(0, idx))
					.node(NicoPreferenceNodes.SCOPE_QUALIFIER)
					.node(qualifier.substring(idx+1));
		}
	}
	
}
