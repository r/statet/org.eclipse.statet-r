/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core;

import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.InstanceScope;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;


public class NicoCore {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.nico.core"; //$NON-NLS-1$
	
	
	public static final int STATUS_CATEGORY = (3 << 16);
	
	/** Status Code for errors when handle Threads/Runnables */
	public static final int STATUSCODE_RUNTIME_ERROR = STATUS_CATEGORY | (2 << 8);
	
	public static final int EXITVALUE_CORE_EXCEPTION = STATUSCODE_RUNTIME_ERROR | 1;
	public static final int EXITVALUE_RUNTIME_EXCEPTION = STATUSCODE_RUNTIME_ERROR | 2;
	
	
	private static PreferenceAccess CONSOLE_PREF_ACCESS= EPreferences.createAccess(
			ImCollections.newList(
					new ConsoleInstanceScope(),
					new ConsoleDefaultScope(),
					InstanceScope.INSTANCE,
					DefaultScope.INSTANCE ));
	
	/**
	 * The instance preferences for consoles with the scope search path:
	 * <ol>
	 *     <li>ConsoleInstance</li>
	 *     <li>ConsoleDefault</li>
	 *     <li>Instance</li>
	 *     <li>Default</li>
	 * @return shared preference access to the preferences
	 */
	public static PreferenceAccess getInstanceConsolePreferences() {
		return CONSOLE_PREF_ACCESS;
	}
	
	
	private NicoCore() {}
	
}
