/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.ts.core.Tool;


/**
 * Public/shared strings of Nico Core.
 */
public class NicoCoreMessages {
	
	
	public static String Status_Starting_label;
	public static String Status_StartedIdle_label;
	public static String Status_StartedProcessing_label;
	public static String Status_StartedPaused_label;
	public static String Status_StartedSuspended_label;
	public static String Status_Terminated_label;
	
	public static String Status_Starting_info;
	public static String Status_StartedIdle_info;
	public static String Status_StartedProcessing_info;
	public static String Status_StartedPaused_info;
	public static String Status_StartedSuspended_info;
	public static String Status_Terminated_info;
	
	public static String ProblemWhileRunningTask_message;
	
	public static String LoadHistoryJob_label;
	public static String SaveHistoryJob_label;
	public static String SubmitTask_label;
	
	
	public static String format_ProblemWhileRunningTask_message(
			final String taskLabel, final Tool tool) {
		return String.format(ProblemWhileRunningTask_message,
				taskLabel, tool.getLabel(Tool.LONG_LABEL) );
	}
	
	
	static {
		NLS.initializeMessages(NicoCoreMessages.class.getName(), NicoCoreMessages.class);
	}
	private NicoCoreMessages() {}
	
}
