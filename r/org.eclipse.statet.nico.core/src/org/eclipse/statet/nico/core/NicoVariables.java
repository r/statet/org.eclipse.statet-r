/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core;

import org.eclipse.core.variables.IDynamicVariable;

import org.eclipse.statet.ecommons.variables.core.DateVariable;
import org.eclipse.statet.ecommons.variables.core.DynamicVariable;
import org.eclipse.statet.ecommons.variables.core.TimeVariable;

import org.eclipse.statet.internal.nico.core.Messages;


public class NicoVariables {
	
	public static final String SESSION_STARTUP_DATE_VARNAME= "session_startup_date"; //$NON-NLS-1$
	public static final IDynamicVariable SESSION_STARTUP_DATE_VARIABLE= new DateVariable(
			SESSION_STARTUP_DATE_VARNAME, Messages.SessionVariables_StartupDate_description );
	
	public static final String SESSION_STARTUP_TIME_VARNAME= "session_startup_time"; //$NON-NLS-1$
	public static final IDynamicVariable SESSION_STARTUP_TIME_VARIABLE= new TimeVariable(
			SESSION_STARTUP_TIME_VARNAME, Messages.SessionVariables_StartupTime_description );
	
	public static final String SESSION_CONNECTION_DATE_VARNAME= "session_connection_date"; //$NON-NLS-1$
	public static final IDynamicVariable SESSION_CONNECTION_DATE_VARIABLE= new DateVariable(
			SESSION_CONNECTION_DATE_VARNAME, Messages.SessionVariables_ConnectionDate_description );
	
	public static final String SESSION_CONNECTION_TIME_VARNAME= "session_connection_time"; //$NON-NLS-1$
	public static final IDynamicVariable SESSION_CONNECTION_TIME_VARIABLE= new TimeVariable(
			SESSION_CONNECTION_TIME_VARNAME, Messages.SessionVariables_ConnectionTime_description );
	
	public static final String SESSION_STARTUP_WD_VARNAME= "session_startup_wd"; //$NON-NLS-1$
	public static final IDynamicVariable SESSION_STARTUP_WD_VARIABLE= new DynamicVariable(
			SESSION_STARTUP_WD_VARNAME, Messages.SessionVariables_StartupWD_description, false );
	
}
