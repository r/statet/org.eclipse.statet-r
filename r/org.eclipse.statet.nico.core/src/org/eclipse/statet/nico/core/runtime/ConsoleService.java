/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;


/**
 * Basic tool interface for consoled based services
 */
@NonNullByDefault
public interface ConsoleService extends ToolService {
	
	
	int META_NONE=                      0;
	int META_HISTORY_DONTADD=           1 << 0;
	int META_PROMPT_DEFAULT=            1 << 1;
	int META_PROMPT_SUSPENDED=          1 << 2;
	
	
	/**
	 * Returns the controller of this tool.
	 * 
	 * @return the tool controller
	 */
	ToolController getController();
	
	/**
	 * Returns the process handler of this tool.
	 * 
	 * A tool returns always the same process instance.
	 * 
	 * @return the tool process
	 */
	@Override
	ToolProcess getTool();
	
	/**
	 * Returns the currently running tool runnable.
	 * 
	 * @return the current tool runnable
	 */
	@Nullable ToolRunnable getCurrentRunnable();
	
	/**
	 * Returns the workspace data of this tool.
	 * 
	 * A tool returns always the same workspace data instance.
	 * 
	 * @return the tool workspace
	 */
	ToolWorkspace getWorkspaceData();
	
	/**
	 * Refreshes the workspace data of this tool.
	 * 
	 * This method can be used to ensure that the workspace data is up-to-date.
	 * 
	 * @param options
	 * @param m the progress monitor of the current run (or a child)
	 * @throws StatusException if an error occurred or the operation was canceled
	 */
	void refreshWorkspaceData(final int options,
			final ProgressMonitor m) throws StatusException;
	
	
	/*---- Console ----*/
	
	/**
	 * Returns if the current prompt is the default prompt.
	 * 
	 * @return <code>true</code> if it is the default prompt, otherwise <code>false</code>
	 */
	boolean isDefaultPrompt();
	
	/**
	 * Returns the current prompt.
	 * 
	 * @return the current prompt
	 */
	Prompt getPrompt();
	
	/**
	 * Submits the text to the tool console.
	 * 
	 * @param input the text to submit
	 * @param m the progress monitor of the current run (or a child)
	 * @throws StatusException if an error occurred or the operation was canceled
	 */
	void submitToConsole(final String input,
			final ProgressMonitor m) throws StatusException;
	
	
	/*---- Status ----*/
	
	/**
	 * Reports a status to the user
	 * 
	 * @param status the status to handle
	 * @param m the progress monitor of the current run (or a child)
	 */
	void handleStatus(final Status status, final ProgressMonitor m);
	
}
