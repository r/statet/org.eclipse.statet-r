/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import org.eclipse.statet.nico.core.runtime.History.Entry;


/**
 * 
 * @see History
 * @see History#addListener(IHistoryListener)
 */
public interface IHistoryListener {
	
	
	void entryAdded(History source, Entry e);
	void entryRemoved(History source, Entry e);
	
	void completeChange(History source, Entry[] es);
	
	
}
