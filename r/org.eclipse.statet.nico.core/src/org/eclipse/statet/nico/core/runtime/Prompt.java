/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;


/**
 * 
 */
public class Prompt {
	
	public static final Prompt NONE = new Prompt("", 0); //$NON-NLS-1$
	public static final Prompt DEFAULT = new Prompt("> ", ConsoleService.META_PROMPT_DEFAULT); //$NON-NLS-1$
	
	
	public final String text;
	public final int meta;
	
	
	public Prompt(final String text, final int meta) {
		assert (text != null);
		this.text = text;
		this.meta = meta;
	}
	
}
