/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.util;

import java.util.EnumSet;

import org.eclipse.core.filesystem.EFS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.models.AbstractSettingsModelObject;

import org.eclipse.statet.nico.core.runtime.SubmitType;


@NonNullByDefault
public class TrackingConfiguration extends AbstractSettingsModelObject {
	
	
	static int DEFAULT_FILE_MODE= EFS.APPEND;
	
	
	private final String id;
	
	private String name;
	
	private boolean trackStreamInfo;
	private boolean trackStreamInput;
	private boolean trackStreamInputHistoryOnly;
	private boolean trackStreamOutput;
	private boolean trackStreamOutputTruncate;
	private int trackStreamOutputTruncateLines;
	
	private EnumSet<SubmitType> submitTypes;
	
	private String fFilePath;
	private int fFileMode;
	
	private boolean prependTimestamp;
	
	
	public TrackingConfiguration(final String id) {
		this.id= id;
		
		this.name= ""; //$NON-NLS-1$
		
		loadDefaults();
	}
	
	public TrackingConfiguration(final String id, final TrackingConfiguration template) {
		this.id= id;
		
		this.name= template.name;
		this.trackStreamInfo= template.trackStreamInfo;
		this.trackStreamInput= template.trackStreamInput;
		this.trackStreamInputHistoryOnly= template.trackStreamInputHistoryOnly;
		this.trackStreamOutput= template.trackStreamOutput;
		this.trackStreamOutputTruncate= template.trackStreamOutputTruncate;
		this.trackStreamOutputTruncateLines= template.trackStreamOutputTruncateLines;
		this.submitTypes= template.submitTypes;
		this.fFilePath= template.fFilePath;
		this.fFileMode= template.fFileMode;
		this.prependTimestamp= template.prependTimestamp;
	}
	
	
	public void loadDefaults() {
		setTrackStreamInfo(true);
		setTrackStreamInput(true);
		setTrackStreamInputHistoryOnly(false);
		setTrackStreamOutput(true);
		setTrackStreamOutputTruncate(false);
		setTrackStreamOutputTruncateLines(50);
		setSubmitTypes(SubmitType.getDefaultSet());
		setFilePath(""); //$NON-NLS-1$
		setFileMode(DEFAULT_FILE_MODE);
		setPrependTimestamp(true);
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(final String label) {
		final String oldValue= this.name;
		this.name= label;
		firePropertyChange("name", oldValue, label);
	}
	
	public boolean getTrackStreamInfo() {
		return this.trackStreamInfo;
	}
	
	public void setTrackStreamInfo(final boolean enable) {
		final boolean oldValue= this.trackStreamInfo;
		this.trackStreamInfo= enable;
		firePropertyChange("trackStreamInfo", oldValue, enable);
	}
	
	public boolean getTrackStreamInput() {
		return this.trackStreamInput;
	}
	
	public void setTrackStreamInput(final boolean enable) {
		final boolean oldValue= this.trackStreamInput;
		this.trackStreamInput= enable;
		firePropertyChange("trackStreamInput", oldValue, enable);
	}
	
	public boolean getTrackStreamInputHistoryOnly() {
		return this.trackStreamInputHistoryOnly;
	}
	
	public void setTrackStreamInputHistoryOnly(final boolean enable) {
		final boolean oldValue= this.trackStreamInputHistoryOnly;
		this.trackStreamInputHistoryOnly= enable;
		firePropertyChange("trackStreamInputHistoryOnly", oldValue, enable);
	}
	
	public boolean getTrackStreamOutput() {
		return this.trackStreamOutput;
	}
	
	public void setTrackStreamOutput(final boolean enable) {
		final boolean oldValue= this.trackStreamOutput;
		this.trackStreamOutput= enable;
		firePropertyChange("trackStreamOutput", oldValue, enable);
	}
	
	public boolean getTrackStreamOutputTruncate() {
		return this.trackStreamOutputTruncate;
	}
	
	public void setTrackStreamOutputTruncate(final boolean enable) {
		final boolean oldValue= this.trackStreamOutputTruncate;
		this.trackStreamOutputTruncate= enable;
		firePropertyChange("trackStreamOutputTruncate", oldValue, enable);
	}
	
	public int getTrackStreamOutputTruncateLines() {
		return this.trackStreamOutputTruncateLines;
	}
	
	public void setTrackStreamOutputTruncateLines(final int lines) {
		final int oldValue= this.trackStreamOutputTruncateLines;
		this.trackStreamOutputTruncateLines= lines;
		firePropertyChange("trackStreamOutputTruncateLines", oldValue, lines);
	}
	
	public EnumSet<SubmitType> getSubmitTypes() {
		return this.submitTypes;
	}
	
	public void setSubmitTypes(final EnumSet<SubmitType> typesToInclude) {
		final EnumSet<SubmitType> oldValue= this.submitTypes;
		this.submitTypes= typesToInclude;
		firePropertyChange("submitTypes", oldValue, typesToInclude);
	}
	
	public String getFilePath() {
		return this.fFilePath;
	}
	
	public void setFilePath(final String path) {
		final String oldValue= this.fFilePath;
		this.fFilePath= path;
		firePropertyChange("filePath", oldValue, path);
	}
	
	public int getFileMode() {
		return this.fFileMode;
	}
	
	public void setFileMode(final int mode) {
		final int oldValue= this.fFileMode;
		this.fFileMode= mode;
		firePropertyChange("fileMode", oldValue, mode);
	}
	
	public String getFileEncoding() {
		return "UTF-8"; //$NON-NLS-1$
	}
	
	
	public boolean getPrependTimestamp() {
		return this.prependTimestamp;
	}
	
	public void setPrependTimestamp(final boolean enable) {
		final boolean oldValue= this.prependTimestamp;
		this.prependTimestamp= enable;
		firePropertyChange("prependTimestamp", oldValue, enable);
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj ||
				(obj instanceof TrackingConfiguration && this.id.equals(((TrackingConfiguration) obj).id)) );
	}
	
}
