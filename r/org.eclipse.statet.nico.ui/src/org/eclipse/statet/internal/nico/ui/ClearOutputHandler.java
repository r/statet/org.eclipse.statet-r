/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsoleView;

import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolSessionData;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.console.NIConsole;


public class ClearOutputHandler extends AbstractHandler {
	
	
	/**
	 * Created by 
	 */
	public ClearOutputHandler() {
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final WorkbenchToolSessionData session= NicoUI.getToolRegistry().getActiveToolSession(
				UIAccess.getActiveWorkbenchPage(true) );
		final NIConsole console= NicoUITools.getConsole(session);
		if (console == null) {
			return null;
		}
		final IConsoleView consoleView = NicoUITools.getConsoleView(console, session.getPage());
		if (consoleView == null) {
			return null;
		}
		consoleView.display(console);
		BusyIndicator.showWhile(ConsolePlugin.getStandardDisplay(), new Runnable() {
			@Override
			public void run() {
				console.clearConsole();
			}
		});
		return null;
	}
	
}
