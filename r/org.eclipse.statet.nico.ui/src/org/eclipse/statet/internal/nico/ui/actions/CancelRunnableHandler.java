/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.commands.IElementUpdater;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;
import org.eclipse.statet.ecommons.ui.actions.WorkbenchScopingHandler;

import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.actions.AbstractToolScopeHandler;


/**
 * Handler to cancel tool tasks.
 */
@NonNullByDefault
public class CancelRunnableHandler extends AbstractToolScopeHandler<ToolProcess> {
	
	
	public static final String MENU_ID= "org.eclipse.statet.nico.menus.Cancel"; //$NON-NLS-1$
	
	public static final String PAR_OPTIONS= "options"; //$NON-NLS-1$
	
	
	private final int options;
	
	
	public CancelRunnableHandler(final Object scope, final String commandId) {
		super(scope, commandId);
		
		switch (commandId) {
		case NicoUI.CANCEL_CURRENT_COMMAND_ID:
			this.options= ToolController.CANCEL_CURRENT;
			break;
		case NicoUI.CANCEL_CURRENT_PAUSE_COMMAND_ID:
			this.options= ToolController.CANCEL_CURRENT | ToolController.CANCEL_PAUSE;
			break;
		case NicoUI.CANCEL_ALL_COMMAND_ID:
			this.options= ToolController.CANCEL_ALL;
			break;
		default:
			throw new IllegalArgumentException("commandId= " + commandId);
		}
	}
	
	
	@Override
	protected boolean evaluateIsEnabled(final ToolProcess tool) {
		return (!tool.isTerminated());
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event,
			final ToolProcess tool, final IEvaluationContext evalContext)
			throws ExecutionException {
		final String optionsParameter= event.getParameter(PAR_OPTIONS);
		int options= this.options;
		if (optionsParameter != null) {
			try {
				options= Integer.decode(optionsParameter);
			}
			catch (final NumberFormatException e) {
				throw new ExecutionException(String.format("Invalid parameter '%1$s'.", PAR_OPTIONS),
						e );
			}
		}
		
		final ToolController controller= (tool != null) ? tool.getController() : null;
		if (controller == null) {
			return null;
		}
		
		if (!controller.cancelTask(options)) {
			Display.getCurrent().beep();
		}
		
		return null;
	}
	
	
	public static class WorkbenchHandler extends WorkbenchScopingHandler
			implements IElementUpdater, IExecutableExtension {
		
		
		/** For instantiation via plugin.xml */
		public WorkbenchHandler() {
		}
		
		
		@Override
		protected AbstractScopeHandler createScopeHandler(final Object scope) {
			final String commandId= nonNullAssert(getCommandId());
			return new CancelRunnableHandler(scope, commandId);
		}
		
		
	}
	
}
