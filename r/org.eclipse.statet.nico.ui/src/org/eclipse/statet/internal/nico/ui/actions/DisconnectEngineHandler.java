/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.nico.core.runtime.IRemoteEngineController;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.actions.AbstractToolHandler;


@NonNullByDefault
public class DisconnectEngineHandler extends AbstractToolHandler<ToolProcess> {
	
	
	private static class DisconnectJob extends Job {
		
		
		private final ToolController controller;
		
		
		DisconnectJob(final ToolProcess process, final ToolController controller) {
			super(NLS.bind("Disconnect {0}", process.getLabel(ToolProcess.DEFAULT_LABEL)));
			setUser(true);
			setPriority(INTERACTIVE);
			
			this.controller= controller;
		}
		
		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			try {
				((IRemoteEngineController) this.controller).disconnect(EStatusUtils.convert(monitor));
				return Status.OK_STATUS;
			}
			catch (final StatusException e) {
				return EStatusUtils.convert(e.getStatus());
			}
			finally {
				monitor.done();
			}
		}
	}
	
	
	public DisconnectEngineHandler(final ToolProvider toolProvider, final IServiceLocator serviceLocator) {
		super(null, IRemoteEngineController.FEATURE_SET_ID,
				toolProvider, serviceLocator );
		init();
	}
	
	
	@Override
	protected @Nullable Object execute(final ToolProcess tool, final ExecutionEvent event) {
		final ToolController controller;
		try {
			controller= NicoUITools.accessController(null, tool);
		}
		catch (final CoreException e) {
			StatusManager.getManager().handle(e.getStatus(), StatusManager.SHOW | StatusManager.LOG);
			return null;
		}
		
		final IServiceLocator serviceLocator= getServiceLocator(event.getApplicationContext());
		final IProgressService progressService= nonNullAssert(
				serviceLocator.getService(IProgressService.class) );
		final Job job= new DisconnectJob(tool, controller);
		job.schedule();
		progressService.showInDialog(WorkbenchUIUtils.getShell(event.getApplicationContext()), job);
		
		return null;
	}
	
}
