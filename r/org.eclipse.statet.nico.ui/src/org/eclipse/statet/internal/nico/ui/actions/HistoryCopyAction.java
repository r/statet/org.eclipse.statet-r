/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.actions.ActionFactory;

import org.eclipse.statet.ecommons.ui.SharedMessages;
import org.eclipse.statet.ecommons.ui.util.DNDUtils;

import org.eclipse.statet.nico.ui.views.HistoryView;


public class HistoryCopyAction extends Action {
	
	
	private final HistoryView fView;
	
	
	public HistoryCopyAction(final HistoryView view) {
		super(SharedMessages.CopyAction_name);
		setToolTipText(SharedMessages.CopyAction_tooltip);
		
		setId(ActionFactory.COPY.getId());
		setActionDefinitionId(IWorkbenchCommandConstants.EDIT_COPY);
		
		fView = view;
	}
	
	@Override
	public void run() {
		final String text = HistoryView.createTextBlock(fView.getSelection());
		DNDUtils.setContent(fView.getClipboard(), 
				new String[] { text }, 
				new Transfer[] { TextTransfer.getInstance() } );
	}
	
}
