/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.IHandlerService;

import org.eclipse.statet.ecommons.ui.actions.WindowContributionsProvider;

import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.actions.WindowToolProvider;


public class NicoWindowContributions extends WindowContributionsProvider {
	
	
	private static class Contributions extends WindowContributions {
		
		public Contributions(final IWorkbenchWindow window) {
			super(window);
		}
		
		@Override
		protected void init() {
			final IWorkbenchWindow window = getWindow();
			final IHandlerService handlerService = window.getService(IHandlerService.class);
			
			add(handlerService.activateHandler(NicoUI.PAUSE_COMMAND_ID, 
					new PauseHandler(new WindowToolProvider(window), window)));
			add(handlerService.activateHandler(NicoUI.DISCONNECT_COMMAND_ID,
					new DisconnectEngineHandler(new WindowToolProvider(window), window)));
			add(handlerService.activateHandler(NicoUI.RECONNECT_COMMAND_ID,
					new ReconnectEngineHandler(new WindowToolProvider(window), window)));
		}
		
	}
	
	
	public NicoWindowContributions() {
	}
	
	
	@Override
	protected String getPluginId() {
		return NicoUI.BUNDLE_ID;
	}
	
	@Override
	protected WindowContributions createWindowContributions(final IWorkbenchWindow window) {
		return new Contributions(window);
	}
	
}
