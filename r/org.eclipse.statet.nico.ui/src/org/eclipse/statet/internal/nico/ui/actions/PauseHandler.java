/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.nico.core.runtime.Queue;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.actions.AbstractToolHandler;


/**
 * Handler to toggle pause state of engine
 */
@NonNullByDefault
public class PauseHandler extends AbstractToolHandler<ToolProcess> implements IElementUpdater,
		IDebugEventSetListener {
	
	
	private boolean isChecked;
	
	
	public PauseHandler(final ToolProvider toolProvider, final IServiceLocator serviceLocator) {
		super(null, null, toolProvider, serviceLocator);
		init();
	}
	
	@Override
	protected void init() {
		DebugPlugin.getDefault().addDebugEventListener(this);
		super.init();
	}
	
	@Override
	public void dispose() {
		final DebugPlugin debugManager = DebugPlugin.getDefault();
		if (debugManager != null) {
			debugManager.removeDebugEventListener(this);
		}
		
		super.dispose();
	}
	
	
	private void setChecked(final boolean isChecked) {
		if (isChecked != this.isChecked) {
			this.isChecked= isChecked;
			refreshUI();
		}
	}
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		super.setEnabled(evaluationContext);
		
		final ToolProcess enabledTool= getActiveTool();
		setChecked((enabledTool != null) ?
				enabledTool.getQueue().isRequested(Queue.PAUSED_STATE) :
				false );
	}
	
	protected void refreshUI() {
		WorkbenchUIUtils.refreshCommandElements(NicoUI.PAUSE_COMMAND_ID, this, null);
	}
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
		try {
			element.setChecked(this.isChecked);
		}
		finally {
			WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
		}
	}
	
	
	@Override
	public void handleDebugEvents(final DebugEvent[] events) {
		final ToolProcess tool= getActiveTool();
		if (tool == null) {
			return;
		}
		ITER_EVENTS: for (final DebugEvent event : events) {
			if (event.getSource() == tool) {
				if (event.getKind() == DebugEvent.TERMINATE) {
					update(tool);
				}
				continue ITER_EVENTS;
			}
			if (event.getSource() == tool.getQueue()) {
				if (Queue.isStateRequest(event)) {
					final Queue.StateDelta delta= (Queue.StateDelta) event.getData();
					if (delta.newState == Queue.PAUSED_STATE) {
						updateChecked(tool, true);
					}
					else {
						updateChecked(tool, false);
					}
				}
				else if (Queue.isStateChange(event)) {
					final Queue.StateDelta delta= (Queue.StateDelta) event.getData();
					if (delta.newState == Queue.PAUSED_STATE) {
						updateChecked(tool, true);
					}
				}
				continue ITER_EVENTS;
			}
		}
	}
	
	private void update(final ToolProcess tool) {
		UIAccess.getDisplay().asyncExec(() -> {
			if (getState() != S_INITIALIZED || getActiveTool() != tool) {
				return;
			}
			setEnabled(null);
		});
	}
	
	private void updateChecked(final ToolProcess tool, final boolean isChecked) {
		UIAccess.getDisplay().asyncExec(() -> {
			if (getState() != S_INITIALIZED || getActiveTool() != tool) {
				return;
			}
			setChecked(isChecked);
		});
	}
	
	
	@Override
	protected @Nullable Object execute(final ToolProcess tool, final ExecutionEvent event) {
		final boolean wasChecked= this.isChecked;
		final boolean success= (!wasChecked) ?
				tool.getQueue().pause() : tool.getQueue().resume();
		setChecked((success) ? !wasChecked : false);
		
		return null;
	}
	
}
