/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.debug.core.util.OverlayLaunchConfiguration;

import org.eclipse.statet.nico.core.runtime.IRemoteEngineController;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.actions.AbstractToolHandler;


@NonNullByDefault
public class ReconnectEngineHandler extends AbstractToolHandler<ToolProcess> {
	
	
	public ReconnectEngineHandler(final ToolProvider toolProvider, final IServiceLocator serviceLocator) {
		super(null, IRemoteEngineController.FEATURE_SET_ID,
				toolProvider, serviceLocator );
		init();
	}
	
	
	@Override
	protected boolean evaluateIsEnabled(final ToolProcess tool, final @Nullable Object evaluationContext) {
		try {
			return (tool.isTerminated()
					&& (tool.getExitValue() == ToolProcess.EXITCODE_DISCONNECTED) );
		}
		catch (final DebugException e) {
			return false;
		}
	}
	
	
	@Override
	protected @Nullable Object execute(final ToolProcess tool, final ExecutionEvent event) {
		final IServiceLocator serviceLocator= getServiceLocator(event.getApplicationContext());
		final IProgressService progressService= nonNullAssert(
				serviceLocator.getService(IProgressService.class) );
		try {
			progressService.busyCursorWhile(createRunnable(tool));
		}
		catch (final InvocationTargetException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, NicoUI.BUNDLE_ID, -1,
							"Reconnecting failed.",
							e.getCause() ),
					StatusManager.SHOW | StatusManager.LOG );
		}
		catch (final InterruptedException e) {
		}
		return null;
	}
	
	private IRunnableWithProgress createRunnable(final ToolProcess process) {
		return new IRunnableWithProgress() {
			@Override
			public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				final var originalLaunch= process.getLaunch();
				ILaunchConfiguration originalConfig= originalLaunch.getLaunchConfiguration();
				if (originalConfig instanceof OverlayLaunchConfiguration) {
					originalConfig= ((OverlayLaunchConfiguration)originalConfig).getOriginal();
				}
				
				final Map<String, Object> reconnect= new HashMap<>();
				process.prepareRestart(reconnect);
				
				final Map<String, Object> add= new HashMap<>();
				add.put(IRemoteEngineController.LAUNCH_RECONNECT_ATTRIBUTE, reconnect);
				final ILaunchConfiguration reconnectConfig= new OverlayLaunchConfiguration(originalConfig, add);
				try {
					@SuppressWarnings("unused")
					final ILaunch reconnectLaunch= reconnectConfig.launch(
							originalLaunch.getLaunchMode(), monitor, false );
					
//					if (dispose != null) {
//						final ILaunchManager launchManager= DebugPlugin.getDefault().getLaunchManager();
//						final ILaunchesListener2 launchListener= new ILaunchesListener2() {
//							public void launchesAdded(final ILaunch[] launches) {
//							}
//							public void launchesChanged(final ILaunch[] launches) {
//								check(launches);
//							}
//							public void launchesTerminated(final ILaunch[] launches) {
//								check(launches);
//							}
//							public void launchesRemoved(final ILaunch[] launches) {
//								check(launches);
//							}
//							private void check(final ILaunch[] launches) {
//								if (contains(launches, reconnectLaunch)) {
//									process.approveDispose(dispose);
//									DebugPlugin.getDefault().getLaunchManager().removeLaunchListener(this);
//								}
//							}
//						};
//						launchManager.addLaunchListener(launchListener);
//						if (!contains(launchManager.getLaunches(), reconnectLaunch)) {
//							process.approveDispose(dispose);
//							launchManager.removeLaunchListener(launchListener);
//						}
//					}
				}
				catch (final CoreException e) {
					if (reconnect != null) {
						process.restartCompleted(reconnect);
					}
					throw new InvocationTargetException(e);
				}
			}
		};
	}
	
//	private boolean contains(final ILaunch[] launches, final ILaunch search) {
//		for (final ILaunch launch : launches) {
//			if (search == launch) {
//				return true;
//			}
//		}
//		return false;
//	}
	
}
