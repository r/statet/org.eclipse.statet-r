/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistry;

import org.eclipse.statet.internal.nico.ui.NicoUIPlugin;


/**
 * Public Nico-UI services.
 * <p>
 * Access via static methods.
 */
@NonNullByDefault
public class NicoUI {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.nico.ui"; //$NON-NLS-1$
	
	
	public static final String HISTORY_VIEW_ID= "org.eclipse.statet.nico.views.HistoryView"; //$NON-NLS-1$
	public static final String QUEUE_VIEW_ID= "org.eclipse.statet.nico.views.QueueView"; //$NON-NLS-1$
	
	public static final String DISCONNECT_COMMAND_ID= "org.eclipse.statet.nico.commands.DisconnectTool"; //$NON-NLS-1$
	public static final String RECONNECT_COMMAND_ID= "org.eclipse.statet.nico.commands.ReconnectTool"; //$NON-NLS-1$
	
	public static final String RELAUNCH_TOOL_COMMAND_ID= "org.eclipse.statet.nico.commands.RelaunchTool"; //$NON-NLS-1$
	
	public static final String PAUSE_COMMAND_ID= "org.eclipse.statet.nico.commands.PauseTool"; //$NON-NLS-1$
	public static final String CANCEL_CURRENT_COMMAND_ID= "org.eclipse.statet.nico.commands.CancelCurrent"; //$NON-NLS-1$
	public static final String CANCEL_CURRENT_PAUSE_COMMAND_ID= "org.eclipse.statet.nico.commands.CancelCurrentAndPause"; //$NON-NLS-1$
	public static final String CANCEL_ALL_COMMAND_ID= "org.eclipse.statet.nico.commands.CancelAll"; //$NON-NLS-1$
	
	
	public static NicoUIResources getUIResources() {
		return NicoUIResources.INSTANCE;
	}
	
	public static WorkbenchToolRegistry getToolRegistry() {
		return NicoUIPlugin.getInstance().getToolRegistry();
	}
	
	
	private NicoUI() {}
	
}
