/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import org.eclipse.jface.wizard.WizardDialog;

import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUIMessages;


public class SaveHistoryAction extends ToolAction  {
	
	
	public SaveHistoryAction(final ToolProvider support) {
		super(support, false);
		
		setText(NicoUIMessages.SaveHistoryAction_name);
		setToolTipText(NicoUIMessages.SaveHistoryAction_tooltip);
//		setImageDescriptor();
//		setDisabledImageDescriptor();
		
		update();
	}
	
	
	@Override
	public void run() {
		final ToolProcess tool = getTool();
		if (tool == null) {
			return;
		}
		
		final WizardDialog dialog = new WizardDialog(UIAccess.getActiveWorkbenchShell(true),
				new SaveHistoryWizard(tool));
		dialog.open();
	}
	
}
