/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;

import org.eclipse.statet.internal.nico.ui.NicoUIPlugin;
import org.eclipse.statet.internal.nico.ui.SaveHistoryPage;
import org.eclipse.statet.nico.core.runtime.History;
import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUIMessages;


/**
 * 
 */
public class SaveHistoryWizard extends Wizard {
	
	
	private static final String STORE_SECTION = LoadHistoryWizard.STORE_SECTION; // shared
	
	private final ToolProcess fProcess;
	private SaveHistoryPage fPage;
	
	
	public SaveHistoryWizard(final ToolProcess process) {
		super();
		
		this.fProcess = process;
		
		setDialogSettings(DialogUtils.getDialogSettings(NicoUIPlugin.getInstance(), STORE_SECTION));
		setWindowTitle(NicoUIMessages.SaveHistory_title);
//		setDefaultPageImageDescriptor();
		setNeedsProgressMonitor(true);
	}
	
	
	@Override
	public void addPages() {
		this.fPage = new SaveHistoryPage(this.fProcess);
		addPage(this.fPage);
	}
	
	@Override
	public boolean performFinish() {
		this.fPage.saveSettings();
		
		try {
			final History history = this.fProcess.getHistory();
			final Object file = this.fPage.getFile();
			final String charset = this.fPage.fEncoding;
			int mode = EFS.NONE;
			if (this.fPage.fOverwriteFile) {
				mode |= EFS.OVERWRITE;
			}
			if (this.fPage.fAppendToFile) {
				mode |= EFS.APPEND;
			}
			final int fmode = mode;
			
			assert (history != null);
			assert (file != null);
			assert (charset != null);
			
			final Set<SubmitType> types = this.fPage.getContentSubmitTypes();
			
			getContainer().run(true, true, new IRunnableWithProgress() {
				@Override
				public void run(final IProgressMonitor monitor) throws InvocationTargetException {
					final Status status= history.save(file, fmode, charset, false, types,
							EStatusUtils.convert(monitor) );
					if (status.getSeverity() == Status.ERROR) {
						throw new InvocationTargetException(
								new CoreException(EStatusUtils.convert(status)) );
					}
				}
			});
			return true;
		}
		catch (final OperationCanceledException e) {
			return false;
		}
		catch (final Exception e) {
			if (e instanceof InvocationTargetException) {
				final Throwable cause = ((InvocationTargetException) e).getTargetException();
				if (cause instanceof CoreException) {
					StatusManager.getManager().handle(((CoreException) cause).getStatus(),
						StatusManager.LOG | StatusManager.SHOW);
					return false;
				}
			}
			NicoUIPlugin.logError(NicoUIPlugin.INTERNAL_ERROR, "Error of unexpected type occured, when performing save history.", e); //$NON-NLS-1$
			return false;
		}
	}
	
}
