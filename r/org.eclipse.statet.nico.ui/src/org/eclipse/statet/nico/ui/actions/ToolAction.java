/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;

import org.eclipse.statet.jcommons.ts.core.ActiveToolListener;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.nico.core.runtime.ToolProcess;


/**
 * Can be used for actions for tools.
 * <p>
 * Same as {@link AbstractLocalToolHandler} for actions
 */
@Deprecated
public class ToolAction extends Action implements ActiveToolListener {
	
	
	private ToolProcess fTool;
	private final boolean fDisableOnTermination;
	
	
	public ToolAction(final ToolProvider support, final boolean disableOnTermination) {
		this(support, SWT.NONE, disableOnTermination);
	}
	
	public ToolAction(final ToolProvider support, final int style, final boolean disableOnTermination) {
		super(null, style);
		
		support.addToolListener(this);
		setTool(support.getTool());
		this.fDisableOnTermination = disableOnTermination;
	}
	
	
	private void setTool(final Tool tool) {
		this.fTool = (tool instanceof ToolProcess) ? (ToolProcess) tool : null;
	}
	@Override
	public void onToolChanged(final ActiveToolEvent event) {
		setTool(event.getTool());
		update();
	}
	
	public void update() {
		setEnabled(this.fTool != null
				&& (!this.fDisableOnTermination || !this.fTool.isTerminated()));
	}
	
	public ToolProcess getTool() {
		return this.fTool;
	}
	
}
