/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;

import org.eclipse.statet.jcommons.ts.core.ActiveToolListener;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener.ActiveToolEvent;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistryListener;
import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolSessionData;

import org.eclipse.statet.nico.ui.NicoUI;


@Deprecated
public class WindowToolProvider implements ToolProvider, WorkbenchToolRegistryListener {
	
	
	private final IWorkbenchPage page;
	
	private ActiveToolListener listener;
	
	
	public WindowToolProvider(final IWorkbenchWindow window) {
		this.page= window.getActivePage();
		assert (this.page != null);
	}
	
	
	@Override
	public void addToolListener(final ActiveToolListener listener) {
		assert (this.listener == null);
		this.listener= listener;
		NicoUI.getToolRegistry().addListener(this, this.page);
	}
	
	@Override
	public void removeToolListener(final ActiveToolListener listener) {
		assert (this.listener == listener || this.listener == null);
		this.listener= null;
		NicoUI.getToolRegistry().removeListener(this);
	}
	
	@Override
	public Tool getTool() {
		return NicoUI.getToolRegistry().getActiveToolSession(this.page).getTool();
	}
	
	@Override
	public void toolSessionActivated(final WorkbenchToolSessionData sessionData) {
		this.listener.onToolChanged(
				new ActiveToolEvent(ActiveToolEvent.TOOL_ACTIVATED, sessionData.getTool()) );
	}
	
	@Override
	public void toolTerminated(final WorkbenchToolSessionData sessionData) {
		this.listener.onToolChanged(
				new ActiveToolEvent(ActiveToolEvent.TOOL_TERMINATED, sessionData.getTool()) );
	}
	
}
