/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.console;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.AbstractDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.util.AbstractFragmentDocument;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.impl.BasicWorkingBuffer;


/**
 * Generic source unit for console
 */
@NonNullByDefault
public abstract class GenericConsoleSourceUnit implements SourceUnit {
	
	
	private final String id;
	private final ElementName name;
	
	private final AbstractFragmentDocument document;
	
	private int counter= 0;
	
	
	public GenericConsoleSourceUnit(final String id, final AbstractFragmentDocument document) {
		this.id= id;
		this.name= new ElementName() {
			@Override
			public int getType() {
				return 0x011; // see RElementName
			}
			@Override
			public String getDisplayName() {
				return GenericConsoleSourceUnit.this.id;
			}
			@Override
			public String getSegmentName() {
				return GenericConsoleSourceUnit.this.id;
			}
			@Override
			public @Nullable ElementName getNextSegment() {
				return null;
			}
		};
		this.document= document;
	}
	
	
	@Override
	public WorkingContext getWorkingContext() {
		return Ltk.EDITOR_CONTEXT;
	}
	
	@Override
	public @Nullable SourceUnit getUnderlyingUnit() {
		return null;
	}
	
	@Override
	public boolean isSynchronized() {
		return true;
	}
	
	@Override
	public int getElementType() {
		return LtkModelElement.C12_SOURCE_CHUNK;
	}
	
	@Override
	public ElementName getElementName() {
		return this.name;
	}
	
	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public boolean exists() {
		return this.counter > 0;
	}
	
	@Override
	public boolean isReadOnly() {
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * A console is always modifiable.
	 */
	@Override
	public boolean checkState(final boolean validate, final IProgressMonitor monitor) {
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * A console has no resource.
	 */
	@Override
	public @Nullable Object getResource() {
		return null;
	}
	
	
	protected final AbstractFragmentDocument getDocument() {
		return this.document;
	}
	
	@Override
	public AbstractDocument getDocument(final @Nullable IProgressMonitor monitor) {
		return this.document;
	}
	
	@Override
	public long getContentStamp(final @Nullable IProgressMonitor monitor) {
		return this.document.getModificationStamp();
	}
	
	@Override
	public SourceContent getContent(final IProgressMonitor monitor) {
		return BasicWorkingBuffer.createContentFromDocument(this.document);
	}
	
	@Override
	public synchronized final void connect(final IProgressMonitor monitor) {
		this.counter++;
	}
	
	@Override
	public synchronized final void disconnect(final IProgressMonitor monitor) {
		this.counter--;
	}
	
	@Override
	public synchronized boolean isConnected() {
		return (this.counter > 0);
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
	
	@Override
	public String toString() {
		return getModelTypeId() + '/' + getWorkingContext() + ": " + getId(); //$NON-NLS-1$
	}
	
}
