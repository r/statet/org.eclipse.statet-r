/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.console;

import org.eclipse.jface.action.Action;

import org.eclipse.statet.ecommons.ui.SharedMessages;
import org.eclipse.statet.ecommons.ui.SharedUIResources;


public class ScrollLockAction extends Action {
	
	
	public interface Receiver {
		
		public void setAutoScroll(boolean enabled);
		
	}
	
	
	private final Receiver view;
	
	
	public ScrollLockAction(final Receiver view, final boolean initialChecked) {
		setText(SharedMessages.ToggleScrollLockAction_name);
		setToolTipText(SharedMessages.ToggleScrollLockAction_tooltip);
		
		setImageDescriptor(SharedUIResources.getImages().getDescriptor(SharedUIResources.TOOL_SCROLLLOCK_IMAGE_ID));
		setDisabledImageDescriptor(SharedUIResources.getImages().getDescriptor(SharedUIResources.TOOL_SCROLLLOCK_DISABLED_IMAGE_ID));
		
		this.view= view;
		setChecked(initialChecked);
	}
	
	
	@Override
	public void run() {
		this.view.setAutoScroll(!isChecked());
	}
	
}
