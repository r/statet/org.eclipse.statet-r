/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.console;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.actions.ActionFactory;

import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.util.StringUtils;

import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;

import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.ui.NicoUIMessages;
import org.eclipse.statet.nico.ui.NicoUITools;


class SubmitPasteAction extends Action {
	
	
	private final NIConsolePage view;
	
	
	public SubmitPasteAction(final NIConsolePage consolePage) {
		super(NicoUIMessages.PasteSubmitAction_name);
		
		setId(ActionFactory.PASTE.getId());
		setActionDefinitionId(IWorkbenchCommandConstants.EDIT_PASTE);
		
		this.view= consolePage;
	}
	
	
	@Override
	public void run() {
		final Transfer transfer= TextTransfer.getInstance();
		final String text= (String) this.view.getClipboard().getContents(transfer);
		final ToolController controller= this.view.getConsole().getProcess().getController();
		
		if (text == null || controller == null) {
			return;
		}
		
		NicoUITools.runSubmitInBackground(
				controller.getTool(),
				createRunnable(controller, text),
				this.view.getSite().getShell());
	}
	
	
	static IRunnableWithProgress createRunnable(final ToolController controller, final String text) {
		return new IRunnableWithProgress () {
			@Override
			public void run(final IProgressMonitor monitor) throws InterruptedException, InvocationTargetException {
				final SubMonitor m= SubMonitor.convert(monitor);
				try {
					m.beginTask(NicoUITools.createSubmitMessage(controller.getTool()), 2 + 8);
					
					final var lines= StringUtils.linesToList(text);
					m.worked(2);
					
					final Status status= controller.submit(lines, SubmitType.CONSOLE,
							m.newChild(8) );
					if (status.getSeverity() >= Status.ERROR) {
						throw new CoreException(StatusUtils.convert(status));
					}
				}
				catch (final CoreException e) {
					throw new InvocationTargetException(e);
				}
				finally {
					m.done();
				}
			}
		};
	}
	
}
