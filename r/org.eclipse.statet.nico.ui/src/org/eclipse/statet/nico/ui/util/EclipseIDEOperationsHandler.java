/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.core.runtime.ConsoleService;
import org.eclipse.statet.nico.core.runtime.IToolEventHandler;
import org.eclipse.statet.nico.core.runtime.ToolWorkspace;
import org.eclipse.statet.nico.core.util.AbstractConsoleCommandHandler;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.views.HistoryView;


/**
 * Handles in Eclipse IDE Platform:
 *  {@link IToolEventHandler#SHOW_HISTORY_ID} - shows the history view
 * 
 */
@NonNullByDefault
public class EclipseIDEOperationsHandler extends AbstractConsoleCommandHandler {
	
	
	public static final String OPEN_FILE_COMMAND_ID= "common/openFile";
	@Deprecated
	public static final String OPEN_FILE_COMMAND_OLD_ID= "common/showFile"; //$NON-NLS-1$
	
	public static final String SHOW_HISTORY_COMMAND_ID= "common/showHistory"; //$NON-NLS-1$
	
	
	@Override
	public Status execute(final String id, final ConsoleService service, final ToolCommandData data,
			final ProgressMonitor m) {
		switch (id) {
		case OPEN_FILE_COMMAND_ID:
		case OPEN_FILE_COMMAND_OLD_ID:
			{	final IFileStore fileStore;
				final IFile workspaceFile;
				String fileName= data.getString("filename"); //$NON-NLS-1$
				if (fileName == null) {
					fileName= data.getString("fileName"); //$NON-NLS-1$
					if (fileName == null) {
						fileName= data.getStringRequired("filePath"); //$NON-NLS-1$
					}
				}
				final ToolWorkspace workspaceData= service.getWorkspaceData();
				try {
					fileStore= workspaceData.toFileStore(fileName);
					workspaceFile= getWorkspaceFile(fileStore, m);
				}
				catch (final CoreException e) {
					final Status status= new ErrorStatus(NicoUI.BUNDLE_ID,
							"Failed to resolve filename.",
							e );
					service.handleStatus(status, m);
					return status;
				}
				UIAccess.getDisplay().syncExec(() -> {
					final IWorkbenchPage page= NicoUI.getToolRegistry().findWorkbenchPage(service.getTool());
					try {
						if (workspaceFile != null) {
							IDE.openEditor(page, workspaceFile);
						}
						else {
							IDE.openEditorOnFileStore(page, fileStore);
						}
					}
					catch (final PartInitException e) {
						final Status status= new ErrorStatus(NicoUI.BUNDLE_ID,
								"An error occurred when trying open/activate the Editor.",
								e );
						service.handleStatus(status, m);
					}
				});
				return OK_STATUS;
			}
			
		case SHOW_HISTORY_COMMAND_ID:
			{	final String pattern= data.getString("pattern"); //$NON-NLS-1$
				UIAccess.getDisplay().syncExec(() -> {
					try {
						final IWorkbenchPage page= NicoUI.getToolRegistry().findWorkbenchPage(service.getTool());
						final HistoryView view= (HistoryView)page.showView(NicoUI.HISTORY_VIEW_ID);
						if (pattern != null) {
							view.search(pattern, false);
						}
					}
					catch (final PartInitException e) {
						final Status status= new ErrorStatus(NicoUI.BUNDLE_ID,
								"An error occurred when trying open/activate the History view.",
								e );
						service.handleStatus(status, m);
					}
				});
				return OK_STATUS;
			}
			
		default:
			throw new UnsupportedOperationException();
		}
	}
	
	private @Nullable IFile getWorkspaceFile(final IFileStore fileStore, final ProgressMonitor m) throws CoreException {
		final IWorkspaceRoot root= ResourcesPlugin.getWorkspace().getRoot();
		final IFile[] files= root.findFilesForLocationURI(fileStore.toURI());
		if (files != null && files.length > 0) {
			final IFile file= files[0];
			if (!file.exists()) { // Bug 549546
				file.refreshLocal(0, EStatusUtils.convert(m));
			}
			return file;
		}
		return null;
	}
	
}
