/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import static org.eclipse.statet.nico.core.runtime.IToolEventHandler.REPORT_STATUS_DATA_KEY;

import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.nico.core.NicoCore;
import org.eclipse.statet.nico.core.NicoCoreMessages;
import org.eclipse.statet.nico.core.runtime.ConsoleService;
import org.eclipse.statet.nico.core.runtime.IToolEventHandler;
import org.eclipse.statet.nico.core.runtime.ToolStreamMonitor;
import org.eclipse.statet.nico.core.util.AbstractConsoleCommandHandler;


/**
 * @see {@link IToolEventHandler#REPORT_STATUS_EVENT_ID}
 */
@NonNullByDefault
public class ReportStatusHandler extends AbstractConsoleCommandHandler {
	
	
	private static final Pattern LINEBREAK_PATTERN= Pattern.compile("\\R"); //$NON-NLS-1$
	private static final String INDENT= "    "; //$NON-NLS-1$
	
	
	public ReportStatusHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final ConsoleService service, final ToolCommandData data,
			final ProgressMonitor m) {
		final Status status= data.get(REPORT_STATUS_DATA_KEY, Status.class);
		if (status != null) {
			final String br= service.getWorkspaceData().getLineSeparator();
			final String br1= br + INDENT;
			final String br2= br1 + INDENT;
			final StringBuilder msg= new StringBuilder(br);
			
			boolean details= false;
			
			switch (status.getSeverity()) {
			case Status.INFO:
				msg.append("[INFO   ] "); //$NON-NLS-1$
				break;
			case Status.WARNING:
				msg.append("[WARNING] "); //$NON-NLS-1$
				break;
			case Status.ERROR:
				msg.append("[ERROR  ] "); //$NON-NLS-1$
				break;
			case Status.CANCEL:
				msg.append("[CANCEL ] "); //$NON-NLS-1$
				break;
			}
			msg.append(status.getMessage());
			
			List<Status> subList= null;
			if (status.isMultiStatus()) {
				details|= (status.getException() != null);
				subList= status.getChildren();
			}
			else {
				final Throwable exception= status.getException();
				if (exception instanceof StatusException) {
					subList= ImCollections.newList(((StatusException) exception).getStatus());
				}
				else if (exception instanceof CoreException) {
					subList= ImCollections.newList(EStatusUtils.convert(
							((CoreException) exception).getStatus() ));
				}
				else if (exception != null) {
					details= true;
				}
			}
			if (subList != null) {
				for (final Status subStatus : subList) {
					msg.append(br1);
					msg.append(LINEBREAK_PATTERN.matcher(subStatus.getMessage()).replaceAll(br2));
					details|= (subStatus.isMultiStatus() || subStatus.getException() != null);
				}
			}
			if (details) {
				msg.append(br);
				msg.append("(more details available in Eclipse error log)");
			}
			
			msg.append(br);
			
			try {
				final ToolStreamMonitor infoStream= service.getController().getStreams().getInfoStreamMonitor();
				infoStream.append(msg.toString(), service.getController().getCurrentSubmitType(), 0);
			}
			catch (final Exception e) {
			}
			
			if ((status.getSeverity() & (Status.ERROR | Status.WARNING)) != 0) {
				StatusManager.getManager().handle(new MultiStatus(NicoCore.BUNDLE_ID, 0,
								new IStatus[] { EStatusUtils.convert(status) },
								NicoCoreMessages.format_ProblemWhileRunningTask_message(
										service.getCurrentRunnable().getLabel(),
										service.getTool() ),
								null ),
						(status.getSeverity() == Status.ERROR) ?
								(StatusManager.LOG | StatusManager.SHOW) :
								(StatusManager.LOG) );
			}
		}
		
		return OK_STATUS;
	}
	
}
