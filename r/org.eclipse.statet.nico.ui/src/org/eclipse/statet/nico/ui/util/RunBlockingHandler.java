/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import static org.eclipse.statet.jcommons.status.Status.CANCEL_STATUS;
import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.internal.nico.ui.Messages;
import org.eclipse.statet.nico.core.NicoCore;
import org.eclipse.statet.nico.core.runtime.IToolEventHandler;


/**
 * @see IToolEventHandler#RUN_BLOCKING_EVENT_ID
 */
@NonNullByDefault
public class RunBlockingHandler implements IToolEventHandler {
	
	
	public RunBlockingHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final ToolService service, final ToolCommandData data,
			final ProgressMonitor m) {
		final IProgressService progressService= PlatformUI.getWorkbench().getProgressService();
		final ToolRunnable toolRunnable= data.getRequired(RUN_RUNNABLE_DATA_KEY, ToolRunnable.class);
		try {
			progressService.busyCursorWhile(new IRunnableWithProgress() {
				@Override
				public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					final ProgressMonitor m= EStatusUtils.convert(monitor, 1);
					try {
						toolRunnable.run(service, m);
					}
					catch (final StatusException e) {
						throw new InvocationTargetException(e);
					}
				}
			});
			return OK_STATUS;
		}
		catch (final InvocationTargetException e) {
			final Throwable targetException= e.getCause();
			if (targetException instanceof StatusException
					&& ((StatusException) targetException).getStatus().getSeverity() == Status.CANCEL) {
				return CANCEL_STATUS;
			}
			return handleError(new ErrorStatus(NicoCore.BUNDLE_ID,
					NLS.bind(Messages.ExecuteHandler_error_message, toolRunnable.getLabel()), targetException) );
		}
		catch (final InterruptedException e) {
			return CANCEL_STATUS;
		}
	}
	
	protected Status handleError(final Status status) {
		StatusManager.getManager().handle(EStatusUtils.convert(status),
				StatusManager.LOG | StatusManager.SHOW );
		return status;
	}
	
}
