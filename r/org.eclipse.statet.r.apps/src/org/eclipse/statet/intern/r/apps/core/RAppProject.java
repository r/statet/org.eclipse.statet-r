/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.intern.r.apps.core;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RAppProject implements IProjectNature {
	
	
	private IProject project;
	
	
	/** plugin.xml */
	@SuppressWarnings("null")
	public RAppProject() {
	}
	
	
	@Override
	public void setProject(final IProject project) {
		this.project= project;
	}
	
	@Override
	public final IProject getProject() {
		return this.project;
	}
	
	@Override
	public void configure() throws CoreException {
	}
	
	@Override
	public void deconfigure() throws CoreException {
	}
	
}
