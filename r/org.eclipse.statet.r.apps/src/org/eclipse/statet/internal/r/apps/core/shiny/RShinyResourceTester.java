/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.core.shiny;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RShinyResourceTester extends PropertyTester {
	
	
	private static final IPath APP_FILE_PATH= new Path("app.R"); //$NON-NLS-1$
	
	private static final IPath UI_FILE_PATH= new Path("ui.R"); //$NON-NLS-1$
	private static final IPath SERVER_FILE_PATH= new Path("server.R"); //$NON-NLS-1$
	
	
	@SuppressWarnings("null")
	private static final IContainer getContainer(final IResource resource) {
		return (resource instanceof IContainer) ?
				(IContainer) resource :
				resource.getParent();
	}
	
	public static final boolean isAppContainer(final IContainer container) {
		return (((container.getFile(SERVER_FILE_PATH).exists()
						&& container.getFile(UI_FILE_PATH).exists() )
				|| (container.getFile(APP_FILE_PATH).exists()) ));
	}
	
	public static final @Nullable IContainer getAppContainer(IContainer container) {
		while (true) {
			if (isAppContainer(container)) {
				return container;
			}
			if (container.getType() == IResource.FOLDER) {
				container= container.getParent();
				continue;
			}
			return null;
		}
	}
	
//	public static final IContainer getContainingShinyContainer(IContainer container) {
//		try {
//			container.accept(new IResourceVisitor() {
//				IContainer appContainer;
//				@Override
//				public boolean visit(final IResource resource) throws CoreException {
//					if (resource.getType() == IResource.FOLDER
//							|| resource.getType() == IResource.PROJECT) {
//						if (isShinyContainer((IContainer) resource)) {
//							if (this.appContainer != null) {
//								
//							}
//							this.appContainer= (IContainer) resource;
//							return false;
//						}
//						return true;
//					}
//					return false;
//				}
//			});
//		}
//		catch (final CoreException e) {
//			return null;
//		}
//	}
	
	public static final boolean isInsideAppContainer(final IResource resource) {
		return (getAppContainer(getContainer(resource)) != null);
	}
	
	public static final @Nullable IContainer getAppContainer(final IResource resource) {
		return getAppContainer(getContainer(resource));
	}
	
	
	/** plugin.xml */
	public RShinyResourceTester() {
	}
	
	
	@Override
	public boolean test(final Object receiver,
			final String property, final Object[] args, final @Nullable Object expectedValue) {
		if (receiver instanceof IResource) {
			final IResource resource= (IResource) receiver;
			switch (property) {
			case "isRShinyAppResource": //$NON-NLS-1$
//				System.out.println("isRShinyAppResource" + resource.getFullPath() + "= " + isInsideAppContainer(resource));
				return (isInsideAppContainer(resource));
//			case "containsApp": //$NON-NLS-1$
			default:
				break;
			}
		}
		return false;
	}
	
	
}
