/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.launching;

import java.util.IdentityHashMap;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.IdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;
import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigPresets;

import org.eclipse.statet.internal.r.apps.ui.Messages;


/**
 * 
 * element= app container
 */
@NonNullByDefault
public class AppControlManager extends LaunchConfigManager<IContainer> {
	
	
	private final AppType appType;
	
	
	public AppControlManager(final AppType appType) {
		super(appType.getId(), AppControlConfigs.TYPE_ID );
		
		this.appType= appType;
	}
	
	
	@Override
	protected Map<String, Object> createRunAttributes(final IContainer element,
			final IdentitySet<String> flags) {
		final Map<String, Object> map= new IdentityHashMap<>(4);
		if (element != null) {
			map.put(AppControlConfigs.SOURCE_PATH_ATTR_NAME, element.getFullPath().toPortableString());
		}
		return map;
	}
	
	
	@Override
	protected String getActionLabel(final byte bits) {
		switch (bits) {
		case 0:
			return Messages.RunAction_RunApp_label;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	
	@Override
	protected String getAutogenConfigName() {
		String name= this.appType.getName();
		if (name.startsWith("R ")) { //$NON-NLS-1$
			name= name.substring(2);
		}
		return NLS.bind("Run {0} and Show in Eclipse", name);
	}
	
	@Override
	protected void initAutogenConfig(final ILaunchConfigurationWorkingCopy config) {
		AppControlConfigTabGroup.initDefaults(config);
		final ILaunchConfiguration preset= AppControlConfigTabGroup.getPresets().get(
				this.appType.getName() );
		if (preset != null) {
			LaunchConfigPresets.apply(preset, config);
		}
	}
	
}
