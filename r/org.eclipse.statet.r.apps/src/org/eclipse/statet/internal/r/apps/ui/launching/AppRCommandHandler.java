/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.launching;

import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.rj.ts.core.AbstractRToolCommandHandler;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
class AppRCommandHandler extends AbstractRToolCommandHandler {
	
	
	private static final String ON_APP_STARTED_ID= "org.eclipse.statet.r.apps.onAppStarted"; //$NON-NLS-1$
	
	private static final String ON_APP_STOPPED_ID= "org.eclipse.statet.r.apps.onAppStopped"; //$NON-NLS-1$
	
	
	static AppRCommandHandler connect(final AppRunner runner,
			final RToolService r, final ProgressMonitor m) throws StatusException {
		final ToolController controller= ((RProcess) r.getTool()).getController();
		AppRCommandHandler handler= (AppRCommandHandler) controller.getCommandHandler(ON_APP_STARTED_ID);
		if (handler == null) {
			handler= new AppRCommandHandler();
			controller.addCommandHandler(ON_APP_STARTED_ID, handler);
			controller.addCommandHandler(ON_APP_STOPPED_ID, handler);
		}
		handler.currentRunner= runner;
		
		return handler;
	}
	
	
	private @Nullable AppRunner currentRunner;
	
	
	private AppRCommandHandler() {
	}
	
	
	@Override
	protected Status execute(final String id, final RToolService r, final ToolCommandData data,
			final ProgressMonitor m) throws StatusException {
		final var currentRunner= this.currentRunner;
		
		switch (id) {
		case ON_APP_STARTED_ID:
			if (currentRunner != null) {
				currentRunner.onAppStarted(
						data.getStringRequired("url"), //$NON-NLS-1$
						data.getString("typeId") ); //$NON-NLS-1$
			}
			return OK_STATUS;
			
		case ON_APP_STOPPED_ID:
			if (currentRunner != null) {
				currentRunner.onAppStopped(
						data.getString("url") ); //$NON-NLS-1$
			}
			return OK_STATUS;
			
		default:
			throw new UnsupportedOperationException();
		}
	}
	
	void disconnect(final AppRunner runner) {
		final var currentRunner= this.currentRunner;
		if (currentRunner == runner) {
			this.currentRunner= null;
		}
	}
	
}
