/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.shiny.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.ui.commands.IElementUpdater;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;
import org.eclipse.statet.ecommons.debug.ui.config.actions.RunActiveConfigScopeHandler;
import org.eclipse.statet.ecommons.ui.actions.WorkbenchScopingHandler;


/**
 * Handlers for document output creation toolchain running with the active configuration.
 */
@NonNullByDefault
public class RunActiveAppConfigWorkbenchHandler extends WorkbenchScopingHandler
		implements IElementUpdater, IExecutableExtension {
	
	
	private ImIdentitySet<String> launchFlags;
	
	
	/** For instantiation via plugin.xml */
	@SuppressWarnings("null")
	public RunActiveAppConfigWorkbenchHandler() {
	}
	
	
	protected ImIdentitySet<String> getLaunchFlags() {
		return this.launchFlags;
	}
	
	
	@Override
	public void setInitializationData(final IConfigurationElement config,
			final String propertyName, final @Nullable Object data) throws CoreException {
		super.setInitializationData(config, propertyName, data);
		
		this.launchFlags= ImCollections.newIdentitySet();
	}
	
	@Override
	protected RunActiveConfigScopeHandler<IContainer> createScopeHandler(final Object scope) {
		return new RunActiveConfigScopeHandler<>(scope, getCommandId(),
				new AppActionUtil(ActionUtil.ACTIVE_EDITOR_MODE), getLaunchFlags() );
	}
	
	
}
