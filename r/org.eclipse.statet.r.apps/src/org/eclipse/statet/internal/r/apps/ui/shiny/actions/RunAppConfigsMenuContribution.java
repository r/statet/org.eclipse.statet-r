/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.shiny.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.ui.menus.IWorkbenchContribution;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;
import org.eclipse.statet.ecommons.debug.ui.config.actions.RunConfigsMenuContribution;


@NonNullByDefault
public class RunAppConfigsMenuContribution extends RunConfigsMenuContribution<IContainer>
		implements IWorkbenchContribution, IExecutableExtension {
	
	
//	protected class AppConfigContribution extends ConfigContribution {
//		
//		
//		public AppConfigContribution(final Image icon, final String label,
//				final ILaunchConfiguration configuration) {
//			super(icon, label, configuration);
//		}
//		
//		
//		@Override
//		protected void fillMenu(final Menu menu) {
//			addLaunchItems(menu);
//			
//			new MenuItem(menu, SWT.SEPARATOR);
//			addActivateItem(menu, DocProcessingUI.ACTIONS_ACTIVATE_CONFIG_HELP_CONTEXT_ID);
//			addEditItem(menu, DocProcessingUI.ACTIONS_EDIT_CONFIG_HELP_CONTEXT_ID);
//		}
//		
//		protected void addLaunchItems(final Menu menu) {
//		}
//		
//		
//		protected String createDetail(final String inputExt, final String outputExt) {
//			if (inputExt == null || outputExt == null) {
//				return null;
//			}
//			final StringBuilder sb= getStringBuilder();
//			sb.append("\u2002["); //$NON-NLS-1$
//			sb.append(inputExt);
//			sb.append("\u2002\u2192\u2002"); //$NON-NLS-1$
//			sb.append(outputExt);
//			sb.append("]"); //$NON-NLS-1$
//			
//			return sb.toString();
//		}
//		
//	}
	
	
	/** For instantiation via plugin.xml */
	public RunAppConfigsMenuContribution() {
		super(new AppActionUtil(ActionUtil.ACTIVE_MENU_SELECTION_MODE));
	}
	
	
	@Override
	protected AppActionUtil getUtil() {
		return (AppActionUtil) super.getUtil();
	}
	
	
//	@Override
//	protected ConfigContribution createConfigContribution(
//			Image icon, StringBuilder label, ILaunchConfiguration configuration) {
//		return new AppConfigContribution(icon, label.toString(), configuration);
//	}
	
}
