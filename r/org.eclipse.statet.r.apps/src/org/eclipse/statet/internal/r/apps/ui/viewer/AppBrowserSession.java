/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.viewer;

import java.net.URL;

import org.eclipse.core.resources.IResource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.ui.mpbv.BrowserSession;

import org.eclipse.statet.r.apps.ui.AppRegistry;
import org.eclipse.statet.r.apps.ui.RApp;


@NonNullByDefault
public class AppBrowserSession extends BrowserSession {
	
	
	private final URL id;
	
	
	public AppBrowserSession(final URL id) {
		this.id= id;
	}
	
	
	@Override
	public URL getId() {
		return this.id;
	}
	
	@Override
	public String getLabel() {
		final StringBuilder sb= new StringBuilder();
		sb.append(super.getLabel());
		
		final RApp app= getLatestApp();
		if (app != null) {
			final IResource resource= app.getResource();
			if (resource != null) {
				if (sb.length() > 0) {
					sb.append("\u2002\u2013\u2002"); //$NON-NLS-1$
				}
				sb.append(resource.getFullPath().toString());
			}
			
			final Tool tool= app.getTool();
			if (tool != null) {
				sb.append(" │ "); //$NON-NLS-1$
				sb.append(tool.getLabel(Tool.DEFAULT_LABEL));
			}
		}
		
		return sb.toString();
	}
	
	public @Nullable RApp getLatestApp() {
		return AppRegistry.getInstance().getApp(this.id);
	}
	
}
