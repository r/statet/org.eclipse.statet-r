/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.core;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;

import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;


public class RConsoleCorePlugin extends Plugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.console.core"; //$NON-NLS-1$
	
	
	private static RConsoleCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static RConsoleCorePlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin = getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	public static final void logError(final String message, final Throwable exception) {
		log(new org.eclipse.core.runtime.Status(IStatus.ERROR, BUNDLE_ID, message, exception));
	}
	
	public static final void log(final Status status) {
		log(StatusUtils.convert(status));
	}
	
	
	private boolean fStarted;
	
	
	/** Created via framework */
	public RConsoleCorePlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance = this;
		
		this.fStarted = true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.fStarted = false;
			}
		}
		finally {
			instance = null;
			super.stop(context);
		}
	}
	
}
