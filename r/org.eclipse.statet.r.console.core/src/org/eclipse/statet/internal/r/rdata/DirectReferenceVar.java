/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.data.CombinedRReference;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;


@NonNullByDefault
public final class DirectReferenceVar extends BasicCombinedRElement
		implements CombinedRReference, ExternalizableRObject {
	
	
	private final CombinedRElement element;
	
	
	public DirectReferenceVar(final CombinedRElement element,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name) {
		super(parent, name);
		
		this.element= element;
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_REFERENCE;
	}
	
	@Override
	public byte getReferencedRObjectType() {
		return this.element.getRObjectType();
	}
	
	@Override
	public String getRClassName() {
		return this.element.getRClassName();
	}
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public long getHandle() {
		return 0;
	}
	
	@Override
	public RObject getResolvedRObject() {
		return this.element;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	
	@Override
	public int getElementType() {
		return R_GENERAL_VARIABLE;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter filter) {
		return false;
	}
	
	@Override
	public List<? extends CombinedRElement> getModelChildren(final @Nullable LtkModelElementFilter filter) {
		return Collections.emptyList();
	}
	
}
