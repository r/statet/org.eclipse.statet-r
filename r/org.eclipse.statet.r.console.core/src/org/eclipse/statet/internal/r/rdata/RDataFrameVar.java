/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;


@NonNullByDefault
public final class RDataFrameVar extends RListVar
		implements RDataFrame, ExternalizableRObject {
	
	
	private final long rowCount;
	
	
	public RDataFrameVar(final RJIO io, final CombinedFactory factory, final int options,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name)
			throws IOException {
		super(io, factory, options, parent, name);
		
		this.rowCount= io.readLong();
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		final int options= 0;
		super.doWriteExternal(io, factory, options);
		io.writeLong(this.rowCount);
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_DATAFRAME;
	}
	
	@Override
	protected String getDefaultRClassName() {
		return RObject.CLASSNAME_DATAFRAME;
	}
	
	
	@Override
	public long getColumnCount() {
		return getLength();
	}
	
	@Override
	public RCharacterStore getColumnNames() {
		return getNames();
	}
	
	@Override
	public RStore<?> getColumn(final int idx) {
		return get(idx).getData();
	}
	
	@Override
	public RStore<?> getColumn(final long idx) {
		return get(idx).getData();
	}
	
	@Override
	public @Nullable RStore getColumn(final String name) {
		final RObject obj= get(name);
		return (obj != null) ? obj.getData() : null;
	}
	
	@Override
	public long getRowCount() {
		return this.rowCount;
	}
	
	@Override
	public @Nullable RStore<?> getRowNames() {
		return null;
	}
	
}
