/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.internal.r.core.rmodel.SourceAnalyzer;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.rmodel.Parameters;
import org.eclipse.statet.r.core.rmodel.RElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RLangElement;
import org.eclipse.statet.r.core.rmodel.RLangMethod;
import org.eclipse.statet.r.core.source.ast.FDef;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.rj.data.RFunction;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public final class RFunction2 extends BasicCombinedRElement
		implements RLangMethod<RLangElement<?>>, RFunction {
	
	
	private static @Nullable Parameters parseParameters(final @Nullable String headerSource) {
		if (headerSource != null && headerSource.length() > 0) {
			final RParser rParser= new RParser(AstInfo.LEVEL_MODEL_DEFAULT);
			final FDef fDef= rParser.parseFDef(new StringParserInput(headerSource).init());
			if (fDef != null) {
				return SourceAnalyzer.createFParameters(fDef, null);
			}
		}
		return null;
	}
	
	
	private final @Nullable Parameters parameters;
	
	
	public RFunction2(final Parameters parameters,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name) {
		super(parent, name);
		
		this.parameters= parameters;
	}
	
	public RFunction2(final RFunction org,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name) {
		super(parent, name);
		
		if (org instanceof RFunction2) {
			this.parameters= ((RFunction2)org).parameters;
		}
		else {
			final String headerSource= org.getHeaderSource();
			this.parameters= parseParameters(headerSource);
		}
	}
	
	public RFunction2(final RJIO io, final RObjectFactory factory,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name)
			throws IOException {
		super(parent, name);
		
		/*final int options =*/ io.readInt();
		final String headerSource= io.readString();
		this.parameters= parseParameters(headerSource);
	}
	
	@Override
	public byte getRObjectType() {
		return TYPE_FUNCTION;
	}
	
	@Override
	public String getRClassName() {
		return "function";
	}
	
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public @Nullable String getHeaderSource() {
		return null;
	}
	
	@Override
	public @Nullable String getBodySource() {
		return null;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	
	@Override
	public int getElementType() {
		return RElement.R_COMMON_FUNCTION;
	}
	
	@Override
	public @Nullable Parameters getParameters() {
		return this.parameters;
	}
	
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter filter) {
		return false;
	}
	
	@Override
	public List<? extends CombinedRElement> getModelChildren(final @Nullable LtkModelElementFilter filter) {
		return Collections.emptyList();
	}
	
}
