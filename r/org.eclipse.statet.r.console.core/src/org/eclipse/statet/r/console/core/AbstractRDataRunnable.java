/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;


public abstract class AbstractRDataRunnable implements ToolRunnable {
	
	
	private final String fTypeId;
	private final String fLabel;
	
	
	public AbstractRDataRunnable(final String typeId, final String label) {
		fTypeId = typeId;
		fLabel = label;
	}
	
	
	@Override
	public String getTypeId() {
		return fTypeId;
	}
	
	@Override
	public String getLabel() {
		return fLabel;
	}
	
	@Override
	public boolean canRunIn(final Tool tool) {
		return tool.isProvidingFeatureSet(RConsoleTool.R_BASIC_FEATURESET_ID);
	}
	
	@Override
	public boolean changed(final int event, final Tool tool) {
		return true;
	}
	
	@Override
	public void run(final ToolService service,
			final ProgressMonitor m) throws StatusException {
		run((IRDataAdapter) service, m);
	}
	
	protected abstract void run(IRDataAdapter r,
			ProgressMonitor m) throws StatusException;
	
}
