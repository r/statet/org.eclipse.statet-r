/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core;

import org.eclipse.statet.nico.core.runtime.Prompt;


/**
 * Prompt to continue incomplete input.
 * 
 * The flag IBasicRAdapter.META_PROMPT_INCOMPLETE_INPUT is setted.
 * The previous incomplete input accesible as field.
 */
public class ContinuePrompt extends Prompt {
	
	
	/**
	 * The whole previous incomplete input (can be multiple lines).
	 */
	private final String previousInput;
	
	
	public ContinuePrompt(final Prompt previousPrompt, String lastInput, final String promptText, final int meta) {
		super(promptText, IRBasicAdapter.META_PROMPT_INCOMPLETE_INPUT | meta);
		if ((previousPrompt.meta & IRBasicAdapter.META_PROMPT_INCOMPLETE_INPUT) != 0) {
			lastInput = ((ContinuePrompt) previousPrompt).getPreviousInput() + lastInput;
		}
		this.previousInput = lastInput;
	}
	
	
	public String getPreviousInput() {
		return this.previousInput;
	}
	
}
