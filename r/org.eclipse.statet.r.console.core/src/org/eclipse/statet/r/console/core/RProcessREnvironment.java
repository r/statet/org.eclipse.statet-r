/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.data.CombinedRList;
import org.eclipse.statet.rj.data.REnvironment;


@NonNullByDefault
public interface RProcessREnvironment extends REnvironment, CombinedRList {
	
	
	RProcess getSource();
	
	int getStamp();
	
}
