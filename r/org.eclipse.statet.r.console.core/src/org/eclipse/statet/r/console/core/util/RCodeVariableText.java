/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core.util;

import java.util.Map;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;
import org.eclipse.statet.ecommons.variables.core.VariableText2;

import org.eclipse.statet.r.console.core.RWorkspace;
import org.eclipse.statet.r.core.RUtil;


@NonNullByDefault
public class RCodeVariableText extends VariableText2 {
	
	
	private final RWorkspace rWorkspace;
	
	
	public RCodeVariableText(final RWorkspace rWorkspace,
			final Map<String, IStringVariable> variables) {
		super(variables);
		
		this.rWorkspace= rWorkspace;
	}
	
	
	@Override
	protected String checkValue(final IStringVariable variable, String value) throws CoreException {
		if (variable.getName().endsWith("_loc")) { //$NON-NLS-1$
			if (this.rWorkspace.isRemote()) {
				final IFileStore store= FileUtil.getFileStore(value);
				try {
					value= this.rWorkspace.toToolPath(store);
				}
				catch (final StatusException e) {
					throw StatusUtils.convert(e);
				}
			}
			return RUtil.escapeBackslash(value);
		}
		return value;
	}
	
}
