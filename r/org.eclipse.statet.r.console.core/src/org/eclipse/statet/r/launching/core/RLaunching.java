/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.launching.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.variables.IStringVariable;
import org.eclipse.debug.core.ILaunchConfiguration;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.variables.core.StringVariable;

import org.eclipse.statet.internal.r.launching.core.Messages;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.renv.IREnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvUtils;

public class RLaunching {
	
	
	public static final String CONFIG_RENV_NS = "org.eclipse.statet.r/renv"; //$NON-NLS-1$
	
	public static final String ATTR_RENV_CODE = CONFIG_RENV_NS + "/REnvCode"; //$NON-NLS-1$
	
	public static final String ATTR_WORKING_DIRECTORY = CONFIG_RENV_NS + "/WorkingDirectory"; //$NON-NLS-1$
	
	/**
	 * {@link IStringVariable String variable} name for the R working directory.
	 */
	public static final String WORKING_DIRECTORY_VARNAME = "r_wd"; //$NON-NLS-1$
	
	/**
	 * String variable for the R working directory.
	 * 
	 * Note: Listing and functionality must be explicitly implemented.
	 */
	public static final IStringVariable WORKING_DIRECTORY_VARIABLE = new StringVariable(WORKING_DIRECTORY_VARNAME, "The configured R working directory");
	
	
	public static @Nullable REnv readREnv(final ILaunchConfiguration configuration) throws CoreException {
		final String code= configuration.getAttribute(ATTR_RENV_CODE, (String) null);
		return REnvUtils.decode(code, RCore.getREnvManager());
	}
	
	/**
	 * Reads the setting from the configuration, resolves the REnvironment and validates the configuration.
	 * @param configuration
	 * @return
	 * @throws CoreException
	 */
	public static IREnvConfiguration getREnvConfig(final ILaunchConfiguration configuration, final boolean local)
			throws CoreException {
		final REnv rEnv = readREnv(configuration);
		final IREnvConfiguration rEnvConfig= (rEnv != null) ? rEnv.get(IREnvConfiguration.class) : null;
		if (rEnvConfig == null) {
			throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					Messages.REnv_Runtime_error_CouldNotFound_message, null));
		}
		
		final IStatus status= rEnvConfig.validate();
		if (status.getSeverity() == IStatus.ERROR) {
			throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					Messages.REnv_Runtime_error_Invalid_message+' '+status.getMessage(), null));
		}
		if (local && !rEnvConfig.isLocal()) {
			throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, -1, "The R environment configuration must specify a local R installation.", null));
		}
		return rEnvConfig;
	}
	
}
