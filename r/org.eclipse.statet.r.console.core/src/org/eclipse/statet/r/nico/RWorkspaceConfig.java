/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.nico;


public class RWorkspaceConfig {
	
	
	private boolean fEnableObjectDB;
	
	private boolean fEnableAutoRefresh;
	
	
	public RWorkspaceConfig() {
	}
	
	
	public void setEnableObjectDB(final boolean enableObjectDB) {
		fEnableObjectDB = enableObjectDB;
	}
	
	public boolean getEnableObjectDB() {
		return fEnableObjectDB;
	}
	
	public void setEnableAutoRefresh(final boolean enableAutoRefresh) {
		fEnableAutoRefresh = enableAutoRefresh;
	}
	
	public boolean getEnableAutoRefresh() {
		return fEnableAutoRefresh;
	}
	
	
}
