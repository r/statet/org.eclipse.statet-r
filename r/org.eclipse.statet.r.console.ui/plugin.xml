<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<!--
 #=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<plugin>
   
<!-- R CMD Launch Config -->
   <extension point="org.eclipse.debug.core.launchConfigurationTypes">
      <launchConfigurationType
            id="org.eclipse.statet.r.launchConfigurations.RCmdTool"
            category="org.eclipse.ui.externaltools"
            delegate="org.eclipse.statet.internal.r.cmd.ui.launching.RCmdLaunchDelegate"
            modes="run"
            name="%launchConfigurations_RCmd_name"
            public="true"
            allowCommandLine="true"
            allowOutputMerging="true">
      </launchConfigurationType>
   </extension>
   <extension point="org.eclipse.debug.ui.launchConfigurationTypeImages">
      <launchConfigurationTypeImage
            id="org.eclipse.statet.r.images.RCmdTool"
            configTypeID="org.eclipse.statet.r.launchConfigurations.RCmdTool"
            icon="icons/tool_16/r-cmd.png">
      </launchConfigurationTypeImage>
   </extension>
   <extension point="org.eclipse.debug.ui.launchConfigurationTabGroups">
      <launchConfigurationTabGroup
            id="org.eclipse.statet.r.launchConfigurationTabGroups.RCmdTool"
            type="org.eclipse.statet.r.launchConfigurations.RCmdTool"
            class="org.eclipse.statet.internal.r.cmd.ui.launching.RCmdToolTabGroup"
            description="%launchConfigurations_RCmd_description"
            helpContextId="org.eclipse.statet.r.ui.r_cmd-tools"/>
   </extension>
   
<!-- R Consoles Launch Config -->
   <extension point="org.eclipse.debug.core.launchConfigurationTypes">
      <launchConfigurationType
            id="org.eclipse.statet.r.launchConfigurations.RConsole"
            delegate="org.eclipse.statet.internal.r.console.ui.launching.RConsoleLaunchDelegate"
            modes="run"
            name="%launchConfigurations_RConsole_name"
            public="true">
      </launchConfigurationType>
   </extension>
   <extension point="org.eclipse.debug.ui.launchConfigurationTypeImages">
      <launchConfigurationTypeImage
            id="org.eclipse.statet.r.images.RConsole"
            configTypeID="org.eclipse.statet.r.launchConfigurations.RConsole"
            icon="icons/tool_16/r-console.png">
      </launchConfigurationTypeImage>
   </extension>
   <extension point="org.eclipse.debug.ui.launchConfigurationTabGroups">
      <launchConfigurationTabGroup
            id="org.eclipse.statet.r.launchConfigurationTabGroups.RConsole"
            type="org.eclipse.statet.r.launchConfigurations.RConsole"
            class="org.eclipse.statet.internal.r.console.ui.launching.RConsoleTabGroup"
            description="%launchConfigurations_RConsole_description"
            helpContextId="org.eclipse.statet.r.ui.r_console-launch"/>
   </extension>
   
   <extension point="org.eclipse.debug.core.launchConfigurationTypes">
      <launchConfigurationType
            id="org.eclipse.statet.r.launchConfigurations.RRemoteConsole"
            delegate="org.eclipse.statet.internal.r.console.ui.launching.RRemoteConsoleLaunchDelegate"
            modes="run"
            name="%launchConfigurations_RRemoteConsole_name"
            public="true">
      </launchConfigurationType>
   </extension>
   <extension point="org.eclipse.debug.ui.launchConfigurationTypeImages">
      <launchConfigurationTypeImage
            id="org.eclipse.statet.r.images.RRemoteConsole"
            configTypeID="org.eclipse.statet.r.launchConfigurations.RRemoteConsole"
            icon="icons/tool_16/r-remoteconsole.png">
      </launchConfigurationTypeImage>
   </extension>
   <extension point="org.eclipse.debug.ui.launchConfigurationTabGroups">
      <launchConfigurationTabGroup
            id="org.eclipse.statet.r.launchConfigurationTabGroups.RRemoteConsole"
            type="org.eclipse.statet.r.launchConfigurations.RRemoteConsole"
            class="org.eclipse.statet.internal.r.console.ui.launching.RRemoteConsoleTabGroup"
            description="%launchConfigurations_RRemoteConsole_description"
            helpContextId="org.eclipse.statet.r.ui.r_remote_console-launch"/>
   </extension>
   <extension
         point="org.eclipse.ui.commands">
      <command
            id="org.eclipse.statet.r.commands.SubmitRSnippet"
            categoryId="org.eclipse.statet.nico.commands.ConsoleCategory"
            name="%commands_RunRSnippet_name"
            description="%commands_RunRSnippet_description"
            defaultHandler="org.eclipse.statet.internal.r.console.ui.snippets.SubmitRSnippetHandler">
         <commandParameter
               id="snippet"
               name="%commands_RunRSnippet_SnippetPar_name"
               optional="false"
               values="org.eclipse.statet.internal.r.console.ui.snippets.SubmitRSnippetParameterValues">
         </commandParameter>
      </command>
      <command
            id="org.eclipse.statet.r.commands.SubmitLastRSnippet"
            categoryId="org.eclipse.statet.nico.commands.ConsoleCategory"
            name="%commands_RunLastRSnippet_name"
            description="%commands_RunLastRSnippet_description"
            defaultHandler="org.eclipse.statet.internal.r.console.ui.snippets.SubmitLastRSnippetHandler">
      </command>
   </extension>
   
<!-- Code Launch Contributions -->
   <extension
         point="org.eclipse.ui.bindings">
      <key
            commandId="org.eclipse.statet.r.commands.SubmitFileToRViaCommand"
            contextId="org.eclipse.ui.globalScope"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M2+M3+X R"/>
       <key
            commandId="org.eclipse.statet.r.commands.SubmitFileToRViaCommand"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+S"/>
     <key
            commandId="org.eclipse.statet.r.commands.SubmitFileToRViaCommand_GotoConsole"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R S"/>
      
      <key
            commandId="org.eclipse.statet.r.launchShortcuts.RScriptDirect.run"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+D"/>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitEntireCommandToR"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+E"/>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitEntireCommandToR_GotoConsole"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R E"/>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitEntireCommandToR_GotoNextCommand"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+3">
      </key>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitFunctionDefToR"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+F"/>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitFunctionDefToR_GotoConsole"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R F"/>
      <key
            commandId="org.eclipse.statet.r.launchShortcuts.RScriptDirectAndConsole.run"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R D"/>
      
      <key
            commandId="org.eclipse.statet.r.commands.SubmitSelectionToR"
            contextId="org.eclipse.ui.contexts.window"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+R"/>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitSelectionToR_GotoConsole"
            contextId="org.eclipse.ui.contexts.window"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R R"/>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitSelectionToR_GotoNextLine"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+4"/>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitSelectionToR_PasteOutput"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+V">
      </key>
      <key
            commandId="org.eclipse.statet.r.commands.SubmitUptoSelectionToR"
            contextId="org.eclipse.statet.r.contexts.REditor"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R M1+U">
      </key>
      <key
            commandId="org.eclipse.statet.r.commands.GotoRConsole"
            contextId="org.eclipse.ui.globalScope"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R C">
      </key>
   </extension>
   <extension
         point="org.eclipse.ui.menus">
      <menuContribution
            locationURI="toolbar:org.eclipse.ui.main.toolbar">
         <toolbar
               id="org.eclipse.statet.r.menus.RunToolbar">
            <separator
                  name="begin"
                  visible="true">
            </separator>
            <command
                  commandId="org.eclipse.statet.r.commands.SubmitFileToRViaCommand"
                  style="push"
                  label="Run File in R via Command">
               <visibleWhen
                     checkEnabled="false">
                  <with
                        variable="activeContexts">
                     <iterate
                           ifEmpty="false"
                           operator="or">
                        <equals
                              value="org.eclipse.statet.r.actionSets.RToolbarSet">
                        </equals>
                     </iterate>
                  </with>
               </visibleWhen>
            </command>
            <separator
                  name="submit_file"
                  visible="false">
            </separator>
            <command
                  commandId="org.eclipse.statet.r.commands.SubmitSelectionToR"
                  style="push"
                  label="Run Selection in R">
               <visibleWhen
                     checkEnabled="false">
                  <with
                        variable="activeContexts">
                     <iterate
                           ifEmpty="false"
                           operator="or">
                        <equals
                              value="org.eclipse.statet.r.actionSets.RToolbarSet">
                        </equals>
                     </iterate>
                  </with>
               </visibleWhen>
            </command>
            <separator
                  name="submit_selection"
                  visible="false">
            </separator>
            <command
                  commandId="org.eclipse.statet.r.commands.SubmitEntireCommandToR"
                  style="push"
                  label="Run Entire Command in R">
               <visibleWhen
                     checkEnabled="false">
                  <with
                        variable="activeContexts">
                     <iterate
                           ifEmpty="false"
                           operator="or">
                        <equals
                              value="org.eclipse.statet.r.actionSets.RToolbarSet">
                        </equals>
                     </iterate>
                  </with>
               </visibleWhen>
            </command>
            <command
                  commandId="org.eclipse.statet.r.commands.SubmitEntireCommandToR_GotoNextCommand"
                  style="push"
                  label="Run Entire Command in R and Go to Next Command">
               <visibleWhen
                     checkEnabled="false">
                  <with
                        variable="activeContexts">
                     <iterate
                           ifEmpty="false"
                           operator="or">
                        <equals
                              value="org.eclipse.statet.r.actionSets.RToolbarSet">
                        </equals>
                     </iterate>
                  </with>
               </visibleWhen>
            </command>
            <separator
                  name="submit_add"
                  visible="false">
            </separator>
         </toolbar>
      </menuContribution>
      <menuContribution
            locationURI="toolbar:org.eclipse.statet.r.menus.RunToolbar?after=submit_selection">
         <command
               commandId="org.eclipse.statet.r.commands.SubmitSelectionToR_GotoNextLine"
               style="push"
               label="Run Selection in R and Go to Next Line">
            <visibleWhen
                  checkEnabled="false">
               <with
                     variable="activeContexts">
                  <iterate
                        ifEmpty="false"
                        operator="or">
                     <equals
                           value="org.eclipse.statet.r.actionSets.RToolbarExt1Set">
                     </equals>
                  </iterate>
               </with>
            </visibleWhen>
         </command>
      </menuContribution>
   </extension>

<!-- R Env -->
   <extension
         point="org.eclipse.ui.commands">
      <command
            id="org.eclipse.statet.r.commands.UpdateREnvIndex"
            categoryId="org.eclipse.statet.nico.commands.ConsoleCategory"
            name="Update R environment index"
            description="Updates the index of the R environment of the current console">
      </command>
      <command
            id="org.eclipse.statet.r.commands.ResetREnvIndex"
            categoryId="org.eclipse.statet.nico.commands.ConsoleCategory"
            name="Reset R environment index"
            description="Resets and newly creates the index of the R environment of the current console">
      </command>
   </extension>
   <extension
         point="org.eclipse.ui.handlers">
      <handler
            commandId="org.eclipse.statet.r.commands.UpdateREnvIndex"
            class="org.eclipse.statet.internal.r.console.ui.actions.REnvIndexUpdateHandler">
         <enabledWhen>
            <with
                  variable="org.eclipse.statet.activeTool">
               <test
                     property="org.eclipse.statet.nico.isMainType"
                     value="R"
                     forcePluginActivation="false">
               </test>
            </with>
         </enabledWhen>
      </handler>
      <handler
            commandId="org.eclipse.statet.r.commands.ResetREnvIndex"
            class="org.eclipse.statet.internal.r.console.ui.actions.REnvIndexUpdateHandler$Completely">
         <enabledWhen>
            <with
                  variable="org.eclipse.statet.activeTool">
               <test
                     property="org.eclipse.statet.nico.isMainType"
                     value="R"
                     forcePluginActivation="false">
               </test>
            </with>
         </enabledWhen>
      </handler>
   </extension>
   <extension
         point="org.eclipse.ui.bindings">
      <key
            commandId="org.eclipse.statet.r.commands.UpdateREnvIndex"
            contextId="org.eclipse.ui.contexts.window"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R I">
      </key>
   </extension>
   
<!-- Console Page -->
   <extension
         point="org.eclipse.statet.ltk.AdvancedContentAssist">
      <computer
            id="org.eclipse.statet.r.contentAssistComputers.PathCompletion"
            contentTypeId="org.eclipse.statet.r.contentTypes.RConsole"
            categoryId="paths"
            class="org.eclipse.statet.r.ui.sourceediting.RPathCompletionComputer">
         <partition
               partitionType="R.String">
         </partition>
      </computer>
      <computer
            id="org.eclipse.statet.r.contentAssistComputers.RCompletion"
            contentTypeId="org.eclipse.statet.r.contentTypes.RConsole"
            categoryId="R.Default"
            class="org.eclipse.statet.r.ui.sourceediting.RElementCompletionComputer">
         <partition
               partitionType="R.Default">
         </partition>
         <partition
               partitionType="R.QuotedSymbol">
         </partition>
         <partition
               partitionType="R.String">
         </partition>
      </computer>
      <computer
            id="org.eclipse.statet.r.contentAssistComputers.RCompletion"
            contentTypeId="org.eclipse.statet.r.contentTypes.RConsole"
            categoryId="R.Expl.PkgNames"
            class="org.eclipse.statet.r.ui.sourceediting.RPkgCompletionComputer">
         <partition
               partitionType="R.Default">
         </partition>
         <partition
               partitionType="R.QuotedSymbol">
         </partition>
         <partition
               partitionType="R.String">
         </partition>
      </computer>
   </extension>
   <extension
         point="org.eclipse.ui.preferencePages">
      <page
            id="org.eclipse.statet.r.preferencePages.RConsoleAdvancedContentAssistPage"
            category="org.eclipse.statet.r.preferencePages.SourceEditors"
            name="%preferencePages_RConsoleContentAssist_name"
            class="org.eclipse.statet.internal.r.console.ui.page.AdvancedContentAssistConfigurationPage">
      </page>
   </extension>
   
   <extension
         point="org.eclipse.ui.handlers">
      <handler
            commandId="org.eclipse.statet.nico.commands.AdjustOutputWidth"
            class="org.eclipse.statet.internal.r.console.ui.actions.AdjustWidthHandler">
         <activeWhen>
            <with
                  variable="org.eclipse.statet.activeTool">
               <test
                     forcePluginActivation="false"
                     property="org.eclipse.statet.nico.isMainType"
                     value="R">
               </test>
            </with>
         </activeWhen>
      </handler>
   </extension>
   
<!-- R Snippet -->
   <extension
         point="org.eclipse.ui.editors.templates">
      <include
            file="templates/default-snippet-templates.xml"
            translations="templates/default-templates.properties">
      </include>
   </extension>
   
   <extension
         point="org.eclipse.ui.preferencePages">
      <page
            id="org.eclipse.statet.r.preferencePages.RSnippets"
            category="org.eclipse.statet.r.preferencePages.RunDebug"
            name="%preferencePages_RSnippet_name"
            class="org.eclipse.statet.internal.r.console.ui.snippets.RSnippetPreferencePage">
      </page>
   </extension>
   
   <extension
         point="org.eclipse.ui.menus">
      <menuContribution
            locationURI="popup:org.eclipse.ui.popup.any?before=additions">
         <separator
               name="stat.submit">
         </separator>
      </menuContribution>
      <menuContribution
            locationURI="popup:org.eclipse.ui.popup.any?endof=stat.submit">
         <menu
               id="org.eclipse.statet.r.menus.RunRSnippetMenu"
               label="Run Code Snippet in R"
               mnemonic="o"
               icon="icons/tool_16/submit_r-snippet.png">
            <visibleWhen
                  checkEnabled="false">
               <or>
                  <iterate
                        operator="or">
                     <adapt
                           type="org.eclipse.core.resources.IResource">
                        <test
                              property="org.eclipse.core.resources.projectNature"
                              value="org.eclipse.statet.r.resourceProjects.R">
                        </test>
                     </adapt>
                  </iterate>
                  <with
                        variable="activeMenuSelection">
                     <test
                           property="org.eclipse.statet.ltk.isElementSelection"
                           value="R*">
                     </test>
                  </with>
                  <and>
                     <with
                           variable="activePart">
                        <instanceof
                              value="org.eclipse.statet.r.ui.editors.RSourceEditor">
                        </instanceof>
                     </with>
                     <with
                           variable="activeMenuEditorInput">
                        <!-- avoids appearance in ruler menu -->
                     </with>
                  </and>
               </or>
            </visibleWhen>
            <dynamic
                  id="org.eclipse.statet.r.menus.RunRSnippetList"
                  class="org.eclipse.statet.internal.r.console.ui.snippets.SubmitRSnippetsContributionItem">
            </dynamic>
         </menu>
      </menuContribution>
      <menuContribution
            locationURI="toolbar:org.eclipse.statet.r.menus.RunToolbar">
         <command
               id="org.eclipse.statet.r.menus.RunRSnippetMain"
               commandId="org.eclipse.statet.r.commands.SubmitLastRSnippet"
               style="pulldown"
               icon="icons/tool_16/submit_r-snippet.png"
               disabledIcon="icons/tool_16/submit_r-snippet$d.png"
               label="%commands_RunLastRSnippet_name">
         </command>
      </menuContribution>
      <menuContribution
            locationURI="menu:org.eclipse.statet.r.menus.RunRSnippetMain">
         <dynamic
               id="org.eclipse.statet.r.menus.RunRSnippetList"
               class="org.eclipse.statet.internal.r.console.ui.snippets.SubmitRSnippetsContributionItem">
         </dynamic>
         <separator
               name="additions"
               visible="true">
         </separator>
         <command
               commandId="org.eclipse.ui.window.preferences"
               label="%menus_RSnippets_Preferences_name"
               mnemonic="%menus_RSnippets_Preferences_mnemonics">
            <parameter
                  name="preferencePageId"
                  value="org.eclipse.statet.r.preferencePages.RSnippets">
            </parameter>
         </command>
      </menuContribution>
   </extension>
   
</plugin>
