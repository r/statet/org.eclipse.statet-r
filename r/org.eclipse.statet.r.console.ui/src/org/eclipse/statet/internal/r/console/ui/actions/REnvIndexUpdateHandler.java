/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.nico.ui.actions.AbstractToolHandler;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.ui.tools.REnvIndexAutoUpdater;


/**
 * Command handler scheduling the update of an R environment index.
 */
@NonNullByDefault
public class REnvIndexUpdateHandler extends AbstractToolHandler<Tool> {
	
	
	public static final int INDEX_COMPLETELY=               0x0000_0001;
	
	
	public static class Completely extends REnvIndexUpdateHandler {
		
		
		public Completely() {
			super(INDEX_COMPLETELY);
		}
		
	}
	
	
	private final int mode;
	
	
	public REnvIndexUpdateHandler() {
		this(0);
	}
	
	public REnvIndexUpdateHandler(final int mode) {
		super(RConsoleTool.TYPE, RConsoleTool.R_DATA_FEATURESET_ID);
		
		this.mode= mode;
	}
	
	
	@Override
	protected @Nullable Object execute(final Tool tool,
			final ExecutionEvent event) throws ExecutionException {
		tool.getQueue().add(new REnvIndexAutoUpdater.UpdateRunnable(
				((this.mode & 0xf) == INDEX_COMPLETELY) ));
		
		return null;
	}
	
}
