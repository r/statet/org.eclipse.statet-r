/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jface.databinding.swt.ISWTObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICheckable;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.core.validation.IntegerValidator;
import org.eclipse.statet.ecommons.databinding.jface.SWTMultiEnabledObservable;
import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigTabWithDbc;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.ui.components.ButtonGroup;
import org.eclipse.statet.ecommons.ui.components.DataAdapter;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;

import org.eclipse.statet.internal.r.console.ui.Messages;
import org.eclipse.statet.ltk.ui.sourceediting.SnippetEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SnippetEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SnippetEditorObservable;
import org.eclipse.statet.nico.core.util.HistoryTrackingConfiguration;
import org.eclipse.statet.nico.core.util.TrackingConfiguration;
import org.eclipse.statet.nico.core.util.TrackingConfiguration2LaunchConfiguration;
import org.eclipse.statet.nico.ui.util.TrackingConfigurationComposite;
import org.eclipse.statet.nico.ui.util.TrackingConfigurationDialog;
import org.eclipse.statet.r.console.ui.launching.RConsoleLaunching;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.launching.ui.RLaunchingUI;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfigurator;
import org.eclipse.statet.r.ui.sourceediting.RTemplateSourceViewerConfigurator;


/**
 * Launch config tab for common R console options
 * <ul>
 *   <li>History (not yet implemented)</li>
 *   <li>Option to pin console page</li>
 *   <li>Option for a startup snippet</li>
 * </ul>
 */
@NonNullByDefault
public class RConsoleOptionsTab extends LaunchConfigTabWithDbc {
	
	
	static final String TRANSCRIPT_TRACKING_ID= "transcript"; //$NON-NLS-1$
	static final String CUSTOM_TRACKING_ID_PREFIX= "custom"; //$NON-NLS-1$
	
	static final String TRACKING_IDS= "tracking.ids"; //$NON-NLS-1$
	static final String TRACKING_ENABLED_IDS= "tracking.enabled.ids"; //$NON-NLS-1$
	
	static final TrackingConfiguration2LaunchConfiguration TRACKING_UTIL= new TrackingConfiguration2LaunchConfiguration();
	
	static final String ATTR_INTEGRATION_ROOT= "org.eclipse.statet.r.debug/integration"; //$NON-NLS-1$
	static final String ATTR_INTEGRATION_RHELP_ENABLED= ATTR_INTEGRATION_ROOT + "integration.rhelp.enabled"; //$NON-NLS-1$
	static final String ATTR_INTEGRATION_PAGER_ENABLED= ATTR_INTEGRATION_ROOT + "integration.pager.enabled"; //$NON-NLS-1$
	static final String ATTR_INTEGRATION_RGRAPHICS_ASDEFAULT= ATTR_INTEGRATION_ROOT + "integration.rgraphics.asdefault"; //$NON-NLS-1$
	static final String ATTR_INTEGRATION_RDBGEXT_ENABLED= ATTR_INTEGRATION_ROOT + "integration.rdbgext.enabled"; //$NON-NLS-1$
	
	
	private final IObservableValue<Boolean> pinValue;
	
	private final IObservableList<TrackingConfiguration> trackingList;
	private final IObservableSet<TrackingConfiguration> trackingEnabledSet;
	
	private final IObservableValue<String> startupSnippetValue;
	
	private final IObservableValue<Boolean> rHelpByStatetValue;
	private final IObservableValue<Boolean> pagerByStatetValue;
	private final IObservableValue<Boolean> rGraphicsByStatetValue;
	private final IObservableValue<Boolean> rDbgExtValue;
	
	private final IObservableValue<Boolean> objectDBEnabledValue;
	private final IObservableValue<Boolean> objectDBAutoEnabledValue;
	private final IObservableValue<Integer> objectDBListsChildrenValue;
	private final IObservableValue<Integer> objectDBEnvsChildrenValue;
	
	private Button pinControl;
	
	private CheckboxTableViewer trackingTable;
	private ButtonGroup<TrackingConfiguration> trackingButtons;
	private int trackingMaxCustomId;
	
	private SnippetEditor1 startupSnippetEditor;
	
	private Button rHelpByStatetControl;
	private Button pagerByStatetControl;
	private Button rGraphicsByStatetControl;
	private Button rDbgExtControl;
	
	private Button objectDBEnabledControl;
	private Button objectDBAutoEnabledControl;
	private Text objectDBListsChildrenControl;
	private Text objectDBEnvsChildrenControl;
	
	
	public RConsoleOptionsTab() {
		final Realm realm= getRealm();
		this.pinValue= new WritableValue<>(realm, false, Boolean.TYPE);
		this.trackingList= new WritableList<>(realm, new ArrayList<>(), TrackingConfiguration.class);
		this.trackingEnabledSet= new WritableSet<>(realm, new HashSet<>(), TrackingConfiguration.class);
		this.startupSnippetValue= new WritableValue<>(realm, "", String.class); //$NON-NLS-1$
		this.rHelpByStatetValue= new WritableValue<>(realm, false, Boolean.TYPE);
		this.pagerByStatetValue= new WritableValue<>(realm, false, Boolean.TYPE);
		this.rGraphicsByStatetValue= new WritableValue<>(realm, false, Boolean.TYPE);
		this.rDbgExtValue= new WritableValue<>(realm, false, Boolean.TYPE);
		this.objectDBEnabledValue= new WritableValue<>(realm, false, Boolean.TYPE);
		this.objectDBAutoEnabledValue= new WritableValue<>(realm, false, Boolean.TYPE);
		this.objectDBListsChildrenValue= new WritableValue<>(realm, 100000, Integer.TYPE);
		this.objectDBEnvsChildrenValue= new WritableValue<>(realm, 100000, Integer.TYPE);
	}
	
	
	@Override
	public String getName() {
		return Messages.RConsole_OptionsTab_name;
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		final Composite mainComposite= new Composite(parent, SWT.NONE);
		setControl(mainComposite);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		mainComposite.setLayout(GridLayoutFactory.swtDefaults().create());
		
		final Composite consoleComposite= createConsoleOptions(mainComposite);
		if (consoleComposite != null) {
			consoleComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		final Composite trackingComposite= createTrackingOptions(mainComposite);
		if (trackingComposite != null) {
			trackingComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		}
		
		{	// Snippet options:
			final Group group= new Group(mainComposite, SWT.NONE);
			group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4));
			group.setText("R snippet run after startup:");
			createSnippetOptions(group);
		}
		
		{	// Object DB options:
			final Group group= new Group(mainComposite, SWT.NONE);
			group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			group.setText("Eclipse Integration:");
			createEclipseOptions(group);
		}
		
		Dialog.applyDialogFont(parent);
		
		initBindings();
		this.trackingButtons.updateState();
	}
	
	private Composite createConsoleOptions(final Composite parent) {
		final Group group= new Group(parent, SWT.NONE);
		group.setText(Messages.RConsole_MainTab_ConsoleOptions_label);
		group.setLayout(LayoutUtils.newGroupGrid(2));
		
		this.pinControl= new Button(group, SWT.CHECK);
		this.pinControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		this.pinControl.setText(Messages.RConsole_MainTab_ConsoleOptions_Pin_label);
		
		return group;
	}
	
	private Composite createTrackingOptions(final Composite parent) {
		final Group group= new Group(parent, SWT.NONE);
		group.setText("History / Transcript / Tracking:");
		group.setLayout(LayoutUtils.newGroupGrid(2));
		
		final ViewerUtils.CheckboxTableComposite trackingTable;
		{	trackingTable= new ViewerUtils.CheckboxTableComposite(group, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
			gd.heightHint= LayoutUtils.hintHeight(trackingTable.table, 5);
			trackingTable.setLayoutData(gd);
			this.trackingTable= trackingTable.viewer;
		}
		{	final TableViewerColumn column= trackingTable.addColumn("Name", SWT.LEFT, new ColumnWeightData(100));
			column.setLabelProvider(new CellLabelProvider() {
				@Override
				public void update(final ViewerCell cell) {
					final TrackingConfiguration config= (TrackingConfiguration) cell.getElement();
					cell.setText(config.getName());
				}
			});
		}
		
		this.trackingButtons= new ButtonGroup<>(group) {
			@Override
			protected @Nullable TrackingConfiguration edit1(@Nullable TrackingConfiguration item,
					final boolean newItem, final @Nullable Object parent) {
				TrackingConfigurationDialog dialog;
				if (!newItem && item != null && item.getId().equals(HistoryTrackingConfiguration.HISTORY_TRACKING_ID)) {
					item= new HistoryTrackingConfiguration(item.getId(), (HistoryTrackingConfiguration) item);
					dialog= new TrackingConfigurationDialog(RConsoleOptionsTab.this.getShell(), item, false) {
						@Override
						protected TrackingConfigurationComposite createConfigComposite(final Composite parent) {
							return new RHistoryConfigurationComposite(parent);
						}
					};
				}
				else {
					if (newItem) {
						final String id= CUSTOM_TRACKING_ID_PREFIX + (RConsoleOptionsTab.this.trackingMaxCustomId + 1);
						if (item == null) {
							item= new TrackingConfiguration(id);
						}
						else {
							item= new TrackingConfiguration(id, item);
						}
					}
					else {
						item= new TrackingConfiguration(item.getId(), item);
					}
					dialog= new TrackingConfigurationDialog(RConsoleOptionsTab.this.getShell(), item, newItem) {
						@Override
						protected TrackingConfigurationComposite createConfigComposite(final Composite parent) {
							return new RTrackingConfigurationComposite(parent);
						}
					};
				}
				if (dialog.open() == Dialog.OK) {
					if (newItem) {
						RConsoleOptionsTab.this.trackingMaxCustomId++;
					}
					return item;
				}
				return null;
			}
		};
		this.trackingButtons.addAddButton(null);
		this.trackingButtons.addDeleteButton(null);
		this.trackingButtons.addEditButton(null);
		this.trackingButtons.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));
		
		ViewerUtils.scheduleStandardSelection(this.trackingTable);
		
		return group;
	}
	
	private void createSnippetOptions(final Composite container) {
		container.setLayout(LayoutUtils.newGroupGrid(1));
		
		final TemplateVariableProcessor templateVariableProcessor= new TemplateVariableProcessor();
		final RSourceViewerConfigurator configurator= new RTemplateSourceViewerConfigurator(
				RCore.WORKBENCH_ACCESS,
				templateVariableProcessor );
		this.startupSnippetEditor= new SnippetEditor1(configurator, null,
				PlatformUI.getWorkbench(), RLaunchingUI.LAUNCH_CONFIG_QUALIFIER );
		this.startupSnippetEditor.create(container, SnippetEditor.DEFAULT_MULTI_LINE_STYLE);
		final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.heightHint= LayoutUtils.hintHeight(this.startupSnippetEditor.getSourceViewer().getTextWidget(), 8);
		this.startupSnippetEditor.getControl().setLayoutData(gd);
	}
	
	private void createEclipseOptions(final Composite container) {
		container.setLayout(LayoutUtils.newGroupGrid(2));
		
		{	this.rHelpByStatetControl= new Button(container, SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
			this.rHelpByStatetControl.setLayoutData(gd);
			this.rHelpByStatetControl.setText("Enable integrated R Help for help functions in R ('help', 'help.start', '?')");
		}
		{	this.pagerByStatetControl= new Button(container, SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
			this.pagerByStatetControl.setLayoutData(gd);
			this.pagerByStatetControl.setText("Enable integrated Pager view as default 'pager' to display text files ('file.show', ...)");
		}
		{	this.rGraphicsByStatetControl= new Button(container, SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
			this.rGraphicsByStatetControl.setLayoutData(gd);
			this.rGraphicsByStatetControl.setText("Set R Graphic view by StatET as default graphic device for new plots in R");
		}
		{	this.rDbgExtControl= new Button(container, SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
			this.rDbgExtControl.setLayoutData(gd);
			this.rDbgExtControl.setText("Enable extensions by StatET for improved debug support of R code");
		}
		
		{	this.objectDBEnabledControl= new Button(container, SWT.CHECK);
			this.objectDBEnabledControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
			this.objectDBEnabledControl.setText("Enable Object DB (for Object Browser etc.)");
		}
		
		{	this.objectDBAutoEnabledControl= new Button(container, SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
			gd.horizontalIndent= LayoutUtils.defaultIndent();
			this.objectDBAutoEnabledControl.setLayoutData(gd);
			this.objectDBAutoEnabledControl.setText("Refresh DB automatically (initial setting)");
		}
		{	final Label label= new Label(container, SWT.NONE);
			label.setText("Max length of R lists to fetch:");
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, false, false);
			gd.horizontalIndent= LayoutUtils.defaultIndent();
			label.setLayoutData(gd);
		}
		{	this.objectDBListsChildrenControl= new Text(container, SWT.BORDER);
			this.objectDBListsChildrenControl.setTextLimit(10);
			final GridData gd= new GridData(SWT.LEFT, SWT.FILL, true, false);
			gd.widthHint= LayoutUtils.hintWidth(this.objectDBListsChildrenControl, 10);
			this.objectDBListsChildrenControl.setLayoutData(gd);
		}
		{	final Label label= new Label(container, SWT.NONE);
			label.setText("Max length of R environments to fetch:");
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, false, false);
			gd.horizontalIndent= LayoutUtils.defaultIndent();
			label.setLayoutData(gd);
		}
		{	this.objectDBEnvsChildrenControl= new Text(container, SWT.BORDER);
			this.objectDBEnvsChildrenControl.setTextLimit(10);
			final GridData gd= new GridData(SWT.LEFT, SWT.FILL, true, false);
			gd.widthHint= LayoutUtils.hintWidth(this.objectDBEnvsChildrenControl, 10);
			this.objectDBEnvsChildrenControl.setLayoutData(gd);
		}
	}
	
	@Override
	protected void addBindings(final DataBindingContext dbc, final Realm realm) {
		dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.pinControl),
				this.pinValue );
		
		this.trackingTable.setContentProvider(new ObservableListContentProvider<TrackingConfiguration>());
		this.trackingTable.setInput(this.trackingList);
		
		dbc.bindValue(
				new SnippetEditorObservable(realm, this.startupSnippetEditor, SWT.Modify),
				this.startupSnippetValue );
		
		dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.rHelpByStatetControl),
				this.rHelpByStatetValue );
		dbc.bindValue(
				WidgetProperties.buttonSelection()
				.observe(this.pagerByStatetControl),
				this.pagerByStatetValue );
		dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.rGraphicsByStatetControl),
				this.rGraphicsByStatetValue );
		dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.rDbgExtControl),
				this.rDbgExtValue );
		
		final ISWTObservableValue<Boolean> objectDBTargetObs= WidgetProperties.buttonSelection()
				.observe(this.objectDBEnabledControl);
		dbc.bindValue(
				objectDBTargetObs,
				this.objectDBEnabledValue );
		dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.objectDBAutoEnabledControl),
				this.objectDBAutoEnabledValue );
		dbc.bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.objectDBListsChildrenControl),
				this.objectDBListsChildrenValue,
				new UpdateValueStrategy<String, Integer>()
						.setAfterGetValidator(new IntegerValidator(100, Integer.MAX_VALUE,
								"Invalid max value for length of R lists to fetch (100-)." )),
				null );
		dbc.bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.objectDBEnvsChildrenControl),
				this.objectDBEnvsChildrenValue,
				new UpdateValueStrategy<String, Integer>()
						.setAfterGetValidator(new IntegerValidator(100, Integer.MAX_VALUE,
								"Invalid max value for length of R environments to fetch (100-)." )),
				null );
		
		dbc.bindValue(new SWTMultiEnabledObservable(realm, new Control[] {
						this.objectDBAutoEnabledControl,
						this.objectDBEnvsChildrenControl,
						this.objectDBListsChildrenControl,
				}, null ),
				objectDBTargetObs );
		
		this.trackingButtons.connectTo(this.trackingTable,
				new DataAdapter.ListAdapter<>(this.trackingList, null) {
			@Override
			public boolean isDeleteAllowed(final Object element) {
				return (super.isDeleteAllowed(element)
						&& ((TrackingConfiguration) element).getId().startsWith(CUSTOM_TRACKING_ID_PREFIX) );
			}
		});
		
		this.trackingButtons.setCheckedModel(this.trackingEnabledSet);
		dbc.bindSet(
				ViewerProperties.checkedElements(TrackingConfiguration.class)
						.observe((ICheckable)this.trackingTable),
				this.trackingEnabledSet );
	}
	
	
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
	}
	
	@Override
	protected void doInitialize(final ILaunchConfiguration configuration) {
		this.pinValue.setValue(readAttribute(configuration,
				RConsoleLaunching.ATTR_PIN_CONSOLE,
				false ));
		
		this.rHelpByStatetValue.setValue(readAttribute(configuration,
				ATTR_INTEGRATION_RHELP_ENABLED,
				true ));
		this.pagerByStatetValue.setValue(readAttribute(configuration,
				ATTR_INTEGRATION_RHELP_ENABLED,
				true ));
		this.rGraphicsByStatetValue.setValue(readAttribute(configuration,
				ATTR_INTEGRATION_RGRAPHICS_ASDEFAULT,
				true ));
		this.rDbgExtValue.setValue(readAttribute(configuration,
				ATTR_INTEGRATION_RDBGEXT_ENABLED,
				true ));
		
		this.startupSnippetValue.setValue(readAttribute(configuration,
				RConsoleLaunching.ATTR_INIT_SCRIPT_SNIPPET,
				"" )); //$NON-NLS-1$
		
		this.objectDBEnabledValue.setValue(readAttribute(configuration,
				RConsoleLaunching.ATTR_OBJECTDB_ENABLED,
				true ));
		
		this.objectDBAutoEnabledValue.setValue(readAttribute(configuration,
				RConsoleLaunching.ATTR_OBJECTDB_AUTOREFRESH_ENABLED,
				true ));
		this.objectDBListsChildrenValue.setValue(readAttribute(configuration,
				RConsoleLaunching.ATTR_OBJECTDB_LISTS_MAX_LENGTH,
				200000 ));
		this.objectDBEnvsChildrenValue.setValue(readAttribute(configuration,
				RConsoleLaunching.ATTR_OBJECTDB_ENVS_MAX_LENGTH,
				20000 ));
		
		{	this.trackingList.clear();
			this.trackingMaxCustomId= 0;
			final List<String> trackingIds= readAttribute(configuration,
					TRACKING_IDS,
					Collections.emptyList() );
			for (final String id : trackingIds) {
				final TrackingConfiguration trackingConfig= id.equals(HistoryTrackingConfiguration.HISTORY_TRACKING_ID) ?
						new HistoryTrackingConfiguration(id) : new TrackingConfiguration(id);
				try {
					TRACKING_UTIL.load(trackingConfig, configuration);
					this.trackingList.add(trackingConfig);
					if (id.startsWith(CUSTOM_TRACKING_ID_PREFIX)) {
						try {
							final int num= Integer.parseInt(id.substring(CUSTOM_TRACKING_ID_PREFIX.length()));
							this.trackingMaxCustomId= Math.max(this.trackingMaxCustomId, num);
						} catch (final Exception e) {}
					}
				}
				catch (final CoreException e) {
					trackingIds.remove(id);
					logReadingError(e);
				}
			}
			if (!trackingIds.contains(HistoryTrackingConfiguration.HISTORY_TRACKING_ID)) {
				final TrackingConfiguration trackingConfig= new HistoryTrackingConfiguration(HistoryTrackingConfiguration.HISTORY_TRACKING_ID);
				trackingConfig.setName("History");
				trackingConfig.setFilePath(RHistoryConfigurationComposite.HISTORY_TRACKING_DEFAULT_PATH);
				this.trackingList.add(trackingConfig);
			}
			if (!trackingIds.contains(TRANSCRIPT_TRACKING_ID)) {
				final TrackingConfiguration trackingConfig= new TrackingConfiguration(TRANSCRIPT_TRACKING_ID);
				trackingConfig.setName("Transcript");
				trackingConfig.setFilePath(RTrackingConfigurationComposite.TRANSCRIPT_TRACKING_DEFAULT_PATH);
				this.trackingList.add(trackingConfig);
			}
		}
		
		{	this.trackingEnabledSet.clear();
			final List<String> trackingIds= readAttribute(configuration,
					TRACKING_ENABLED_IDS,
					Collections.emptyList() );
			final List<TrackingConfiguration> trackingList= this.trackingList;
			for (final TrackingConfiguration trackingConfig : trackingList) {
				if (trackingIds.contains(trackingConfig.getId())) {
					this.trackingEnabledSet.add(trackingConfig);
				}
			}
		}
	}
	
	@Override
	protected void doSave(final ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(RConsoleLaunching.ATTR_PIN_CONSOLE,
				this.pinValue.getValue().booleanValue() );
		
		configuration.setAttribute(ATTR_INTEGRATION_RHELP_ENABLED,
				this.rHelpByStatetValue.getValue().booleanValue() );
		configuration.setAttribute(ATTR_INTEGRATION_PAGER_ENABLED,
				this.pagerByStatetValue.getValue().booleanValue() );
		configuration.setAttribute(ATTR_INTEGRATION_RGRAPHICS_ASDEFAULT,
				this.rGraphicsByStatetValue.getValue().booleanValue() );
		configuration.setAttribute(ATTR_INTEGRATION_RDBGEXT_ENABLED,
				this.rDbgExtValue.getValue().booleanValue() );
		
		{	final String startupSnippet= this.startupSnippetValue.getValue();
			if (startupSnippet != null && startupSnippet.length() > 0) {
				configuration.setAttribute(RConsoleLaunching.ATTR_INIT_SCRIPT_SNIPPET, startupSnippet);
			}
			else {
				configuration.removeAttribute(RConsoleLaunching.ATTR_INIT_SCRIPT_SNIPPET);
			}
		}
		
		configuration.setAttribute(RConsoleLaunching.ATTR_OBJECTDB_ENABLED,
				this.objectDBEnabledValue.getValue().booleanValue() );
		
		configuration.setAttribute(RConsoleLaunching.ATTR_OBJECTDB_AUTOREFRESH_ENABLED,
				this.objectDBAutoEnabledValue.getValue().booleanValue() );
		configuration.setAttribute(RConsoleLaunching.ATTR_OBJECTDB_LISTS_MAX_LENGTH,
				this.objectDBListsChildrenValue.getValue().intValue() );
		configuration.setAttribute(RConsoleLaunching.ATTR_OBJECTDB_ENVS_MAX_LENGTH,
				this.objectDBEnvsChildrenValue.getValue().intValue() );
		
		{	final List<String> trackingIds= new ArrayList<>(this.trackingList.size());
			
			final List<TrackingConfiguration> trackingList= this.trackingList;
			for (final TrackingConfiguration trackingConfig : trackingList) {
				final String id= trackingConfig.getId();
				trackingIds.add(id);
				TRACKING_UTIL.save(trackingConfig, configuration);
			}
			configuration.setAttribute(TRACKING_IDS, trackingIds);
		}
		
		{	final List<String> trackingIds= new ArrayList<>(this.trackingEnabledSet.size());
			final Set<TrackingConfiguration> trackingEnabledSet= this.trackingEnabledSet;
			for (final TrackingConfiguration trackingConfig : trackingEnabledSet) {
				final String id= trackingConfig.getId();
				trackingIds.add(id);
			}
			configuration.setAttribute(TRACKING_ENABLED_IDS, trackingIds);
		}
	}
	
}
