/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

import org.eclipse.statet.nico.ui.util.CommonTabForConsole;
import org.eclipse.statet.r.launching.ui.REnvTab;


public class RRemoteConsoleTabGroup extends AbstractLaunchConfigurationTabGroup {
	
	
	public RRemoteConsoleTabGroup() {
	}
	
	
	@Override
	public void createTabs(final ILaunchConfigurationDialog dialog, final String mode) {
		final ILaunchConfigurationTab[] tabs= new ILaunchConfigurationTab[] {
				new RRemoteConsoleMainTab(),
				new REnvTab(false),
				new RConsoleOptionsTab(),
				
				new CommonTabForConsole()
		};
		setTabs(tabs);
	}
	
}
