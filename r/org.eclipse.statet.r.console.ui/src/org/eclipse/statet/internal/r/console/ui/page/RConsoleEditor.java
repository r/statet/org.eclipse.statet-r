/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.page;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.ecommons.ui.ISettingsChangedHandler;
import org.eclipse.statet.ecommons.ui.workbench.ContextHandlers;

import org.eclipse.statet.ltk.ui.LtkActions;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.nico.core.runtime.Prompt;
import org.eclipse.statet.nico.ui.console.ConsolePageEditor;
import org.eclipse.statet.r.console.core.ContinuePrompt;
import org.eclipse.statet.r.console.core.IRBasicAdapter;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.r.ui.sourceediting.InsertAssignmentHandler;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfigurator;


/**
 * R Console input line
 */
public class RConsoleEditor extends ConsolePageEditor implements RSourceEditor, ISettingsChangedHandler {
	
	
	private RSourceViewerConfigurator rConfig;
	
	
	public RConsoleEditor(final RConsolePage page) {
		super(page, RCore.R_CONTENT_TYPE);
	}
	
	
	@Override
	protected RSourceUnit createSourceUnit() {
		return new RConsoleSourceUnit((RConsolePage) getConsolePage(), getDocument());
	}
	
	
	@Override
	public RCoreAccess getRCoreAccess() {
		return this.rConfig.getRCoreAccess();
	}
	
	@Override
	public RSourceUnit getSourceUnit() {
		return (RSourceUnit)super.getSourceUnit();
	}
	
	@Override
	protected void onPromptUpdate(final Prompt prompt) {
		if ((prompt.meta & IRBasicAdapter.META_PROMPT_INCOMPLETE_INPUT) != 0) {
			final ContinuePrompt p= (ContinuePrompt) prompt;
			setInputPrefix(p.getPreviousInput());
		}
		else {
			setInputPrefix(""); //$NON-NLS-1$
		}
	}
	
	@Override
	public Composite createControl(final Composite parent, final SourceEditorViewerConfigurator editorConfig) {
		this.rConfig= (RSourceViewerConfigurator) editorConfig;
		final Composite control= super.createControl(parent, editorConfig);
		return control;
	}
	
	@Override
	public void initActions(final IServiceLocator serviceLocator, final ContextHandlers handlers) {
		super.initActions(serviceLocator, handlers);
		
		serviceLocator.getService(IContextService.class)
				.activateContext("org.eclipse.statet.r.contexts.REditor"); //$NON-NLS-1$
		
		final IHandlerService handlerService= handlers.getHandlerService();
		
		{	final IHandler2 handler= new InsertAssignmentHandler(this);
			handlerService.activateHandler(LtkActions.INSERT_ASSIGNMENT_COMMAND_ID, handler);
		}
	}
	
	
	@Override
	public <T> T getAdapter(final Class<T> adapterType) {
		return super.getAdapter(adapterType);
	}
	
}
