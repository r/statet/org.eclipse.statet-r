/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.page;

import static org.eclipse.statet.ecommons.ui.actions.UIActions.ADDITIONS_GROUP_ID;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.help.IContextProvider;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.events.HelpEvent;
import org.eclipse.swt.events.HelpListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.console.ui.Messages;
import org.eclipse.statet.nico.core.runtime.IRemoteEngineController;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.console.NIConsole;
import org.eclipse.statet.nico.ui.console.NIConsolePage;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.console.ui.IRConsoleHelpContextIds;
import org.eclipse.statet.r.console.ui.RConsole;
import org.eclipse.statet.r.console.ui.tools.ChangeWorkingDirectoryWizard;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.pkgmanager.RPkgUtils;
import org.eclipse.statet.r.ui.RUIHelp;
import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.r.ui.pkgmanager.OpenRPkgManagerHandler;
import org.eclipse.statet.r.ui.pkgmanager.StartAction;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;


public class RConsolePage extends NIConsolePage {
	
	
	private IContextProvider helpContextProvider;
	
	
	public RConsolePage(final RConsole console, final IConsoleView view) {
		super(console, view);
	}
	
	
	@Override
	public RConsole getConsole() {
		return (RConsole) super.getConsole();
	}
	
	@Override
	public void createControl(final Composite parent) {
		super.createControl(parent);
	}
	
	@Override
	protected RInputConfigurator createInputEditorConfigurator() {
		return new RInputConfigurator(this, (RSourceEditor) getInputEditor());
	}
	
	@Override
	protected RConsoleEditor createInputEditor() {
		return new RConsoleEditor(this);
	}
	
	@Override
	protected void initActions(final IServiceLocator serviceLocator, final HandlerCollection handlers) {
		super.initActions(serviceLocator, handlers);
		
		final IContextService contextService= serviceLocator.getService(IContextService.class);
		contextService.activateContext("org.eclipse.statet.r.actionSets.RSessionTools"); //$NON-NLS-1$
		
		this.helpContextProvider= RUIHelp.createEnrichedRHelpContextProvider(
				getInputEditor().getViewer(), IRConsoleHelpContextIds.R_CONSOLE );
		getInputEditor().getViewer().getTextWidget().addHelpListener(new HelpListener() {
			@Override
			public void helpRequested(final HelpEvent e) {
				PlatformUI.getWorkbench().getHelpSystem().displayHelp(RConsolePage.this.helpContextProvider.getContext(null));
			}
		});
	}
	
	@Override
	protected void contributeToActionBars(final IServiceLocator serviceLocator,
			final IActionBars actionBars, final HandlerCollection handlers) {
		super.contributeToActionBars(serviceLocator, actionBars, handlers);
		
		final IMenuManager menuManager= actionBars.getMenuManager();
		
		menuManager.appendToGroup(NICO_CONTROL_MENU_ID, new CommandContributionItem(
				new CommandContributionItemParameter(serviceLocator,
						null, NicoUI.PAUSE_COMMAND_ID, null,
						null, null, null,
						null, null, null,
						CommandContributionItem.STYLE_CHECK, null, false )));
		if (getConsole().getProcess().isProvidingFeatureSet(IRemoteEngineController.FEATURE_SET_ID)) {
			menuManager.appendToGroup(NICO_CONTROL_MENU_ID, new CommandContributionItem(
					new CommandContributionItemParameter(serviceLocator,
							null, NicoUI.DISCONNECT_COMMAND_ID, null,
							null, null, null,
							null, null, null,
							CommandContributionItem.STYLE_PUSH, null, false )));
			menuManager.appendToGroup(NICO_CONTROL_MENU_ID, new CommandContributionItem(
					new CommandContributionItemParameter(serviceLocator,
							null, NicoUI.RECONNECT_COMMAND_ID, null,
							null, null, null,
							null, null, null,
							CommandContributionItem.STYLE_PUSH, null, false )));
		}
		
		menuManager.insertBefore(ADDITIONS_GROUP_ID, new Separator("workspace")); //$NON-NLS-1$
		menuManager.appendToGroup("workspace", //$NON-NLS-1$
				new ChangeWorkingDirectoryWizard.ChangeAction(this));
		
		menuManager.insertBefore(ADDITIONS_GROUP_ID, new Separator("view")); //$NON-NLS-1$
		
		final RProcess process= getConsole().getProcess();
		final REnv rEnv= process.getREnv();
		if (process.isProvidingFeatureSet(RConsoleTool.R_DATA_FEATURESET_ID) && rEnv != null) {
			menuManager.appendToGroup(NICO_CONTROL_MENU_ID,
					new CommandContributionItem(new CommandContributionItemParameter(
							getSite(), null, "org.eclipse.statet.r.commands.OpenRPkgManager", null, //$NON-NLS-1$
							null, null, null,
							"Open Package Manager", "P", null,
							CommandContributionItem.STYLE_PUSH, null, false )));
			
			final MenuManager rEnvMenu= new MenuManager("R &Environment");
			menuManager.appendToGroup(NICO_CONTROL_MENU_ID, rEnvMenu);
			
			rEnvMenu.add(new CommandContributionItem(new CommandContributionItemParameter(
							getSite(), null, "org.eclipse.statet.r.commands.OpenRPkgManager", null, //$NON-NLS-1$
							null, null, null,
							"Open Package Manager", "P", null,
							CommandContributionItem.STYLE_PUSH, null, false )));
			if (RPkgUtils.DEBUG) {
				rEnvMenu.add(new HandlerContributionItem(new CommandContributionItemParameter(
								getSite(), null, HandlerContributionItem.NO_COMMAND_ID, null,
								null, null, null,
								"Open Package Manager - Clean", null, null, //$NON-NLS-1$
								CommandContributionItem.STYLE_PUSH, null, false ),
						new OpenRPkgManagerHandler() {
							@Override
							protected int getRequestFlags() {
								return RPkgManager.RESET;
							}
						}));
				rEnvMenu.add(new HandlerContributionItem(new CommandContributionItemParameter(
								getSite(), null, HandlerContributionItem.NO_COMMAND_ID, null,
								null, null, null,
								"Open Package Manager - Install 'zic'", null, null, //$NON-NLS-1$
								CommandContributionItem.STYLE_PUSH, null, false ),
						new OpenRPkgManagerHandler() {
							@Override
							protected StartAction getStartAction() {
								return new StartAction(StartAction.INSTALL, ImCollections.newList("zic")); //$NON-NLS-1$
							}
				}));
			}
			
			rEnvMenu.add(new Separator());
			
			rEnvMenu.add(new CommandContributionItem(new CommandContributionItemParameter(
					getSite(), null, "org.eclipse.statet.r.commands.UpdateREnvIndex", null, //$NON-NLS-1$
					null, null, null,
					"Update &index (changes)", null, null,
					CommandContributionItem.STYLE_PUSH, null, false )));
			rEnvMenu.add(new CommandContributionItem(new CommandContributionItemParameter(
					getSite(), null, "org.eclipse.statet.r.commands.ResetREnvIndex", null, //$NON-NLS-1$
					null, null, null,
					"Reset inde&x (completely)", null, null,
					CommandContributionItem.STYLE_PUSH, null, false)));
		}
	}
	
	@Override
	protected void fillOutputContextMenu(final IMenuManager menuManager) {
		final var site= getSite();
		
		super.fillOutputContextMenu(menuManager);
		
		menuManager.appendToGroup("view", new CommandContributionItem( //$NON-NLS-1$
				new CommandContributionItemParameter(site,
						null, NIConsole.ADJUST_OUTPUT_WIDTH_COMMAND_ID, null,
						null, null, null,
						Messages.AdjustWidth_label, Messages.AdjustWidth_mnemonic, null,
						CommandContributionItem.STYLE_PUSH, null, false )));
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Class<T> adapterType) {
		if (adapterType == IContextProvider.class) {
			return (T) this.helpContextProvider;
		}
		if (adapterType == REnv.class) {
			return (T) getTool().getAdapter(REnv.class);
		}
		return super.getAdapter(adapterType);
	}
	
	@Override
	protected void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		if (groupIds.contains(RCodeStyleSettings.INDENT_GROUP_ID) 
				&& UIAccess.isOkToUse(getOutputViewer())) {
			final RCodeStyleSettings codeStyle= (getConsole()).getRCodeStyle();
			if (codeStyle.isDirty()) {
				getOutputViewer().setTabWidth(codeStyle.getTabSize());
			}
		}
	}
	
	@Override
	protected void collectContextMenuPreferencePages(final List<String> pageIds) {
		super.collectContextMenuPreferencePages(pageIds);
		pageIds.add("org.eclipse.statet.r.preferencePages.REditorOptions"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.r.preferencePages.RTextStyles"); //$NON-NLS-1$
	}
	
}
