/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.snippets;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.ui.StringVariableSelectionDialog.VariableFilter;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.text.templates.ContextTypeRegistry;
import org.eclipse.text.templates.TemplatePersistenceData;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.ui.components.ButtonGroup;
import org.eclipse.statet.ecommons.ui.components.CustomizableVariableSelectionDialog;
import org.eclipse.statet.ecommons.ui.util.VariableFilterUtils;

import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.templates.config.CodeTemplateConfigurationBlock;
import org.eclipse.statet.ltk.ui.templates.config.EditTemplateDialog;
import org.eclipse.statet.ltk.ui.templates.config.ITemplateCategoryConfiguration;
import org.eclipse.statet.ltk.ui.templates.config.ITemplateContribution;
import org.eclipse.statet.ltk.ui.templates.config.TemplateCategory;
import org.eclipse.statet.ltk.ui.templates.config.TemplateStoreContribution;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.launching.ui.RLaunchingUI;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.sourceediting.RTemplateSourceViewerConfigurator;


public class RSnippetPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public RSnippetPreferencePage() {
	}
	
	
	@Override
	protected CodeTemplateConfigurationBlock createConfigurationBlock() throws CoreException {
		return new SnippetConfigurationBlock();
	}
	
}


class SnippetConfigurationBlock extends CodeTemplateConfigurationBlock {
	
	
	private static final String R_SNIPPET_CATEGORY_ID= "r.ConsoleSnippet"; //$NON-NLS-1$
	
	
	private static class RSnippetTemplateConfiguration implements ITemplateCategoryConfiguration {
		
		
		private final RSnippets snippets;
		
		
		public RSnippetTemplateConfiguration(final RSnippets snippets) {
			this.snippets= snippets;
		}
		
		
		@Override
		public ITemplateContribution getTemplates() {
			return new TemplateStoreContribution(this.snippets.getTemplateStore());
		}
		
		@Override
		public Preference<String> getDefaultPref() {
			return null;
		}
		
		@Override
		public ContextTypeRegistry getContextTypeRegistry() {
			return this.snippets.getTemplateContextRegistry();
		}
		
		@Override
		public String getDefaultContextTypeId() {
			return RSnippetTemplateContextType.TYPE_ID;
		}
		
		@Override
		public String getViewerConfigId(final TemplatePersistenceData data) {
			return RSnippetTemplateContextType.TYPE_ID;
		}
		
		@Override
		public SourceEditorViewerConfigurator createViewerConfiguator(final String viewerConfigId,
				final TemplatePersistenceData data,
				final TemplateVariableProcessor templateProcessor, final IProject project) {
			return new RTemplateSourceViewerConfigurator(
					RCore.WORKBENCH_ACCESS,
					templateProcessor );
		}
		
	}
	
	private static class RSnippetEditDialog extends EditTemplateDialog {
		
		
		private final RSnippets snippets;
		
		
		private RSnippetEditDialog(final Shell parent, final Template template, final boolean edit, final int flags,
				final SourceEditorViewerConfigurator configurator,
				final TemplateVariableProcessor processor, final ContextTypeRegistry registry,
				final RSnippets snippets) {
			super(parent, template, edit, flags, configurator, processor, registry,
					RLaunchingUI.LAUNCH_CONFIG_QUALIFIER );
			this.snippets= snippets;
		}
		
		
		@Override
		protected IStatus validate(final TemplateContextType contextType, final String text) {
			return this.snippets.validate(contextType, text);
		}
		
		@Override
		protected void insertVariablePressed() {
			final CustomizableVariableSelectionDialog dialog= new CustomizableVariableSelectionDialog(getShell());
			final List<VariableFilter> filters= VariableFilterUtils.DEFAULT_INTERACTIVE_FILTERS;
			for (final VariableFilter filter : filters) {
				dialog.addVariableFilter(filter);
			}
			dialog.addAdditional(RSnippets.ECHO_ENABLED_VARIABLE);
			dialog.addAdditional(RSnippets.RESOURCE_ENCODING_VARIABLE);
			
			if (dialog.open() != Dialog.OK) {
				return;
			}
			final String variable= dialog.getVariableExpression();
			if (variable == null) {
				return;
			}
			insertText(variable);
		}
	}
	
	
	private final ICommandService commandService;
	
	private final RSnippets snippets;
	
	
	public SnippetConfigurationBlock() throws CoreException {
		super(Messages.SnippetTemplates_title, ADD_ITEM, null);
		
		this.snippets= RConsoleUIPlugin.getInstance().getRSnippets();
		setCategories(	ImCollections.newList(
						new TemplateCategory(R_SNIPPET_CATEGORY_ID,
								RConsoleUIPlugin.getInstance().getImageRegistry()
										.getDescriptor(RConsoleUIPlugin.IMG_OBJ_SNIPPETS),
								Messages.SnippetTemplates_RSnippet_label,
								RUI.getImageDescriptor(RUI.IMG_OBJ_R_SCRIPT),
								new RSnippetTemplateConfiguration(this.snippets) )
				));
		
		this.commandService= PlatformUI.getWorkbench().getService(ICommandService.class);
	}
	
	@Override
	protected String getListLabel() {
		return "Code Snippe&ts";
	}
	
	@Override
	protected void createBlockArea(final Composite pageComposite) {
		super.createBlockArea(pageComposite);
		
		final Link keyLink= addLinkControl(pageComposite, Messages.SnippetTemplates_KeysNote_label,
				new LinkSelectionListener() {
			private ParameterizedCommand command;
			@Override
			protected Object getData(final SelectionEvent e) {
				if (this.command == null) {
					final Command command= SnippetConfigurationBlock.this.commandService
							.getCommand(RSnippets.SUBMIT_SNIPPET_COMMAND_ID);
					final List<TemplateItem> templates= getTemplates(getCategories().get(0));
					if (!templates.isEmpty()) {
						final String name= templates.get(0).getData().getTemplate().getName();
						final Map<String, String> parameters= Collections.singletonMap(RSnippets.SNIPPET_PAR, name);
						this.command= ParameterizedCommand.generateCommand(command, parameters);
					}
				}
				return this.command;
			}
		});
		keyLink.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
	}
	
	@Override
	protected EditTemplateDialog createEditDialog(final Template template, final int command,
			final SourceEditorViewerConfigurator configurator, final TemplateVariableProcessor processor,
			final ContextTypeRegistry registry) {
		return new RSnippetEditDialog(getShell(), template,
				((command & ButtonGroup.ADD_ANY) != 0), EditTemplateDialog.CUSTOM_TEMPLATE,
				configurator, processor, registry, this.snippets );
	}
	
}
