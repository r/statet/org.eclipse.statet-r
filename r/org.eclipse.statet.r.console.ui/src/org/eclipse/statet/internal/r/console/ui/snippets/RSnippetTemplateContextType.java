/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.snippets;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.variables.IStringVariable;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.IValueVariableListener;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.text.templates.TemplateException;
import org.eclipse.jface.text.templates.TemplateVariableResolver;

import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;


public class RSnippetTemplateContextType extends TemplateContextType
		implements IValueVariableListener {
	
	
	private static class VariableResolver extends TemplateVariableResolver {
		
		public VariableResolver(final IStringVariable variable) {
			super(variable.getName(), variable.getDescription());
		}
		
	}
	
	
	public static final String TYPE_ID= "org.eclipse.statet.r.templates.RConsoleSnippetContextType"; //$NON-NLS-1$
	
	public static final String TEMPLATES_KEY= "org.eclipse.statet.r.templates.rsnippets"; //$NON-NLS-1$
	
	
	private boolean update;
	
	
	public RSnippetTemplateContextType() {
		super(TYPE_ID);
		
		this.update= true;
		VariablesPlugin.getDefault().getStringVariableManager().addValueVariableListener(this);
	}
	
	
	@Override
	public void variablesAdded(final IValueVariable[] variables) {
		this.update= true;
	}
	
	@Override
	public void variablesChanged(final IValueVariable[] variables) {
	}
	
	@Override
	public void variablesRemoved(final IValueVariable[] variables) {
		this.update= true;
	}
	
	@Override
	public Iterator resolvers() {
		if (this.update) {
			updateResolvers();
		}
		return super.resolvers();
	}
	
	
	private void updateResolvers() {
		this.update= false;
		
		removeAllResolvers();
		
		final RSnippets snippets= RConsoleUIPlugin.getInstance().getRSnippets();
		final List<IStringVariable> variables= snippets.getVariables();
		for (final IStringVariable variable : variables) {
			addResolver(new VariableResolver(variable));
		}
	}
	
	@Override
	public void validate(final String pattern) throws TemplateException {
	}
	
}
