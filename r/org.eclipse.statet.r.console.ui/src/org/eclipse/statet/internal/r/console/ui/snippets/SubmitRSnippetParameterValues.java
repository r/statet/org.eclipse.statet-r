/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.snippets;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.IParameterValues;
import org.eclipse.jface.text.templates.Template;

import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;


public class SubmitRSnippetParameterValues implements IParameterValues {
	
	
	private final RSnippets snippets;
	
	
	public SubmitRSnippetParameterValues() {
		this.snippets= RConsoleUIPlugin.getInstance().getRSnippets();
	}
	
	
	@Override
	public Map getParameterValues() {
		final Template[] templates= this.snippets.getTemplateStore().getTemplates();
		
		final Map<String, String> parameters= new HashMap<>();
		for (final Template template : templates) {
			parameters.put(template.getDescription(), template.getName());
		}
		return parameters;
	}
	
}
