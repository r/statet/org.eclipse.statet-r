/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.snippets;

import java.util.List;
import java.util.Map;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.menus.IWorkbenchContribution;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.ecommons.ui.util.MessageUtils;

import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;


public class SubmitRSnippetsContributionItem extends CompoundContributionItem
		implements IWorkbenchContribution {
	
	
	private IServiceLocator serviceLocator;
	
	private final RSnippets snippets;
	
	
	public SubmitRSnippetsContributionItem() {
		this.snippets= RConsoleUIPlugin.getInstance().getRSnippets();
	}
	
	
	@Override
	public void initialize(final IServiceLocator serviceLocator) {
		this.serviceLocator= serviceLocator;
	}
	
	@Override
	protected IContributionItem[] getContributionItems() {
		final List<Template> filtered= this.snippets.validate(
				this.snippets.getTemplateStore().getTemplates() );
		
		IServiceLocator serviceLocator= this.serviceLocator;
		if (serviceLocator == null) {
			serviceLocator= PlatformUI.getWorkbench();
		}
		final IContributionItem[] items= new IContributionItem[filtered.size()];
		for (int i= 0; i < items.length; i++) {
			final Template template= filtered.get(i);
			String label= MessageUtils.escapeForMenu(template.getDescription());
			String mnemonic= null;
			if (i < 9) {
				mnemonic= Integer.toString(i + 1);
				label= mnemonic + ' ' + label;
			}
			items[i]= new CommandContributionItem(
					new CommandContributionItemParameter(serviceLocator,
							null, RSnippets.SUBMIT_SNIPPET_COMMAND_ID,
							Map.of(RSnippets.SNIPPET_PAR, template.getName()),
							null, null, null,
							label, mnemonic, null,
							CommandContributionItem.STYLE_PUSH, null, false ));
		}
		return items;
	}
	
}
