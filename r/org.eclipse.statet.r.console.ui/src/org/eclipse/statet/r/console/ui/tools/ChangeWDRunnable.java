/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.ui.tools;

import org.eclipse.core.filesystem.IFileStore;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.internal.r.console.ui.Messages;
import org.eclipse.statet.r.console.core.IRBasicAdapter;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.core.RUtil;


/**
 * ToolRunnable to change the working directory of R.
 * 
 * Supports path mapping (e.g. for remote console).
 */
public class ChangeWDRunnable implements ToolRunnable {
	
	public static final String TYPE_ID = "r/tools/changeWorkingDir"; //$NON-NLS-1$
	
	
	private final IFileStore workingDir;
	
	
	public ChangeWDRunnable(final IFileStore workingdir) {
		this.workingDir= workingdir;
	}
	
	
	@Override
	public String getTypeId() {
		return TYPE_ID;
	}
	
	@Override
	public boolean canRunIn(final Tool tool) {
		return (tool.isProvidingFeatureSet(RConsoleTool.R_BASIC_FEATURESET_ID));
	}
	
	@Override
	public String getLabel() {
		return Messages.ChangeWorkingDir_Task_label;
	}
	
	@Override
	public boolean changed(final int event, final Tool process) {
		return true;
	}
	
	@Override
	public void run(final ToolService service, final ProgressMonitor m) throws StatusException {
		final IRBasicAdapter r= (IRBasicAdapter) service;
		final String toolPath= r.getWorkspaceData().toToolPath(this.workingDir);
		
		try {
			final String command= "setwd(\"" + RUtil.escapeCompletely(toolPath) + "\")"; //$NON-NLS-1$ //$NON-NLS-2$
			r.submitToConsole(command, m);
		}
		finally {
			r.refreshWorkspaceData(0, m);
		}
	}
	
}
