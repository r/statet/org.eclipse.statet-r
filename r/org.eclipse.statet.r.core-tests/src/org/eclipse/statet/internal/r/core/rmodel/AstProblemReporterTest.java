/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.RSourceConstants;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public class AstProblemReporterTest {
	
	
	private final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	private final AstProblemReporter syntaxProblemReporter= new AstProblemReporter();
	
	
	public AstProblemReporterTest() {
	}
	
	
	@Test
	public void StringS_invalidText() {
		List<Problem> problems;
		
		problems= collectProblems("   'abc \\U{11F0F0} e' ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_CODEPOINT_INVALID,
				"Syntax Error/Invalid String: the specified code point U+11F0F0 is invalid.",
				8, 10,
				problems.get(0) );
	}
	
	
	@Test
	public void StringR_incompleteOpening() {
		List<Problem> problems;
		
		problems= collectProblems("   r\" + 2");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE,
				"Syntax Error/Invalid Character String: the opening delimiter for raw string literal is incomplete.",
				3, 2,
				problems.get(0) );
		
		// with dashes
		problems= collectProblems("   R\'---");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE,
				"Syntax Error/Invalid Character String: the opening delimiter for raw string literal is incomplete.",
				3, 5,
				problems.get(0) );
	}
	
	@Test
	public void StringR_notClosed() {
		List<Problem> problems;
		
		problems= collectProblems("   r\"(abc)");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				"Syntax Error/Unclosed Character String: the raw string literal › abc) ‹ is not closed; delimiter › )\" ‹ expected.",
				3, 7,
				problems.get(0) );
		
		// with dashes
		problems= collectProblems("   R\'---{abc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				"Syntax Error/Unclosed Character String: the raw string literal › abc ‹ is not closed; delimiter › }---' ‹ expected.",
				3, 9,
				problems.get(0) );
		
		// escaped cite
		problems= collectProblems("   R\'---{a\tbc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				"Syntax Error/Unclosed Character String: the raw string literal › a\\tbc ‹ is not closed; delimiter › }---' ‹ expected.",
				3, 10,
				problems.get(0) );
	}
	
	@Test
	public void StringR_invalidText() {
		List<Problem> problems;
		
		// null char
		problems= collectProblems("   r\"(ab\u0000c)\"  ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE123_SYNTAX_TEXT_NULLCHAR,
				"Syntax Error/Invalid String: null character is not allowed.",
				8, 1,
				problems.get(0) );
	}
	
	
	@Test
	public void FDef_Args_notOpened() {
		List<Problem> problems;
		
		problems= collectProblems("   function;\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_MISSING,
				"Syntax Error/Incomplete Statement: function argument list is missing, › ( ‹ expected.",
				3 + 8 - 1, 2,
				problems.get(0) );
		
		problems= collectProblems("function");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_MISSING,
				"Syntax Error/Incomplete Statement: function argument list is missing, › ( ‹ expected.",
				8 - 1, 1,
				problems.get(0) );
		
		problems= collectProblems("   \\   ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_MISSING,
				"Syntax Error/Incomplete Statement: function argument list is missing, › ( ‹ expected.",
				3, 2,
				problems.get(0) );
	}
	
	@Test
	public void FDef_Args_notClosed() {
		List<Problem> problems;
		
		problems= collectProblems("   function(x;\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED,
				"Syntax Error/Incomplete Statement: function argument list is not closed, › ) ‹ expected.",
				3 + 8 + 1, 2,
				problems.get(0) );
		
		problems= collectProblems("function(x");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED,
				"Syntax Error/Incomplete Statement: function argument list is not closed, › ) ‹ expected.",
				8 + 1, 1,
				problems.get(0) );
		
		problems= collectProblems("   \\(x   ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED,
				"Syntax Error/Incomplete Statement: function argument list is not closed, › ) ‹ expected.",
				3 + 1 + 1, 2,
				problems.get(0) );
	}
	
	@Test
	public void FDef_Body_missing() {
		List<Problem> problems;
		
		problems= collectProblems("   function(x);\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | RSourceConstants.CTX12_FDEF,
				"Syntax Error/Missing Body: an expression/block as function body is expected.",
				3 + 8 + 2, 2,
				problems.get(0) );
		
		problems= collectProblems("function(x)");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | RSourceConstants.CTX12_FDEF,
				"Syntax Error/Missing Body: an expression/block as function body is expected.",
				8 + 2, 1,
				problems.get(0) );
		
		problems= collectProblems("   \\(x)");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | RSourceConstants.CTX12_FDEF,
				"Syntax Error/Missing Body: an expression/block as function body is expected.",
				3 + 1 + 2, 1,
				problems.get(0) );
	}
	
	
	@Test
	public void Pipe_Source_missing() {
		List<Problem> problems;
		
		problems= collectProblems("   |> f();\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Missing Expression: an expression is expected before the operator.",
				3 - 1, 2,
				problems.get(0) );
		
		problems= collectProblems("|>");
		assertEquals(2, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Missing Expression: an expression is expected before the operator.",
				0, 1,
				problems.get(0) );
	}
	
	@Test
	public void Pipe_Target_missing() {
		List<Problem> problems;
		
		problems= collectProblems("   1 |>;\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Missing Call: a function call is expected after the pipe operator › |> ‹.",
				7 - 1, 1,
				problems.get(0) );
		
		problems= collectProblems("1 |>");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Missing Call: a function call is expected after the pipe operator › |> ‹.",
				4 - 1, 1,
				problems.get(0) );
	}
	
	@Test
	public void Pipe_Target_invalid() {
		List<Problem> problems;
		
		problems= collectProblems("   1 |> f;\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Unexpected Expression: a function call is expected after the pipe operator \u203A\u200A|>\u200A\u2039.",
				8, 1,
				problems.get(0) );
		
		problems= collectProblems("1 |> if (true)");
		assertTrue(1 < problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Unexpected Expression: a function call is expected after the pipe operator \u203A\u200A|>\u200A\u2039.",
				5, 9,
				problems.get(0) );
	}
	
	
	@Test
	public void pre_4_1() {
		this.rParser.setRSourceConfig(new RSourceConfig(RSourceConstants.LANG_VERSION_4_0));
		List<Problem> problems;
		
		problems= collectProblems("   \\(x) x");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | RSourceConstants.CTX12_FDEF,
				"Syntax Error/Unsupported Statement: shorthand notation for function definition requires R language version 4.1 or above.",
				3, 4,
				problems.get(0) );
		
		problems= collectProblems("   \\(x");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | RSourceConstants.CTX12_FDEF,
				"Syntax Error/Unsupported Statement: shorthand notation for function definition requires R language version 4.1 or above.",
				3, 3,
				problems.get(0) );
		
		problems= collectProblems("   1 |> f()");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Unsupported Statement: pipe operator › |> ‹ requires R language version 4.1 or above.",
				3, 8,
				problems.get(0) );
		
		problems= collectProblems("   1 |> f");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | RSourceConstants.CTX12_PIPE,
				"Syntax Error/Unsupported Statement: pipe operator › |> ‹ requires R language version 4.1 or above.",
				3, 6,
				problems.get(0) );
	}
	
	
	private List<Problem> collectProblems(final String source) {
		final RSourceUnit sourceUnit= SourceModelTests.createSourceUnit(source);
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput(sourceContent.getString()).init() );
		final List<Problem> collectedProblems= new ArrayList<>();
		this.syntaxProblemReporter.run(sourceComponent, sourceContent, new IssueRequestor() {
			@Override
			public boolean isInterestedInProblems(final String categoryId) {
				return true;
			}
			@Override
			public void acceptProblems(final String categoryId, final List<Problem> problems) {
				collectedProblems.addAll(problems);
			}
			@Override
			public void acceptProblems(final Problem problem) {
				collectedProblems.add(problem);
			}
			@Override
			public boolean isInterestedInTasks() {
				return false;
			}
			@Override
			public void acceptTask(final Task task) {
			}
			@Override
			public void finish() {
			}
		});
		return collectedProblems;
	}
	
	private void assertProblem(final int expectedSeverity, final int expectedCode, final String expectedMessage,
			final int expectedStartOffset, final int expectedLength,
			final Problem actual) {
		assertEquals(expectedSeverity, actual.getSeverity(), "severity");
		assertEquals(String.format("0x%1$08X", expectedCode), String.format("0x%1$08X", actual.getCode()), "code");
		assertEquals(expectedMessage, actual.getMessage(), "message");
		assertEquals(expectedStartOffset, actual.getSourceStartOffset(), "startOffset");
		assertEquals(expectedStartOffset + expectedLength, actual.getSourceEndOffset(), "endOffset");
	}
	
}
