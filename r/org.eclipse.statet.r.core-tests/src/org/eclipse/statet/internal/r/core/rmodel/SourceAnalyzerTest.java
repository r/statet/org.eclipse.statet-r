/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.internal.r.core.rmodel.SourceModelTests.assertElementName;
import static org.eclipse.statet.internal.r.core.rmodel.SourceModelTests.assertRegion;

import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.r.core.rmodel.RElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RLangPackageLoad;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public class SourceAnalyzerTest {
	
	
	private final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	private final SourceAnalyzer rSourceAnalyzer= new SourceAnalyzer();
	
	
	public SourceAnalyzerTest() {
	}
	
	
	@Test
	public void PackageLoad_base_require() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSourceElement> elements;
		
		modelInfo= createModel("require(MASS)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 13, elements.get(0));
		
		modelInfo= createModel("require(MASS, character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(0, elements.size());
		
		modelInfo= createModel("require(\"MASS\")\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 15, elements.get(0));
		
		modelInfo= createModel("require(\"MASS\", character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 37, elements.get(0));
	}
	
	@Test
	public void PackageLoad_base_library() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSourceElement> elements;
		
		modelInfo= createModel("library(MASS)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 13, elements.get(0));
		
		modelInfo= createModel("library(MASS, character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(0, elements.size());
		
		modelInfo= createModel("library(\"MASS\")\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 15, elements.get(0));
		
		modelInfo= createModel("library(\"MASS\", character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 37, elements.get(0));
	}
	
	
	protected RLangSourceElement assertRElement(final int expectedType,
			final int expectedStartOffset, final int expectedLength,
			final SourceStructElement<?, ?> actual) {
		assertEquals(RModel.R_TYPE_ID, actual.getModelTypeId());
		assertEquals(expectedType, (actual.getElementType() & LtkModelElement.MASK_C123));
		assertRegion(expectedStartOffset, expectedLength, actual.getSourceRange(), "sourceRange");
		assertTrue(actual instanceof RLangSourceElement);
		final RLangSourceElement rElement= (RLangSourceElement)actual;
		return rElement;
	}
	
	protected void assertPackageLoad(final int expectedStartOffset, final int expectedLength,
			final RLangSourceElement actual) {
		assertRElement(RElement.R_PACKAGE_LOAD,
				expectedStartOffset, expectedLength,
				actual );
		assertTrue(actual instanceof RLangPackageLoad);
		assertElementName(RElementName.SCOPE_PACKAGE, "MASS",
				actual.getElementName() );
	}
	
	
	protected RSourceUnitModelInfo createModel(final String code) {
		final RSourceUnit sourceUnit= SourceModelTests.createSourceUnit(code);
		
		final RSourceConfig rSourceConfig= RSourceConfig.DEFAULT_CONFIG;
		this.rParser.setRSourceConfig(rSourceConfig);
		
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput(sourceContent.getString()).init() );
		final var ast= new AstInfo(ModelManager.MODEL_FILE, new BasicSourceModelStamp(0),
				sourceComponent );
		
		final var model= this.rSourceAnalyzer.createModel(sourceUnit, ast);
		assertNotNull(model);
		return model;
	}
	
	
}
