/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.eclipse.jface.text.Document;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.r.core.input.BasicRSourceFragment;
import org.eclipse.statet.r.core.input.RSourceFragment;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFragmentSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.source.RSourceConfig;


@NonNullByDefault
public class SourceModelTests {
	
	
	public static RSourceUnit createSourceUnit(final String source) {
		final RSourceFragment fragment= new BasicRSourceFragment("test", "Test", "Test",
				RSourceConfig.DEFAULT_CONFIG,
				new Document(source) );
		return new RFragmentSourceUnit("test", fragment) {
			@Override
			public WorkingContext getWorkingContext() {
				return Ltk.PERSISTENCE_CONTEXT;
			}
			
		};
	}
	
	
	public static void assertRegion(final int expectedOffset, final int expectedLength,
			final @Nullable TextRegion actual, final String elementLabel) {
		assertNotNull(actual);
		assertEquals(expectedOffset, actual.getStartOffset(),
				() -> elementLabel + ".startOffset" );
		assertEquals(expectedOffset +  expectedLength, actual.getEndOffset(),
				() -> elementLabel + ".endOffset" );
	}
	
	public static void assertElementName(final int expectedType, final String expectedName,
			final RElementName actual) {
		assertNotNull(actual);
		assertEquals(expectedType, actual.getType());
		assertEquals(expectedName, actual.getSegmentName());
		assertNull(actual.getNextSegment());
	}
	
	
	private SourceModelTests() {
	}
	
}
