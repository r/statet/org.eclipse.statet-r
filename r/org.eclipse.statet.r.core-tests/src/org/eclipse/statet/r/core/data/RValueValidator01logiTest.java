/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.data;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;


public class RValueValidator01logiTest extends AbstractRValueValidatorTest {
	
	
	public RValueValidator01logiTest() {
		super(DefaultRObjectFactory.LOGI_STRUCT_DUMMY);
	}
	
	
	@Test
	@Override
	public void parseNum() {
		testValidate(true, "0");
		testValidate(true, "1");
		testValidate(false, "0x7FFFFFF");
		testValidate(false, "0x7FFFFFFFFL");
		testValidate(false, "-1.55");
		testValidate(false, "-1.0e-5");
		testValidate(false, "NA_real_");
	}
	
	
}
