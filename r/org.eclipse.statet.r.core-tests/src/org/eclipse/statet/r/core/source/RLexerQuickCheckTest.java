/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RLexerQuickCheckTest extends RLexerTest {
	
	
	public RLexerQuickCheckTest() {
	}
	
	
	@Override
	protected int getConfig() {
		return RLexer.ENABLE_QUICK_CHECK;
	}
	
	
}
