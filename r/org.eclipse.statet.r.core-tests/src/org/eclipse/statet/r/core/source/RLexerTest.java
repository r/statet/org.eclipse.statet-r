/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;


@NonNullByDefault
public class RLexerTest {
	
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RLexerTest() {
	}
	
	
	protected int getConfig() {
		return 0;
	}
	
	
	@Test
	public void empty() {
		final RLexer lexer= new RLexer(getConfig());
		lexer.reset(this.input.reset("").init());
		
		assertEquals(RTerminal.EOF, lexer.next());
		assertEquals(0, lexer.getOffset());
	}
	
	@Test
	public void matchWhitespace() {
		final RLexer lexer= new RLexer(getConfig());
		lexer.reset(this.input.reset(" \t").init());
		
		assertEquals(RTerminal.BLANK, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(0, lexer.getOffset());
		assertEquals(2, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void skipWhitespace() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset("  \t").init());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchLinebreak() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" \n ").init());
		
		assertEquals(RTerminal.LINEBREAK, lexer.next());
		assertEquals(1, lexer.getOffset());
		assertEquals(1, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchLinebreakWin() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" \r\n ").init());
		
		assertEquals(RTerminal.LINEBREAK, lexer.next());
		assertEquals(1, lexer.getOffset());
		assertEquals(2, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchComment() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset("  # comment").init());
		
		assertEquals(RTerminal.COMMENT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(2, lexer.getOffset());
		assertEquals(9, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchComment_Linebreak() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset("  # comment\n  ").init());
		
		assertEquals(RTerminal.COMMENT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(2, lexer.getOffset());
		assertEquals(9, lexer.getLength());
		assertEquals(RTerminal.LINEBREAK, lexer.next());
		assertEquals(2 + 9, lexer.getOffset());
		assertEquals(1, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchRoxygenComment() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" #' comment").init());
		
		assertEquals(RTerminal.ROXYGEN_COMMENT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(10, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void skipComment() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE | RLexer.SKIP_COMMENT);
		lexer.reset(this.input.reset("  # comment\n  ").init());
		
		assertEquals(RTerminal.LINEBREAK, lexer.next());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset("  #' comment").init());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
	@Test
	public void matchBlockOpen() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" { ").init());
		
		assertEquals(RTerminal.BLOCK_OPEN, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_BLOCK_OPEN.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchBlockClose() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" } ").init());
		
		assertEquals(RTerminal.BLOCK_CLOSE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_BLOCK_CLOSE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchGroupOpen() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ( ").init());
		
		assertEquals(RTerminal.GROUP_OPEN, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_GROUP_OPEN.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchGroupClose() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ) ").init());
		
		assertEquals(RTerminal.GROUP_CLOSE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_GROUP_CLOSE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSubIndexedSOpen() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" [ ").init());
		
		assertEquals(RTerminal.SUB_INDEXED_S_OPEN, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_SUB_INDEXED_OPEN.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSubIndexedDOpen() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" [[ ").init());
		
		assertEquals(RTerminal.SUB_INDEXED_D_OPEN, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_SUB_INDEXED_D_OPEN.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSubIndexedClose() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ] ").init());
		
		assertEquals(RTerminal.SUB_INDEXED_CLOSE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_SUB_INDEXED_CLOSE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSubNamedPart() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" $ ").init());
		
		assertEquals(RTerminal.SUB_NAMED_PART, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_SUB_NAMED.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSubSlotPart() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" @ ").init());
		
		assertEquals(RTerminal.SUB_NAMED_SLOT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_SUB_AT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNsGet() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" :: ").init());
		
		assertEquals(RTerminal.NS_GET, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NS_GET.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNsGetInt() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ::: ").init());
		
		assertEquals(RTerminal.NS_GET_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NS_GET_INT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchPlus() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" + ").init());
		
		assertEquals(RTerminal.PLUS, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_PLUS.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchMinus() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" - ").init());
		
		assertEquals(RTerminal.MINUS, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_MINUS.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchMult() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" * ").init());
		
		assertEquals(RTerminal.MULT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_MULT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchDiv() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" / ").init());
		
		assertEquals(RTerminal.DIV, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_DIV.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchOr() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" | ").init());
		
		assertEquals(RTerminal.OR, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_OR.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchOrD() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" || ").init());
		
		assertEquals(RTerminal.OR_D, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_OR_D.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchAnd() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" & ").init());
		
		assertEquals(RTerminal.AND, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_AND.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchAndD() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" && ").init());
		
		assertEquals(RTerminal.AND_D, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_AND_D.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNot() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ! ").init());
		
		assertEquals(RTerminal.NOT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NOT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchPower() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ^ ").init());
		
		assertEquals(RTerminal.POWER, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_POWER.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSeq() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" : ").init());
		
		assertEquals(RTerminal.SEQ, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_COLON.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSpecial() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" %/% ").init());
		
		assertEquals(RTerminal.SPECIAL, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(3, lexer.getLength());
		assertEquals("/", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSpecial_empty() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" %% ").init());
		
		assertEquals(RTerminal.SPECIAL, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(2, lexer.getLength());
		assertEquals("", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSpecial_notClosed() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" %/ ").init());
		
		assertEquals(RTerminal.SPECIAL, lexer.next());
		assertEquals(TYPE12_SYNTAX_TOKEN_NOT_CLOSED, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(3, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" %/ \n%").init());
		
		assertEquals(RTerminal.SPECIAL, lexer.next());
		assertEquals(TYPE12_SYNTAX_TOKEN_NOT_CLOSED, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(3, lexer.getLength());
		
		assertEquals(RTerminal.LINEBREAK, lexer.next());
	}
	
	@Test
	public void matchQuestionmark() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ? ").init());
		
		assertEquals(RTerminal.QUESTIONMARK, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_QUESTIONMARK.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchComma() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" , ").init());
		
		assertEquals(RTerminal.COMMA, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_COMMA.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSemicolon() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ; ").init());
		
		assertEquals(RTerminal.SEMICOLON, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_SEMICOLON.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchPipeRight() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" |> ").init());
		
		assertEquals(RTerminal.PIPE_RIGHT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_PIPE_RIGHT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchArrowLeftS() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" <- ").init());
		
		assertEquals(RTerminal.ARROW_LEFT_S, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_ARROW_LEFT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchArrowLeftD() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" <<- ").init());
		
		assertEquals(RTerminal.ARROW_LEFT_D, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_ARROW_LEFT_D.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchArrowRightS() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" -> ").init());
		
		assertEquals(RTerminal.ARROW_RIGHT_S, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_ARROW_RIGHT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchArrowRightD() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ->> ").init());
		
		assertEquals(RTerminal.ARROW_RIGHT_D, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_ARROW_RIGHT_D.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchEqual() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" = ").init());
		
		assertEquals(RTerminal.EQUAL, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_EQUAL.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchColonEqual() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE | RLexer.ENABLE_COLON_EQUAL);
		lexer.reset(this.input.reset(" := ").init());
		
		assertEquals(RTerminal.COLON_EQUAL, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(2, lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchColonEqual_disabled() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" := ").init());
		
		assertEquals(RTerminal.SEQ, lexer.next());
		assertEquals(RTerminal.EQUAL, lexer.next());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchTilde() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" ~ ").init());
		
		assertEquals(RTerminal.TILDE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_TILDE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchRelNE() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" != ").init());
		
		assertEquals(RTerminal.REL_NE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_REL_NE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchRelEQ() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" == ").init());
		
		assertEquals(RTerminal.REL_EQ, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_REL_EQ.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchRelLT() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" < ").init());
		
		assertEquals(RTerminal.REL_LT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_REL_LT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchRelLE() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" <= ").init());
		
		assertEquals(RTerminal.REL_LE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_REL_LE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchRelGT() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" > ").init());
		
		assertEquals(RTerminal.REL_GT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_REL_GT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchRelGE() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" >= ").init());
		
		assertEquals(RTerminal.REL_GE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_REL_GE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
	@Test
	public void matchKey_if() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" if ").init());
		
		assertEquals(RTerminal.IF, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_IF.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_else() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" else ").init());
		
		assertEquals(RTerminal.ELSE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_ELSE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_for() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" for ").init());
		
		assertEquals(RTerminal.FOR, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_FOR.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_in() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" in ").init());
		
		assertEquals(RTerminal.IN, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_IN.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_while() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" while ").init());
		
		assertEquals(RTerminal.WHILE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_WHILE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_repeat() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" repeat ").init());
		
		assertEquals(RTerminal.REPEAT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_REPEAT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_next() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" next ").init());
		
		assertEquals(RTerminal.NEXT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NEXT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_break() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" break ").init());
		
		assertEquals(RTerminal.BREAK, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_BREAK.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_function() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" function ").init());
		
		assertEquals(RTerminal.FUNCTION, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_FUNCTION.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_function_B() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" \\ ").init());
		
		assertEquals(RTerminal.FUNCTION_B, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_BACKSLASH.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_TRUE() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" TRUE ").init());
		
		assertEquals(RTerminal.TRUE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_TRUE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_FALSE() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" FALSE ").init());
		
		assertEquals(RTerminal.FALSE, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_FALSE.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_NA() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NA ").init());
		
		assertEquals(RTerminal.NA, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NA.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_NA_real() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NA_real_ ").init());
		
		assertEquals(RTerminal.NA_REAL, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NA_REAL.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_NA_integer() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NA_integer_ ").init());
		
		assertEquals(RTerminal.NA_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NA_INT.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_NA_complex() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NA_complex_ ").init());
		
		assertEquals(RTerminal.NA_CPLX, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NA_CPLX.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_NA_character() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NA_character_ ").init());
		
		assertEquals(RTerminal.NA_CHAR, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NA_CHAR.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_NULL() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NULL ").init());
		
		assertEquals(RTerminal.NULL, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NULL.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_NaN() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NaN ").init());
		
		assertEquals(RTerminal.NAN, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_NAN.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchKey_Inf() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" Inf ").init());
		
		assertEquals(RTerminal.INF, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(1, lexer.getOffset());
		assertEquals(RTerminal.S_INF.length(), lexer.getLength());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
}
