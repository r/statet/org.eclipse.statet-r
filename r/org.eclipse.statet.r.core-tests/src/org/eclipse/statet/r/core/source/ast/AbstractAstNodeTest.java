/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.r.core.source.ast.RAstTests.SYMBOL_STD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.assertExpr0Node;
import static org.eclipse.statet.r.core.source.ast.RAstTests.assertNode;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.r.core.source.ast.RAstTests.NodeDescr;


@NonNullByDefault
public abstract class AbstractAstNodeTest {
	
	
	protected final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	protected final StringParserInput input= new StringParserInput();
	
	
	public AbstractAstNodeTest() {
	}
	
	
	protected RAstNode assertExpr(final String code,
			final int startOffset, final int endOffset,
			final NodeDescr nodeDescr) {
		final SourceComponent root= this.rParser.parseSourceFragment(this.input.reset(code).init(),
				null, false );
		assertEquals(1, root.getChildCount());
		assertEquals(startOffset, root.getStartOffset(), "root.startOffset");
		assertEquals(endOffset, root.getEndOffset(), "root.endOffset");
		
		final RAstNode expr0= root.getChild(0);
		assertNode(startOffset, endOffset, nodeDescr, expr0, "expr[0]");
		return expr0;
	}
	
	protected RAstNode assertExpr(final String code,
			final int startOffset, final int endOffset,
			final NodeDescr nodeDescr, final int statusCode, final @Nullable String text) {
		final SourceComponent root= this.rParser.parseSourceFragment(this.input.reset(code).init(),
				null, false );
		assertEquals(1, root.getChildCount());
		assertEquals(startOffset, root.getStartOffset(), "root.startOffset");
		assertEquals(endOffset, root.getEndOffset(), "root.endOffset");
		
		final RAstNode expr0= root.getChild(0);
		assertNode(startOffset, endOffset, nodeDescr, statusCode, text, expr0, "expr[0]");
		return expr0;
	}
	
	protected void assertTwoOps_differentPrio(final NodeDescr lowDescr, final NodeDescr highDescr) {
		RAstNode expr0;
		final String ls= lowDescr.op0.text;
		final int ll= lowDescr.op0.text.length();
		final String hs= highDescr.op0.text;
		final int hl= highDescr.op0.text.length();
		
		expr0= assertExpr("r " + ls + " x " + hs + " y",
						 0,            7 + ll + hl, lowDescr);
		assertExpr0Node( 0,            1,           SYMBOL_STD,		expr0, new int[] { 0 });
		assertExpr0Node( 3 + ll,       7 + ll + hl, highDescr,		expr0, new int[] { 1 });
		assertExpr0Node( 3 + ll,       4 + ll,      SYMBOL_STD,		expr0, new int[] { 1, 0 });
		assertExpr0Node( 6 + ll + hl,  7 + ll + hl, SYMBOL_STD,		expr0, new int[] { 1, 1 });
		
		expr0= assertExpr("r " + ls + " x " + hs + " y[z]",
						 0,           10 + ll + hl, lowDescr);
		assertExpr0Node( 0,            1,           SYMBOL_STD,		expr0, new int[] { 0 });
		assertExpr0Node( 3 + ll,      10 + ll + hl, highDescr,		expr0, new int[] { 1 });
		
		expr0= assertExpr("r " + ls + " x[y] " + hs + " z",
						 0,           10 + ll + hl, lowDescr);
		assertExpr0Node( 0,            1,           SYMBOL_STD,		expr0, new int[] { 0 });
		assertExpr0Node( 3 + ll,      10 + ll + hl, highDescr,		expr0, new int[] { 1 });
		
		expr0= assertExpr("r " + hs + " s " + ls + " x",
						 0,            7 + hl + ll, lowDescr);
		assertExpr0Node( 0,            4 + hl,      highDescr,		expr0, new int[] { 0 });
		assertExpr0Node( 0,            1,           SYMBOL_STD,		expr0, new int[] { 0, 0 });
		assertExpr0Node( 3 + hl,       4 + hl,      SYMBOL_STD,		expr0, new int[] { 0, 1 });
		assertExpr0Node( 6 + hl + ll,  7 + hl + ll, SYMBOL_STD,		expr0, new int[] { 1 });
		
		expr0= assertExpr("r " + hs + " s[t] " + ls + " x",
						 0,           10 + hl + ll, lowDescr);
		assertExpr0Node( 0,            7 + hl,      highDescr,		expr0, new int[] { 0 });
		assertExpr0Node( 9 + hl + ll, 10 + hl + ll, SYMBOL_STD,		expr0, new int[] { 1 });
	}
	
}
