/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.collections.ImCollections.newIntList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newList;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * Tests for
 *   {@link RAsts#computeRExpressionIndex(RAstNode, RAstNode)}
 *   {@link RAsts#computeRExpressionNodes(RAstNode, RAstNode)}}
 */
@NonNullByDefault
public class RExpressionComputingTest {
	
	
	private final RParser rParser= new RParser(AstInfo.LEVEL_MODEL_DEFAULT);
	
	
	public RExpressionComputingTest() {
	}
	
	
	@Test
	public void SourceComponent() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y + 3; fname(a= 1, 'B')").init());
		RAstNode child;
		
		child= sourceComponent.getChild(0);
		assertEquals(child.getNodeType(), NodeType.A_LEFT);
		assertRExpressionEquals(newIntList(	1),
								newList(	child),
				child, sourceComponent );
		
		child= sourceComponent.getChild(1);
		assertEquals(child.getNodeType(), NodeType.F_CALL);
		assertRExpressionEquals(newIntList(	2),
								newList(	child),
				child, sourceComponent );
	}
	
	@Test
	public void Block() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("{ x <- y + 3; fname(a= 1, 'B') }").init());
		final Block block= (Block)sourceComponent.getChild(0);
		RAstNode child;
		
		child= block.getChild(0);
		assertEquals(child.getNodeType(), NodeType.A_LEFT);
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	block,	child),
				child, sourceComponent );
		
		child= block.getChild(1);
		assertEquals(child.getNodeType(), NodeType.F_CALL);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	block,	child),
				child, sourceComponent );
	}
	
	@Test
	public void Group() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- (y + 3); fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Group group= (Group)assign.getSourceChild();
		assertTrue(group.getNodeType() == NodeType.GROUP);
		RAstNode child;
		
		// parse(text="x <- (y + 3); fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= group.getChild(0);
		assertEquals(child.getNodeType(), NodeType.ADD);
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	group,	child),
				child, sourceComponent );
	}
	
	
	static Stream<RTerminal> Assign_ALeft_parameters() {
		return Stream.of(RTerminal.ARROW_LEFT_S, RTerminal.ARROW_LEFT_D);
	}
	
	@ParameterizedTest
	@MethodSource("Assign_ALeft_parameters")
	public void Assign_ALeft(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x " + terminal.text + " y + 3; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		assertTrue(assign.getNodeType() == NodeType.A_LEFT
				&& assign.getOperator(0) == terminal );
		RAstNode child;
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[1]][[2]]
		child= assign.getChild(0);
		assertSymbol(child, "x");
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	assign,	child),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[1]][[3]]
		child= assign.getChild(1);
		assertEquals(child.getNodeType(), NodeType.ADD);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	assign,	child),
				child, sourceComponent );
	}
	
	static Stream<RTerminal> Assign_ARight_parameters() {
		return Stream.of(RTerminal.ARROW_RIGHT_S, RTerminal.ARROW_RIGHT_D);
	}
	
	@ParameterizedTest
	@MethodSource("Assign_ARight_parameters")
	public void Assign_ARight(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("y + 3 " + terminal.text + " x; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		assertTrue(assign.getNodeType() == NodeType.A_RIGHT
				&& assign.getOperator(0) == terminal );
		RAstNode child;
		
		// parse(text="y + 3 -> x; fname(a= 1, 'B')")[[1]][[3]]
		child= assign.getChild(0);
		assertEquals(child.getNodeType(), NodeType.ADD);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	assign,	child),
				child, sourceComponent );
		
		// parse(text="y + 3 -> x; fname(a= 1, 'B')")[[1]][[2]]
		child= assign.getChild(1);
		assertSymbol(child, "x");
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	assign,	child),
				child, sourceComponent );
	}
	
	
	static Stream<RTerminal> NsGet_parameters() {
		return Stream.of(RTerminal.NS_GET, RTerminal.NS_GET_INT);
	}
	
	@ParameterizedTest
	@MethodSource("NsGet_parameters")
	public void NsGet(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("pkg" + terminal.text + "fname").init());
		final NSGet qual= (NSGet)sourceComponent.getChild(0);
		assertTrue(qual.getOperator(0) == terminal );
		RAstNode child;
		
		// parse(text="pkg::fname")[[1]][[2]]
		child= qual.getChild(0);
		assertSymbol(child, "pkg");
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	qual,	child),
				child, sourceComponent );
		
		// parse(text="pkg::fname")[[1]][[3]]
		child= qual.getChild(1);
		assertSymbol(child, "fname");
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	qual,	child),
				child, sourceComponent );
	}
	
	
	static Stream<RTerminal> SubNamed_parameters() {
		return Stream.of(RTerminal.SUB_NAMED_PART, RTerminal.SUB_NAMED_SLOT);
	}
	
	@ParameterizedTest
	@MethodSource("SubNamed_parameters")
	public void SubNamed(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("obj" + terminal.text + "item").init());
		final SubNamed sub= (SubNamed)sourceComponent.getChild(0);
		assertTrue(sub.getOperator(0) == terminal );
		RAstNode child;
		
		// parse(text="obj$item")[[1]][[2]]
		child= sub.getChild(0);
		assertSymbol(child, "obj");
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	sub,	child),
				child, sourceComponent );
		
		// parse(text="obj$item")[[1]][[3]]
		child= sub.getChild(1);
		assertSymbol(child, "item");
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	sub,	child),
				child, sourceComponent );
	}
	
	
	static Stream<RTerminal> Power_parameters() {
		return Stream.of(RTerminal.POWER);
	}
	
	@ParameterizedTest
	@MethodSource("Power_parameters")
	public void Power(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y " + terminal.text + " 3; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Power op= (Power)assign.getSourceChild();
		assertTrue(op.getNodeType() == NodeType.POWER
				&& op.getOperator(0) == terminal );
		RAstNode child;
		
		// parse(text="x <- y ^ 3; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= op.getChild(0);
		assertSymbol(child, "y");
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	op,		child),
				child, sourceComponent );
		
		// parse(text="x <- y ^ 3; fname(a= 1, 'B')")[[1]][[3]][[3]]
		child= op.getChild(1);
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		3),
								newList(	assign,	op,		child),
				child, sourceComponent );
	}
	
	static Stream<RTerminal> Sign_parameters() {
		return Stream.of(RTerminal.PLUS, RTerminal.MINUS);
	}
	
	@ParameterizedTest
	@MethodSource("Sign_parameters")
	public void Sign(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- " + terminal.text + " y; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Sign op= (Sign)assign.getSourceChild();
		assertTrue(op.getOperator(0) == terminal);
		RAstNode child;
		
		// parse(text="x <- + y; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= op.getChild(0);
		assertSymbol(child, "y");
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	op,		child),
				child, sourceComponent );
	}
	
	@Test
	public void Seq() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- 1:y; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Seq op= (Seq)assign.getSourceChild();
		assertTrue(op.getOperator(0) == RTerminal.SEQ);
		RAstNode child;
		
		// parse(text="x <- 1:y; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= op.getChild(0);
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	op,		child),
				child, sourceComponent );
		
		child= op.getChild(1);
		assertSymbol(child, "y");
		assertRExpressionEquals(newIntList(	1,		3,		3),
				newList(	assign,	op,		child),
				child, sourceComponent );
	}
	
	static Stream<RTerminal> Arithmetic_parameters() {
		return Stream.of(RTerminal.PLUS, RTerminal.MINUS, RTerminal.MULT, RTerminal.DIV);
	}
	
	@ParameterizedTest
	@MethodSource("Arithmetic_parameters")
	public void Arithmetic(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y " + terminal.text + " 3; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Arithmetic op= (Arithmetic)assign.getSourceChild();
		assertTrue(op.getOperator(0) == terminal);
		RAstNode child;
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= op.getChild(0);
		assertSymbol(child, "y");
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	op,		child),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[1]][[3]][[3]]
		child= op.getChild(1);
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		3),
								newList(	assign,	op,		child),
				child, sourceComponent );
	}
	
	
	static Stream<RTerminal> Logical_parameters() {
		return Stream.of(RTerminal.AND, RTerminal.AND_D, RTerminal.OR, RTerminal.OR_D);
	}
	
	@ParameterizedTest
	@MethodSource("Logical_parameters")
	public void Logical(final RTerminal terminal) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y " + terminal.text + " 3; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Logical op= (Logical)assign.getSourceChild();
		assertTrue(op.getOperator(0) == terminal);
		RAstNode child;
		
		// parse(text="x <- y & 3; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= op.getChild(0);
		assertSymbol(child, "y");
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	op,		child),
				child, sourceComponent );
		
		// parse(text="x <- y & 3; fname(a= 1, 'B')")[[1]][[3]][[3]]
		child= op.getChild(1);
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		3),
								newList(	assign,	op,		child),
				child, sourceComponent );
	}
	
	@Test
	public void Not() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- !(y || z == 3)").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Sign op= (Sign)assign.getSourceChild();
		assertTrue(op.getOperator(0) == RTerminal.NOT);
		RAstNode child;
		
		// parse(text="x <- !(y || z == 3)")[[1]][[3]][[2]]
		child= op.getChild(0);
		assertEquals(child.getNodeType(), NodeType.GROUP);
		assertRExpressionEquals(newIntList(	1,		3,		2),
				newList(	assign,	op,		child),
				child, sourceComponent );
	}
	
	
	@Test
	public void Special() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y %/% 3; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final Special op= (Special)assign.getSourceChild();
		assertTrue(op.getOperator(0) == RTerminal.SPECIAL);
		RAstNode child;
		
		// parse(text="x <- y %/% 3; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= op.getChild(0);
		assertSymbol(child, "y");
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	op,		child),
				child, sourceComponent );
		
		// parse(text="x <- y %/% 3; fname(a= 1, 'B')")[[1]][[3]][[3]]
		child= op.getChild(1);
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		3),
								newList(	assign,	op,		child),
				child, sourceComponent );
	}
	
	
	@Test
	public void CIf() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("if (x == y) a").init());
		final CIfElse c= (CIfElse)sourceComponent.getChild(0);
		RAstNode child;
		
		// parse(text="if (x == y) a")[[1]][[2]]
		child= c.getChild(0);
		assertEquals(child.getNodeType(), NodeType.RELATIONAL);
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	c,		child),
				child, sourceComponent );
		
		// parse(text="if (x == y) a")[[1]][[3]]
		child= c.getChild(1);
		assertSymbol(child, "a");
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	c,		child),
				child, sourceComponent );
	}
	
	@Test
	public void CIf_withElse() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("if (x == y) a else b").init());
		final CIfElse c= (CIfElse)sourceComponent.getChild(0);
		RAstNode child;
		
		// parse(text="if (x == y) a else b")[[1]][[2]]
		child= c.getChild(0);
		assertEquals(child.getNodeType(), NodeType.RELATIONAL);
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	c,		child),
				child, sourceComponent );
		
		// parse(text="if (x == y) a else b")[[1]][[3]]
		child= c.getChild(1);
		assertSymbol(child, "a");
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	c,		child),
				child, sourceComponent );
		
		// parse(text="if (x == y) a else b")[[1]][[4]]
		child= c.getChild(2);
		assertSymbol(child, "b");
		assertRExpressionEquals(newIntList(	1,		4),
								newList(	c,		child),
				child, sourceComponent );
	}
	
	@Test
	public void CFor() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("for (i in 1:10) add(i)").init());
		final CForLoop c= (CForLoop)sourceComponent.getChild(0);
		RAstNode child;
		
		// parse(text="for (i in 1:10) add(i)")[[1]][[2]]
		child= c.getChild(0);
		assertSymbol(child, "i");
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	c,		child),
				child, sourceComponent );
		
		// parse(text="for (i in 1:10) add(i)")[[1]][[3]]
		child= c.getChild(1);
		assertEquals(child.getNodeType(), NodeType.SEQ);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	c,		child),
				child, sourceComponent );
	}
	
	@Test
	public void CWhile() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("while (x < 10) do()").init());
		final CWhileLoop c= (CWhileLoop)sourceComponent.getChild(0);
		RAstNode child;
		
		// parse(text="while (x < 10) do()")[[1]][[2]]
		child= c.getChild(0);
		assertEquals(child.getNodeType(), NodeType.RELATIONAL);
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	c,		child),
				child, sourceComponent );
		
		// parse(text="while (x < 10) do()")[[1]][[3]]
		child= c.getChild(1);
		assertEquals(child.getNodeType(), NodeType.F_CALL);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	c,		child),
				child, sourceComponent );
	}
	
	@Test
	public void CRepeat() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("repeat { do() }").init());
		final CRepeatLoop c= (CRepeatLoop)sourceComponent.getChild(0);
		RAstNode child;
		
		// parse(text="repeat { do() }")[[1]][[2]]
		child= c.getChild(0);
		assertEquals(child.getNodeType(), NodeType.BLOCK);
		assertRExpressionEquals(newIntList(	1,		2),
								newList(	c,		child),
				child, sourceComponent );
	}
	
	
	@Test
	public void FDef() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final FDef fDef= (FDef)assign.getSourceChild();
		assertTrue(fDef.getNodeType() == NodeType.F_DEF);
		RAstNode child;
		
		// parse(text="fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= fDef.getChild(0);
		assertEquals(child.getNodeType(), NodeType.F_DEF_ARGS);
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	fDef,	child),
				child, sourceComponent );
		
		// parse(text="fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')")[[1]][[3]][[3]]
		child= fDef.getChild(1);
		assertEquals(child.getNodeType(), NodeType.BLOCK);
		assertRExpressionEquals(newIntList(	1,		3,		3),
								newList(	assign,	fDef,	child),
				child, sourceComponent );
	}
	
	@Test
	public void FDef_Args() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final FDef fDef= (FDef)assign.getSourceChild();
		final FDef.Args args= fDef.getArgsChild();
		assertTrue(args.getNodeType() == NodeType.F_DEF_ARGS);
		RAstNode child;
		
		// parse(text="fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= args.getChild(0);
		assertEquals(child.getNodeType(), NodeType.F_DEF_ARG);
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	fDef,	args),
				child, sourceComponent );
		
		// parse(text="fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= args.getChild(1);
		assertEquals(child.getNodeType(), NodeType.F_DEF_ARG);
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	fDef,	args),
				child, sourceComponent );
	}
	
	@Test
	public void FDef_Arg() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')").init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final FDef fDef= (FDef)assign.getSourceChild();
		final FDef.Args args= fDef.getArgsChild();
		RAstNode child;
		
		// parse(text="fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= args.getChild(0).getNameChild();
		assertEquals(child.getNodeType(), NodeType.SYMBOL);
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	fDef,	args),
				child, sourceComponent );
		
		// parse(text="fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')")[[1]][[3]][[2]][[1]]
		child= args.getChild(0).getDefaultChild();
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		2,		1),
								newList(	assign,	fDef,	args,	child),
				child, sourceComponent );
		
		// parse(text="fname <- function(a= 0, b) { a + 3 }; fname(a= 1, 'B')")[[1]][[3]][[2]]
		child= args.getChild(1).getNameChild();
		assertEquals(child.getNodeType(), NodeType.SYMBOL);
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	fDef,	args),
				child, sourceComponent );
	}
	
	
	@Test
	public void FCall() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y + 3; fname(a= 1, 'B')").init());
		final FCall fCall= (FCall)sourceComponent.getChild(1);
		RAstNode child;
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[2]][[1]]
		child= fCall.getChild(0);
		assertSymbol(child, "fname");
		assertRExpressionEquals(newIntList(	2,		1),
								newList(	fCall,	child),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[2]]
		child= fCall.getChild(1);
		assertEquals(child.getNodeType(), NodeType.F_CALL_ARGS);
		assertRExpressionEquals(newIntList(	2),
								newList(	fCall),
				child, sourceComponent );
	}
	
	@Test
	public void FCall_Args() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y + 3; fname(a= 1, 'B')").init());
		final FCall fCall= (FCall)sourceComponent.getChild(1);
		final FCall.Args args= fCall.getArgsChild();
		RAstNode child;
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[2]]
		child= args.getChild(0);
		assertEquals(child.getNodeType(), NodeType.F_CALL_ARG);
		assertRExpressionEquals(newIntList(	2),
								newList(	fCall),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[2]]
		child= args.getChild(1);
		assertEquals(child.getNodeType(), NodeType.F_CALL_ARG);
		assertRExpressionEquals(newIntList(	2),
								newList(	fCall),
				child, sourceComponent );
	}
	
	@Test
	public void FCall_Arg() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y + 3; fname(a= 1, 'B')").init());
		final FCall fCall= (FCall)sourceComponent.getChild(1);
		final FCall.Args args= fCall.getArgsChild();
		RAstNode child;
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[2]]
		child= args.getChild(0).getNameChild();
		assertSymbol(child, "a");
		assertRExpressionEquals(newIntList(	2),
								newList(	fCall),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[2]][[2]]
		child= args.getChild(0).getValueChild();
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	2,		2),
								newList(	fCall,	child),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; fname(a= 1, 'B')")[[2]][[3]]
		child= args.getChild(1).getValueChild();
		assertEquals(child.getNodeType(), NodeType.STRING_CONST);
		assertRExpressionEquals(newIntList(	2,		3),
								newList(	fCall,	child),
				child, sourceComponent );
	}
	
	@Test
	public void PipeForward() {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y + 3; x |> fname('B')").init());
		final Pipe pipe= (Pipe)sourceComponent.getChild(1);
		final FCall fCall= (FCall)pipe.getChild(1);
		final FCall.Args args= fCall.getArgsChild();
		RAstNode child;
		
		// parse(text="x <- y + 3; x |> fname('B')")[[2]]
		assertRExpressionEquals(newIntList(	2),
								newList(	pipe),
				pipe, sourceComponent );
		
		// parse(text="x <- y + 3; x |> fname('B')")[[2]][[2]]
		child= pipe.getChild(0);
		assertSymbol(child, "x");
		assertRExpressionEquals(newIntList(	2,		2),
								newList(	pipe,	child),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; x |> fname('B')")[[2]]
		child= fCall;
		assertRExpressionEquals(newIntList(	2),
								newList(	pipe),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; x |> fname('B')")[[2]]
		child= args;
		assertRExpressionEquals(newIntList(	2),
				newList(	pipe),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; x |> fname()")[[2]]
		child= args;
		assertRExpressionEquals(newIntList(	2),
								newList(	pipe),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; x |> fname('B')")[[2]]
		child= args.getChild(0);
		assertRExpressionEquals(newIntList(	2),
								newList(	pipe),
				child, sourceComponent );
		
		// parse(text="x <- y + 3; x |> fname('B')")[[2]][[3]]
		child= args.getChild(0).getValueChild();
		assertEquals(child.getNodeType(), NodeType.STRING_CONST);
		assertRExpressionEquals(newIntList(	2,		3),
								newList(	pipe,	child),
				child, sourceComponent );
	}
	
	
	static Stream<Arguments> SubIndexed_parameters() {
		return Stream.of(
				Arguments.of(NodeType.SUB_INDEXED_S, "[", "]"),
				Arguments.of(NodeType.SUB_INDEXED_D, "[[", "]]") );
	}
	
	@ParameterizedTest
	@MethodSource("SubIndexed_parameters")
	public void SubIndexed(final NodeType nodeType, final String open, final String close) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y" + open + "a= 1, 'B'" + close).init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final SubIndexed sub= (SubIndexed)assign.getSourceChild();
		assertTrue(sub.getNodeType() == nodeType);
		RAstNode child;
		
		// parse(text="x <- y[a= 1, 'B']")[[1]][[3]][[2]]
		child= sub.getChild(0);
		assertEquals(child.getNodeType(), NodeType.SYMBOL);
		assertArrayEquals(new int[] { 1, 3, 2 },
				RAsts.computeRExpressionIndex(child, sourceComponent) );
		assertRExpressionEquals(newIntList(	1,		3,		2),
								newList(	assign,	sub,	child),
				child, sourceComponent );
		
		// parse(text="x <- y[a= 1, 'B']")[[1]][[3]]
		child= sub.getChild(1);
		assertEquals(child.getNodeType(), NodeType.SUB_INDEXED_ARGS);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	assign,	sub),
				child, sourceComponent );
	}
	
	@ParameterizedTest
	@MethodSource("SubIndexed_parameters")
	public void SubIndexed_Args(final NodeType nodeType, final String open, final String close) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y" + open + "a= 1, 'B'" + close).init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final SubIndexed sub= (SubIndexed)assign.getSourceChild();
		final SubIndexed.Args args= sub.getArgsChild();
		RAstNode child;
		
		// parse(text="x <- y[a= 1, 'B']")[[1]][[3]]
		child= args.getChild(0);
		assertEquals(child.getNodeType(), NodeType.SUB_INDEXED_ARG);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	assign,	sub),
				child, sourceComponent );
		
		// parse(text="x <- y[a= 1, 'B']")[[1]][[3]]
		child= args.getChild(1);
		assertEquals(child.getNodeType(), NodeType.SUB_INDEXED_ARG);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	assign,	sub),
				child, sourceComponent );
	}
	
	@ParameterizedTest
	@MethodSource("SubIndexed_parameters")
	public void SubIndexed_Arg(final NodeType nodeType, final String open, final String close) {
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput("x <- y" + open + "a= 1, 'B'" + close).init());
		final Assignment assign= (Assignment)sourceComponent.getChild(0);
		final SubIndexed sub= (SubIndexed)assign.getSourceChild();
		final SubIndexed.Args args= sub.getArgsChild();
		RAstNode child;
		
		// parse(text="x <- y[a= 1, 'B']")[[1]][[3]]
		child= args.getChild(0).getNameChild();
		assertEquals(child.getNodeType(), NodeType.SYMBOL);
		assertRExpressionEquals(newIntList(	1,		3),
								newList(	assign,	sub),
				child, sourceComponent );
		
		// parse(text="x <- y[a= 1, 'B']")[[1]][[3]][[3]]
		child= args.getChild(0).getValueChild();
		assertEquals(child.getNodeType(), NodeType.NUM_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		3),
								newList(	assign,	sub,	child),
				child, sourceComponent );
		
		// parse(text="x <- y[a= 1, 'B']")[[1]][[3]][[4]]
		child= args.getChild(1).getValueChild();
		assertEquals(child.getNodeType(), NodeType.STRING_CONST);
		assertRExpressionEquals(newIntList(	1,		3,		4),
								newList(	assign,	sub,	child),
				child, sourceComponent );
	}
	
	
	private void assertRExpressionEquals(final ImIntList expectedIndexes, final ImCollection<RAstNode> expectedAstNodes,
			final RAstNode node, final RAstNode baseNode) {
		final int[] actualIndexes= RAsts.computeRExpressionIndex(node, baseNode);
		assertNotNull(actualIndexes);
		assertArrayEquals(expectedIndexes.toArray(), actualIndexes, "indexes");
		final List<RAstNode> actualNodes= RAsts.computeRExpressionNodes(node, baseNode);
		assertNotNull(actualNodes);
		assertArrayEquals(expectedAstNodes.toArray(), actualNodes.toArray(), "nodes");
	}
	
	@SuppressWarnings("null")
	private void assertSymbol(final RAstNode node, final String text) {
		assertTrue(node.getNodeType() == NodeType.SYMBOL && node.getText().equals(text));
	}
	
}
