/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_PIPE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ERROR_TERM;
import static org.eclipse.statet.r.core.source.ast.RAstTests.FCALL;
import static org.eclipse.statet.r.core.source.ast.RAstTests.MULT_DIV;
import static org.eclipse.statet.r.core.source.ast.RAstTests.MULT_MULT;
import static org.eclipse.statet.r.core.source.ast.RAstTests.PIPE_FORWARD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SEQ;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SYMBOL_STD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.assertExpr0Node;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.RSourceConstants;


@NonNullByDefault
public class RParserPipeTest extends AbstractAstNodeTest {
	
	
	public RParserPipeTest() {
	}
	
	
	@Test
	public void PipeForward_basic() {
		RAstNode expr0;
		
		expr0= assertExpr("x |> f()",
						 0,  8, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("x |> f1() |> f2()",
						 0, 17, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  9, PIPE_FORWARD, 0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 5,  9, FCALL,		0, null,	expr0, new int[] { 0, 1 });
		assertExpr0Node( 5,  7, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 1, 0 });
		assertExpr0Node(13, 17, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node(13, 15, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("f1() |> f2()",
						 0, 12, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  4, FCALL,		0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  2, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 0 });
		assertExpr0Node( 8, 12, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 8, 10, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
		
		assertTwoOps_differentPrio(PIPE_FORWARD, SEQ);
		assertTwoOps_differentPrio(MULT_MULT, PIPE_FORWARD);
		assertTwoOps_differentPrio(MULT_DIV, PIPE_FORWARD);
	}
	
	@Test
	public void PipeForward_missingTarget() {
		RAstNode expr0;
		
		expr0= assertExpr("x |> ;",
				0,  4, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 4,  4, ERROR_TERM,	TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | CTX12_PIPE,
											null,		expr0, new int[] { 1 });
	}
	
	@Test
	public void PipeForward_invalidTarget() {
		RAstNode expr0;
		
		expr0= assertExpr("x |> f",
				0,  6, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE,
				"f",		expr0, new int[] { 1 });
	}
	
	@Test
	public void PipeForward_incompatible() {
		this.rParser.setRSourceConfig(new RSourceConfig(RSourceConstants.LANG_VERSION_4_0));
		RAstNode expr0;
		
		expr0= assertExpr("x |> f()",
						 0,  8, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
								null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("x |> f1() |> f2()",
						 0, 17, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | ERROR_IN_CHILD,
								null );
		assertExpr0Node( 0,  9, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
								null,					expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 5,  9, FCALL,		0, null,	expr0, new int[] { 0, 1 });
		assertExpr0Node( 5,  7, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 1, 0 });
		assertExpr0Node(13, 17, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node(13, 15, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("f1() |> f2()",
						 0, 12, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
								null );
		assertExpr0Node( 0,  4, FCALL,		0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  2, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 0 });
		assertExpr0Node( 8, 12, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 8, 10, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
	}
	
	
}
