/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RLangElement;


@NonNullByDefault
public class FilteredFrame<TModelChild extends RLangElement>
		implements RFrame<TModelChild>, LtkModelElementFilter<LtkModelElement> {
	
	
	private final RFrame<TModelChild> frame;
	private final SourceUnit exclude;
	
	
	public FilteredFrame(final RFrame<TModelChild> frame, final SourceUnit exclude) {
		this.frame= frame;
		this.exclude= exclude;
	}
	
	
	@Override
	public @Nullable String getFrameId() {
		return this.frame.getFrameId();
	}
	
	@Override
	public int getFrameType() {
		return this.frame.getFrameType();
	}
	
	@Override
	public RElementName getElementName() {
		return this.frame.getElementName();
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter) {
		return this.frame.hasModelChildren((this.exclude != null) ? this : null);
	}
	
	@Override
	public List<? extends TModelChild> getModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter) {
		return this.frame.getModelChildren((this.exclude != null) ? this : null);
	}
	
	@Override
	public List<? extends TModelChild> getModelElements() {
		return this.frame.getModelElements();
	}
	
	@Override
	public List<? extends RFrame> getPotentialParents() {
		return this.frame.getPotentialParents();
	}
	
	
	@Override
	public boolean include(final LtkModelElement element) {
		final SourceUnit su= (element instanceof SourceElement) ?
				((SourceElement)element).getSourceUnit() : null;
		return (su == null || !this.exclude.getId().equals(su.getId()) );
	}
	
	
	@Override
	public String toString() {
		final ObjectUtils.ToStringBuilder builder= new ObjectUtils.ToStringBuilder(
				"FilteredFrame", getClass() ); //$NON-NLS-1$
		builder.addProp("frame", this.frame); //$NON-NLS-1$
		builder.addProp("exclude", this.exclude); //$NON-NLS-1$
		return builder.build();
	}
	
}
