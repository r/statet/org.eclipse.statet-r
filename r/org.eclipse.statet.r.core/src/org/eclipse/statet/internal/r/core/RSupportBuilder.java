/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core;

import java.util.ArrayList;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.base.core.preferences.StatetCorePreferenceNodes;
import org.eclipse.statet.internal.r.core.builder.RBuilder;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;


public class RSupportBuilder extends IncrementalProjectBuilder {
	
	
	public static final String ID= "org.eclipse.statet.r.resourceProjects.RBuilder"; //$NON-NLS-1$
	
	
	static class ExceptionCollector {
		
		private final ArrayList<IStatus> exceptionList= new ArrayList<>(20);
		
		public void reset() {
			this.exceptionList.clear();
			if (this.exceptionList.size() > 20) {
				this.exceptionList.trimToSize();
			}
		}
		
		private void add(final IStatus e) {
			this.exceptionList.add(e);
		}
		
		void checkException() throws CoreException {
			if (this.exceptionList != null && this.exceptionList.size() > 0) {
				final IStatus[] allStatus= this.exceptionList.toArray(new IStatus[this.exceptionList.size()]);
				
				final IStatus status= new MultiStatus(RCore.BUNDLE_ID, 0, allStatus,
						NLS.bind(Messages.Builder_error_MultipleErrors_message, Integer.toString(allStatus.length)),
						null );
				
				throw new CoreException(status);
			}
		}
	}
	
	private class SettingsListener implements IEclipsePreferences.IPreferenceChangeListener {
		
		@Override
		public void preferenceChange(final PreferenceChangeEvent event) {
			RSupportBuilder.this.initialized= false;
		}
	}
	
	
	private RProject rProject;
	private SettingsListener settingsListener;
	private ExceptionCollector exceptions;
	
	private boolean startupSuccessfull= false;
	private boolean initialized= false;
	
	private RBuilder rBuilder;
	
	
	public RSupportBuilder() {
		super();
	}
	
	@Override
	public void setInitializationData(final IConfigurationElement config, final String propertyName, final Object data) throws CoreException {
		super.setInitializationData(config, propertyName, data);
	}
	
	@Override
	protected void startupOnInitialize() {
		this.startupSuccessfull= false;
		super.startupOnInitialize();
		
		// Listen to preference changes
		try {
			this.settingsListener= new SettingsListener();
			
			this.rProject= (RProject) getProject().getNature(RProjects.R_NATURE_ID);
			if (this.rProject == null) {
				throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, -1, "R Project Nature is missing", null)); //$NON-NLS-1$
			}
			this.rProject.addPreferenceNodeListener(
					StatetCorePreferenceNodes.CAT_MANAGMENT_QUALIFIER,
					this.settingsListener);
			this.rBuilder= new RBuilder();
			this.startupSuccessfull= true;
		}
		catch (final CoreException e) {
			RCorePlugin.log(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					NLS.bind("Error occured while initizalizing the builder (''{0}'').", ID), //$NON-NLS-1$
					e));
		}
	}
	
	private void init() throws CoreException {
		if (!this.startupSuccessfull) {
			throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					Messages.Builder_error_OnStartup_message, null ));
		}
		
		this.exceptions= new ExceptionCollector();
		
		this.initialized= true;
	}
	
	@Override
	protected IProject[] build(final int kind, final Map args, final IProgressMonitor monitor)
			throws CoreException {
		if (!this.initialized) {
			init();
		}
		
		if (kind == IncrementalProjectBuilder.FULL_BUILD) {
			doFullBuild(monitor);
		}
		else {
			final IResourceDelta delta= getDelta(getProject());
			if (delta == null) {
				doFullBuild(monitor);
			}
			else {
				doIncrementalBuild(delta, monitor);
			}
		}
		monitor.done();
		return null;
	}
	
	protected void doFullBuild(final IProgressMonitor monitor) throws CoreException {
		this.exceptions.reset();
		
		final IStatus status= this.rBuilder.buildFull(this.rProject, monitor);
		if (!status.isOK()) {
			this.exceptions.add(status);
		}
		
		this.exceptions.checkException();
	}
	
	protected void doIncrementalBuild(final IResourceDelta delta, final IProgressMonitor monitor)
			throws CoreException {
		this.exceptions.reset();
		
		final IStatus status= this.rBuilder.buildIncremental(this.rProject, delta, monitor);
		if (!status.isOK()) {
			this.exceptions.add(status);
		}
		
		this.exceptions.checkException();
	}
	
	@Override
	protected void clean(final IProgressMonitor monitor) throws CoreException {
//		if (!initialized)
		init();
		
		this.rBuilder.clean(getProject(), monitor);
	}
	
}
