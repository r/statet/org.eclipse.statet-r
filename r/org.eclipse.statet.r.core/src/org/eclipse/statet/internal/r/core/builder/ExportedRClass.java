/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.Serializable;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.rmodel.RLangClass;
import org.eclipse.statet.r.core.rmodel.RLangElement;


@NonNullByDefault
public class ExportedRClass extends ExportedRElement implements RLangClass<RLangElement<?>>, Serializable {
	
	
	private static final long serialVersionUID= -7356541747661973279L;
	
	
	private List<String> superClassNames;
	
	
	public ExportedRClass(final RLangElement<?> parent, final RLangClass<?> sourceElement) {
		super(parent, sourceElement);
		this.superClassNames= sourceElement.getExtendedClassNames();
	}
	
	public ExportedRClass() {
	}
	
	
	@Override
	public List<String> getExtendedClassNames() {
		return this.superClassNames;
	}
	
}
