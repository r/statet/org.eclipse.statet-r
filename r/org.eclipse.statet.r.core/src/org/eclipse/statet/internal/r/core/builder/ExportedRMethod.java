/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.Serializable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.Parameters;
import org.eclipse.statet.r.core.rmodel.RLangElement;
import org.eclipse.statet.r.core.rmodel.RLangMethod;


@NonNullByDefault
public class ExportedRMethod extends ExportedRElement implements RLangMethod<RLangElement<?>>, Serializable {
	
	
	private static final long serialVersionUID= -5410258006288951401L;
	
	
	private @Nullable Parameters parameters;
	
	
	public ExportedRMethod(final RLangElement<?> parent, final RLangMethod<?> sourceElement) {
		super(parent, sourceElement);
		this.parameters= sourceElement.getParameters();
	}
	
	public ExportedRMethod() {
	}
	
	
	@Override
	public @Nullable Parameters getParameters() {
		return this.parameters;
	}
	
}
