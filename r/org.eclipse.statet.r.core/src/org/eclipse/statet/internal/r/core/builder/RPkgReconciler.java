/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.io.FileUtil.ReadTextFileOperation;
import org.eclipse.statet.ecommons.io.FileUtil.ReaderAction;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.project.RBuildpaths;
import org.eclipse.statet.r.core.project.RIssues;


public class RPkgReconciler {
	
	
	private static final String DESCRIPTION_PACKAGE_FIELD= "Package:"; //$NON-NLS-1$
	
	
	public RPkgReconciler() {
	}
	
	
	public RPkgData parsePkgData(final IContainer pkgRoot,
			final MultiStatus status, final SubMonitor m) {
		final RPkgData result= new RPkgData();
		
		try {
			final IProject project= pkgRoot.getProject();
			
			if (!pkgRoot.exists()) {
				project.createMarker(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, Map.of(
						IMarker.SEVERITY, IMarker.SEVERITY_ERROR,
						IMarker.MESSAGE, NLS.bind("R package folder ''{0}'' is missing.",
								pkgRoot.getProjectRelativePath() ),
						IMarker.LOCATION, "R Project Configuration" ));
				
				return result;
			}
			
			final IFile file= pkgRoot.getFile(RBuildpaths.PKG_DESCRIPTION_FILE_PATH);
			if (file.exists()) {
				final ReadTextFileOperation fileOp= FileUtil.getFileUtil(file).createReadTextFileOp(
						new ReaderAction() {
					@Override
					public void run(final BufferedReader reader,
							final IProgressMonitor monitor) throws IOException, CoreException {
						String line;
						while ((line= reader.readLine()) != null) {
							if (line.startsWith(DESCRIPTION_PACKAGE_FIELD)) {
								final String value= line.substring(DESCRIPTION_PACKAGE_FIELD.length()).trim();
								if (!value.isEmpty()) {
									result.setPkgName(value);
								}
								return;
							}
						}
					}
				});
				fileOp.doOperation(m);
				
				if (result.getPkgName() == null) {
					file.createMarker(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, Map.of(
							IMarker.SEVERITY, IMarker.SEVERITY_ERROR,
							IMarker.MESSAGE, "The declaration of 'Package' (R package name) is missing." ));
				}
			}
			else {
				pkgRoot.createMarker(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, Map.of(
						IMarker.SEVERITY, IMarker.SEVERITY_ERROR,
						IMarker.MESSAGE, "'DESCRIPTION' file for R package is missing." ));
				
				return result;
			}
		}
		catch (final CoreException e) {
			status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					NLS.bind("An error occurred when parsing R package ''DESCRIPTION'' file ''{0}''.",
							pkgRoot.getFullPath().toString() ),
					e ));
		}
		
		return result;
	}
	
	
	private String checkRPkgName(final String pkgName) {
		if (pkgName != null) {
			return pkgName.intern();
		}
		return null;
	}
	
}
