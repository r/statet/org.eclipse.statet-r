/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RLangElement;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;


@NonNullByDefault
public class RUnitElement implements RLangElement<RLangElement<?>>, Serializable {
	
	
	private static final long serialVersionUID= 2909953007129363256L;
	
	
	public static RUnitElement read(final RSourceUnit su, final CompositeFrame envir, final InputStream input) throws IOException, ClassNotFoundException {
		final ObjectInputStream o= new ObjectInputStream(input);
		final RUnitElement element= (RUnitElement) o.readObject();
		element.sourceUnit= su;
		element.envir= envir;
		return element;
	}
	
	
	private transient SourceUnit sourceUnit;
	transient CompositeFrame envir;
	private List<RLangElement<?>> elements;
	
	
	public RUnitElement(final RSourceUnit su, final List<RLangElement> children) {
		this.sourceUnit= su;
		this.elements= Collections.unmodifiableList(children);
	}
	
	/** For serialization */
	public RUnitElement() {
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public int getElementType() {
		return LtkModelElement.C12_SOURCE_FILE;
	}
	
	@Override
	public RElementName getElementName() {
		final ElementName elementName= this.sourceUnit.getElementName();
		if (elementName instanceof RElementName) {
			return (RElementName)elementName;
		}
		return RElementName.create(RElementName.RESOURCE, elementName.getSegmentName());
	}
	
	@Override
	public String getId() {
		return this.sourceUnit.getId();
	}
	
	@Override
	public boolean exists() {
		return true;
	}
	
	@Override
	public boolean isReadOnly() {
		return this.sourceUnit.isReadOnly();
	}
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.sourceUnit;
	}
	
	@Override
	public @Nullable RLangElement<?> getModelParent() {
		return null;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangElement<?>> filter) {
		return LtkModelUtils.<RLangElement<?>>hasChildren(this.elements, filter);
	}
	
	@Override
	public List<? extends RLangElement<?>> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangElement<?>> filter) {
		return LtkModelUtils.<RLangElement<?>>getChildren(this.elements, filter);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RFrame.class) {
			return (T) this.envir;
		}
		return null;
	}
	
	
	public void save(final OutputStream outputStream) throws IOException {
		final ObjectOutputStream o= new ObjectOutputStream(outputStream);
		o.writeObject(this);
		o.flush();
	}
	
	
	@Override
	public @Nullable TextRegion getSourceRange() {
		return null;
	}
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		return null;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
}
