/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.rj.renv.core.RPkgType;


final class Cache {
	
	private static final String BAK_POSTFIX= "-bak"; //$NON-NLS-1$
	
	private final IFileStore binDir;
	private final IFileStore srcDir;
	
	
	public Cache(final IFileStore parent) {
		this.binDir= parent.getChild("pkg-bin"); //$NON-NLS-1$
		this.srcDir= parent.getChild("pkg-src"); //$NON-NLS-1$
	}
	
	
	private void checkDir(final IProgressMonitor monitor) throws CoreException {
		if (!this.binDir.fetchInfo().exists()) {
			this.binDir.mkdir(EFS.NONE, monitor);
		}
		if (!this.srcDir.fetchInfo().exists()) {
			this.srcDir.mkdir(EFS.NONE, monitor);
		}
	}
	
	public void add(final String pkgName, final RPkgType type,
			final IFileStore store, final IProgressMonitor monitor) throws CoreException {
		checkDir(monitor);
		
		final IFileStore dir= (type == RPkgType.SOURCE) ? this.srcDir : this.binDir;
		final String[] names= dir.childNames(EFS.NONE, monitor);
		final String prefix= pkgName + '_';
		String oldName= null;
		for (final String name : names) {
			if (name.startsWith(prefix)) {
				if (name.endsWith(BAK_POSTFIX) || oldName != null) {
					removeBak(dir.getChild(name), monitor);
				}
				else {
					oldName= name;
				}
			}
		}
		if (oldName != null) {
			makeBak(dir.getChild(oldName), monitor);
		}
		store.copy(dir.getChild(store.getName()), EFS.NONE, monitor);
	}
	
	private void removeBak(final IFileStore file, final IProgressMonitor monitor) throws CoreException {
		file.delete(EFS.NONE, monitor);
	}
	
	private void makeBak(final IFileStore file, final IProgressMonitor monitor) throws CoreException {
		file.move(file.getParent().getChild(file.getName() + BAK_POSTFIX), EFS.OVERWRITE, monitor);
	}
	
	public IFileStore get(final String pkgName, final RPkgType type,
			final IProgressMonitor monitor) throws CoreException {
		final IFileStore dir= (type == RPkgType.SOURCE) ? this.srcDir : this.binDir;
		final String[] names= dir.childNames(EFS.NONE, monitor);
		final String prefix= pkgName + '_';
		for (final String name : names) {
			if (name.startsWith(prefix)) {
				if (!name.endsWith(BAK_POSTFIX)) {
					return dir.getChild(name);
				}
			}
		}
		throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
				"R package '" + pkgName + "' not available in local package cache.", null ));
	}
	
}
