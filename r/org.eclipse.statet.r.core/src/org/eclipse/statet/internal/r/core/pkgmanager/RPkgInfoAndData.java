/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import org.eclipse.statet.r.core.pkgmanager.IRPkgInfoAndData;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.core.RNumVersion;


public class RPkgInfoAndData extends RPkgData implements IRPkgInfoAndData {
	
	
	private final String title;
	
	private final String built;
	
	private final RLibLocation lib;
	
	private final int flags;
	private final long installStamp;
	
	
	public RPkgInfoAndData(final String name, final RNumVersion version, final String built,
			final String title, final RLibLocation lib,
			final int flags, final long installStamp, final String repoId) {
		super(name, version, repoId);
		this.built= (built != null) ? built : ""; //$NON-NLS-1$
		this.title= (title != null && !title.isEmpty()) ? title : null;
		this.lib= lib;
		this.flags= flags;
		this.installStamp= installStamp;
	}
	
	
	@Override
	public String getTitle() {
		return this.title;
	}
	
	@Override
	public String getBuilt() {
		return this.built;
	}
	
	@Override
	public RLibLocation getLibLocation() {
		return this.lib;
	}
	
	@Override
	public int getFlags() {
		return this.flags;
	}
	
	@Override
	public long getInstallStamp() {
		return this.installStamp;
	}
	
}
