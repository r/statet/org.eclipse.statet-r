/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RUtil;
import org.eclipse.statet.r.core.pkgmanager.IRPkgInfoAndData;
import org.eclipse.statet.r.core.pkgmanager.ISelectedRepos;
import org.eclipse.statet.r.core.pkgmanager.RPkgAction;
import org.eclipse.statet.r.core.pkgmanager.RPkgUtils;
import org.eclipse.statet.r.core.pkgmanager.RRepo;
import org.eclipse.statet.r.core.renv.IREnvConfiguration;
import org.eclipse.statet.r.core.tool.IRConsoleService;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.runtime.RLibLocationInfo;


public class RPkgOperator {
	
	
	private final RPkgManagerImpl manager;
	private final IREnvConfiguration envConfig;
	private final ISelectedRepos repos;
	
	private IRConsoleService r;
	
	private String tempDir;
	private Set<String> createdDirs;
	
	
	public RPkgOperator(final RPkgManagerImpl manager, final IREnvConfiguration config,
			final ISelectedRepos repos) {
		this.manager= manager;
		this.envConfig= config;
		this.repos= repos;
	}
	
	public RPkgOperator(final RPkgManagerImpl manager) {
		this(manager, manager.getREnv().get(IREnvConfiguration.class), manager.getSelectedRepos());
	}
	
	
	private String requireTempDir(final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		if (this.tempDir == null) {
			this.tempDir= RDataUtils.checkSingleCharValue(this.r.evalData("tempdir()", m)); //$NON-NLS-1$
		}
		return this.tempDir;
	}
	
	private void requireLibDir(final String dir,
			final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		if (this.createdDirs == null) {
			this.createdDirs= new HashSet<>();
		}
		if (this.createdDirs.contains(dir)) {
			return;
		}
		
		final StringBuilder sb= new StringBuilder();
		sb.setLength(0);
		sb.append("dir.create("); //$NON-NLS-1$
		sb.append('"').append(RUtil.escapeCompletely(dir)).append('"');
		sb.append(", showWarnings= FALSE"); //$NON-NLS-1$
		sb.append(", recursive= TRUE"); //$NON-NLS-1$
		sb.append(')');
		this.r.evalVoid(sb.toString(), m);
		
		sb.setLength(0);
		sb.append("file.access("); //$NON-NLS-1$
		sb.append('"').append(RUtil.escapeCompletely(dir)).append('"');
		sb.append(", 3L"); //$NON-NLS-1$
		sb.append(')');
		if (RDataUtils.checkSingleIntValue(this.r.evalData(sb.toString(), m)) != 0) {
			throw new StatusException(new ErrorStatus(RCore.BUNDLE_ID, 0,
					NLS.bind("An error occurred when creating the R library location ''{0}''.", dir), null ));
		}
		
		sb.setLength(0);
		sb.append(".libPaths("); //$NON-NLS-1$
		sb.append("c("); //$NON-NLS-1$
		sb.append('"').append(RUtil.escapeCompletely(dir)).append('"');
		sb.append(", .libPaths())"); //$NON-NLS-1$
		sb.append(')');
		this.r.evalVoid(sb.toString(), m);
		
		this.createdDirs.add(dir);
	}
	
	void runActions(final List<? extends RPkgAction> actions,
			final IRConsoleService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		m.setWorkRemaining(actions.size());
		this.r= r;
		try {
			final StringBuilder sb= new StringBuilder();
			for (int i= 0; i < actions.size(); i++) {
				m.setWorkRemaining(actions.size() - i);
				
				final RPkgAction action= actions.get(i);
				if (action.getAction() == RPkgAction.INSTALL) {
					installPkg((RPkgAction.Install) action, sb, m.newSubMonitor(1));
				}
				else if (action.getAction() == RPkgAction.UNINSTALL) {
					uninstallPkg((RPkgAction.Uninstall) action, sb, m.newSubMonitor(1));
				}
			}
		}
		finally {
			this.r= null;
		}
	}
	
	private void installPkg(final RPkgAction.Install action, final StringBuilder sb,
			final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final String name= action.getPkg().getName();
		m.beginSubTask(NLS.bind("Installing R package ''{0}''...", name));
		m.setWorkRemaining(10);
		
		sb.setLength(0);
		sb.append("install.packages("); //$NON-NLS-1$
		final RRepo repo= this.repos.getRepo(action.getRepoId());
		if (repo == null) {
			throw new StatusException(new ErrorStatus(RCore.BUNDLE_ID,
					NLS.bind("Repository ({0}) not found.", action.getRepoId()) ));
		}
		if (repo.getId().startsWith(RRepo.WS_CACHE_PREFIX)) {
			try {
				final IFileStore store= this.manager.getCache().get(name, repo.getPkgType(),
						EStatusUtils.convert(m.newSubMonitor(1)) );
				if (RPkgUtils.checkPkgType(store.getName(), this.manager.getRPlatform()) != repo.getPkgType()) {
					throw new IllegalStateException();
				}
				String fileName;
				if (this.envConfig.isRemote()) {
					final InputStream in= store.openInputStream(EFS.NONE, EStatusUtils.convert(m.newSubMonitor(0)));
					try {
						fileName= requireTempDir(m.newSubMonitor(1)) + this.r.getPlatform().getFileSep() + store.getName();
						this.r.uploadFile(in, store.fetchInfo().getLength(), fileName, 0, m.newSubMonitor(3));
					}
					finally {
						try {
							in.close();
						}
						catch (final IOException e) {}
					}
				}
				else {
					fileName= store.toLocalFile(EFS.NONE, EStatusUtils.convert(m.newSubMonitor(0))).getPath();
				}
				sb.append('"').append(RUtil.escapeCompletely(fileName)).append('"');
				sb.append(", repos= NULL"); //$NON-NLS-1$
			}
			catch (final CoreException e) {
				throw new StatusException(new ErrorStatus(RCore.BUNDLE_ID,
						"An error occurred while preparing the package file",
						e ));
			}
		}
		else {
			sb.append('"').append(name).append('"');
			{	sb.append(", repos= "); //$NON-NLS-1$
				sb.append('"').append(RUtil.escapeCompletely(repo.getURL())).append('"');
			}
		}
		if (repo.getPkgType() != null) {
			sb.append(", type= "); //$NON-NLS-1$
			sb.append('"').append(RPkgUtils.getPkgTypeInstallKey(this.r.getPlatform(), repo.getPkgType())).append('"');
		}
		{	sb.append(", lib= "); //$NON-NLS-1$
			final RLibLocationInfo info= getLibLocationInfo(action.getLibLocation());
			if (!info.isWritable()) {
				return;
			}
			if (!info.isDirectoryExists()) {
				requireLibDir(info.getDirectoryRPath(), m);
			}
			sb.append('"').append(RUtil.escapeCompletely(info.getDirectoryRPath())).append('"');
		}
		sb.append(", dependencies= FALSE"); //$NON-NLS-1$
		sb.append(')');
		this.r.submitToConsole(sb.toString(), m);
		
		this.manager.pkgScanner.addExpectedPkg(action.getLibLocation(), action.getPkg());
	}
	
	private void uninstallPkg(final RPkgAction.Uninstall action, final StringBuilder sb,
			final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final String name= action.getPkg().getName();
		m.beginSubTask(NLS.bind("Uninstalling R package ''{0}''...", name));
		
		sb.setLength(0);
		sb.append("remove.packages("); //$NON-NLS-1$
		sb.append('"').append(name).append('"');
		{	sb.append(", lib= "); //$NON-NLS-1$
			final RLibLocationInfo info= getLibLocationInfo(action.getLibLocation());
			if (!info.isWritable()) {
				return;
			}
			sb.append('"').append(RUtil.escapeCompletely(info.getDirectoryRPath())).append('"');
		}
		sb.append(')');
		this.r.submitToConsole(sb.toString(), m);
	}
	
	void loadPkgs(final List<? extends IRPkgInfoAndData> pkgs, final boolean expliciteLocation,
			final IRConsoleService r, final ProgressMonitor m) throws StatusException {
		this.r= r;
		this.r.briefAboutToChange();
		try {
			final StringBuilder sb= new StringBuilder();
			for (int i= 0; i < pkgs.size(); i++) {
				final IRPkgInfoAndData pkgData= pkgs.get(i);
				m.beginSubTask(NLS.bind("Loading R package ''{0}''...", pkgData.getName()));
				
				sb.setLength(0);
				sb.append("library("); //$NON-NLS-1$
				sb.append('"').append(pkgData.getName()).append('"');
				if (expliciteLocation) {
					final RLibLocationInfo info= getLibLocationInfo(pkgData.getLibLocation());
					sb.append(", lib.loc= "); //$NON-NLS-1$
					sb.append('"').append(RUtil.escapeCompletely(info.getDirectoryRPath())).append('"');
				}
				sb.append(')');
				this.r.submitToConsole(sb.toString(), m);
			}
		}
		finally {
			this.r.briefChanged(0x1); // auto
			this.r= null;
		}
	}
	
	private RLibLocationInfo getLibLocationInfo(final RLibLocation libLocation)
			throws StatusException {
		final RLibLocationInfo info= this.manager.getRLibPaths().getInfo(libLocation);
		if (info == null) {
			throw new StatusException(new ErrorStatus(RCore.BUNDLE_ID,
					String.format("Location '%1$s' not available.", libLocation) ));
		}
		return info;
	}
	
}
