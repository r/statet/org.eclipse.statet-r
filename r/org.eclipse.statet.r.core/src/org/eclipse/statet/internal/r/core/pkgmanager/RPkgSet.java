/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import org.eclipse.statet.r.core.pkgmanager.IRPkgInfo;
import org.eclipse.statet.r.core.pkgmanager.IRPkgSet;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.RLibPaths;
import org.eclipse.statet.rj.renv.core.RPkgCompilation;
import org.eclipse.statet.rj.renv.runtime.BasicRPkgManagerData;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;


class RPkgSet extends BasicRPkgManagerData<IRPkgInfo> implements IRPkgSet {
	
	
	public RPkgSet(final REnv rEnv, final RLibPaths rLibPaths,
			final RPkgCompilation<IRPkgInfo> installed) {
		super(rEnv, RPkgManager.INSTALLED, rLibPaths, installed);
	}
	
	
}
