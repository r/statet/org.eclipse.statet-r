/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.r.core.pkgmanager.IRView;


class RView implements IRView {
	
	
	private final String name;
	private String topic;
	
	private final List<String> pkgs= new ArrayList<>();
	
	
	public RView(final String name) {
		this.name= name;
		this.topic= ""; //$NON-NLS-1$
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	public void setTopic(final String topic) {
		this.topic= topic;
	}
	
	@Override
	public String getTopic() {
		return this.topic;
	}
	
	@Override
	public List<String> getPkgList() {
		return this.pkgs;
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		return (this == obj || (obj instanceof IRView 
				&& this.name.equals(((IRView) obj).getName()) ));
	}
	
}
