/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.util.List;
import java.util.Locale;

import org.eclipse.statet.r.core.pkgmanager.RPkgUtils;
import org.eclipse.statet.r.core.pkgmanager.RRepo;
import org.eclipse.statet.rj.renv.core.RPkg;
import org.eclipse.statet.rj.renv.core.RPkgType;


public final class Util extends RPkgUtils {
	
	
	public static RRepo createRepoFromR(String id, final String name, String url) {
		if (id == null) {
			id= ""; //$NON-NLS-1$
		}
		if ("@CRAN@".equals(url)) { //$NON-NLS-1$
			url= "%cran"; //$NON-NLS-1$
			if (id.isEmpty()) {
				id= "CRAN"; //$NON-NLS-1$
			}
		}
		else {
			url= checkURL(url);
			if (url.isEmpty()) {
				return null;
			}
		}
		if (!id.isEmpty()
				&& !id.startsWith(RRepo.CUSTOM_PREFIX)
				&& !id.startsWith(RRepo.SPECIAL_PREFIX) ) {
			id= RRepo.R_PREFIX + id;
		}
		return RVarRepo.create(id, name, url, null);
	}
	
	public static String checkURL(final String url) {
		if (url == null || url.isEmpty() || url.equals("@CRAN@")) { //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
		if (url.charAt(url.length() - 1) == '/') {
			return url.substring(0, url.length() - 1);
		}
		return url;
	}
	
	/** For unsorted lists */
	public static int findPkg(final List<? extends RPkg> list, final String name) {
		for (int i= 0; i < list.size(); i++) {
			if (list.get(i).getName().equals(name)) {
				return i;
			}
		}
		return -1;
	}
	
	/** For backward compatibility **/
	public static RPkgType getPkgType(final String name) {
		try {
			return RPkgType.valueOf(name);
		}
		catch (final IllegalArgumentException e) {
			return RPkgType.valueOf(name.toUpperCase(Locale.ROOT));
		}
	}
	
}
