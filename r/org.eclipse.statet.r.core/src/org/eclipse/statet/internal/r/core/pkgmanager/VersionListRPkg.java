/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import org.eclipse.statet.rj.renv.core.RNumVersion;
import org.eclipse.statet.rj.renv.core.RPkg;


public class VersionListRPkg implements RPkg {
	
	
	private final String name;
	
	private RNumVersion version;
	
	
	public VersionListRPkg(final String name, final RNumVersion version) {
		if (name == null) {
			throw new NullPointerException();
		}
		this.name= name;
		this.version= (version != null) ? version : RNumVersion.NONE;
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public RNumVersion getVersion() {
		return this.version;
	}
	
	public void addVersion(final RNumVersion version) {
		if (this.version.equals(version) || version == RNumVersion.NONE) {
			return;
		}
		if (this.version == RNumVersion.NONE) {
			this.version= version;
			return;
		}
		final String listString= this.version.toString();
		final String addString= version.toString();
		for (int i= 0; i < listString.length(); i++) {
			final int idx= listString.indexOf(addString, i);
			if (idx >= 0) {
				if ((idx == 0 || listString.regionMatches(idx - 2, ", ", 0, 2)) //$NON-NLS-1$
						&& (idx + addString.length() == listString.length()
								|| listString.regionMatches(idx + addString.length(), ", ", 0, 2) )) { //$NON-NLS-1$
					return;
				}
				continue;
			}
			break;
		}
		this.version= RNumVersion.create(listString + ", " + addString); //$NON-NLS-1$
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode() + this.version.hashCode() * 7;
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RPkg)) {
			return false;
		}
		final RPkg other= (RPkg) obj;
		return (this.name.equals(other.getName()) && this.version.equals(other.getVersion()));
	}
	
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
