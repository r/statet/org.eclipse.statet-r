/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.renv;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.renv.IREnvConfiguration;
import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;


@NonNullByDefault
public class ActualREnv extends REnv {
	
	
	public static final String ID_DEFAULT_PREFIX= "default-"; //$NON-NLS-1$
	
	
	String name;
	
	boolean isDeleted;
	
	IREnvConfiguration config;
	
	
	public ActualREnv(final String id) {
		super(id.intern());
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public boolean isDeleted() {
		return (this.isDeleted);
	}
	
	@Override
	public <T> @Nullable T get(final Class<T> type) {
		if (type == REnvConfiguration.class
				|| type == REnvHelpConfiguration.class
				|| type == IREnvConfiguration.class) {
			return (T) this.config;
		}
		return null;
	}
	
	
	@Override
	public REnv resolve() {
		return this;
	}
	
}
