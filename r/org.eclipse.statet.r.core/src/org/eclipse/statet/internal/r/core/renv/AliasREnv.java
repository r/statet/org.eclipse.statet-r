/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.renv;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public class AliasREnv extends REnv {
	
	
	String name;
	
	@Nullable REnv link;
	
	
	public AliasREnv(final String id) {
		super(id);
	}
	
	
	@Override
	public String getName() {
		final REnv rEnv= resolve();
		return (rEnv != null) ? rEnv.getName() : "";
	}
	
	@Override
	public <T> @Nullable T get(final Class<T> type) {
		final REnv rEnv= resolve();
		return (rEnv != null) ? rEnv.get(type) : null;
	}
	
	@Override
	public @Nullable REnv resolve() {
		return this.link;
	}
	
}
