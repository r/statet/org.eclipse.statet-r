/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rhelp;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.http.RHelpHttpService;


@NonNullByDefault
public class WorkbenchRHelpHttpService extends RHelpHttpService implements Disposable {
	
	
	private boolean running;
	
	private boolean httpdStarted;
	private @Nullable JettyServer httpd;
	
	
	public WorkbenchRHelpHttpService(final RHelpManager rHelpManager) {
		super(rHelpManager);
	}
	
	
	@Override
	public boolean ensureIsRunning() {
		if (!this.httpdStarted) {
			startServer();
		}
		return this.running;
	}
	
	@Override
	public void dispose() {
		stopServer();
	}
	
	@Override
	@SuppressWarnings("null")
	protected String getHost() {
		return this.httpd.getHost();
	}
	
	@Override
	@SuppressWarnings("null")
	protected int getPort() {
		return this.httpd.getPort();
	}
	
	
	private synchronized void startServer() {
		if (this.httpdStarted) {
			return;
		}
		final JettyServer httpd= new JettyServer();
		try {
			httpd.startServer();
			this.httpd= httpd;
			this.running= true;
		}
		catch (final Exception e) {
			RCorePlugin.log(new Status(IStatus.ERROR, RCore.BUNDLE_ID, -1,
						"An error occured when starting webserver for R help.", e));
			try {
				httpd.stopServer();
			}
			catch (final Exception ignore) {}
		}
		this.httpdStarted= true;
	}
	
	private synchronized void stopServer() {
		if (this.httpd == null) {
			return;
		}
		this.running= false;
		try {
			this.httpd.stopServer();
			this.httpd= null;
		}
		catch (final Exception e) {
			RCorePlugin.log(new Status(IStatus.ERROR, RCore.BUNDLE_ID, -1,
					"An error occured when stopping webserver for R help.", e));
		}
	}
	
}
