/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rhelp;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService.ChangeEvent;

import org.eclipse.statet.r.core.renv.IREnvManager;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rj.renv.core.REnvManager;


@NonNullByDefault
public class WorkbenchRHelpManager extends RHelpManager implements PreferenceSetService.ChangeListener, Disposable {
	
	
	private static final ImIdentitySet<String> PREF_QUALIFIERS= ImCollections.newIdentitySet(
			IREnvManager.PREF_QUALIFIER );
	
	
	private final PreferenceAccess prefAccess;
	
	
	public WorkbenchRHelpManager(final REnvManager rEnvManager) {
		super(rEnvManager);
		this.prefAccess= EPreferences.getInstancePrefs();
		this.prefAccess.addPreferenceSetListener(this, PREF_QUALIFIERS);
		
		initServerSupport("jetty");
	}
	
	
	@Override
	public void dispose() {
		this.prefAccess.removePreferenceSetListener(this);
	}
	
	
	@Override
	public void onPreferenceChanged(final ChangeEvent event) {
		if (event.contains(IREnvManager.PREF_QUALIFIER)) {
			onREnvChanged();
		}
	}
	
}
