/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;
import static org.eclipse.statet.ltk.core.StatusCodes.CTX12;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE12;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE123;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_ELSE;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_FDEF;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_FOR;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_IF;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_PIPE;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_REPEAT;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_WHILE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AFTER_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_ARGVALUE_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_FORSEQ_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_IN_GROUP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_INT_WITH_DEC_POINT;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_SEQREL_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_CODEPOINT_INVALID;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNKOWN;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_NULLCHAR;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_CC_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_CONDITION_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_CONDITION_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_ELEMENTNAME_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_IF_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_IN_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_NODE_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_OPERATOR_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_SYMBOL_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TEXT_INVALID;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_UNKNOWN;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.util.AbstractAstProblemReporter;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.core.source.StatusDetail;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.r.core.data.RValueFormatter;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.source.RSourceConstants;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.Arithmetic;
import org.eclipse.statet.r.core.source.ast.Assignment;
import org.eclipse.statet.r.core.source.ast.Block;
import org.eclipse.statet.r.core.source.ast.CForLoop;
import org.eclipse.statet.r.core.source.ast.CIfElse;
import org.eclipse.statet.r.core.source.ast.CLoopCommand;
import org.eclipse.statet.r.core.source.ast.CRepeatLoop;
import org.eclipse.statet.r.core.source.ast.CWhileLoop;
import org.eclipse.statet.r.core.source.ast.Dummy;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.FDef;
import org.eclipse.statet.r.core.source.ast.Group;
import org.eclipse.statet.r.core.source.ast.Help;
import org.eclipse.statet.r.core.source.ast.Logical;
import org.eclipse.statet.r.core.source.ast.Model;
import org.eclipse.statet.r.core.source.ast.NSGet;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.NullConst;
import org.eclipse.statet.r.core.source.ast.NumberConst;
import org.eclipse.statet.r.core.source.ast.Pipe;
import org.eclipse.statet.r.core.source.ast.Power;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAstVisitor;
import org.eclipse.statet.r.core.source.ast.Relational;
import org.eclipse.statet.r.core.source.ast.Seq;
import org.eclipse.statet.r.core.source.ast.Sign;
import org.eclipse.statet.r.core.source.ast.SourceComponent;
import org.eclipse.statet.r.core.source.ast.Special;
import org.eclipse.statet.r.core.source.ast.StringConst;
import org.eclipse.statet.r.core.source.ast.SubIndexed;
import org.eclipse.statet.r.core.source.ast.SubNamed;
import org.eclipse.statet.r.core.source.ast.Symbol;


/**
 * Reports syntax problems in AST to {@link IssueRequestor} of source units
 */
@NonNullByDefault
public class AstProblemReporter extends AbstractAstProblemReporter {
	
	
	private final Visitor visitor= new Visitor();
	
	
	public AstProblemReporter() {
		super(RModel.R_TYPE_ID);
	}
	
	
	public void run(final RAstNode node,
			final SourceContent content,
			final IssueRequestor requestor) {
		try {
			init(content, requestor);
			
			node.acceptInR(this.visitor);
			
			flush();
		}
		catch (final OperationCanceledException | InvocationTargetException e) {}
		finally {
			clear();
		}
	}
	
	
	protected void handleCommonCodes(final RAstNode node, final int code)
			throws BadLocationException, InvocationTargetException {
		TYPE12: switch (code & TYPE12) {
		
		case TYPE12_SYNTAX_TOKEN_UNEXPECTED:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
							ProblemMessages.Syntax_TokenUnexpected_message,
							getMessageUtil().getFullText(node) ),
					node.getStartOffset(), node.getEndOffset() );
			return;
		
		case TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED:
			switch (code & CTX12) {
			case CTX12_PIPE:
				addProblem(Problem.SEVERITY_ERROR, code,
						ProblemMessages.Syntax_ExprUnexpected_FCallAfterPipeExpected_message,
						node.getStartOffset(), node.getEndOffset() );
				return;
			default:
				break TYPE12;
			}
		
		default:
			break TYPE12;
		}
		
		super.handleCommonCodes(node, code);
	}
	
	private void handleTextInvalid(final RAstNode node, final int code) throws BadLocationException {
		final StatusDetail detail= StatusDetail.getStatusDetail(node);
		switch (code & TYPE123) {
		case TYPE123_SYNTAX_TEXT_NULLCHAR:
			addProblem(Problem.SEVERITY_ERROR, code,
					ProblemMessages.Syntax_String_NullCharNotAllowed_message,
					detail.getStartOffset(), detail.getEndOffset() );
			return;
		case TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
							ProblemMessages.Syntax_String_EscapeSeqHexDigitMissing_message,
							detail.getText() ),
					detail.getStartOffset(), detail.getEndOffset() );
			return;
		case TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
							ProblemMessages.Syntax_String_EscapeSeqNotClosed_message,
							detail.getText() ),
					detail.getStartOffset(), detail.getEndOffset() );
			return;
		case TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
							ProblemMessages.Syntax_String_QuotedSymbol_EscapeSeqUnexpected_message,
							detail.getText() ),
					detail.getStartOffset(), detail.getEndOffset() );
			return;
		case TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNKOWN:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
							ProblemMessages.Syntax_String_EscapeSeqUnknown_message,
							detail.getText() ),
					detail.getStartOffset(), detail.getEndOffset() );
			return;
		case TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_CODEPOINT_INVALID:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
							ProblemMessages.Syntax_String_CodePointInvalid_message,
							detail.getText() ),
					detail.getStartOffset(), detail.getEndOffset() );
			return;
		default:
			handleUnknownCodes(node);
			return;
		}
	}
	
	private class Visitor extends RAstVisitor {
		
		
		private final RValueFormatter valueFormatter= new RValueFormatter();
		
		
		@Override
		public void visit(final SourceComponent node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Block node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_CC_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_BlockNotClosed_message,
								node.getEndOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Group node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				try {
					STATUS: switch (code & TYPE12) {
					case TYPE12_SYNTAX_CC_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_GroupNotClosed_message,
								node.getEndOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final CIfElse node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_IF_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_IfOfElseMissing_message,
								node.getStartOffset(), node.getStartOffset() + 1 );
						break STATUS;
					case TYPE12_SYNTAX_CONDITION_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ConditionMissing_If_message,
								node.getStartOffset() + 1, node.getStartOffset() + 3 );
						break STATUS;
					case TYPE12_SYNTAX_CONDITION_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ConditionNotClosed_If_message,
								node.getCondChild().getEndOffset() - 1, node.getCondChild().getEndOffset() + 1 );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final CForLoop node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_CONDITION_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ConditionMissing_For_message,
								node.getStartOffset() + 2, node.getStartOffset() + 4 );
						break STATUS;
					case TYPE12_SYNTAX_CONDITION_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ConditionNotClosed_For_message,
								node.getCondChild().getEndOffset() - 1, node.getCondChild().getEndOffset() + 1 );
						break STATUS;
					case TYPE12_SYNTAX_IN_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_InOfForConditionMissing_message,
								node.getVarChild().getEndOffset() - 1, node.getVarChild().getEndOffset() + 1 );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		
		@Override
		public void visit(final CRepeatLoop node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final CWhileLoop node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_CONDITION_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ConditionMissing_While_message,
								node.getStartOffset() + 4, node.getStartOffset() + 6 );
						break;
					case TYPE12_SYNTAX_CONDITION_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ConditionNotClosed_While_message,
								node.getCondChild().getEndOffset() - 1, node.getCondChild().getEndOffset() + 1 );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final CLoopCommand node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FCall node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_NODE_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FcallArgsNotClosed_message,
								node.getArgsChild().getEndOffset() - 1, node.getArgsChild().getEndOffset() + 1 );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FCall.Args node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FCall.Arg node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FDef node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					final int offset;
					switch (code & (TYPE12 | CTX12)) {
					case TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_FDEF:
						offset= node.getArgsCloseOffset();
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FdefShorthandUnsupported_message,
								node.getStartOffset(), (offset != NA_OFFSET) ? offset + 1 : node.getArgsChild().getEndOffset() );
						break STATUS;
					case TYPE12_SYNTAX_FDEF_ARGS_MISSING:
						offset= node.getStartOffset() + node.getOperator(0).text.length();
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FdefArgsMissing_message,
								offset - 1, offset + 1 );
						break STATUS;
					case TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED:
						offset= node.getArgsChild().getEndOffset();
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FdefArgsNotClosed_message,
								offset - 1, offset + 1 );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FDef.Args node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FDef.Arg node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Assignment node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Pipe node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & (TYPE12 | CTX12)) {
					case TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_PipeRightUnsupported_message,
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Model node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Relational node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					case TYPE123_SYNTAX_SEQREL_UNEXPECTED:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_TokenUnexpected_SeqRel_message,
										node.getOperator(0).text ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Logical node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Arithmetic node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Power node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Seq node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Special node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_TOKEN_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_SpecialNotClosed_message,
										this.valueFormatter.escapeString(
												getMessageUtil().getStartText(node, 1)) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Sign node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final SubIndexed node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case RSourceConstants.TYPE12_SYNTAX_NODE_NOT_CLOSED:
						if (node.getNodeType() == NodeType.SUB_INDEXED_S) {
							addProblem(Problem.SEVERITY_ERROR, code,
									getMessageBuilder().bind(
											ProblemMessages.Syntax_SubindexedNotClosed_S_message,
											getMessageUtil().getStartText(node, 0) ),
									node.getEndOffset() - 1, node.getEndOffset() + 1 );
							break STATUS;
						}
						else if (node.getSublistCloseOffset() != NA_OFFSET) {
							addProblem(Problem.SEVERITY_ERROR, code,
									getMessageBuilder().bind(
											ProblemMessages.Syntax_SubindexedNotClosed_Done_message,
											getMessageUtil().getStartText(node, 0) ),
									node.getEndOffset() - 1, node.getEndOffset() + 1 );
							break STATUS;
						}
						else {
							addProblem(Problem.SEVERITY_ERROR, code,
									getMessageBuilder().bind(
											ProblemMessages.Syntax_SubindexedNotClosed_Dboth_message,
											getMessageUtil().getStartText(node, 0) ),
									node.getEndOffset() - 1, node.getEndOffset() + 1 );
							break STATUS;
						}
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final SubIndexed.Args node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final SubIndexed.Arg node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final SubNamed node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final NSGet node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		@SuppressWarnings("null")
		public void visit(final StringConst node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE:
	//					(node.getOperator(0) == RTerminal.STRING_R)
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_StringROpeningIncomplete_message,
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE12_SYNTAX_TOKEN_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								(node.getOperator(0) == RTerminal.STRING_R) ?
										getMessageBuilder().bind(
												ProblemMessages.Syntax_StringRNotClosed_message,
												this.valueFormatter.escapeString(getMessageUtil().getStartText(node, 1)),
												StatusDetail.getStatusDetail(node).getText() ) :
										getMessageBuilder().bind(
												ProblemMessages.Syntax_StringNotClosed_message,
												this.valueFormatter.escapeString(getMessageUtil().getStartText(node, 1)),
												node.getOperator(0).text ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE12_SYNTAX_TEXT_INVALID:
						handleTextInvalid(node, code);
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final NumberConst node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					case TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_Number_HexDigitMissing_message,
										getMessageUtil().getFullText(node) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_Number_HexFloatExpMissing_message,
										getMessageUtil().getFullText(node) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_Number_ExpDigitMissing_message,
										getMessageUtil().getFullText(node) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L:
						addProblem(Problem.SEVERITY_WARNING, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_Number_NonIntWithLLiteral_message,
										getMessageUtil().getFullText(node) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE123_SYNTAX_NUMBER_INT_WITH_DEC_POINT:
						addProblem(Problem.SEVERITY_WARNING, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_Number_IntWithDecPoint_message,
										getMessageUtil().getFullText(node) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final NullConst node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Symbol node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_SYMBOL_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_SymbolMissing_message,
								node.getStartOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					case TYPE12_SYNTAX_ELEMENTNAME_MISSING:
						// this can be a status for string too, but never used there
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ElementnameMissing_message,
								node.getStartOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					case TYPE12_SYNTAX_TOKEN_NOT_CLOSED:
						// assert(node.getOperator(0) == RTerminal.SYMBOL_G)
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_QuotedSymbolNotClosed_message,
										this.valueFormatter.escapeString(
												getMessageUtil().getStartText(node, 1)) ),
								node.getEndOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					case TYPE12_SYNTAX_TEXT_INVALID:
						// assert(node.getOperator(0) == RTerminal.SYMBOL_G)
						handleTextInvalid(node, code);
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Help node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
	//				switch (code & TYPE12) {
	//				default:
						handleCommonCodes(node, code);
						break STATUS;
	//				}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final Dummy node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					case TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ExprBeforeOpMissing_message,
								expandSpaceStart(node.getStartOffset()), node.getEndOffset() + 1 );
						break STATUS;
					case TYPE123_SYNTAX_EXPR_AFTER_OP_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_ExprAfterOpMissing_message,
										getMessageUtil().getFullText(node) ),
								node.getStartOffset() - 1, expandSpaceEnd(node.getEndOffset()) );
						break STATUS;
					case TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING:
						switch (code & CTX12) {
						case CTX12_PIPE:
							addProblem(Problem.SEVERITY_ERROR, code,
									ProblemMessages.Syntax_FCallAfterPipeMissing_message,
									node.getStartOffset() - 1, expandSpaceEnd(node.getEndOffset()) );
							break STATUS;
						default:
							handleUnknownCodes(node);
							break STATUS;
						}
	//				case STATUS12_SYNTAX_EXPR_AS_REF_MISSING:
	//					addProblem(Problem.ERROR, code,
	//							ProblemMessages.,
	//							node.getStartOffset() - 1, node.getEndOffset() + 1);
	//					break;
					case TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ExprAsConditionMissing_message,
								node.getStartOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					case TYPE123_SYNTAX_EXPR_AS_FORSEQ_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ExprAsForSequenceMissing_message,
								node.getStartOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					case TYPE123_SYNTAX_EXPR_AS_BODY_MISSING:
						{	final String message;
							CTX12: switch (code & CTX12) {
							case CTX12_IF:
								message= ProblemMessages.Syntax_ExprAsThenBodyMissing_message;
								break CTX12;
							case CTX12_ELSE:
								message= ProblemMessages.Syntax_ExprAsElseBodyMissing_message;
								break CTX12;
							case CTX12_FOR:
							case CTX12_WHILE:
							case CTX12_REPEAT:
								message= ProblemMessages.Syntax_ExprAsLoopBodyMissing_message;
								break CTX12;
							case CTX12_FDEF:
								message= ProblemMessages.Syntax_ExprAsFdefBodyMissing_message;
								break CTX12;
							default:
								handleUnknownCodes(node);
								break STATUS;
							}
							if (node.getLength() > 0) {
								addProblem(Problem.SEVERITY_ERROR, code, message,
										node.getStartOffset(), node.getEndOffset() );
							}
							else {
								addProblem(Problem.SEVERITY_ERROR, code, message,
										node.getStartOffset() - 1, node.getEndOffset() + 1 );
							}
							break STATUS;
						}
					case TYPE123_SYNTAX_EXPR_IN_GROUP_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_ExprInGroupMissing_message,
								node.getStartOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					case TYPE123_SYNTAX_EXPR_AS_ARGVALUE_MISSING:
						if ((code & CTX12) == RSourceConstants.CTX12_FDEF) {
							addProblem(Problem.SEVERITY_ERROR, code,
									ProblemMessages.Syntax_ExprAsFdefArgDefaultMissing_message,
									node.getStartOffset() - 1, node.getEndOffset() );
						}
						else {
							break;
						}
						break STATUS;
					}
					switch (code & TYPE12) {
					case TYPE12_SYNTAX_TOKEN_UNKNOWN:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Syntax_TokenUnknown_message,
										getMessageUtil().getFullText(node) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE12_SYNTAX_OPERATOR_MISSING:
						if (node.getChildCount() == 2) {
							addProblem(Problem.SEVERITY_ERROR, code,
									ProblemMessages.Syntax_OperatorMissing_message,
									node.getChild(0).getEndOffset() - 1, node.getChild(1).getStartOffset() + 1 );
							break STATUS;
						}
						handleUnknownCodes(node);
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInRChildren(this);
		}
	
	}
	
}
