/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.eclipse.statet.internal.r.core.rmodel.BuildSourceFrame.CREATED_IMPORTED;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.rmodel.BuildSourceFrame.ElementAccessList;
import org.eclipse.statet.r.core.rmodel.PackageReferences;
import org.eclipse.statet.r.core.rmodel.RElementAccess;


@NonNullByDefault
public class BasicPackageReferences implements PackageReferences {
	
	
	protected final Map<String, ElementAccessList> data;
	
	
	public BasicPackageReferences() {
		this.data= new HashMap<>();
	}
	
	
	@Override
	public Set<String> getAllPackageNames() {
		return Collections.unmodifiableSet(this.data.keySet());
	}
	
	@Override
	public boolean isImported(final String name) {
		final ElementAccessList list= this.data.get(name);
		if (list == null) {
			return false;
		}
		return (list.isCreated == CREATED_IMPORTED);
	}
	
	@Override
	public @Nullable List<? extends RElementAccess> getAllAccessOfPackage(final String name) {
		final ElementAccessList list= this.data.get(name);
		if (list == null) {
			return null;
		}
		return Collections.unmodifiableList(list.entries);
	}
	
	public void add(final String name, final ElementAccess access) {
		ElementAccessList detail= this.data.get(name);
		if (detail == null) {
			detail= new ElementAccessList(name);
			this.data.put(name, detail);
		}
		detail.entries.add(access);
		/*if (access.isWriteAccess() && !access.isDeletion()) {
			detail.isCreated= CREATED_EXPLICITE;
		}
		else */if (access.isImport()) {
			detail.isCreated= CREATED_IMPORTED;
		}
		access.shared= detail;
		
		access.fullNode.addAttachment(access);
	}
	
}
