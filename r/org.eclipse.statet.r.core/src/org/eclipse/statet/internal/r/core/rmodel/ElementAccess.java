/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.RElementAccess;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
public abstract class ElementAccess extends RElementAccess {
	
	
	public static final int A_READ=                        0x000000;
	public static final int A_CALL=                        0x000001;
	public static final int A_WRITE=                       0x000002;
	public static final int A_DELETE=                      0x000003;
	public static final int A_IMPORT=                      0x000004;
	
	public static final int A_SUB=                         0x000100;
	public static final int A_S4=                          0x000200;
	
	public static final int A_FUNC=                        0x000010;
	public static final int A_ARG=                         0x000020;
	
	
	public static abstract class Scope extends ElementAccess {
		
		public Scope(final RAstNode fullNode, final RAstNode nameNode) {
			super(fullNode, nameNode);
		}
		
	}
	
	public final static class Package extends Scope {
		
		public Package(final RAstNode fullNode, final RAstNode nameNode) {
			super(fullNode, nameNode);
		}
		
		@Override
		public int getType() {
			return RElementName.SCOPE_PACKAGE;
		}
		
	}
	
	public final static class Namespace extends Scope {
		
		public Namespace(final RAstNode fullNode, final RAstNode nameNode) {
			super(fullNode, nameNode);
		}
		
		@Override
		public int getType() {
			return RElementName.SCOPE_NS;
		}
		
	}
	
	public final static class NamespaceInternal extends Scope {
		
		public NamespaceInternal(final RAstNode fullNode, final RAstNode nameNode) {
			super(fullNode, nameNode);
		}
		
		@Override
		public int getType() {
			return RElementName.SCOPE_NS_INT;
		}
		
	}
	
	
	public static abstract class Main extends ElementAccess {
		
		private @Nullable RElementAccess expliciteScope;
		
		public Main(final RAstNode fullNode, final @Nullable RAstNode nameNode) {
			super(fullNode, nameNode);
		}
		
		void setScope(final ElementAccess packageAccess) {
			this.expliciteScope= packageAccess;
		}
		
		@Override
		public @Nullable RElementName getScope() {
			if (this.expliciteScope != null) {
				return this.expliciteScope;
			}
			if (this.shared.frame != null
					&& (this.shared.frame.getFrameType() == RFrame.PACKAGE || this.shared.isCreated >= BuildSourceFrame.CREATED_RESOLVED) ) {
				return this.shared.frame.getElementName();
			}
			return null;
		}
		
	}
	
	public final static class Default extends Main {
		
		public Default(final RAstNode fullNode) {
			super(fullNode, null);
		}
		
		public Default(final RAstNode fullNode, final RAstNode nameNode) {
			super(fullNode, nameNode);
		}
		
		@Override
		public int getType() {
			return RElementName.MAIN_DEFAULT;
		}
		
	}
	
	public final static class Slot extends ElementAccess {
		
		public Slot(final RAstNode fullNode) {
			super(fullNode, null);
		}
		
		public Slot(final RAstNode fullNode, final RAstNode nameNode) {
			super(fullNode, nameNode);
		}
		
		@Override
		public int getType() {
			return RElementName.SUB_NAMEDSLOT;
		}
		
	}
	
	public final static class Class extends Main {
		
		public Class(final RAstNode fullNode) {
			super(fullNode, null);
		}
		
		@Override
		public int getType() {
			return RElementName.MAIN_CLASS;
		}
		
	}
	
	
	int flags;
	RAstNode fullNode;
	@Nullable RAstNode nameNode;
	BuildSourceFrame.ElementAccessList shared;
	@Nullable SubAbstractElementAccess nextSegment;
	RLangSourceElement modelElement;
	
	
	private ElementAccess(final RAstNode fullNode, final @Nullable RAstNode nameNode) {
		this.fullNode= fullNode;
		this.nameNode= nameNode;
	}
	
	
	@Override
	public final @Nullable String getSegmentName() {
		return this.shared.getName();
	}
	
	@Override
	public String getDisplayName() {
		return RElementName.createDisplayName(this, 0);
	}
	
	@Override
	public final RFrame getFrame() {
		return this.shared.frame;
	}
	
	@Override
	public @Nullable RElementName getScope() {
		return null;
	}
	
	@Override
	public final ImList<? extends RElementAccess> getAllInUnit(final boolean includeSlaves) {
		return this.shared.getAll(includeSlaves);
	}
	
	@Override
	public final boolean isWriteAccess() {
		return ((this.flags & A_WRITE) == A_WRITE); // A_WRITE | A_DELETE
	}
	
	@Override
	public boolean isCallAccess() {
		return ((this.flags & 0xf) == A_CALL);
	}
	
	public final boolean isDeletion() {
		return ((this.flags & 0xf) == A_DELETE);
	}
	
	public final boolean isImport() {
		return ((this.flags & 0xf) == A_IMPORT);
	}
	
	@Override
	public final boolean isFunctionAccess() {
		return ((this.flags & 0xf0) == A_FUNC);
	}
	
	@Override
	public final RAstNode getNode() {
		return this.fullNode;
	}
	
	@Override
	public final @Nullable RAstNode getNameNode() {
		return this.nameNode;
	}
	
	@Override
	public final @Nullable RElementAccess getNextSegment() {
		return this.nextSegment;
	}
	
	final void appendSubElement(final SubAbstractElementAccess newSub) {
		SubAbstractElementAccess parent= this.nextSegment;
		if (parent == null) {
			this.nextSegment= newSub;
			return;
		}
		while (parent.nextSegment != null) {
			parent= parent.nextSegment;
		}
		parent.nextSegment= newSub;
	}
	
}
