/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.r.core.rmodel.RElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public class EmbeddedInlineElement extends EmbeddedRBuildElement {
	
	
	private final static RElementName NAME= RElementName.create(RElementName.MAIN_OTHER, "Inline R Code");
	
	
	public EmbeddedInlineElement(final SourceStructElement<?, ?> parent, final SourceComponent node) {
		super(parent, node, NAME, null);
	}
	
	
	protected SourceComponent getSourceComponent() {
		return (SourceComponent)this.node;
	}
	
	
	@Override
	public int getElementType() {
		return RElement.C12_SOURCE_CHUNK | 0x4;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == SourceComponent.class) {
			return (T)getSourceComponent();
		}
		return super.getAdapter(adapterType);
	}
	
}
