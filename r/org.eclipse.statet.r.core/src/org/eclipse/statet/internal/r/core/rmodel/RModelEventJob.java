/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementDelta;
import org.eclipse.statet.ltk.model.core.impl.AbstractModelEventJob;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;


/**
 * R model update event job
 */
public class RModelEventJob extends AbstractModelEventJob<RSourceUnit, RSourceUnitModelInfo> {
	
	
	RModelEventJob(final RModelManagerImpl manager) {
		super(manager);
	}
	
	
	@Override
	protected LtkModelElementDelta createDelta(final Task task) {
		return new ModelDelta(task.getElement(), task.getOldInfo(), task.getNewInfo());
	}
	
	@Override
	protected void dispose() {
		super.dispose();
	}
	
}
