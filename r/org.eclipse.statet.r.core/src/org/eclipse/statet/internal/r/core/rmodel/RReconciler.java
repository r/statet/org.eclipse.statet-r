/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.concurrent.CancellationException;

import com.ibm.icu.text.DecimalFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CacheStringFactory;
import org.eclipse.statet.jcommons.string.StringFactory;
import org.eclipse.statet.jcommons.text.core.input.OffsetStringParserInput;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.TaskIssueConfig;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.rmodel.RChunkElement;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.rmodel.build.RIssueReporter;
import org.eclipse.statet.r.core.rmodel.build.RSourceUnitModelContainer;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.r.core.source.ast.RoxygenParser;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


/**
 * Worker for r model manager
 */
@NonNullByDefault
public class RReconciler {
	
	
	private static final boolean LOG_TIME= false;
	
	protected static final class Data {
		
		public final RSourceUnitModelContainer adapter;
		
		public final RSourceUnit sourceUnit;
		
		public final SourceContent content;
		
		@Nullable AstInfo ast;
		
		@Nullable RSourceUnitModelInfo oldModel;
		@Nullable RSourceUnitModelInfo newModel;
		
		
		public Data(final RSourceUnitModelContainer adapter, final SubMonitor monitor) {
			this.adapter= adapter;
			this.sourceUnit= adapter.getSourceUnit();
			this.content= adapter.getParseContent(monitor);
		}
		
		
		boolean isOK() {
			return (this.content != null);
		}
		
		@SuppressWarnings("null")
		public AstInfo getAst() {
			return this.ast;
		}
		
		@SuppressWarnings("null")
		public RSourceUnitModelInfo getModel() {
			return this.newModel;
		}
		
	}
	
	
	private final RModelManagerImpl rManager;
	protected boolean stop= false;
	
	private final Object raLock= new Object();
	private final StringFactory raAstStringCache= new CacheStringFactory(0x20);
	private final StringParserInput raInput= new StringParserInput(0x1000);
	private final RParser raRParser= new RParser(RSourceConfig.DEFAULT_CONFIG,
				AstInfo.LEVEL_MODEL_DEFAULT, this.raAstStringCache );
	private final RoxygenParser raRoxygenParser= new RoxygenParser(this.raAstStringCache);
	
	private final Object rmLock= new Object();
	private final SourceAnalyzer rmScopeAnalyzer= new SourceAnalyzer();
	
	private final Object riLock= new Object();
	private final RIssueReporter riReporter= new RIssueReporter();
	
	private @Nullable RProject project;
	private @Nullable MultiStatus statusCollector;
	
	
	public RReconciler(final RModelManagerImpl manager) {
		this.rManager= manager;
	}
	
	
	public void init(final RProject project, final MultiStatus statusCollector) throws CoreException {
		this.project= nonNullAssert(project);
		this.statusCollector= statusCollector;
		
		final var taskIssueConfig= TaskIssueConfig.getConfig(project.getPrefs());
		this.riReporter.configure(taskIssueConfig);
	}
	
	void stop() {
		this.stop= true;
	}
	
	
	public @Nullable RSourceUnitModelInfo reconcile(final RSourceUnitModelContainer adapter,
			final int flags,
			final IProgressMonitor monitor) {
		final var m= SubMonitor.convert(monitor);
		
		final Data data;
		{	final RSourceUnit su= adapter.getSourceUnit();
			final int type= (su.getModelTypeId().equals(RModel.R_TYPE_ID) ? su.getElementType() : 0);
			data= (type != 0) ? new Data(adapter, m) : null;
		}
		if (data == null || !data.isOK()) {
			adapter.clear();
			return null;
		}
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		
		synchronized (this.raLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			updateAst(data, flags);
		}
		
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		if ((flags & 0xf) < ModelManager.MODEL_FILE) {
			return null;
		}
		
		synchronized (this.rmLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			final boolean updated= updateModel(data, flags);
			if (updated) {
				this.rManager.getEventJob().addUpdate(data.sourceUnit,
						data.oldModel, data.newModel );
			}
		}
		
		if ((flags & ModelManager.RECONCILE) != 0 && data.newModel != null) {
			synchronized (this.riLock) {
				if (this.stop || m.isCanceled()
						|| data.newModel != data.adapter.getCurrentModel() ) {
					return null;
				}
				
				reportIssues(data, flags);
			}
		}
		
		return data.newModel;
	}
	
	public RSourceUnitModelInfo reconcile(final RSourceUnit su, final SourceUnitModelInfo modelInfo,
			final List<? extends RChunkElement> chunkElements, final List<? extends SourceComponent> inlineNodes,
			final int level, final IProgressMonitor monitor) {
		synchronized (this.rmLock) {
			return updateModel(su, modelInfo, chunkElements, inlineNodes);
		}
	}
	
	
	protected final void updateAst(final Data data, final int flags) {
		final RSourceConfig rSourceConfig= data.adapter.getSourceUnit().getRCoreAccess().getRSourceConfig();
		final BasicSourceModelStamp stamp= new BasicSourceModelStamp(data.content.getStamp(),
				ImCollections.newList(rSourceConfig) );
		
		AstInfo ast= data.adapter.getCurrentAst();
		if (ast != null && !stamp.equals(ast.getStamp())) {
			ast= null;
		}
		if (ast != null) {
			data.ast= ast;
		}
		else {
			final long startAst;
			final long stopAst;
			startAst= System.nanoTime();
			
			final TextParserInput input;
			if (data.content.getStartOffset() != 0) {
				input= new OffsetStringParserInput(data.content.getString(), data.content.getStartOffset());
			}
			else {
				input= this.raInput.reset(data.content.getString());
			}
			
			this.raRParser.setRSourceConfig(rSourceConfig);
			this.raRParser.setCommentLevel(RParser.COLLECT_COMMENTS | RParser.ROXYGEN_COMMENTS);
			final SourceComponent sourceComponent= this.raRParser.parseSourceFragment(
					input.init(data.content.getStartOffset(), data.content.getEndOffset()),
					null );
			ast= new AstInfo(this.raRParser.getAstLevel(), stamp, sourceComponent);
			
			stopAst= System.nanoTime();
			
			this.raRoxygenParser.init(
					input.init(data.content.getStartOffset(), data.content.getEndOffset()));
			this.raRoxygenParser.update(sourceComponent);
			
			if (LOG_TIME) {
				System.out.println(this.raAstStringCache.toString());
				System.out.println("RReconciler/createAST   : " + DecimalFormat.getInstance().format(stopAst - startAst)); //$NON-NLS-1$
			}
			
			synchronized (data.adapter) {
				data.adapter.setAst(ast);
			}
			data.ast= ast;
		}
	}
	
	protected final boolean updateModel(final Data data, final int flags) {
		RSourceUnitModelInfo model= data.adapter.getCurrentModel();
		if (model != null && !data.getAst().getStamp().equals(model.getStamp())) {
			model= null;
		}
		if (model != null) {
			data.newModel= model;
			return false;
		}
		else {
			final long startModel;
			final long stopModel;
			startModel= System.nanoTime();
			
			model= this.rmScopeAnalyzer.createModel(data.adapter.getSourceUnit(), data.getAst());
			final boolean isOK= (model != null);
			
			stopModel= System.nanoTime();
			
			if (LOG_TIME) {
				System.out.println("RReconciler/createMODEL : " + DecimalFormat.getInstance().format(stopModel - startModel)); //$NON-NLS-1$
			}
			
			if (isOK) {
				synchronized (data.adapter) {
					data.oldModel= data.adapter.getCurrentModel();
					data.adapter.setModel(model);
				}
				data.newModel= model;
				return true;
			}
			return false;
		}
	}
	
	private RSourceUnitModelInfo updateModel(final RSourceUnit su, final SourceUnitModelInfo modelInfo,
			final List<? extends RChunkElement> chunkElements,
			final List<? extends SourceComponent> inlineNodes) {
		RSourceUnitModelInfo model;
		try {
			final AstInfo ast= modelInfo.getAst();
			this.rmScopeAnalyzer.beginChunkSession(su, ast);
			for (final var chunkElement : chunkElements) {
				final List<SourceComponent> rootNodes;
				{	final Object source= chunkElement.getAdapter(SourceComponent.class);
					if (source instanceof SourceComponent) {
						rootNodes= ImCollections.newList((SourceComponent)source);
					}
					else if (source instanceof List<?>) {
						rootNodes= (List<SourceComponent>)source;
					}
					else {
						continue;
					}
				}
				this.rmScopeAnalyzer.processChunk(chunkElement, rootNodes);
			}
			for (final SourceComponent inlineNode : inlineNodes) {
				final EmbeddedInlineElement inlineElement= new EmbeddedInlineElement(modelInfo.getSourceElement(), inlineNode);
				this.rmScopeAnalyzer.processInlineNode(inlineElement, inlineNode);
			}
		}
		finally {
			model= this.rmScopeAnalyzer.stopChunkSession();
		}
		return model;
	}
	
	protected void reportIssues(final Data data, final int flags) {
		try {
			final var issueSupport= data.adapter.getIssueSupport();
			if (issueSupport == null) {
				return;
			}
			final var issueRequestor= issueSupport.createIssueRequestor(data.sourceUnit);
			if (issueRequestor != null) {
				try {
					this.riReporter.run(data.sourceUnit, data.getModel(),
							data.content,
							issueRequestor, flags );
				}
				finally {
					issueRequestor.finish();
				}
			}
		}
		catch (final Exception e) {
			handleStatus(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					String.format("An error occurred when reporting issues for source unit %1$s.",
							data.sourceUnit ),
					e ));
		}
	}
	
	
	protected void handleStatus(final IStatus status) {
		final MultiStatus collector= this.statusCollector;
		if (collector != null) {
			collector.add(status);
		}
		else {
			RCorePlugin.log(status);
		}
	}
	
}
