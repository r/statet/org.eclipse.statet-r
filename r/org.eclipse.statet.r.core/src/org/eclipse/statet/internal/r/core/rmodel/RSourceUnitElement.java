/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
final class RSourceUnitElement extends RSourceFileElement
		implements BuildSourceFrameElement {
	
	
	private final RAstNode sourceNode;
	private List<? extends RLangSourceElement> sourceChildrenProtected= RSourceModel.NO_R_SOURCE_CHILDREN;
	
	
	public RSourceUnitElement(final RSourceUnit sourceUnit,
			final SourceModelStamp stamp,
			final BuildSourceFrame envir, final RAstNode node) {
		super(sourceUnit, stamp, envir);
		this.sourceNode= node;
	}
	
	
	@Override
	public void setSourceChildren(final List<? extends RLangSourceElement> children) {
		this.sourceChildrenProtected= children;
	}
	
	@Override
	public BuildSourceFrame getBuildFrame() {
		return this.envir;
	}
	
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return LtkModelUtils.<RLangSourceElement>hasChildren(this.sourceChildrenProtected, filter);
	}
	
	@Override
	public List<? extends RLangSourceElement> getSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return LtkModelUtils.<RLangSourceElement>getChildren(this.sourceChildrenProtected, filter);
	}
	
	
	@Override
	public TextRegion getSourceRange() {
		return this.sourceNode;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == AstNode.class) {
			return (T) this.sourceNode;
		}
		return null;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("RSourceUnitElement"); //$NON-NLS-1$
		final RElementName elementName= getElementName();
		if (elementName != null) {
			sb.append(' ').append(elementName);
		}
		else {
			sb.append(" <unnamed>"); //$NON-NLS-1$
		}
		
		return sb.toString();
	}
	
}
