/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceUnitModelInfo;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RSourceFrame;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;


@NonNullByDefault
public class RSourceUnitModelInfoImpl extends BasicSourceUnitModelInfo implements RSourceUnitModelInfo {
	
	
	final BuildSourceFrame topFrame;
	final LinkedHashMap<String, BuildSourceFrame> localFrames;
	private final Map<String, BuildSourceFrame> protectedLocalFrames;
	
	final BasicPackageReferences packageRefs;
	final Map<String, BuildSourceFrame> namespaceFrames;
	private final Map<String, BuildSourceFrame> protectedNamespaceFrames;
	
	private final RLangSourceElement sourceElement;
	
	
	RSourceUnitModelInfoImpl(final AstInfo ast,
			final LinkedHashMap<String, BuildSourceFrame> localFrames,
			final BuildSourceFrame topFrame,
			final BasicPackageReferences packageRefs,
			final Map<String, BuildSourceFrame> namespaceFrames,
			final RLangSourceElement unitElement) {
		super(ast);
		this.topFrame= topFrame;
		this.localFrames= localFrames;
		this.protectedLocalFrames= Collections.unmodifiableMap(localFrames);
		this.packageRefs= packageRefs;
		this.namespaceFrames= namespaceFrames;
		this.protectedNamespaceFrames= Collections.unmodifiableMap(namespaceFrames);
		this.sourceElement= unitElement;
	}
	
	
	@Override
	public RLangSourceElement getSourceElement() {
		return this.sourceElement;
	}
	
	@Override
	public BuildSourceFrame getTopFrame() {
		return this.topFrame;
	}
	
	@Override
	public final Map<String, ? extends RSourceFrame> getSourceFrames() {
		return this.protectedLocalFrames;
	}
	
	@Override
	public BasicPackageReferences getReferencedPackages() {
		return this.packageRefs;
	}
	
	@Override
	public final Map<String, ? extends RFrame> getReferencedFrames() {
		return this.protectedNamespaceFrames;
	}
	
}
