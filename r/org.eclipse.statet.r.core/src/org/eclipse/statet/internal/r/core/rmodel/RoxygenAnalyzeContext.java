/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.rmodel.RSourceElementByElementAccess.RClass;
import org.eclipse.statet.internal.r.core.rmodel.RSourceElementByElementAccess.RMethod;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RSourceFrame;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.ast.DocuTag;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
public interface RoxygenAnalyzeContext {
	
	
	RSourceUnitModelInfo getModelInfo();
	
	@Nullable RSourceFrame getNamespaceFrame(final String name);
	
	void createSelfAccess(final DocuTag docuTag,
			final RLangSourceElement element, final RAstNode symbol);
	void createNamespaceImportAccess(final DocuTag docuTag,
			final RAstNode symbol);
	void createNamespaceObjectImportAccess(final DocuTag docuTag,
			final RSourceFrame namespace, final RAstNode symbol);
	void createSlotAccess(final DocuTag docuTag,
			final RClass rClass, final RAstNode symbol);
	void createArgAccess(final DocuTag docuTag,
			final RMethod rMethod, final RAstNode symbol);
	void createRSourceRegion(final RAstNode node);
	
}
