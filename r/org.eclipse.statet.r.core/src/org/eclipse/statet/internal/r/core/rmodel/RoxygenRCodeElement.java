/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.rmodel.RElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
final class RoxygenRCodeElement implements BuildSourceFrameElement {
	
	
	private final RLangSourceElement parent;
	private final int number;
	
	private final RAstNode sourceNode;
	private final List<? extends RLangSourceElement> sourceChildrenProtected= RSourceModel.NO_R_SOURCE_CHILDREN;
	private BuildSourceFrame envir;
	
	
	public RoxygenRCodeElement(final RLangSourceElement parent, final int number, final BuildSourceFrame envir, final RAstNode node) {
		this.parent= parent;
		this.number= number;
		this.sourceNode= node;
	}
	
	
	@Override
	public void setSourceChildren(final List<? extends RLangSourceElement> children) {
//		sourceChildrenProtected= children;
	}
	
	@Override
	public BuildSourceFrame getBuildFrame() {
		return this.envir;
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public int getElementType() {
		return RElement.R_DOC_EXAMPLE_CHUNK;
	}
	
	@Override
	public @Nullable RElementName getElementName() {
		return null;
	}
	
	@Override
	public String getId() {
		return Integer.toHexString(RElement.R_DOC_EXAMPLE_CHUNK) + ":#" + this.number;
	}
	
	@Override
	public boolean exists() {
		return this.parent.exists();
	}
	
	@Override
	public boolean isReadOnly() {
		return this.parent.isReadOnly();
	}
	
	
	@Override
	public @Nullable RElement<?> getModelParent() {
		return null;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return false;
	}
	
	@Override
	public List<? extends RLangSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return RSourceModel.NO_R_SOURCE_CHILDREN;
	}
	
	@Override
	public RLangSourceElement getSourceParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return LtkModelUtils.<RLangSourceElement>hasChildren(this.sourceChildrenProtected, filter);
	}
	
	@Override
	public List<? extends RLangSourceElement> getSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return LtkModelUtils.<RLangSourceElement>getChildren(this.sourceChildrenProtected, filter);
	}
	
	@Override
	public RSourceUnit getSourceUnit() {
		return this.parent.getSourceUnit();
	}
	
	
	@Override
	public TextRegion getSourceRange() {
		return this.sourceNode;
	}
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		return null;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == AstNode.class) {
			return (T) this.sourceNode;
		}
		if (adapterType == RFrame.class) {
			return (T) this.envir;
		}
		return null;
	}
	
	@Override
	public int hashCode() {
		return this.parent.hashCode() + this.number;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RoxygenRCodeElement) {
			final RoxygenRCodeElement other= (RoxygenRCodeElement) obj;
			return ( ((other.getElementType() & LtkModelElement.MASK_C123) == RElement.R_DOC_EXAMPLE_CHUNK)
					&& this.number == other.number
					&& this.parent.equals(other.parent) );
		}
		return false;
	}
	
}
