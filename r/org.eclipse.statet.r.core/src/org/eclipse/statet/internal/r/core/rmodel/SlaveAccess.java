/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.r.core.rmodel.RElementAccess;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.source.ast.RAstNode;


public final class SlaveAccess extends RElementAccess {
	
	
	private final RElementAccess master;
	
	private RElementAccess nextSegment;
	
	
	public SlaveAccess(final RElementAccess master) {
		this.master= master;
	}
	
	
	@Override
	public int getType() {
		return this.master.getType();
	}
	
	@Override
	public String getSegmentName() {
		return this.master.getSegmentName();
	}
	
	@Override
	public RFrame getFrame() {
		return this.master.getFrame();
	}
	
	@Override
	public boolean isWriteAccess() {
		return this.master.isWriteAccess();
	}
	
	@Override
	public boolean isFunctionAccess() {
		return this.master.isFunctionAccess();
	}
	
	@Override
	public boolean isCallAccess() {
		return this.master.isCallAccess();
	}
	
	@Override
	public boolean isMaster() {
		return false;
	}
	
	@Override
	public boolean isSlave() {
		return true;
	}
	
	@Override
	public RElementAccess getMaster() {
		return this.master;
	}
	
	@Override
	public RAstNode getNode() {
		return this.master.getNameNode();
	}
	
	@Override
	public RAstNode getNameNode() {
		return this.master.getNameNode();
	}
	
	@Override
	public RElementAccess getNextSegment() {
		return this.nextSegment;
	}
	
	@Override
	public ImList<? extends RElementAccess> getAllInUnit(final boolean includeSlaves) {
		return this.master.getAllInUnit(includeSlaves);
	}
	
	@Override
	public RElementName getScope() {
		return this.master.getScope();
	}
	
}
