/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.RElementAccess;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;


@NonNullByDefault
abstract class SubAbstractElementAccess extends RElementAccess {
	
	
	private final ElementAccess root;
	
	@Nullable SubAbstractElementAccess nextSegment;
	
	
	public SubAbstractElementAccess(final ElementAccess root) {
		this.root= root;
	}
	
	
	public final ElementAccess getRoot() {
		return this.root;
	}
	
	
	@Override
	public String getDisplayName() {
		return RElementName.createDisplayName(this, 0);
	}
	
	@Override
	public @Nullable RElementName getScope() {
		return null;
	}
	
	@Override
	public final @Nullable RElementAccess getNextSegment() {
		return this.nextSegment;
	}
	
	@Override
	public final RFrame getFrame() {
		return this.root.getFrame();
	}
	
	@Override
	public final boolean isWriteAccess() {
		return this.root.isWriteAccess();
	}
	
	@Override
	public boolean isCallAccess() {
		return this.root.isCallAccess();
	}
	
	@Override
	public boolean isFunctionAccess() {
		return this.root.isFunctionAccess();
	}
	
	@Override
	public ImList<? extends RElementAccess> getAllInUnit(final boolean includeSlaves) {
		final List<ElementAccess> all= this.root.shared.entries;
		final List<RElementAccess> elements= new ArrayList<>();
		ITER_ACCESS: for (RElementAccess element : all) {
			RElementAccess me= this.root;
			while (true) {
				me= me.getNextSegment();
				final String segmentName;
				if (me == null || (segmentName= me.getSegmentName()) == null) {
					continue ITER_ACCESS;
				}
				element= element.getNextSegment();
				if (element == null || me.getType() != element.getType() 
						|| !segmentName.equals(element.getSegmentName()) ) {
					continue ITER_ACCESS;
				}
				if (me == this) {
					if (includeSlaves || element.isMaster()) {
						elements.add(element);
					}
					continue ITER_ACCESS;
				}
			}
		}
		Collections.sort(elements, RElementAccess.NAME_POSITION_COMPARATOR);
		return ImCollections.toList(elements);
	}
	
}
