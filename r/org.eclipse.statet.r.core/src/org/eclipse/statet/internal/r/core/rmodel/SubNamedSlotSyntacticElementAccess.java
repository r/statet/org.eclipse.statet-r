/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.SubNamed;


@NonNullByDefault
final class SubNamedSlotSyntacticElementAccess extends SubAbstractElementAccess {
	
	
	private final SubNamed node;
	
	
	SubNamedSlotSyntacticElementAccess(final ElementAccess root, final SubNamed node) {
		super(root);
		this.node= node;
	}
	
	
	@Override
	public int getType() {
		return RElementName.SUB_NAMEDSLOT;
	}
	
	@Override
	public final @Nullable String getSegmentName() {
		return this.node.getSubnameChild().getText();
	}
	
	@Override
	public final RAstNode getNode() {
		return this.node;
	}
	
	@Override
	public final RAstNode getNameNode() {
		return this.node.getSubnameChild();
	}
	
}
