/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core;

import java.net.URI;

import org.eclipse.core.resources.IFile;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.model.core.impl.AbstractFilePersistenceSourceUnitFactory;
import org.eclipse.statet.ltk.model.core.impl.GenericResourceSourceUnit;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.r.core.rmodel.RElementName;


/**
 * Generic source unit for R related files.
 */
@NonNullByDefault
public abstract class BasicRResourceSourceUnit
		extends GenericResourceSourceUnit {
	
	
	public static String createResourceId(final URI uri) {
		return AbstractFilePersistenceSourceUnitFactory.createResourceId(uri);
	}
	
	@Deprecated
	public static BasicRResourceSourceUnit createTempUnit(final IFile file, final String modelTypeId) {
		final String id= AbstractFilePersistenceSourceUnitFactory.createResourceId(file);
		return new BasicRResourceSourceUnit(id, file) {
			@Override
			public String getModelTypeId() {
				return modelTypeId;
			}
			@Override
			public DocContentSections getDocumentContentInfo() {
				return null;
			}
		};
	}
	
	
	public BasicRResourceSourceUnit(final String id, final IFile file) {
		super(id, RElementName.create(RElementName.RESOURCE, file.getName()), file);
	}
	
	
	public RCoreAccess getRCoreAccess() {
		final RProject rProject= RProjects.getRProject(getResource().getProject());
		return (rProject != null) ? rProject : RCore.WORKBENCH_ACCESS;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RCoreAccess.class) {
			return (T)getRCoreAccess();
		}
		if (adapterType == PreferenceAccess.class) {
			return (T)getRCoreAccess().getPrefs();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
