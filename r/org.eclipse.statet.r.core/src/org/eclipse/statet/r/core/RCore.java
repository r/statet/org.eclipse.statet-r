/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core;

import java.util.Objects;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.r.core.pkgmanager.IRPkgManager;
import org.eclipse.statet.r.core.renv.IREnvManager;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.http.RHelpHttpService;
import org.eclipse.statet.rj.renv.core.REnv;


/**
 * Provides r core services.
 */
@NonNullByDefault
public class RCore {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.core"; //$NON-NLS-1$
	
	
	/**
	 * Content type id for R sources
	 */
	public static final String R_CONTENT_ID= "org.eclipse.statet.r.contentTypes.R"; //$NON-NLS-1$
	
	public static final IContentType R_CONTENT_TYPE;
	
	/**
	 * Content type id for Rd sources
	 */
	public static final String RD_CONTENT_ID= "org.eclipse.statet.r.contentTypes.Rd"; //$NON-NLS-1$
	
	public static final IContentType RD_CONTENT_TYPE;
	
	/**
	 * Content type id for RPkg 'DESCRIPTION' sources
	 */
	public static final String RPKG_DESCRIPTION_CONTENT_ID= "org.eclipse.statet.r.contentTypes.RPkgDescription"; //$NON-NLS-1$
	
	public static final IContentType RPKG_DESCRIPTION_CONTENT_TYPE;
	
	/**
	 * Content type id for RPkg 'NAMESPACE' sources
	 */
	public static final String RPKG_NAMESPACE_CONTENT_ID= "org.eclipse.statet.r.contentTypes.RPkgNamespace"; //$NON-NLS-1$
	
	public static final IContentType RPKG_NAMESPACE_CONTENT_TYPE;
	
	static {
		final IContentTypeManager contentTypeManager= Platform.getContentTypeManager();
		R_CONTENT_TYPE= Objects.requireNonNull(
				contentTypeManager.getContentType(R_CONTENT_ID) );
		RD_CONTENT_TYPE= Objects.requireNonNull(
				contentTypeManager.getContentType(RD_CONTENT_ID) );
		RPKG_DESCRIPTION_CONTENT_TYPE= Objects.requireNonNull(
				contentTypeManager.getContentType(RPKG_DESCRIPTION_CONTENT_ID) );
		RPKG_NAMESPACE_CONTENT_TYPE= Objects.requireNonNull(
				contentTypeManager.getContentType(RPKG_NAMESPACE_CONTENT_ID) );
	}
	
	
	/**
	 * The R environment id for the default configuration in the workbench
	 */
	public static final String DEFAULT_WORKBENCH_ENV_ID= "default-workbench"; //$NON-NLS-1$
	
	
	public static final String DEFAULT_RHELP_BROWSE_URL= RHelpHttpService.PORTABLE_URI_SCHEME + ":/browse/" + DEFAULT_WORKBENCH_ENV_ID + '/'; //$NON-NLS-1$
	
	
	public static final RCoreAccess WORKBENCH_ACCESS= RCorePlugin.getInstance().getWorkspaceRCoreAccess();
	
	
	/**
	 * Usually used, if no other context (e.g. project) specified.
	 */
	public static RCoreAccess getWorkbenchAccess() {
		return WORKBENCH_ACCESS;
	}
	
	/**
	 * Usually only used in special cases like preference dialogs.
	 */
	public static RCoreAccess getDefaultsAccess() {
		return RCorePlugin.getInstance().getDefaultsRCoreAccess();
	}
	
	/**
	 * @return the manager with with shared configurations of the R environments.
	 */
	public static IREnvManager getREnvManager() {
		return RCorePlugin.getInstance().getREnvManager();
	}
	
	public static @Nullable IRPkgManager getRPkgManager(final REnv env) {
		return RCorePlugin.getInstance().getREnvPkgManager().getManager(env);
	}
	
	public static RHelpManager getRHelpManager() {
		return RCorePlugin.getInstance().getRHelpManager();
	}
	
	public static RHelpHttpService getRHelpHttpService() {
		return RCorePlugin.getInstance().getRHelpHttpService();
	}
	
}
