/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import java.util.List;

import org.eclipse.statet.rj.renv.core.RPkg;


public interface IRPkgData extends RPkg {
	
	
	String getLicense();
	
	List<? extends RPkg> getDepends();
	List<? extends RPkg> getImports();
	List<? extends RPkg> getLinkingTo();
	List<? extends RPkg> getSuggests();
	List<? extends RPkg> getEnhances();
	
	String getPriority();
	String getRepoId();
	
}
