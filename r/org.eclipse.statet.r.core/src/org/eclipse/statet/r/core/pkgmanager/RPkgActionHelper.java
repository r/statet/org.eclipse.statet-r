/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import java.util.List;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.runtime.RLibLocationInfo;
import org.eclipse.statet.rj.renv.runtime.RuntimeRLibPaths;


public class RPkgActionHelper {
	
	
	private final RuntimeRLibPaths rLibPaths;
	
	private final boolean sameLocation;
	private final RLibLocation defaultLibLocation;
	
	
	public RPkgActionHelper(final boolean sameLocation, final RLibLocation defaultLocation,
			final RuntimeRLibPaths rLibPaths) {
		this.rLibPaths= rLibPaths;
		this.sameLocation= sameLocation;
		this.defaultLibLocation= defaultLocation;
	}
	
	
	public void updateLocation(final RPkgAction.Install action) {
		if (this.sameLocation) {
			final IRPkgInfoAndData referencePkg= action.getReferencePkg();
			if (referencePkg != null) {
				final RLibLocation libLocation= referencePkg.getLibLocation();
				final RLibLocationInfo info= this.rLibPaths.getInfo(libLocation);
				if (info != null && info.isWritable()) {
					action.setLibraryLocation(libLocation);
				}
				return;
			}
		}
		if (this.defaultLibLocation == null) {
			throw new UnsupportedOperationException("missing default location");
		}
		action.setLibraryLocation(this.defaultLibLocation);
	}
	
	
	public void update(final List<? extends RPkgAction.Install> actions) {
		for (final RPkgAction.Install action : actions) {
			updateLocation(action);
		}
	}
	
	
	@Override
	public int hashCode() {
		int h= (this.defaultLibLocation != null) ? this.defaultLibLocation.hashCode() : 0;
		if (this.sameLocation) {
			h++;
		}
		return h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RPkgActionHelper) {
			final RPkgActionHelper other= (RPkgActionHelper) obj;
			return (Objects.equals(this.defaultLibLocation, other.defaultLibLocation)
					&& (this.sameLocation == other.sameLocation) );
		}
		return false;
	}
	
}
