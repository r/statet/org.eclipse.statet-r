/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import org.eclipse.statet.rj.renv.core.BasicRPkgBuilt;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.core.RNumVersion;


public class RPkgInfo extends BasicRPkgBuilt implements IRPkgInfo {
	
	
	private final int flags;
	private final long installStamp;
	private final String repoId;
	
	
	public RPkgInfo(final String name, final RNumVersion version, final String built,
			final String title, final RLibLocation lib,
			final int flags, final long installStamp, final String repoId) {
		super(name, version,
				nonNullElse(built, ""), //$NON-NLS-1$
				nonNullElse(title, ""), //$NON-NLS-1$
				lib);
		this.flags= flags;
		this.installStamp= installStamp;
		this.repoId= (repoId != null) ? repoId.intern() : ""; //$NON-NLS-1$
	}
	
	
	@Override
	public int getFlags() {
		return this.flags;
	}
	
	@Override
	public long getInstallStamp() {
		return this.installStamp;
	}
	
	@Override
	public String getRepoId() {
		return this.repoId;
	}
	
}
