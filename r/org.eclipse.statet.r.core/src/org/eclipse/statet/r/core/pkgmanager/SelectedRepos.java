/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import java.util.List;


public class SelectedRepos implements ISelectedRepos {
	
	
	private final List<RRepo> repos;
	
	private final RRepo cranMirror;
	
	private final String biocVersion;
	private final RRepo biocMirror;
	
	
	public SelectedRepos(final List<RRepo> repos, final RRepo cranMirror,
			final String bioCVersion, final RRepo bioCMirror) {
		this.repos= repos;
		this.cranMirror= cranMirror;
		this.biocVersion= bioCVersion;
		this.biocMirror= bioCMirror;
	}
	
	
	@Override
	public List<RRepo> getRepos() {
		return this.repos;
	}
	
	@Override
	public RRepo getRepo(final String repoId) {
		if (repoId == RRepo.WS_CACHE_SOURCE_ID) {
			return RRepo.WS_CACHE_SOURCE_REPO;
		}
		if (repoId == RRepo.WS_CACHE_BINARY_ID) {
			return RRepo.WS_CACHE_BINARY_REPO;
		}
		return RPkgUtils.getRepoById(this.repos, repoId);
	}
	
	@Override
	public RRepo getCRANMirror() {
		return this.cranMirror;
	}
	
	@Override
	public RRepo getBioCMirror() {
		return this.biocMirror;
	}
	
	@Override
	public String getBioCVersion() {
		return this.biocVersion;
	}
	
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ISelectedRepos)) {
			return false;
		}
		final ISelectedRepos other= (ISelectedRepos) obj;
		return (this.repos.equals(other.getRepos())
				&& ((this.cranMirror != null) ? this.cranMirror.equals(other.getCRANMirror()) : null == other.getCRANMirror())
				&& ((this.biocMirror != null) ? this.biocMirror.equals(other.getBioCMirror()) : null == other.getBioCMirror()) );
	}
	
}
