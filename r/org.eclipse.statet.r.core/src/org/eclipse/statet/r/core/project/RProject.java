/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.project;

import org.eclipse.core.runtime.IPath;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.NullableStringPref;

import org.eclipse.statet.internal.r.core.RProjectNature;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.project.core.LtkProject;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;


@NonNullByDefault
public interface RProject extends LtkProject, RCoreAccess {
	
	
	String BUILD_PREF_QUALIFIER= RCore.BUNDLE_ID + "/build/RProject"; //$NON-NLS-1$
	
	Preference<@Nullable String> PKG_BASE_FOLDER_PATH_PREF= new NullableStringPref(
			BUILD_PREF_QUALIFIER, RProjectNature.RPKG_ROOT_FOLDER_PATH_KEY );
	
	Preference<@Nullable String> RENV_CODE_PREF= new NullableStringPref(
			BUILD_PREF_QUALIFIER, RProjectNature.RENV_CODE_KEY );
	
	String SOURCE_PREF_QUALIFIER= RCore.BUNDLE_ID + "/source/R"; //$NON-NLS-1$
	
	Preference<@Nullable String> RSOURCE_CONFIG_LANG_VERSION_PREF= new NullableStringPref(
			SOURCE_PREF_QUALIFIER, RProjectNature.R_LANG_VERSION_KEY );
	
	
	ImList<BuildpathElement> getRawBuildpath();
	
	
	/**
	 * Returns the name of the R package.
	 * 
	 * @return the name
	 */
	@Nullable String getPkgName();
	
	/**
	 * Returns the base path of the R package.
	 * 
	 * @return the full path
	 */
	@Nullable IPath getPkgRootPath();
	
}
