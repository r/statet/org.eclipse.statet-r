/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.project;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.resources.core.ProjectUtils;

import org.eclipse.statet.base.core.StatetProject;
import org.eclipse.statet.internal.r.core.Messages;
import org.eclipse.statet.internal.r.core.RProjectNature;


@NonNullByDefault
public class RProjects {
	
	
	// "org.eclipse.statet.r.natures.R"
	public static final String R_NATURE_ID= "org.eclipse.statet.r.resourceProjects.R"; //$NON-NLS-1$
	
	public static final String R_PKG_NATURE_ID= "org.eclipse.statet.r.resourceProjects.RPkg"; //$NON-NLS-1$
	
	
	public static @Nullable RProject getRProject(final @Nullable IProject project) {
		return RProjectNature.getRProject(project);
	}
	
	/**
	 * 
	 * @param project the project to setup
	 * @param monitor SubMonitor-recommended
	 * @throws CoreException
	 */
	public static void setupRProject(final IProject project,
			final @Nullable IProgressMonitor monitor) throws CoreException {
		final SubMonitor progress= SubMonitor.convert(monitor,
				NLS.bind(Messages.RProject_ConfigureTask_label, project.getName()),
				2 + 8 );
		
		final IProjectDescription description= project.getDescription();
		boolean changed= false;
		changed|= ProjectUtils.addNature(description, StatetProject.NATURE_ID);
		changed|= ProjectUtils.addNature(description, R_NATURE_ID);
		progress.worked(2);
		
		if (changed) {
			project.setDescription(description, progress.newChild(8));
		}
	}
	
	/**
	 * 
	 * @param project the project to setup
	 * @param pkgBasePath project relative path to the root folder of the R package structure
	 * @param monitor SubMonitor-recommended
	 * @throws CoreException
	 */
	public static void setupRPkgProject(final IProject project,
			final @Nullable IPath pkgBasePath,
			final @Nullable IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor,
				NLS.bind(Messages.RProject_ConfigureTask_label, project.getName()),
				2 + 8 );
		
		final IProjectDescription description= project.getDescription();
		boolean changed= false;
		changed|= ProjectUtils.addNature(description, StatetProject.NATURE_ID);
		changed|= ProjectUtils.addNature(description, R_NATURE_ID);
		changed|= ProjectUtils.addNature(description, R_PKG_NATURE_ID);
		m.worked(2);
		
		if (changed) {
			project.setDescription(description, m.newChild(8));
		}
		
		m.setWorkRemaining(2);
		
		final RProjectNature rProject= nonNullAssert(RProjectNature.getRProject(project));
		
		PreferenceUtils.setPrefValue(rProject.getProjectContext(), RProject.PKG_BASE_FOLDER_PATH_PREF,
				(pkgBasePath != null) ? pkgBasePath.toPortableString() : null,
				PreferenceUtils.FLUSH_SYNC );
	}
	
}
