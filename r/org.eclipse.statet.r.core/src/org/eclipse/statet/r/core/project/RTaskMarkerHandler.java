/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.project;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.issues.core.Issues;
import org.eclipse.statet.ltk.issues.core.impl.TaskMarkerHandler;


@NonNullByDefault
public class RTaskMarkerHandler extends TaskMarkerHandler {
	
	
	public RTaskMarkerHandler() {
		super(RIssues.TASK_MARKER_TYPE);
	}
	
	
	public void init(final RProject project) {
		final var taskTags= Issues.loadTaskTags(project.getPrefs());
		initTaskPattern(taskTags);
	}
	
}
