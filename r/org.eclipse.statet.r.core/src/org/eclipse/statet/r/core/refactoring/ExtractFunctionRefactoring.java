/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.ReplaceEdit;

import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.core.refactoring.Messages;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringDescriptor;
import org.eclipse.statet.ltk.refactoring.core.RefactoringChange;
import org.eclipse.statet.ltk.refactoring.core.RefactoringMessages;
import org.eclipse.statet.ltk.refactoring.core.SourceUnitChange;
import org.eclipse.statet.ltk.refactoring.core.TextChangeCompatibility;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RUtil;
import org.eclipse.statet.r.core.rmodel.RElementAccess;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RModelManager;
import org.eclipse.statet.r.core.rmodel.RSourceFrame;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.RHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.ast.Assignment;
import org.eclipse.statet.r.core.source.ast.Block;
import org.eclipse.statet.r.core.source.ast.FDef;
import org.eclipse.statet.r.core.source.ast.GenericVisitor;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;


public class ExtractFunctionRefactoring extends Refactoring {
	
	
	private class VariableSearcher extends GenericVisitor {
		
		private final int start= ExtractFunctionRefactoring.this.operationRegion.getStartOffset();
		private final int stop= ExtractFunctionRefactoring.this.operationRegion.getEndOffset();
		
		@Override
		public void visitNode(final RAstNode node) throws InvocationTargetException {
			if (node.getStartOffset() >= this.stop || node.getEndOffset() < this.start) {
				return;
			}
			final List<Object> attachments= node.getAttachments();
			for (final Object attachment : attachments) {
				if (attachment instanceof RElementAccess) {
					final RElementAccess access= (RElementAccess) attachment;
					if (access.getType() != RElementName.MAIN_DEFAULT
							|| access.getSegmentName() == null) {
						continue;
					}
					final RAstNode nameNode= access.getNameNode();
					if (nameNode.getStartOffset() >= this.start && nameNode.getEndOffset() <= this.stop) {
						add(access);
					}
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FDef node) throws InvocationTargetException {
		}
		
		private void add(final RElementAccess access) {
			final RFrame frame= access.getFrame();
			if (!(frame instanceof RSourceFrame)
					|| frame.getFrameType() == RFrame.PACKAGE) {
				return;
			}
			final String name= access.getSegmentName();
			Variable variable= ExtractFunctionRefactoring.this.variablesMap.get(name);
			if (variable == null) {
				variable= new Variable(name);
				ExtractFunctionRefactoring.this.variablesMap.put(name, variable);
			}
			variable.checkAccess(access);
		}
		
	}
	
	public class Variable {
		
		
		private final String name;
		private boolean asArgument;
		private boolean asArgumentDefault;
		private RElementAccess firstAccess;
		private RElementAccess lastAccess;
		
		
		public Variable(final String name) {
			this.name= name;
		}
		
		
		void checkAccess(final RElementAccess access) {
			if (this.firstAccess == null
					|| access.getNode().getStartOffset() < this.firstAccess.getNode().getStartOffset()) {
				this.firstAccess= access;
				this.asArgumentDefault= this.asArgument= (!access.isWriteAccess() && !access.isFunctionAccess());
			}
			if (this.lastAccess == null
					|| access.getNode().getStartOffset() > this.lastAccess.getNode().getStartOffset()) {
				this.lastAccess= access;
			}
		}
		
		public String getName() {
			return this.name;
		}
		
		public boolean getUseAsArgumentDefault() {
			return this.asArgumentDefault;
		}
		
		public boolean getUseAsArgument() {
			return this.asArgument;
		}
		
		public void setUseAsArgument(final boolean enable) {
			this.asArgument= enable;
		}
		
	}
	
	
	private final RRefactoringAdapter adapter= new RRefactoringAdapter();
	private final ElementSet elementSet;
	
	private TextRegion selectionRegion;
	private TextRegion operationRegion;
	
	private final RSourceUnit sourceUnit;
	private RAstNode[] expressions;
	
//	private RAstNode container;
	private Map<String, Variable> variablesMap;
	private List<Variable> variablesList;
	private String functionName;
	
	
	/**
	 * Creates a new extract function refactoring.
	 * @param su the source unit
	 * @param region (selected) region of the statements to extract
	 */
	public ExtractFunctionRefactoring(final RSourceUnit su, final TextRegion selection) {
		this.sourceUnit= su;
		this.elementSet= new ElementSet(su);
		
		if (selection != null && selection.getStartOffset() >= 0 && selection.getLength() >= 0) {
			this.selectionRegion= selection;
		}
	}
	
	
	@Override
	public String getName() {
		return Messages.ExtractFunction_label;
	}
	
	public String getIdentifier() {
		return RRefactoring.EXTRACT_FUNCTION_REFACTORING_ID;
	}
	
	
	public void setFunctionName(final String newName) {
		this.functionName= newName;
	}
	
	public String getFunctionName() {
		return this.functionName;
	}
	
	public List<Variable> getVariables() {
		return this.variablesList;
	}
	
	
	@Override
	public RefactoringStatus checkInitialConditions(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 6);
		try {
			if (this.selectionRegion != null) {
				this.sourceUnit.connect(m.newChild(1));
				try {
					final AbstractDocument document= this.sourceUnit.getDocument(monitor);
					final RHeuristicTokenScanner scanner= this.adapter.getScanner(this.sourceUnit);
					
					final RSourceUnitModelInfo modelInfo= (RSourceUnitModelInfo) this.sourceUnit.getModelInfo(RModel.R_TYPE_ID, RModelManager.MODEL_FILE, m.newChild(1));
					if (modelInfo != null) {
						final TextRegion region= this.adapter.trimToAstRegion(document,
								this.selectionRegion, scanner );
						final AstInfo ast= modelInfo.getAst();
						if (ast != null) {
							final AstSelection astSelection= AstSelection.search(ast.getRoot(),
									region.getStartOffset(), region.getEndOffset(),
									AstSelection.MODE_COVERING_SAME_LAST );
							final AstNode covering= astSelection.getCovering();
							if (covering instanceof RAstNode) {
								final RAstNode rCovering= (RAstNode) covering;
								if ((rCovering.getStartOffset() == region.getStartOffset() && rCovering.getLength() == region.getLength())
										|| (rCovering.getNodeType() != NodeType.SOURCELINES && rCovering.getNodeType() != NodeType.BLOCK)) {
									this.expressions= new RAstNode[] { rCovering };
								}
								else {
									final int count= rCovering.getChildCount();
									final List<RAstNode> childList= new ArrayList<>(count);
									int i= 0;
									for (; i < count; i++) {
										final RAstNode child= rCovering.getChild(i);
										if (child == astSelection.getChildFirstTouching()) {
											break;
										}
									}
									for (; i < count; i++) {
										final RAstNode child= rCovering.getChild(i);
										childList.add(child);
										if (child == astSelection.getChildLastTouching()) {
											break;
										}
									}
									if (!childList.isEmpty()) {
										this.expressions= childList.toArray(new RAstNode[childList.size()]);
									}
								}
							}
						}
					}
					
					if (this.expressions != null) {
						final TextRegion region= new BasicTextRegion(
								this.expressions[0].getStartOffset(),
								this.expressions[this.expressions.length - 1].getEndOffset() );
						this.operationRegion= this.adapter.expandSelectionRegion(document,
								region, this.selectionRegion, scanner );
					}
				}
				finally {
					this.sourceUnit.disconnect(m.newChild(1));
				}
			}
			
			if (this.expressions == null) {
				return RefactoringStatus.createFatalErrorStatus(Messages.ExtractFunction_error_InvalidSelection_message);
			}
			final RefactoringStatus result= new RefactoringStatus();
			this.adapter.checkInitialToModify(result, this.elementSet);
			m.worked(1);
			
			if (result.hasFatalError()) {
				return result;
			}
			
			checkExpressions(result);
			m.worked(2);
			return result;
		}
		finally {
			m.done();
		}
	}
	
	private void checkExpressions(final RefactoringStatus result) {
		for (final RAstNode node : this.expressions) {
			if (RAsts.hasErrors(node)) {
				result.merge(RefactoringStatus.createWarningStatus(Messages.ExtractFunction_warning_SelectionSyntaxError_message));
				break;
			}
		}
		if (this.selectionRegion != null
				&& (this.selectionRegion.getStartOffset() != this.operationRegion.getStartOffset()
						|| this.selectionRegion.getLength() != this.operationRegion.getLength() )) {
			result.merge(RefactoringStatus.createWarningStatus(Messages.ExtractFunction_warning_ChangedRange_message));
		}
		
		this.variablesMap= new HashMap<>();
		final VariableSearcher searcher= new VariableSearcher();
		try {
			for (final RAstNode node : this.expressions) {
				node.acceptInR(searcher);
			}
		} catch (final InvocationTargetException e) {}
		this.variablesList= new ArrayList<>(this.variablesMap.values());
		Collections.sort(this.variablesList, new Comparator<Variable>() {
			@Override
			public int compare(final Variable o1, final Variable o2) {
				return RElementAccess.NAME_POSITION_COMPARATOR.compare(o1.firstAccess, o2.firstAccess);
			}
		});
		this.functionName= ""; //$NON-NLS-1$
	}
	
	public RefactoringStatus checkFunctionName(final String name) {
		final String message= this.adapter.validateIdentifier(name, "The function name");
		if (message != null) {
			return RefactoringStatus.createFatalErrorStatus(message);
		}
		return new RefactoringStatus();
	}
	
	
	@Override
	public RefactoringStatus checkFinalConditions(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label, 3);
		try {
			final RefactoringStatus status= checkFunctionName(this.functionName);
			this.adapter.checkFinalToModify(status, this.elementSet, m.newChild(2));
			return status;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public Change createChange(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_CreateChanges_label, 3);
		try {
			final TextFileChange textFileChange= new SourceUnitChange(this.sourceUnit);
			if (this.sourceUnit.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
				textFileChange.setSaveMode(TextFileChange.LEAVE_DIRTY);
			}
			createChanges(textFileChange, m.newChild(2));
			
			final Map<String, String> arguments= new HashMap<>();
			final String varName= RRefactoringAdapter.getUnquotedIdentifier(this.functionName);
			final String description= NLS.bind(Messages.ExtractFunction_Descriptor_description,
					RUtil.formatVarName(varName) );
			final IProject resource= this.elementSet.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ? NLS.bind(RefactoringMessages.Common_Source_Project_label, project) : RefactoringMessages.Common_Source_Workspace_label;
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			m.worked(1);
			
			return new RefactoringChange(descriptor,
					Messages.ExtractFunction_label, 
					new Change[] { textFileChange });
		}
		catch (final BadLocationException e) {
			throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, "Unexpected error (concurrent change?)", e));
		}
		finally {
			m.done();
		}
	}
	
	private void createChanges(final TextFileChange change, final SubMonitor m) throws BadLocationException, CoreException {
		m.setWorkRemaining(3 + 2 * 4);
		
		this.sourceUnit.connect(m.newChild(1));
		try {
			final AbstractDocument document= this.sourceUnit.getDocument(m.newChild(1));
			final RHeuristicTokenScanner scanner= this.adapter.getScanner(this.sourceUnit);
			final RCodeStyleSettings codeStyle= RRefactoringAdapter.getCodeStyle(this.sourceUnit);
			final StringBuilder sb= new StringBuilder();
			
			final String nl= document.getDefaultLineDelimiter();
			final String defAssign= " <- ";
			final String argAssign= codeStyle.getArgAssignString();
			
			RAstNode firstParentChild= this.expressions[0];
			while (true) {
				final RAstNode parent= firstParentChild.getRParent();
				if (parent == null
						|| parent.getNodeType() == NodeType.SOURCELINES || parent.getNodeType() == NodeType.BLOCK) {
					break;
				}
				firstParentChild= parent;
			}
			
			int startOffset;
			int endOffset;
			final RAstNode lastNode;
			if (this.expressions.length == 1 && this.expressions[0].getNodeType() == NodeType.BLOCK) {
				final Block block= (Block) this.expressions[0];
				startOffset= block.getStartOffset() + 1;
				endOffset= block.getBlockCloseOffset();
				if (endOffset == NA_OFFSET) {
					endOffset= block.getEndOffset();
				}
				lastNode= (block.getChildCount() > 0) ?
						block.getChild(block.getChildCount() - 1) : block.getChild(123);
			}
			else {
				startOffset= this.expressions[0].getStartOffset();
				lastNode= this.expressions[this.expressions.length - 1];
				endOffset= lastNode.getEndOffset();
			}
			
			// define fun
			sb.setLength(0);
			sb.append(this.functionName);
			sb.append(defAssign);
			sb.append("function("); //$NON-NLS-1$
			boolean hasArguments= false;
			for (final Variable variable : this.variablesList) {
				if (variable.getUseAsArgument()) {
					sb.append(variable.getName());
					sb.append(", "); //$NON-NLS-1$
					hasArguments= true;
				}
			}
			if (hasArguments) {
				sb.delete(sb.length() - 2, sb.length());
			}
			sb.append(')');
			if (codeStyle.getNewlineFDefBodyBlockBefore()) {
				sb.append(nl);
			}
			else {
				sb.append(' ');
			}
			sb.append('{');
			sb.append(nl);
			if (startOffset < endOffset) {
				final IRegion lastLine= document.getLineInformationOfOffset(endOffset - 1);
				endOffset= Math.min(endOffset, lastLine.getOffset() + lastLine.getLength());
			}
			sb.append(document.get(startOffset, endOffset - startOffset));
			sb.append(nl);
			sb.append("}"); //$NON-NLS-1$
			sb.append(nl);
			final String fdef= RRefactoringAdapter.indent(sb, document, firstParentChild.getStartOffset(),
					this.sourceUnit, scanner );
			
			final TextRegion region= this.adapter.expandWhitespaceBlock(document, this.operationRegion, scanner);
			final int insertOffset= this.adapter.expandWhitespaceBlock(document,
					this.adapter.expandSelectionRegion(document,
							new BasicTextRegion(firstParentChild.getStartOffset()), this.operationRegion, scanner ),
					scanner ).getStartOffset();
			if (insertOffset == region.getStartOffset()) {
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_ReplaceOldWithFunctionDef_name,
						new ReplaceEdit(insertOffset, region.getLength(), fdef) );
			}
			else {
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_DeleteOld_name,
						new DeleteEdit(region.getStartOffset(), region.getLength()) );
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_AddFunctionDef_name,
						new InsertEdit(insertOffset, fdef) );
			}
			m.worked(4);
			
			// replace by fun call
			{	sb.setLength(0);
				if (firstParentChild == this.expressions[0] && lastNode.getNodeType() == NodeType.A_LEFT) {
					final Assignment assignment= (Assignment) lastNode;
					sb.append(document.get(assignment.getTargetChild().getStartOffset(), assignment.getTargetChild().getLength()));
					sb.append(" "); //$NON-NLS-1$
					sb.append(assignment.getOperator(0).text);
					sb.append(" "); //$NON-NLS-1$
				}
				sb.append(this.functionName);
				sb.append("("); //$NON-NLS-1$
				for (final Variable variable : this.variablesList) {
					if (variable.getUseAsArgument()) {
						sb.append(variable.getName());
						sb.append(argAssign);
						sb.append(variable.getName());
						sb.append(", "); //$NON-NLS-1$
					}
				}
				if (hasArguments) {
					sb.delete(sb.length() - 2, sb.length());
				}
				sb.append(")"); //$NON-NLS-1$
				if (firstParentChild == this.expressions[0]) {
					sb.append(nl);
				}
				
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_AddFunctionCall_name,
						new InsertEdit(region.getEndOffset(), sb.toString()) );
				m.worked(4);
			}
		}
		finally {
			this.sourceUnit.disconnect(m.newChild(1));
		}
	}
	
}
