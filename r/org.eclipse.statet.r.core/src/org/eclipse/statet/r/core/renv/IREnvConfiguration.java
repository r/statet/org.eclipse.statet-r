/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.renv;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.RLibGroup;


/**
 * Configuration and properties of an R "runtime" environment.
 */
public interface IREnvConfiguration extends REnvConfiguration {
	
	
	String PROP_NAME= "name"; //$NON-NLS-1$
	
	String PROP_ID= "id"; //$NON-NLS-1$
	
	String PROP_RHOME= "RHomeDirectory"; //$NON-NLS-1$
	
	String PROP_SUBARCH= "RArch"; //$NON-NLS-1$
	
	String PROP_ROS= "ROS"; //$NON-NLS-1$
	
	String PROP_RLIBS= "RLibraries"; //$NON-NLS-1$
	
	String PROP_RDOC_DIRECTORY= "RDocDirectory"; //$NON-NLS-1$
	
	String PROP_RSHARE_DIRECTORY= "RShareDirectory"; //$NON-NLS-1$
	
	String PROP_RINCLUDE_DIRECTORY= "RIncludeDirectory"; //$NON-NLS-1$
	
	String PROP_STATE_SHARED_TYPE= "stateSharedType"; //$NON-NLS-1$
	String PROP_STATE_SHARED_DIRECTORY= "stateSharedDirectory"; //$NON-NLS-1$
	String PROP_STATE_SHARED_SERVER= "stateSharedServer"; //$NON-NLS-1$
	
	
	String USER_LOCAL_TYPE= "user-local"; //$NON-NLS-1$
	
	String USER_REMOTE_TYPE= "user-remote"; //$NON-NLS-1$
	
	String EPLUGIN_LOCAL_TYPE= "eplugin-local"; //$NON-NLS-1$
	
	String CONTRIB_TEMP_REMOTE_TYPE= "contrib.temp-remote"; //$NON-NLS-1$
	
	
	static enum Exec {
		COMMON,
		CMD,
		TERM;
	}
	
	
	static interface WorkingCopy extends IREnvConfiguration {
		
		
		void load(IREnvConfiguration template);
		
		
		void setName(String label);
		
		
		void setRHomeDirectory(String directory);
		
		void setRArch(String arch);
		
		void setROS(String type);
		
		void setRDocDirectory(String directory);
		
		void setRShareDirectory(String directory);
		
		void setRIncludeDirectory(String directory);
		
		@Override
		ImList<? extends RLibGroupWorkingCopy> getRLibGroups();
		
		@Override
		RLibGroupWorkingCopy getRLibGroup(String id);
		
		boolean isValidRHomeLocation(IFileStore rHome);
		
		List<String> searchAvailableSubArchs(IFileStore rHome);
		
		
		void setStateSharedType(String type);
		void setStateSharedDirectory(String directory);
		void setStateSharedServer(String url);
		
	}
	
	
	String getRVersion();
	
	String getPrefNodeQualifier();
	
	
	String getType();
	
	boolean isEditable();
	
	@Override
	boolean isLocal();
	@Override
	boolean isRemote();
	
	
	WorkingCopy createWorkingCopy();
	
	
	String getROS();
	
	@Override
	ImList<? extends RLibGroup> getRLibGroups();
	
	Map<String, String> getEnvironmentsVariables() throws CoreException;
	
	
	IStatus validate();
	
	List<String> getExecCommand(final Exec execType)
			throws CoreException;
	
	List<String> getExecCommand(String arg1, Set<Exec> execTypes)
			throws CoreException;
	
}
