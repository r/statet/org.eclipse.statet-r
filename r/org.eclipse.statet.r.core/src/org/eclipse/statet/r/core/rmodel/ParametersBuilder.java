/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.Builder;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.Parameters.Parameter;


/**
 * Builder for {@link Parameters}
 */
@NonNullByDefault
public final class ParametersBuilder implements Builder<Parameters> {
	
	
	private final List<Parameter> parameters= new ArrayList<>();
	
	
	public ParametersBuilder() {
	}
	
	
	public ParametersBuilder add(final @Nullable String name) {
		this.parameters.add(new Parameter(this.parameters.size(), name, 0, null));
		return this;
	}
	
	public ParametersBuilder add(final @Nullable String... name) {
		for (int i= 0; i < name.length; i++) {
			this.parameters.add(new Parameter(this.parameters.size(), name[i], 0, null));
		}
		return this;
	}
	
	public ParametersBuilder add(final @Nullable String name, final int type) {
		this.parameters.add(new Parameter(this.parameters.size(), name, type, null));
		return this;
	}
	
	public ParametersBuilder add(final @Nullable String name, final int type,
			final @Nullable String className) {
		this.parameters.add(new Parameter(this.parameters.size(), name, type, className));
		return this;
	}
	
	
	@Override
	public Parameters build() {
		return new Parameters(this.parameters.toArray(new @NonNull Parameter[this.parameters.size()]));
	}
	
}
