/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import static org.eclipse.statet.r.core.rmodel.Parameters.CLASS_NAME;
import static org.eclipse.statet.r.core.rmodel.Parameters.HELP_TOPIC_NAME;
import static org.eclipse.statet.r.core.rmodel.Parameters.METHOD_NAME;
import static org.eclipse.statet.r.core.rmodel.Parameters.METHOD_OBJ;
import static org.eclipse.statet.r.core.rmodel.Parameters.NAME_AS_STRING;
import static org.eclipse.statet.r.core.rmodel.Parameters.NAME_AS_SYMBOL;
import static org.eclipse.statet.r.core.rmodel.Parameters.PACKAGE_NAME;
import static org.eclipse.statet.r.core.rmodel.Parameters.UNSPECIFIC_NAME;
import static org.eclipse.statet.r.core.rmodel.Parameters.UNSPECIFIC_OBJ;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RCoreFunctions {
	
	
	public static final RCoreFunctions DEFAULT= new RCoreFunctions();
	
	
	public static final String BASE_PACKAGE_NAME= "base";
	
	public static final String BASE_ASSIGN_NAME= "assign";
	public final Parameters BASE_ASSIGN_parameters;
	
	public static final String BASE_REMOVE_NAME= "remove";
	public static final String BASE_REMOVE_ALIAS_RM= "rm";
	public final Parameters BASE_REMOVE_parameters;
	
	public static final String BASE_EXISTS_NAME= "exists";
	public final Parameters BASE_EXISTS_parameters;
	
	public static final String BASE_GET_NAME= "get";
	public final Parameters BASE_GET_parameters;
	
	public static final String BASE_SAVE_NAME= "save";
	public final Parameters BASE_SAVE_parameters;
	
	public static final String BASE_CALL_NAME= "call";
	public final Parameters BASE_CALL_parameters;
	
	public static final String BASE_DOCALL_NAME= "do.call";
	public final Parameters BASE_DOCALL_parameters;
	
	
	public static final String BASE_LIBRARY_NAME= "library";
	public final Parameters BASE_LIBRARY_parameters;
	
	public static final String BASE_REQUIRE_NAME= "require";
	public final Parameters BASE_REQUIRE_parameters;
	
	public static final String BASE_GLOBALENV_NAME= "globalenv";
	public final Parameters BASE_GLOBALENV_parameters;
	
	public static final String BASE_TOPENV_NAME= "topenv";
	public final Parameters BASE_TOPENV_parameters;
	
	public static final String BASE_GETNAMESPACE_NAME= "getNamespace";
	public final Parameters BASE_GETNAMESPACE_parameters;
	
	public static final String BASE_GETNAMESPACENAME_NAME= "getNamespaceName";
	public final Parameters BASE_GETNAMESPACENAME_parameters;
	
	public static final String BASE_GETNAMESPACEVERSION_NAME= "getNamespaceVersion";
	public final Parameters BASE_GETNAMESPACEVERSION_parameters;
	
	public static final String BASE_GETNAMESPACEEXPORTS_NAME= "getNamespaceExports";
	public final Parameters BASE_GETNAMESPACEEXPORTS_parameters;
	
	public static final String BASE_GETNAMESPACEIMPORTS_NAME= "getNamespaceImports";
	public final Parameters BASE_GETNAMESPACEIMPORTS_parameters;
	
	public static final String BASE_GETNAMESPACEUSERS_NAME= "getNamespaceUsers";
	public final Parameters BASE_GETNAMESPACEUSERS_parameters;
	
	public static final String BASE_GETEXPORTEDVALUE_NAME= "getExportedValue";
	public final Parameters BASE_GETEXPORTEDVALUE_parameters;
	
	public static final String BASE_ATTACHNAMESPACE_NAME= "attachNamespace";
	public final Parameters BASE_ATTACHNAMESPACE_parameters;
	
	public static final String BASE_LOADNAMESPACE_NAME= "loadNamespace";
	public final Parameters BASE_LOADNAMESPACE_parameters;
	
	public static final String BASE_REQUIRENAMESPACE_NAME= "requireNamespace";
	public final Parameters BASE_REQUIRENAMESPACE_parameters;
	
	public static final String BASE_ISNAMESPACELOADED_NAME= "isNamespaceLoaded";
	public final Parameters BASE_ISNAMESPACELOADED_parameters;
	
	public static final String BASE_UNLOADNAMESPACE_NAME= "unloadNamespace";
	public final Parameters BASE_UNLOADNAMESPACE_parameters;
	
	
	public static final String BASE_C_NAME= "c";
	public final Parameters BASE_C_parameters;
	
	public static final String BASE_DATAFRAME_NAME= "data.frame";
	public final Parameters BASE_DATAFRAME_parameters;
	
	public static final String BASE_USEMETHOD_NAME= "UseMethod";
	public final Parameters BASE_USEMETHOD_parameters;
	
	public static final String BASE_NEXTMETHOD_NAME= "NextMethod";
	public final Parameters BASE_NEXTMETHOD_parameters;
	
	
	public static final String UTILS_PACKAGE_NAME= "utils";
	
	public static final String UTILS_METHODS_NAME= "methods";
	public final Parameters UTILS_METHODS_parameters;
	
	public static final String UTILS_GETS3METHOD_NAME= "getS3method";
	public final Parameters UTILS_GETS3METHOD_parameters;
	
	public static final String UTILS_HELP_NAME= "help";
	public final Parameters UTILS_HELP_parameters;
	
	
	public static final String METHODS_PACKAGE_NAME= "methods";
	
	public static final String METHODS_SETGENERIC_NAME= "setGeneric";
	public final Parameters METHODS_SETGENERIC_parameters;
	
	public static final String METHODS_SETGROUPGENERIC_NAME= "setGroupGeneric";
	public final Parameters METHODS_SETGROUPGENERIC_parameters;
	
	public static final String METHODS_REMOVEGENERIC_NAME= "removeGeneric";
	public final Parameters METHODS_REMOVEGENERIC_parameters;
	
	public static final String METHODS_ISGENERIC_NAME= "isGeneric";
	public final Parameters METHODS_ISGENERIC_parameters;
	
	public static final String METHODS_ISGROUP_NAME= "isGroup";
	public final Parameters METHODS_ISGROUP_parameters;
	
	public static final String METHODS_SIGNATURE_NAME= "signature";
	public final Parameters METHODS_SIGNATURE_parameters;
	
	
	public static final String METHODS_SETCLASS_NAME= "setClass";
	public final Parameters METHODS_SETCLASS_parameters;
	
	public static final String METHODS_SETCLASSUNION_NAME= "setClassUnion";
	public final Parameters METHODS_SETCLASSUNION_parameters;
	
	public static final String METHODS_REPRESENTATION_NAME= "representation";
	public final Parameters METHODS_REPRESENTATION_parameters;
	
	public static final String METHODS_PROTOTYPE_NAME= "prototype";
	public final Parameters METHODS_PROTOTYPE_parameters;
	
	public static final String METHODS_SETIS_NAME= "setIs";
	public final Parameters METHODS_SETIS_parameters;
	
	public static final String METHODS_REMOVECLASS_NAME= "removeClass";
	public final Parameters METHODS_REMOVECLASS_parameters;
	
	public static final String METHODS_RESETCLASS_NAME= "resetClass";
	public final Parameters METHODS_RESETCLASS_parameters;
	
	public static final String METHODS_SETAS_NAME= "setAs";
	public final Parameters METHODS_SETAS_parameters;
	
	public static final String METHODS_SETVALIDITY_NAME= "setValidity";
	public final Parameters METHODS_SETVALIDITY_parameters;
	
	public static final String METHODS_ISCLASS_NAME= "isClass";
	public final Parameters METHODS_ISCLASS_parameters;
	
	public static final String METHODS_EXTENDS_NAME= "extends";
	public final Parameters METHODS_EXTENDS_parameters;
	
	public static final String METHODS_GETCLASS_NAME= "getClass";
	public final Parameters METHODS_GETCLASS_parameters;
	
	public static final String METHODS_GETCLASSDEF_NAME= "getClassDef";
	public final Parameters METHODS_GETCLASSDEF_parameters;
	
	public static final String METHODS_FINDCLASS_NAME= "findClass";
	public final Parameters METHODS_FINDCLASS_parameters;
	
	
	public static final String METHODS_NEW_NAME= "new";
	public final Parameters METHODS_NEW_parameters;
	
	public static final String METHODS_AS_NAME= "as";
	public final Parameters METHODS_AS_parameters;
	
	public static final String METHODS_IS_NAME= "is";
	public final Parameters METHODS_IS_parameters;
	
	
	public static final String METHODS_SETMETHOD_NAME= "setMethod";
	public final Parameters METHODS_SETMETHOD_parameters;
	
	public static final String METHODS_REMOVEMETHOD_NAME= "removeMethod";
	public final Parameters METHODS_REMOVEMETHOD_parameters;
	
	public static final String METHODS_REMOVEMETHODS_NAME= "removeMethods";
	public final Parameters METHODS_REMOVEMETHODS_parameters;
	
	public static final String METHODS_EXISTSMETHOD_NAME= "existsMethod";
	public final Parameters METHODS_EXISTSMETHOD_parameters;
	
	public static final String METHODS_HASMETHOD_NAME= "hasMethod";
	public final Parameters METHODS_HASMETHOD_parameters;
	
	public static final String METHODS_GETMETHOD_NAME= "getMethod";
	public final Parameters METHODS_GETMETHOD_parameters;
	
	public static final String METHODS_SELECTMETHOD_NAME= "selectMethod";
	public final Parameters METHODS_SELECTMETHOD_parameters;
	
	public static final String METHODS_GETMETHODS_NAME= "getMethods";
	public final Parameters METHODS_GETMETHODS_parameters;
	
	public static final String METHODS_FINDMETHOD_NAME= "findMethod";
	public final Parameters METHODS_FINDMETHOD_parameters;
	
	public static final String METHODS_DUMPMETHOD_NAME= "dumpMethod";
	public final Parameters METHODS_DUMPMETHOD_parameters;
	
	public static final String METHODS_DUMPMETHODS_NAME= "dumpMethods";
	public final Parameters METHODS_DUMPMETHODS_parameters;
	
	public static final String METHODS_SLOT_NAME= "slot";
	public static final String METHODS_SLOT_assign_NAME= "slot<-";
	public final Parameters METHODS_SLOT_parameters;
	
	
	private final ImSet<String> packageNames;
	
	private final HashMap<String, Parameters> nameDefMap= new HashMap<>();
	
	private final Map<String, Parameters> nameDefMapIm= Collections.unmodifiableMap(this.nameDefMap);
	
	
	protected RCoreFunctions() {
		this.packageNames= ImCollections.newSet(
				BASE_PACKAGE_NAME,
				UTILS_PACKAGE_NAME,
				METHODS_PACKAGE_NAME );
		
		this.BASE_ASSIGN_parameters= createBaseAssign();
		this.nameDefMap.put(BASE_ASSIGN_NAME, this.BASE_ASSIGN_parameters);
		
		this.BASE_REMOVE_parameters= createBaseRemove();
		this.nameDefMap.put(BASE_REMOVE_NAME, this.BASE_REMOVE_parameters);
		this.nameDefMap.put(BASE_REMOVE_ALIAS_RM, this.BASE_REMOVE_parameters);
		
		this.BASE_EXISTS_parameters= createBaseExists();
		this.nameDefMap.put(BASE_EXISTS_NAME, this.BASE_EXISTS_parameters);
		
		this.BASE_GET_parameters= createBaseGet();
		this.nameDefMap.put(BASE_GET_NAME, this.BASE_GET_parameters);
		
		this.BASE_SAVE_parameters= createBaseSave();
		this.nameDefMap.put(BASE_SAVE_NAME, this.BASE_SAVE_parameters);
		
		this.BASE_CALL_parameters= createBaseCall();
		this.nameDefMap.put(BASE_CALL_NAME, this.BASE_CALL_parameters);
		
		this.BASE_DOCALL_parameters= createBaseDoCall();
		this.nameDefMap.put(BASE_DOCALL_NAME, this.BASE_DOCALL_parameters);
		
		
		this.BASE_LIBRARY_parameters= createBaseLibrary();
		this.nameDefMap.put(BASE_LIBRARY_NAME, this.BASE_LIBRARY_parameters);
		
		this.BASE_REQUIRE_parameters= createBaseRequire();
		this.nameDefMap.put(BASE_REQUIRE_NAME, this.BASE_REQUIRE_parameters);
		
		this.BASE_GLOBALENV_parameters= createBaseGlobalenv();
		this.nameDefMap.put(BASE_GLOBALENV_NAME, this.BASE_GLOBALENV_parameters);
		
		this.BASE_TOPENV_parameters= createBaseTopenv();
		this.nameDefMap.put(BASE_TOPENV_NAME, this.BASE_TOPENV_parameters);
		
		this.BASE_GETNAMESPACE_parameters= createBaseGetNamespace();
		this.nameDefMap.put(BASE_GETNAMESPACE_NAME, this.BASE_GETNAMESPACE_parameters);
		
		this.BASE_GETNAMESPACENAME_parameters= createBaseGetNamespaceName();
		this.nameDefMap.put(BASE_GETNAMESPACENAME_NAME, this.BASE_GETNAMESPACENAME_parameters);
		
		this.BASE_GETNAMESPACEVERSION_parameters= createBaseGetNamespaceVersion();
		this.nameDefMap.put(BASE_GETNAMESPACEVERSION_NAME, this.BASE_GETNAMESPACEVERSION_parameters);
		
		this.BASE_GETNAMESPACEEXPORTS_parameters= createBaseGetNamespaceExports();
		this.nameDefMap.put(BASE_GETNAMESPACEEXPORTS_NAME, this.BASE_GETNAMESPACEEXPORTS_parameters);
		
		this.BASE_GETNAMESPACEIMPORTS_parameters= createBaseGetNamespaceImports();
		this.nameDefMap.put(BASE_GETNAMESPACEIMPORTS_NAME, this.BASE_GETNAMESPACEIMPORTS_parameters);
		
		this.BASE_GETNAMESPACEUSERS_parameters= createBaseGetNamespaceUsers();
		this.nameDefMap.put(BASE_GETNAMESPACEUSERS_NAME, this.BASE_GETNAMESPACEUSERS_parameters);
		
		this.BASE_GETEXPORTEDVALUE_parameters= createBaseGetExportedValue();
		this.nameDefMap.put(BASE_GETEXPORTEDVALUE_NAME, this.BASE_GETEXPORTEDVALUE_parameters);
		
		this.BASE_ATTACHNAMESPACE_parameters= createBaseAttachNamespace();
		this.nameDefMap.put(BASE_ATTACHNAMESPACE_NAME, this.BASE_ATTACHNAMESPACE_parameters);
		
		this.BASE_LOADNAMESPACE_parameters= createBaseLoadNamespace();
		this.nameDefMap.put(BASE_LOADNAMESPACE_NAME, this.BASE_LOADNAMESPACE_parameters);
		
		this.BASE_REQUIRENAMESPACE_parameters= createBaseRequireNamespace();
		this.nameDefMap.put(BASE_REQUIRENAMESPACE_NAME, this.BASE_REQUIRENAMESPACE_parameters);
		
		this.BASE_ISNAMESPACELOADED_parameters= createBaseIsNamespaceLoaded();
		this.nameDefMap.put(BASE_ISNAMESPACELOADED_NAME, this.BASE_ISNAMESPACELOADED_parameters);
		
		this.BASE_UNLOADNAMESPACE_parameters= createBaseUnloadNamespace();
		this.nameDefMap.put(BASE_UNLOADNAMESPACE_NAME, this.BASE_UNLOADNAMESPACE_parameters);
		
		
		this.BASE_C_parameters= createBaseC();
		this.nameDefMap.put(BASE_C_NAME, this.BASE_C_parameters);
		
		this.BASE_DATAFRAME_parameters= createBaseC();
		this.nameDefMap.put(BASE_DATAFRAME_NAME, this.BASE_C_parameters);
		
		this.BASE_USEMETHOD_parameters= createBaseUseMethod();
		this.nameDefMap.put(BASE_USEMETHOD_NAME, this.BASE_USEMETHOD_parameters);
		
		this.BASE_NEXTMETHOD_parameters= createBaseNextMethod();
		this.nameDefMap.put(BASE_NEXTMETHOD_NAME, this.BASE_NEXTMETHOD_parameters);
		
		
		this.UTILS_METHODS_parameters= createUtilsMethods();
		this.nameDefMap.put(UTILS_METHODS_NAME, this.UTILS_METHODS_parameters);
		
		this.UTILS_GETS3METHOD_parameters= createUtilsGetS3Method();
		this.nameDefMap.put(UTILS_GETS3METHOD_NAME, this.UTILS_GETS3METHOD_parameters);
		
		this.UTILS_HELP_parameters= createUtilsHelp();
		this.nameDefMap.put(UTILS_HELP_NAME, this.UTILS_HELP_parameters);
		
		
		this.METHODS_SETGENERIC_parameters= createMethodsSetGeneric();
		this.nameDefMap.put(METHODS_SETGENERIC_NAME, this.METHODS_SETGENERIC_parameters);
		
		this.METHODS_SETGROUPGENERIC_parameters= createMethodsSetGroupGeneric();
		this.nameDefMap.put(METHODS_SETGROUPGENERIC_NAME, this.METHODS_SETGROUPGENERIC_parameters);
		
		this.METHODS_REMOVEGENERIC_parameters= createMethodsRemoveGeneric();
		this.nameDefMap.put(METHODS_REMOVEGENERIC_NAME, this.METHODS_REMOVEGENERIC_parameters);
		
		this.METHODS_ISGENERIC_parameters= createMethodsIsGeneric();
		this.nameDefMap.put(METHODS_ISGENERIC_NAME, this.METHODS_ISGENERIC_parameters);
		
		this.METHODS_ISGROUP_parameters= createMethodsIsGroup();
		this.nameDefMap.put(METHODS_ISGROUP_NAME, this.METHODS_ISGROUP_parameters);
		
		this.METHODS_SIGNATURE_parameters= createMethodsSignature();
		this.nameDefMap.put(METHODS_SIGNATURE_NAME, this.METHODS_SIGNATURE_parameters);
		
		this.METHODS_SETCLASS_parameters= createMethodsSetClass();
		this.nameDefMap.put(METHODS_SETCLASS_NAME, this.METHODS_SETCLASS_parameters);
		
		this.METHODS_SETCLASSUNION_parameters= createMethodsSetClassUnion();
		this.nameDefMap.put(METHODS_SETCLASSUNION_NAME, this.METHODS_SETCLASSUNION_parameters);
		
		this.METHODS_REPRESENTATION_parameters= createMethodsRepresentation();
		this.nameDefMap.put(METHODS_REPRESENTATION_NAME, this.METHODS_REPRESENTATION_parameters);
		
		this.METHODS_PROTOTYPE_parameters= createMethodsPrototype();
		this.nameDefMap.put(METHODS_PROTOTYPE_NAME, this.METHODS_PROTOTYPE_parameters);
		
		this.METHODS_SETIS_parameters= createMethodsSetIs();
		this.nameDefMap.put(METHODS_SETIS_NAME, this.METHODS_SETIS_parameters);
		
		this.METHODS_REMOVECLASS_parameters= createMethodsRemoveClass();
		this.nameDefMap.put(METHODS_REMOVECLASS_NAME, this.METHODS_REMOVECLASS_parameters);
		
		this.METHODS_RESETCLASS_parameters= createMethodsResetClass();
		this.nameDefMap.put(METHODS_RESETCLASS_NAME, this.METHODS_RESETCLASS_parameters);
		
		this.METHODS_SETAS_parameters= createMethodsSetAs();
		this.nameDefMap.put(METHODS_SETAS_NAME, this.METHODS_SETAS_parameters);
		
		this.METHODS_SETVALIDITY_parameters= createMethodsSetValidity();
		this.nameDefMap.put(METHODS_SETVALIDITY_NAME, this.METHODS_SETVALIDITY_parameters);
		
		this.METHODS_ISCLASS_parameters= createMethodsIsClass();
		this.nameDefMap.put(METHODS_ISCLASS_NAME, this.METHODS_ISCLASS_parameters);
		
		this.METHODS_EXTENDS_parameters= createMethodsExtends();
		this.nameDefMap.put(METHODS_EXTENDS_NAME, this.METHODS_EXTENDS_parameters);
		
		this.METHODS_GETCLASS_parameters= createMethodsGetClass();
		this.nameDefMap.put(METHODS_GETCLASS_NAME, this.METHODS_GETCLASS_parameters);
		
		this.METHODS_GETCLASSDEF_parameters= createMethodsGetClassDef();
		this.nameDefMap.put(METHODS_GETCLASSDEF_NAME, this.METHODS_GETCLASSDEF_parameters);
		
		this.METHODS_FINDCLASS_parameters= createMethodsFindClass();
		this.nameDefMap.put(METHODS_FINDCLASS_NAME, this.METHODS_FINDCLASS_parameters);
		
		this.METHODS_NEW_parameters= createMethodsNew();
		this.nameDefMap.put(METHODS_NEW_NAME, this.METHODS_NEW_parameters);
		
		this.METHODS_AS_parameters= createMethodsAs();
		this.nameDefMap.put(METHODS_AS_NAME, this.METHODS_AS_parameters);
		
		this.METHODS_IS_parameters= createMethodsIs();
		this.nameDefMap.put(METHODS_IS_NAME, this.METHODS_IS_parameters);
		
		this.METHODS_SETMETHOD_parameters= createMethodsSetMethod();
		this.nameDefMap.put(METHODS_SETMETHOD_NAME, this.METHODS_SETMETHOD_parameters);
		
		this.METHODS_REMOVEMETHOD_parameters= createMethodsRemoveMethod();
		this.nameDefMap.put(METHODS_REMOVEMETHOD_NAME, this.METHODS_REMOVEMETHOD_parameters);
		
		this.METHODS_REMOVEMETHODS_parameters= createMethodsRemoveMethods();
		this.nameDefMap.put(METHODS_REMOVEMETHODS_NAME, this.METHODS_REMOVEMETHODS_parameters);
		
		this.METHODS_EXISTSMETHOD_parameters= createMethodsExistsMethod();
		this.nameDefMap.put(METHODS_EXISTSMETHOD_NAME, this.METHODS_EXISTSMETHOD_parameters);
		
		this.METHODS_HASMETHOD_parameters= createMethodsHasMethod();
		this.nameDefMap.put(METHODS_HASMETHOD_NAME, this.METHODS_HASMETHOD_parameters);
		
		this.METHODS_GETMETHOD_parameters= createMethodsGetMethod();
		this.nameDefMap.put(METHODS_GETMETHOD_NAME, this.METHODS_GETMETHOD_parameters);
		
		this.METHODS_SELECTMETHOD_parameters= createMethodsSelectMethod();
		this.nameDefMap.put(METHODS_SELECTMETHOD_NAME, this.METHODS_SELECTMETHOD_parameters);
		
		this.METHODS_GETMETHODS_parameters= createMethodsGetMethods();
		this.nameDefMap.put(METHODS_GETMETHODS_NAME, this.METHODS_GETMETHODS_parameters);
		
		this.METHODS_FINDMETHOD_parameters= createMethodsFindMethod();
		this.nameDefMap.put(METHODS_FINDMETHOD_NAME, this.METHODS_FINDMETHOD_parameters);
		
		this.METHODS_DUMPMETHOD_parameters= createMethodsDumpMethod();
		this.nameDefMap.put(METHODS_DUMPMETHOD_NAME, this.METHODS_DUMPMETHOD_parameters);
		
		this.METHODS_DUMPMETHODS_parameters= createMethodsDumpMethods();
		this.nameDefMap.put(METHODS_DUMPMETHODS_NAME, this.METHODS_DUMPMETHODS_parameters);
		
		this.METHODS_SLOT_parameters= createMethodsSlot();
		this.nameDefMap.put(METHODS_SLOT_NAME, this.METHODS_SLOT_parameters);
	}
	
	
	protected Parameters createBaseAssign() {
		return new ParametersBuilder()
				.add("x", UNSPECIFIC_NAME | NAME_AS_STRING)
				.add("value", "pos", "envir", "inherits", "immediate")
				.build();
	}
	
	protected Parameters createBaseRemove() {
		return new ParametersBuilder()
				.add("...", UNSPECIFIC_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.add("list", "pos", "envir", "inherits")
				.build();
	}
	
	protected Parameters createBaseExists() {
		return new ParametersBuilder()
				.add("x", UNSPECIFIC_NAME | NAME_AS_STRING)
				.add("where", "envir", "frame", "mode", "inherits")
				.build();
	}
	
	protected Parameters createBaseGet() {
		return new ParametersBuilder()
				.add("x", UNSPECIFIC_NAME | NAME_AS_STRING)
				.add("pos", "envir", "mode", "inherits")
				.build();
	}
	
	protected Parameters createBaseGet0() {
		return new ParametersBuilder()
				.add("x", UNSPECIFIC_NAME | NAME_AS_STRING)
				.add("envir", "mode", "inherits", "ifnotfound")
				.build();
	}
	
	protected Parameters createBaseSave() {
		return new ParametersBuilder()
				.add("...", UNSPECIFIC_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.add("list", UNSPECIFIC_NAME | NAME_AS_STRING)
				.add("file", "ascii", "version", "envir", "compress", "compress_level",
						"eval.promises", "precheck" )
				.build();
	}
	
	protected Parameters createBaseCall() {
		return new ParametersBuilder()
				.add("name", UNSPECIFIC_NAME | NAME_AS_STRING)
				.add("...")
				.build();
	}
	
	protected Parameters createBaseDoCall() {
		return new ParametersBuilder()
				.add("what", UNSPECIFIC_NAME | NAME_AS_STRING | UNSPECIFIC_OBJ)
				.add("args", "quote", "envir")
				.build();
	}
	
	protected Parameters createBaseLibrary() {
		return new ParametersBuilder()
				.add("package", PACKAGE_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.add("help", PACKAGE_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.add("pos", "lib.loc", "character.only", "logical.return", "warn.conflicts",
						"quietly", "verbose" )
				.build();
	}
	
	protected Parameters createBaseRequire() {
		return new ParametersBuilder()
				.add("package", PACKAGE_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.add("lib.loc", "quietly", "warn.conflicts", "character.only")
				.build();
	}
	
	protected Parameters createBaseGlobalenv() {
		return new Parameters();
	}
	
	protected Parameters createBaseTopenv() {
		return new Parameters(
				"envir", "matchThisEnv");
	}
	
	protected Parameters createBaseGetNamespace() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.build();
	}
	
	protected Parameters createBaseGetNamespaceName() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.build();
	}
	
	protected Parameters createBaseGetNamespaceVersion() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.build();
	}
	
	protected Parameters createBaseGetNamespaceExports() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.build();
	}
	
	protected Parameters createBaseGetNamespaceImports() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.build();
	}
	
	protected Parameters createBaseGetNamespaceUsers() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.build();
	}
	
	protected Parameters createBaseGetExportedValue() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.add("name")
				.build();
	}
	
	protected Parameters createBaseAttachNamespace() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.add("pos", "depends")
				.build();
	}
	
	protected Parameters createBaseLoadNamespace() {
		return new ParametersBuilder()
				.add("package", PACKAGE_NAME | NAME_AS_STRING)
				.add("lib.loc", "keep.source", "partial", "versionCheck")
				.build();
	}
	
	protected Parameters createBaseRequireNamespace() {
		return new ParametersBuilder()
				.add("package", PACKAGE_NAME | NAME_AS_STRING)
				.add("...", "quietly")
				.build();
	}
	
	protected Parameters createBaseIsNamespaceLoaded() {
		return new ParametersBuilder()
				.add("name", PACKAGE_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.build();
	}
	
	protected Parameters createBaseUnloadNamespace() {
		return new ParametersBuilder()
				.add("ns", PACKAGE_NAME | NAME_AS_STRING)
				.build();
	}
	
	
	protected Parameters createBaseC() {
		return new ParametersBuilder()
				.add("...")
				.add("recursive")
				.build();
	}
	
	protected Parameters createBaseDataFrame() {
		return new ParametersBuilder()
				.add("...")
				.add("row.names")
				.add("check.rows")
				.add("check.names")
				.add("stringsAsFactors")
				.build();
	}
	
	protected Parameters createBaseUseMethod() {
		return new ParametersBuilder()
				.add("generic", METHOD_NAME)
				.add("object")
				.build();
	}
	
	protected Parameters createBaseNextMethod() {
		return new ParametersBuilder()
				.add("name", METHOD_NAME)
				.add("object", "...")
				.build();
	}
	
	protected Parameters createUtilsMethods() {
		return new ParametersBuilder()
				.add("generic.function", METHOD_OBJ | METHOD_NAME)
				.add("class", CLASS_NAME)
				.build();
	}
	
	protected Parameters createUtilsGetS3Method() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("class", CLASS_NAME)
				.add("optional")
				.build();
	}
	
	protected Parameters createUtilsHelp() {
		return new ParametersBuilder()
				.add("topic", HELP_TOPIC_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.add("package", PACKAGE_NAME | NAME_AS_STRING | NAME_AS_SYMBOL)
				.add("lib.loc", "verbose", "try.all.packages", "help_type")
				.build();
	}
	
	
	protected Parameters createMethodsSetGeneric() {
		return new Parameters(
				"name", "def", "group", "valueClass", "where", "package",
				"signature" , "useAsDefault" , "genericFunction");
	}
	
	protected Parameters createMethodsSetGroupGeneric() {
		return new Parameters(
				"name", "def", "group", "valueClass", "knownMembers",
				"where", "package");
	}
	
	protected Parameters createMethodsRemoveGeneric() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("where")
				.build();
	}
	
	protected Parameters createMethodsIsGeneric() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("where", "fdef", "getName")
				.build();
	}
	
	protected Parameters createMethodsIsGroup() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("where", "fdef")
				.build();
	}
	
	protected Parameters createMethodsSignature() {
		return new Parameters(
				"...");
	}
	
	protected Parameters createMethodsSetClass() {
		return new ParametersBuilder()
			.add("Class", CLASS_NAME)
			.add("representation", "prototype", "contains", "validity",
				"access", "where", "version", "sealed", "package")
			.build();
	}
	
	protected Parameters createMethodsSetClassUnion() {
		return new ParametersBuilder()
			.add("name", CLASS_NAME)
			.add("members", "where")
			.build();
	}
	
	protected Parameters createMethodsRepresentation() {
		return new Parameters(
				"...");
	}
	
	protected Parameters createMethodsPrototype() {
		return new Parameters(
				"...");
	}
	
	protected Parameters createMethodsSetIs() {
		return new Parameters(
				"class1", "class2", "test", "coerce", "replace", "by", "where",
				"classDef", "extensionObject", "doComplete");
	}
	
	protected Parameters createMethodsRemoveClass() {
		return new ParametersBuilder()
				.add("Class", CLASS_NAME)
				.add("where")
				.build();
	}
	
	protected Parameters createMethodsResetClass() {
		return new ParametersBuilder()
				.add("Class") // no naming
				.add("classDef", "where")
				.build();
	}
	
	protected Parameters createMethodsSetAs() {
		return new ParametersBuilder()
				.add("from", CLASS_NAME)
				.add("to", CLASS_NAME)
				.add("def", "replace", "where")
				.build();
	}
	
	protected Parameters createMethodsSetValidity() {
		return new ParametersBuilder()
				.add("Class", CLASS_NAME) // no naming
				.add("method", "where")
				.build();
	}
	
	protected Parameters createMethodsIsClass() {
		return new ParametersBuilder()
				.add("Class", CLASS_NAME)
				.add("formal", "where")
				.build();
	}
	
	protected Parameters createMethodsGetClass() {
		return new Parameters(
				"Class", ".Force", "where");
	}
	
	protected Parameters createMethodsGetClassDef() {
		return new Parameters(
				"Class", "where", "package");
	}
	
	protected Parameters createMethodsFindClass() {
		return new Parameters(
				"Class", "where", "unique");
	}
	
	protected Parameters createMethodsExtends() {
		return new ParametersBuilder()
				.add("class1", CLASS_NAME)
				.add("class2", CLASS_NAME)
				.add("maybe", "fullInfo")
				.build();
	}
	
	protected Parameters createMethodsNew() {
		return new ParametersBuilder()
				.add("Class", CLASS_NAME)
				.add("...")
				.build();
	}
	
	protected Parameters createMethodsAs() {
		return new ParametersBuilder()
				.add("object")
				.add("Class", CLASS_NAME)
				.add("strict", "ext")
				.build();
	}
	
	protected Parameters createMethodsIs() {
		return new ParametersBuilder()
				.add("object")
				.add("class2", CLASS_NAME)
				.build();
	}
	
	protected Parameters createMethodsSetMethod() {
		return new Parameters(
				"f", "signature", "definition", "where", "valueClass", "sealed");
	}
	
	protected Parameters createMethodsRemoveMethod() {
		return new Parameters(
				"f", "signature", "where");
	}
	
	protected Parameters createMethodsRemoveMethods() {
		return new Parameters(
				"f", "where", "all");
	}
	
	protected Parameters createMethodsExistsMethod() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("signature", "where")
				.build();
	}
	
	protected Parameters createMethodsHasMethod() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("signature", "where")
				.build();
	}
	
	protected Parameters createMethodsGetMethod() {
		return new Parameters(
				"f", "signature", "where", "optional", "mlist", "fdef");
	}
	
	protected Parameters createMethodsSelectMethod() {
		return new Parameters(
				"f", "signature", "optional", "useInherited", "mlist", "fdef", "verbose");
	}
	
	protected Parameters createMethodsGetMethods() {
		return new Parameters(
				"f", "where");
	}
	
	protected Parameters createMethodsFindMethod() {
		return new Parameters(
				"f", "signature", "where");
	}
	
	protected Parameters createMethodsDumpMethod() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("signature", "file", "where", "def")
				.build();
	}
	
	protected Parameters createMethodsDumpMethods() {
		return new ParametersBuilder()
				.add("f", METHOD_NAME)
				.add("file", "signature", "methods", "where")
				.build();
	}
	
	protected Parameters createMethodsSlot() {
		return new Parameters(
				"object", "name", "check");
	}
	
	
	public ImSet<String> getPackageNames() {
		return this.packageNames;
	}
	
	public Set<String> getKnownFunctions() {
		return this.nameDefMapIm.keySet();
	}
	
	public @Nullable Parameters getParameters(final @Nullable String name) {
		return this.nameDefMap.get(name);
	}
	
}
