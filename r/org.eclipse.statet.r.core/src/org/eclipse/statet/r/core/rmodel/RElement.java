/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;


@NonNullByDefault
public interface RElement<TModelChild extends RElement<?>>
		extends LtkModelElement<TModelChild> {
	
	
	static final int R_S4CLASS=                C1_CLASS    | 0x80;
	static final int R_S4CLASS_EXTENSION=      C1_CLASS    | 0x90;
	
	static final int R_PACKAGE_LOAD=           C1_IMPORT   | 0x10;
	
	static final int R_COMMON_FUNCTION=        C1_METHOD   | 0x10;
	static final int R_COMMON_LOCAL_FUNCTION=  C1_METHOD   | 0x11;
	static final int R_GENERIC_FUNCTION=       C1_METHOD   | 0x20;
	static final int R_S4METHOD=               C1_METHOD   | 0x80;
	
	static final int R_GENERAL_VARIABLE=       C1_VARIABLE | 0x10;
	static final int R_GENERAL_LOCAL_VARIABLE= C1_VARIABLE | 0x11;
	static final int R_ARGUMENT=               C1_VARIABLE | 0x31;
	static final int R_S4SLOT=                 C1_VARIABLE | 0x80;
	
	static final int R_DOC_EXAMPLE_CHUNK=      C12_SOURCE_CHUNK | 0x4;
	
	
	static final LtkModelElementFilter<RElement<?>> R_S4SLOT_FILTER= new LtkModelElementFilter<>() {
		@Override
		public boolean include(final RElement<?> element) {
			return (element.getElementType() == R_S4SLOT);
		}
	};
	
	
	@Override
	RElementName getElementName();
	
	@Override
	@Nullable RElement<? extends RElement<?>> getModelParent();
	@Override
	boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter);
	@Override
	List<? extends TModelChild> getModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter);
	
}
