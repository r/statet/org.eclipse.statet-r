/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import java.util.Comparator;

import com.ibm.icu.text.Collator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.r.core.RSymbolComparator;


@NonNullByDefault
public class RElementComparator implements Comparator<RElement> {
	
	
	private final Collator nameComparator= RSymbolComparator.R_NAMES_COLLATOR;
	
	
	@Override
	public int compare(final RElement o1, final RElement o2) {
		final ElementName elementName1= o1.getElementName();
		final ElementName elementName2= o2.getElementName();
		final String s1= elementName1.getSegmentName();
		final String s2= elementName2.getSegmentName();
		if (s1 == s2) {
			return 0;
		}
		if (s1 != null) {
			if (s2 != null) {
				return this.nameComparator.compare(s1, s2);
			}
			return -1;
		}
		else /* (s2 != null) */ {
			return 1;
		}
	}
	
}
