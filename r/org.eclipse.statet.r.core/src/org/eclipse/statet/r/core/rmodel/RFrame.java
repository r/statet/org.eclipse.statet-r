/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;


// TODO extend RElement? return as model parent of elements
@NonNullByDefault
public interface RFrame<TModelChild extends RLangElement> {
	
	/** Simple project */
	static final int PROJECT= 1;
	/** Package project */
	static final int PACKAGE= 2;
	
	static final int EXPLICIT= 4;
	
	/** Function frame */
	static final int FUNCTION= 5;
	/** Class context (not a real frame) */
	static final int CLASS= 6;
	
	
	int getFrameType();
//	String getFrameName();
	/** Combination of frametype and name */
	@Nullable String getFrameId();
	@Nullable RElementName getElementName();
	
	List<? extends TModelChild> getModelElements();
	boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter);
	List<? extends TModelChild> getModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter);
	List<? extends RFrame> getPotentialParents();
	
}
