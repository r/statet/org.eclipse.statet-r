/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * An R class definition element
 */
@NonNullByDefault
public interface RLangClass<TModelChild extends RLangElement>
		extends RLangElement<TModelChild> {
	
	
	List<String> getExtendedClassNames();
	
}
