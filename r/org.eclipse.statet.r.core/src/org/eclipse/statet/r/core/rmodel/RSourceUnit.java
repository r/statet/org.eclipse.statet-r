/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.RCoreAccess;


/**
 * An R source (script) file
 */
@NonNullByDefault
public interface RSourceUnit extends SourceUnit {
	
	
	/**
	 * Model type id for R source files in workspace
	 */
	int R_WORKSPACE_SU= C12_SOURCE_FILE | 1;
	
	/**
	 * Model type id for R source files in libraries
	 */
	int R_LIBRARY_SU= C12_SOURCE_FILE | 2;
	
	/**
	 * Model type id for other R source files, e.g. external files
	 */
	int R_OTHER_SU= C12_SOURCE_FILE | 3;
	
	
	/**
	 * Returns the R core access provider for the source unit
	 * @return access of scope of the source unit
	 */
	RCoreAccess getRCoreAccess();
	
}
