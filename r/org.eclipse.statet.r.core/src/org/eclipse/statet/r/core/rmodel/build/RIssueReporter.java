/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel.build;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;

import org.eclipse.statet.internal.r.core.rmodel.AstProblemReporter;
import org.eclipse.statet.internal.r.core.rmodel.RTaskTagReporter;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.TaskIssueConfig;
import org.eclipse.statet.r.core.rmodel.RCompositeSourceElement;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
public class RIssueReporter {
	
	
	private final AstProblemReporter syntaxProblemReporter= new AstProblemReporter();
	
	private final RTaskTagReporter taskReporter= new RTaskTagReporter();
	
	/* explicite configs */
	private @Nullable TaskIssueConfig taskIssueConfig;
	
	private boolean runProblems;
	private boolean runTasks;
	
	
	public RIssueReporter() {
	}
	
	
	public void configure(final TaskIssueConfig taskIssueConfig) {
		this.taskIssueConfig= taskIssueConfig;
	}
	
	
	public void run(final RSourceUnit sourceUnit, final RSourceUnitModelInfo modelInfo,
			final SourceContent content,
			final IssueRequestor requestor, final int level) {
		this.runProblems= requestor.isInterestedInProblems(RModel.R_TYPE_ID);
		this.runTasks= requestor.isInterestedInTasks();
		if (!(this.runProblems || this.runTasks)) {
			return;
		}
		
		var taskIssueConfig= this.taskIssueConfig;
		if (taskIssueConfig == null) {
			final var prefs= EPreferences.getContextPrefs(sourceUnit);
			taskIssueConfig= TaskIssueConfig.getConfig(prefs);
		}
		if (this.runTasks) {
			this.taskReporter.configure(taskIssueConfig);
		}
		
		final RLangSourceElement element= modelInfo.getSourceElement();
		if (element instanceof RCompositeSourceElement) {
			final var elements= ((RCompositeSourceElement)element).getCompositeElements();
			for (final RLangSourceElement rChunk : elements) {
				runReporters(sourceUnit, rChunk.getAdapter(AstNode.class),
						modelInfo, content, requestor, level );
			}
		}
		else {
			runReporters(sourceUnit, element.getAdapter(AstNode.class),
					modelInfo, content, requestor, level );
		}
	}
	
	public void run(final RAstNode node,
			final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (requestor.isInterestedInProblems(RModel.R_TYPE_ID)) {
			this.syntaxProblemReporter.run(node, content, requestor);
		}
	}
	
	
	private void runReporters(final RSourceUnit sourceUnit, final RAstNode node,
			final @Nullable RSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (this.runProblems) {
			this.syntaxProblemReporter.run(node, content, requestor);
		}
		if (this.runTasks) {
			this.taskReporter.run(sourceUnit, node, content, requestor);
		}
	}
	
	private void runReporters(final RSourceUnit sourceUnit, final @Nullable AstNode node,
			final @Nullable RSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (node == null) {
			return;
		}
		if (node instanceof RAstNode) {
			runReporters(sourceUnit, (RAstNode)node,
					modelInfo, content, requestor, level );
		}
		else {
			final int n= node.getChildCount();
			for (int i= 0; i < n; i++) {
				final AstNode child= node.getChild(i);
				if (child instanceof RAstNode) {
					runReporters(sourceUnit, (RAstNode)child,
							modelInfo, content, requestor, level );
				}
			}
		}
	}
	
}
