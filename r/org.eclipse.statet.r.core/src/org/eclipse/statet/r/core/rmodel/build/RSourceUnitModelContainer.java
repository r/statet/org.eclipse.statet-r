/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel.build;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.project.RIssues;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;


@NonNullByDefault
public class RSourceUnitModelContainer extends SourceUnitModelContainer<RSourceUnit, RSourceUnitModelInfo> {
	
	
	public static final IssueTypeSet ISSUE_TYPE_SET= new IssueTypeSet(RCore.BUNDLE_ID,
			RIssues.TASK_CATEGORY,
			ImCollections.newList(
					RIssues.R_MODEL_PROBLEM_CATEGORY ));
	
	
	public RSourceUnitModelContainer(final RSourceUnit unit,
			final @Nullable SourceUnitIssueSupport issueSupport) {
		super(unit, issueSupport);
	}
	
	
	@Override
	public Class<?> getAdapterClass() {
		return RSourceUnitModelContainer.class;
	}
	
	@Override
	public boolean isContainerFor(final String modelTypeId) {
		return (modelTypeId == RModel.R_TYPE_ID);
	}
	
	@Override
	protected ModelManager getModelManager() {
		return RModel.getRModelManager();
	}
	
	
}
