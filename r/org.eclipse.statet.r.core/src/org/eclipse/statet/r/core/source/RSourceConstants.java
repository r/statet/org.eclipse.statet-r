/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_CTX2;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE1;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE2;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE3;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.util.Version;

import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public interface RSourceConstants {
	
	
	Version LANG_VERSION_4_0=           new Version(4, 0, 0);
	Version LANG_VERSION_4_1=           new Version(4, 1, 0);
	
	ImList<Version> LANG_VERSIONS= ImCollections.newList(
			LANG_VERSION_4_1,
			LANG_VERSION_4_0 );
	
	static Version getSuitableLangVersion(final Version version) {
		if (version.compareTo(LANG_VERSION_4_1) >= 0) {
			return LANG_VERSION_4_1;
		}
		return LANG_VERSION_4_0;
	}
	
	
	/**
	 * An existing token is not OK.
	 */
	static int TYPE1_SYNTAX_INCORRECT_TOKEN=                StatusCodes.TYPE1_SYNTAX_INCORRECT_TOKEN;
	static int TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE=          TYPE1_SYNTAX_INCORRECT_TOKEN | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_TOKEN_NOT_CLOSED=                  TYPE1_SYNTAX_INCORRECT_TOKEN | 0x2 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_NUMBER_INVALID=                    TYPE1_SYNTAX_INCORRECT_TOKEN | 0x3 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING=             TYPE12_SYNTAX_NUMBER_INVALID | 0x5 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING=         TYPE12_SYNTAX_NUMBER_INVALID | 0x6 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING=             TYPE12_SYNTAX_NUMBER_INVALID | 0x7 << SHIFT_TYPE3;
	static int TYPE12_SYNTAX_NUMBER_MISLEADING=                 TYPE1_SYNTAX_INCORRECT_TOKEN | 0x4 << SHIFT_TYPE2;
	static int TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L=                TYPE12_SYNTAX_NUMBER_MISLEADING | 0x1 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_NUMBER_INT_WITH_DEC_POINT=            TYPE12_SYNTAX_NUMBER_MISLEADING | 0x2 << SHIFT_TYPE3;
	static int TYPE12_SYNTAX_TEXT_INVALID=                      TYPE1_SYNTAX_INCORRECT_TOKEN | 0x5 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_TEXT_NULLCHAR=                        TYPE12_SYNTAX_TEXT_INVALID | 0x1 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED=           TYPE12_SYNTAX_TEXT_INVALID | 0x3 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED=           TYPE12_SYNTAX_TEXT_INVALID | 0x4 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING=    TYPE12_SYNTAX_TEXT_INVALID | 0x5 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNKOWN=               TYPE12_SYNTAX_TEXT_INVALID | 0x9 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_CODEPOINT_INVALID=    TYPE12_SYNTAX_TEXT_INVALID | 0xB << SHIFT_TYPE3;
	static int TYPE12_SYNTAX_TOKEN_UNKNOWN=                     TYPE1_SYNTAX_INCORRECT_TOKEN | 0x9 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_TOKEN_UNEXPECTED=                  TYPE1_SYNTAX_INCORRECT_TOKEN | 0xA << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_SEQREL_UNEXPECTED=                    TYPE12_SYNTAX_TOKEN_UNEXPECTED | 0x1;
	static int TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED=    TYPE1_SYNTAX_INCORRECT_TOKEN | 0xB << SHIFT_TYPE2 | ERROR;
	
	/**
	 * A token (represented by an node) is missing.
	 */
	static int TYPE1_SYNTAX_MISSING_TOKEN=                  0x3 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_EXPR_MISSING=                      TYPE1_SYNTAX_MISSING_TOKEN | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_EXPR_AS_REF_MISSING=                  TYPE12_SYNTAX_EXPR_MISSING | 0x1 << SHIFT_TYPE3 | ERROR;
	static int TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING=               TYPE12_SYNTAX_EXPR_MISSING | 0x2 << SHIFT_TYPE3 | ERROR;
	static int TYPE123_SYNTAX_EXPR_AFTER_OP_MISSING=                TYPE12_SYNTAX_EXPR_MISSING | 0x3 << SHIFT_TYPE3 | ERROR;
	static int TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING=            TYPE12_SYNTAX_EXPR_MISSING | 0x4 << SHIFT_TYPE3 | ERROR;
	static int TYPE123_SYNTAX_EXPR_AS_FORSEQ_MISSING=               TYPE12_SYNTAX_EXPR_MISSING | 0x5 << SHIFT_TYPE3 | ERROR;
	static int TYPE123_SYNTAX_EXPR_AS_BODY_MISSING=                 TYPE12_SYNTAX_EXPR_MISSING | 0x6 << SHIFT_TYPE3 | ERROR;
	static int TYPE123_SYNTAX_EXPR_IN_GROUP_MISSING=                TYPE12_SYNTAX_EXPR_MISSING | 0x7 << SHIFT_TYPE3 | ERROR;
	static int TYPE123_SYNTAX_EXPR_AS_ARGVALUE_MISSING=             TYPE12_SYNTAX_EXPR_MISSING | 0x8 << SHIFT_TYPE3 | ERROR;
	static int TYPE12_SYNTAX_ELEMENTNAME_MISSING=               TYPE1_SYNTAX_MISSING_TOKEN | 0x2 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_SYMBOL_MISSING=                    TYPE1_SYNTAX_MISSING_TOKEN | 0x3 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_OPERATOR_MISSING=                  TYPE1_SYNTAX_MISSING_TOKEN | 0x8 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING=            TYPE1_SYNTAX_MISSING_TOKEN | 0xB << SHIFT_TYPE2 | ERROR;
	
	static int TYPE1_SYNTAX_INCOMPLETE_NODE=                0x4 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_NODE_NOT_CLOSED=                   TYPE1_SYNTAX_INCOMPLETE_NODE | 0x1 << SHIFT_TYPE2 | ERROR;
	
	/**
	 * A control statement (part of an existing node) is incomplete.
	 */
	static int TYPE1_SYNTAX_INCOMPLETE_CC=                  0x5 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_CC_NOT_CLOSED=                     TYPE1_SYNTAX_INCOMPLETE_CC | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_IF_MISSING=                        TYPE1_SYNTAX_INCOMPLETE_CC | 0x3 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_CONDITION_MISSING=                 TYPE1_SYNTAX_INCOMPLETE_CC | 0x4 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_IN_MISSING=                        TYPE1_SYNTAX_INCOMPLETE_CC | 0x5 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_CONDITION_NOT_CLOSED=              TYPE1_SYNTAX_INCOMPLETE_CC | 0x6 << SHIFT_TYPE2 | ERROR;
	
	/**
	 * A function definition is incomplete.
	 */
	static int TYPE1_SYNTAX_INCOMPLETE_FDEF=                0x6 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_FDEF_ARGS_MISSING=                 TYPE1_SYNTAX_INCOMPLETE_FDEF | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED=              TYPE1_SYNTAX_INCOMPLETE_FDEF | 0x2 << SHIFT_TYPE2 | ERROR;
	
	/**
	 * Syntax not supported by current config.
	 */
	static int TYPE1_SYNTAX_INCOMPATIBLE=                   0x7 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION=       TYPE1_SYNTAX_INCOMPATIBLE | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_OPTIONAL_IN_LANG_VERSION=          TYPE1_SYNTAX_INCOMPATIBLE | 0x2 << SHIFT_TYPE2;
	
	
	static int CTX12_IF=                    0x1 << SHIFT_CTX2;
	static int CTX12_ELSE=                  0x2 << SHIFT_CTX2;
	static int CTX12_FOR=                   0x3 << SHIFT_CTX2;
	static int CTX12_WHILE=                 0x4 << SHIFT_CTX2;
	static int CTX12_REPEAT=                0x5 << SHIFT_CTX2;
	static int CTX12_FDEF=                  0x6 << SHIFT_CTX2;
	static int CTX12_FCALL=                 0x7 << SHIFT_CTX2;
	static int CTX12_PIPE=                  0x8 << SHIFT_CTX2;
	
}
