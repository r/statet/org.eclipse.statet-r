/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.eclipse.statet.r.core.source.RSourceConstants.LANG_VERSION_4_0;
import static org.eclipse.statet.r.core.source.RSourceConstants.LANG_VERSION_4_1;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.util.Version;


@NonNullByDefault
public enum RTerminal {
	
	EOF (""), //$NON-NLS-1$
	BLANK (RTerminal.S_WHITESPACE),
	LINEBREAK ("\n"), //$NON-NLS-1$
	COMMENT (RTerminal.S_COMMENT),
	ROXYGEN_COMMENT ("#'"), //$NON-NLS-1$
	UNKNOWN (""), //$NON-NLS-1$
	OTHER (""), //$NON-NLS-1$
	
	BLOCK_OPEN (RTerminal.S_BLOCK_OPEN),
	BLOCK_CLOSE (RTerminal.S_BLOCK_CLOSE),
	GROUP_OPEN (RTerminal.S_GROUP_OPEN),
	GROUP_CLOSE (RTerminal.S_GROUP_CLOSE),
	
	SUB_INDEXED_S_OPEN (RTerminal.S_SUB_INDEXED_OPEN),
	SUB_INDEXED_D_OPEN (RTerminal.S_SUB_INDEXED_D_OPEN),
	SUB_INDEXED_CLOSE (RTerminal.S_SUB_INDEXED_CLOSE),
	SUB_NAMED_PART (RTerminal.S_SUB_NAMED),
	SUB_NAMED_SLOT (RTerminal.S_SUB_AT),
	
	NS_GET (RTerminal.S_NS_GET),
	NS_GET_INT (RTerminal.S_NS_GET_INT),
	
	PLUS (RTerminal.S_PLUS),
	MINUS (RTerminal.S_MINUS),
	MULT (RTerminal.S_MULT),
	DIV (RTerminal.S_DIV),
	OR (RTerminal.S_OR),
	OR_D (RTerminal.S_OR_D),
	AND (RTerminal.S_AND),
	AND_D (RTerminal.S_AND_D),
	NOT (RTerminal.S_NOT),
	POWER (RTerminal.S_POWER),
	SEQ (RTerminal.S_COLON),
	SPECIAL (RTerminal.S_SPECIAL),
	
	QUESTIONMARK (RTerminal.S_QUESTIONMARK),
	COMMA (RTerminal.S_COMMA),
	SEMICOLON (RTerminal.S_SEMICOLON),
	
	ARROW_LEFT_S (RTerminal.S_ARROW_LEFT),
	ARROW_LEFT_D (RTerminal.S_ARROW_LEFT_D),
	ARROW_RIGHT_S (RTerminal.S_ARROW_RIGHT),
	ARROW_RIGHT_D (RTerminal.S_ARROW_RIGHT_D),
	EQUAL (RTerminal.S_EQUAL),
	PIPE_RIGHT (RTerminal.S_PIPE_RIGHT, LANG_VERSION_4_1),
	COLON_EQUAL (":="),
	TILDE (RTerminal.S_TILDE),
	REL_NE (RTerminal.S_REL_NE),
	REL_EQ (RTerminal.S_REL_EQ),
	REL_LT (RTerminal.S_REL_LT),
	REL_LE (RTerminal.S_REL_LE),
	REL_GT (RTerminal.S_REL_GT),
	REL_GE (RTerminal.S_REL_GE),
	
	IF (RTerminal.S_IF),
	ELSE (RTerminal.S_ELSE),
	FOR (RTerminal.S_FOR),
	IN (RTerminal.S_IN),
	WHILE (RTerminal.S_WHILE),
	REPEAT (RTerminal.S_REPEAT),
	NEXT (RTerminal.S_NEXT),
	BREAK (RTerminal.S_BREAK),
	FUNCTION (RTerminal.S_FUNCTION),
	FUNCTION_B (RTerminal.S_BACKSLASH, LANG_VERSION_4_1),
	
	SYMBOL (""), //$NON-NLS-1$
	SYMBOL_G ("`"), //$NON-NLS-1$
	NUM_INT (""), //$NON-NLS-1$
	NUM_NUM (""), //$NON-NLS-1$
	NUM_CPLX (""), //$NON-NLS-1$
	STRING_D (RTerminal.S_QUOT_D),
	STRING_S (RTerminal.S_QUOT_S),
	STRING_R ("r\"("), //$NON-NLS-1$
	
	NULL (RTerminal.S_NULL),
	TRUE (RTerminal.S_TRUE),
	FALSE (RTerminal.S_FALSE),
	NA (RTerminal.S_NA),
	NA_INT (RTerminal.S_NA_INT),
	NA_REAL (RTerminal.S_NA_REAL),
	NA_CPLX (RTerminal.S_NA_CPLX),
	NA_CHAR (RTerminal.S_NA_CHAR),
	NAN (RTerminal.S_NAN),
	INF (RTerminal.S_INF);
	
	
	public static final String S_WHITESPACE= " "; //$NON-NLS-1$
	public static final String S_TAB= "\t"; //$NON-NLS-1$
	public static final String S_LINEBREAK_CRLF ="\r\n"; //$NON-NLS-1$
	public static final String S_LINEBREAK_LF ="\r"; //$NON-NLS-1$
	public static final String S_LINEBREAK_CR ="\n"; //$NON-NLS-1$
	public static final String S_COMMENT= "#"; //$NON-NLS-1$
	public static final String S_BLOCK_OPEN= "{"; //$NON-NLS-1$
	public static final String S_BLOCK_CLOSE= "}"; //$NON-NLS-1$
	public static final String S_GROUP_OPEN= "("; //$NON-NLS-1$
	public static final String S_GROUP_CLOSE= ")"; //$NON-NLS-1$
	public static final String S_SUB_INDEXED_OPEN= "["; //$NON-NLS-1$
	public static final String S_SUB_INDEXED_D_OPEN= "[["; //$NON-NLS-1$
	public static final String S_SUB_INDEXED_CLOSE= "]"; //$NON-NLS-1$
	public static final String S_SUB_NAMED= "$"; //$NON-NLS-1$
	public static final String S_SUB_AT= "@"; //$NON-NLS-1$
	public static final String S_NS_GET= "::"; //$NON-NLS-1$
	public static final String S_NS_GET_INT= ":::"; //$NON-NLS-1$
	public static final String S_PLUS= "+"; //$NON-NLS-1$
	public static final String S_MINUS= "-"; //$NON-NLS-1$
	public static final String S_MULT= "*"; //$NON-NLS-1$
	public static final String S_DIV= "/"; //$NON-NLS-1$
	public static final String S_OR= "|"; //$NON-NLS-1$
	public static final String S_OR_D= "||"; //$NON-NLS-1$
	public static final String S_AND= "&"; //$NON-NLS-1$
	public static final String S_AND_D= "&&"; //$NON-NLS-1$
	public static final String S_NOT= "!"; //$NON-NLS-1$
	public static final String S_POWER= "^"; //$NON-NLS-1$
	public static final String S_COLON= ":"; //$NON-NLS-1$
	public static final String S_SPECIAL= "%"; //$NON-NLS-1$
	public static final String S_QUESTIONMARK= "?"; //$NON-NLS-1$
	public static final String S_COMMA= ","; //$NON-NLS-1$
	public static final String S_SEMICOLON= ";"; //$NON-NLS-1$
	public static final String S_ARROW_LEFT= "<-"; //$NON-NLS-1$
	public static final String S_ARROW_LEFT_D= "<<-"; //$NON-NLS-1$
	public static final String S_ARROW_RIGHT= "->"; //$NON-NLS-1$
	public static final String S_ARROW_RIGHT_D= "->>"; //$NON-NLS-1$
	public static final String S_EQUAL= "="; //$NON-NLS-1$
	public static final String S_PIPE_RIGHT= "|>"; //$NON-NLS-1$
	public static final String S_TILDE= "~"; //$NON-NLS-1$
	public static final String S_REL_NE= "!="; //$NON-NLS-1$
	public static final String S_REL_EQ= "=="; //$NON-NLS-1$
	public static final String S_REL_LT= "<"; //$NON-NLS-1$
	public static final String S_REL_LE= "<="; //$NON-NLS-1$
	public static final String S_REL_GT= ">"; //$NON-NLS-1$
	public static final String S_REL_GE= ">="; //$NON-NLS-1$
	public static final String S_IF= "if"; //$NON-NLS-1$
	public static final String S_ELSE= "else"; //$NON-NLS-1$
	public static final String S_FOR= "for"; //$NON-NLS-1$
	public static final String S_IN= "in"; //$NON-NLS-1$
	public static final String S_WHILE= "while"; //$NON-NLS-1$
	public static final String S_REPEAT= "repeat"; //$NON-NLS-1$
	public static final String S_NEXT= "next"; //$NON-NLS-1$
	public static final String S_BREAK= "break"; //$NON-NLS-1$
	public static final String S_FUNCTION= "function"; //$NON-NLS-1$
	public static final String S_BACKSLASH= "\\"; //$NON-NLS-1$
	public static final String S_ELLIPSIS= "..."; //$NON-NLS-1$
	public static final String S_QUOT_S= "\'"; //$NON-NLS-1$
	public static final String S_QUOT_D= "\""; //$NON-NLS-1$
	public static final String S_TRUE= "TRUE"; //$NON-NLS-1$
	public static final String S_FALSE= "FALSE"; //$NON-NLS-1$
	public static final String S_NA= "NA"; //$NON-NLS-1$
	public static final String S_NA_INT= "NA_integer_"; //$NON-NLS-1$
	public static final String S_NA_REAL= "NA_real_"; //$NON-NLS-1$
	public static final String S_NA_CPLX= "NA_complex_"; //$NON-NLS-1$
	public static final String S_NA_CHAR= "NA_character_"; //$NON-NLS-1$
	public static final String S_NULL= "NULL"; //$NON-NLS-1$
	public static final String S_NAN= "NaN"; //$NON-NLS-1$
	public static final String S_INF= "Inf"; //$NON-NLS-1$
	
	public static final boolean isHWhitespace(final char c) {
		switch (c) {
		case ' ':
		case '\t':
			return true;
		default:
			return false;
		}
	}
	
	public static final boolean isAnyWhitespace(final char c) {
		switch (c) {
		case ' ':
		case '\t':
		case '\n':
		case '\r':
			return true;
		default:
			return false;
		}
	}
	
	
	public static @NonNull String[] textArray(final List<RTerminal> list) {
		final var texts= new @NonNull String[list.size()];
		for (int i= 0; i < texts.length; i++) {
			texts[i]= list.get(i).text;
		}
		return texts;
	}
	
	
	public final String text;
	
	private final Version minRLangVersion;
	
	
	RTerminal(final String text, final Version minRLangVersion) {
		this.text= text;
		this.minRLangVersion= minRLangVersion;
	}
	
	RTerminal(final String text) {
		this(text, LANG_VERSION_4_0);
	}
	
	
	public Version getMinRLangVersion() {
		return this.minRLangVersion;
	}
	
}
