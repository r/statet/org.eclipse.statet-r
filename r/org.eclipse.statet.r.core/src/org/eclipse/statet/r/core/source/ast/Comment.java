/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


@NonNullByDefault
public abstract class Comment extends RAstNode {
	
	
	static final class RoxygenLine extends Comment {
		
		
		public RoxygenLine() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.COMMENT;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.ROXYGEN_COMMENT;
		}
		
		
		@Override
		public final boolean hasChildren() {
			return false;
		}
		
		@Override
		public final int getChildCount() {
			return 0;
		}
		
		@Override
		public final RAstNode getChild(final int index) {
			throw new IndexOutOfBoundsException();
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			return -1;
		}
		
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			if (element.getNodeType() != NodeType.COMMENT
					|| element.getOperator(0) != RTerminal.ROXYGEN_COMMENT) {
				return false;
			}
			// ?
			return true;
		}
		
	}
	
	static final class CommonLine extends Comment {
		
		
		public CommonLine() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.COMMENT;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.COMMENT;
		}
		
		
		@Override
		public final boolean hasChildren() {
			return false;
		}
		
		@Override
		public final int getChildCount() {
			return 0;
		}
		
		@Override
		public final RAstNode getChild(final int index) {
			throw new IndexOutOfBoundsException();
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			return -1;
		}
		
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			if (element.getNodeType() != NodeType.COMMENT
					|| element.getOperator(0) != RTerminal.ROXYGEN_COMMENT) {
				return false;
			}
			// ?
			return true;
		}
		
	}
	
	
	Comment() {
	}
	
	
	@Override
	public void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
	}
	
	@Override
	public void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
	}
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		throw new IllegalArgumentException();
	}
	
}
