/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


@NonNullByDefault
public final class DocuTag extends RAstNode {
	
	
	private final String name;
	RAstNode[] fragments= NO_CHILDREN;
	
	
	DocuTag(final String name) {
		this.name= name;
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.DOCU_TAG;
	}
	
	@Override
	public final @Nullable RTerminal getOperator(final int index) {
		return null;
	}
	
	@Override
	public final String getText() {
		return this.name;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return (this.fragments.length > 0);
	}
	
	@Override
	public final int getChildCount() {
		return this.fragments.length;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		return this.fragments[index];
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		for (int i= 0; i < this.fragments.length; i++) {
			if (this.fragments[i] == child) {
				return i;
			}
		}
		return -1;
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		for (int i= 0; i < this.fragments.length; i++) {
			this.fragments[i].acceptInR(visitor);
		}
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		for (int i= 0; i < this.fragments.length; i++) {
			visitor.visit(this.fragments[i]);
		}
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		if (NodeType.DOCU_TAG == element.getNodeType()) {
			final DocuTag other= (DocuTag)element;
			return (Objects.equals(this.name, other.name));
		}
		return false;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		throw new IllegalArgumentException();
	}
	
	final void updateOffsets() {
	}
	
}
