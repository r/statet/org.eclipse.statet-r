/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AFTER_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code></code>
 */
@NonNullByDefault
public abstract class Dummy extends RAstNode {
	
	
	static class Terminal extends Dummy {
		
		
		@Nullable String text;
		
		
		Terminal(final int status) {
			super(status);
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.ERROR_TERM;
		}
		
		@Override
		public final @Nullable RTerminal getOperator(final int index) {
			return null;
		}
		
		@Override
		public final @Nullable String getText() {
			return this.text;
		}
		
		@Override
		public final boolean hasChildren() {
			return false;
		}
		
		@Override
		public final int getChildCount() {
			return 0;
		}
		
		@Override
		public final RAstNode getChild(final int index) {
			throw new IndexOutOfBoundsException();
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			return -1;
		}
		
		@Override
		public final void acceptInRChildren(final RAstVisitor visitor) {
		}
		
		@Override
		public final void acceptInChildren(final AstVisitor visitor) {
		}
		
		
		@Override
		final @Nullable Expression getExpr(final RAstNode child) {
			return null;
		}
		
		@Override
		final @Nullable Expression getLeftExpr() {
			return null;
		}
		
		@Override
		final @Nullable Expression getRightExpr() {
			return null;
		}
		
		
		@Override
		final int getMissingExprStatus(final Expression expr) {
			throw new IllegalArgumentException();
		}
		
	}
	
	
	static class Operator extends Dummy {
		
		
		final Expression leftExpr= new Expression();
		final Expression rightExpr= new Expression();
		
		
		Operator(final int status) {
			super(status);
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.ERROR;
		}
		
		@Override
		public final @Nullable RTerminal getOperator(final int index) {
			return null;
		}
		
		@Override
		public final boolean hasChildren() {
			return true;
		}
		
		@Override
		public final int getChildCount() {
			return 2;
		}
		
		@Override
		public final RAstNode getChild(final int index) {
			switch (index) {
			case 0:
				return this.leftExpr.node;
			case 1:
				return this.rightExpr.node;
			default:
				throw new IndexOutOfBoundsException();
			}
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			if (this.leftExpr.node == child) {
				return 0;
			}
			if (this.rightExpr.node == child) {
				return 1;
			}
			return -1;
		}
		
		@Override
		public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
			if (this.leftExpr.node != null) {
				this.leftExpr.node.acceptInR(visitor);
			}
			this.rightExpr.node.acceptInR(visitor);
		}
		
		@Override
		public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			if (this.leftExpr.node != null) {
				visitor.visit(this.leftExpr.node);
			}
			visitor.visit(this.rightExpr.node);
		}
		
		
		@Override
		final @Nullable Expression getExpr(final RAstNode child) {
			if (this.rightExpr.node == child) {
				return this.rightExpr;
			}
			if (this.leftExpr.node == child) {
				return this.leftExpr;
			}
			return null;
		}
		
		@Override
		final Expression getLeftExpr() {
			return this.leftExpr;
		}
		
		@Override
		final Expression getRightExpr() {
			return this.rightExpr;
		}
		
		@Override
		final int getMissingExprStatus(final Expression expr) {
			if (expr == this.leftExpr) {
				return TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING;
			}
			if (expr == this.rightExpr) {
				return TYPE123_SYNTAX_EXPR_AFTER_OP_MISSING;
			}
			throw new IllegalArgumentException();
		}
		
	}
	
	
	Dummy(final int status) {
		super(status);
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.ERROR_TERM == element.getNodeType());
	}
	
	
	final void updateOffsets() {
		final Expression left= getLeftExpr();
		if (left != null && left.node != null) {
			this.startOffset= left.node.startOffset;
		}
		final Expression right= getRightExpr();
		if (right != null && right.node != null) {
			this.endOffset= right.node.endOffset;
		}
	}
	
}
