/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_REF_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§ref§ ( §args§ )</code>
 */
@NonNullByDefault
public final class FCall extends RAstNode {
	
	
	public static final class Args extends RAstNode {
		
		
		private ImList<Arg> args;
		private ImIntList sepOffsets;
		
		
		@SuppressWarnings("null")
		Args(final FCall parent) {
			this.rParent= parent;
		}
		
//		Args(final List<Arg> args) {
//			this.rParent= null;
//			this.args= args;
//		}
		
		void finish(final RParser.ArgsBuilder<Arg> argsBuilder) {
			this.args= ImCollections.toList(argsBuilder.args);
			this.sepOffsets= ImCollections.toIntList(argsBuilder.sepOffsets);
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.F_CALL_ARGS;
		}
		
		@Override
		public final @Nullable RTerminal getOperator(final int index) {
			return null;
		}
		
		public ImIntList getSeparatorOffsets() {
			return this.sepOffsets;
		}
		
		
		@Override
		public final FCall getRParent() {
			return (FCall)this.rParent;
		}
		
		@Override
		public final boolean hasChildren() {
			return (!this.args.isEmpty());
		}
		
		@Override
		public final int getChildCount() {
			return this.args.size();
		}
		
		@Override
		public final Arg getChild(final int index) {
			return this.args.get(index);
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			for (int i= this.args.size() - 1; i >= 0; i--) {
				if (this.args.get(i) == child) {
					return i;
				}
			}
			return -1;
		}
		
		public ImList<Arg> getArgChildren() {
			return this.args;
		}
		
		@Override
		public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
			visitor.visit(this);
		}
		
		@Override
		public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
			for (final RAstNode arg : this.args) {
				arg.acceptInR(visitor);
			}
		}
		
		@Override
		public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			for (final RAstNode child : this.args) {
				visitor.visit(child);
			}
		}
		
		
		@Override
		final @Nullable Expression getExpr(final RAstNode child) {
			return null;
		}
		
		@Override
		final @Nullable Expression getLeftExpr() {
			return null;
		}
		
		@Override
		final @Nullable Expression getRightExpr() {
			return null;
		}
		
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.F_CALL_ARGS == element.getNodeType());
		}
		
		@Override
		final int getMissingExprStatus(final Expression expr) {
			throw new IllegalArgumentException();
		}
		
	}
	
	public static final class Arg extends SpecItem {
		
		
		Arg(final Args parent) {
			this.rParent= parent;
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.F_CALL_ARG;
		}
		
		@Override
		public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
			visitor.visit(this);
		}
		
		
		@Override
		public @Nullable Args getRParent() {
			return (Args)this.rParent;
		}
		
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.F_CALL_ARG == element.getNodeType());
		}
		
	}
	
	
	final Expression refExpr= new Expression();
	int argsOpenOffset= NA_OFFSET;
	final Args args= new Args(this);
	int argsCloseOffset= NA_OFFSET;
	
	
	FCall() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.F_CALL;
	}
	
	@Override
	public final @Nullable RTerminal getOperator(final int index) {
		return null;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 2;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		switch (index) {
		case 0:
			return this.refExpr.node;
		case 1:
			return this.args;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.refExpr.node == child) {
			return 0;
		}
		if (this.args == child) {
			return 1;
		}
		return -1;
	}
	
	public final RAstNode getRefChild() {
		return this.refExpr.node;
	}
	
	public final int getArgsOpenOffset() {
		return this.argsOpenOffset;
	}
	
	public final Args getArgsChild() {
		return this.args;
	}
	
	public final int getArgsCloseOffset() {
		return this.argsCloseOffset;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.refExpr.node.acceptInR(visitor);
		this.args.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.refExpr.node);
		visitor.visit(this.args);
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.refExpr.node == child) {
			return this.refExpr;
		}
		return null;
	}
	
	@Override
	final Expression getLeftExpr() {
		return this.refExpr;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		if (NodeType.F_CALL == element.getNodeType()) {
			return (this.refExpr.node.equalsSingle(((FCall)element).refExpr.node));
		}
		return false;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (expr == this.refExpr) {
			return TYPE123_SYNTAX_EXPR_AS_REF_MISSING;
		}
		throw new IllegalArgumentException();
	}
	
	final void updateOffsets() {
		this.startOffset= this.refExpr.node.startOffset;
		if (this.argsCloseOffset != NA_OFFSET) {
			this.endOffset= this.argsCloseOffset + 1;
		}
		else {
			this.endOffset= this.args.endOffset;
		}
	}
	
}
