/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_FDEF;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_ARGVALUE_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>function( §args§ ) §cont§</code>
 */
@NonNullByDefault
public abstract class FDef extends RAstNode {
	
	
	static final class Function extends FDef {
		
		
		Function() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.FUNCTION;
		}
		
		@Override
		int getOpLength() {
			return 8;
		}
		
	}
	
	static final class B extends FDef {
		
		
		B() {
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.FUNCTION_B;
		}
		
		@Override
		int getOpLength() {
			return 1;
		}
		
	}
	
	
	public static final class Args extends RAstNode {
		
		
		private ImList<Arg> args;
//		private ImIntList sepOffsets;
		
		
		@SuppressWarnings("null")
		Args(final FDef parent) {
			this.rParent= parent;
		}
		
		void finish(final RParser.ArgsBuilder<Arg> argsBuilder) {
			this.args= ImCollections.toList(argsBuilder.args);
//			this.sepOffsets= ImCollections.toIntList(argsBuilder.sepOffsets);
		}
		
		void finishMissing(final int offset) {
			this.startOffset= offset;
			this.endOffset= offset;
			this.args= ImCollections.emptyList();
//			this.sepOffsets= ImCollections.toIntList(argsBuilder.sepOffsets);
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.F_DEF_ARGS;
		}
		
		@Override
		public final @Nullable RTerminal getOperator(final int index) {
			return null;
		}
		
		
		@Override
		public final FDef getRParent() {
			return (FDef)this.rParent;
		}
		
		@Override
		public final boolean hasChildren() {
			return (!this.args.isEmpty());
		}
		
		@Override
		public final int getChildCount() {
			return this.args.size();
		}
		
		@Override
		public final Arg getChild(final int index) {
			return this.args.get(index);
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			for (int i= this.args.size() - 1; i >= 0; i--) {
				if (this.args.get(i) == child) {
					return i;
				}
			}
			return -1;
		}
		
		public ImList<Arg> getArgChildren() {
			return this.args;
		}
		
		@Override
		public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
			visitor.visit(this);
		}
		
		@Override
		public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
			for (final RAstNode arg : this.args) {
				arg.acceptInR(visitor);
			}
		}
		
		@Override
		public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			for (final RAstNode child : this.args) {
				visitor.visit(child);
			}
		}
		
		
		@Override
		final @Nullable Expression getExpr(final RAstNode child) {
			return null;
		}
		
		@Override
		final @Nullable Expression getLeftExpr() {
			return null;
		}
		
		@Override
		final @Nullable Expression getRightExpr() {
			return null;
		}
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.F_DEF_ARGS == element.getNodeType());
		}
		
		
		@Override
		final int getMissingExprStatus(final Expression expr) {
			throw new IllegalArgumentException();
		}
		
	}
	
	
	public static final class Arg extends RAstNode {
		
		
		SingleValue argName;
		boolean withDefault;
		final Expression defaultExpr= new Expression();
		
		
		Arg(final Args parent) {
			this.rParent= parent;
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.F_DEF_ARG;
		}
		
		@Override
		public final @Nullable RTerminal getOperator(final int index) {
			return null;
		}
		
		
		@Override
		public @Nullable Args getRParent() {
			return (Args)this.rParent;
		}
		
		@Override
		public final boolean hasChildren() {
			return true;
		}
		
		@Override
		public final int getChildCount() {
			return 2;
		}
		
		@Override
		public final RAstNode getChild(final int index) {
			switch (index) {
			case 0:
				return this.argName;
			case 1:
				if (this.withDefault) {
					return this.defaultExpr.node;
				}
				//$FALL-THROUGH$
			default:
				throw new IndexOutOfBoundsException();
			}
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			if (this.argName == child) {
				return 0;
			}
			if (this.defaultExpr.node == child) {
				return 1;
			}
			return -1;
		}
		
		public final RAstNode getNameChild() {
			return this.argName;
		}
		
		public final boolean hasDefault() {
			return this.withDefault;
		}
		
		public final RAstNode getDefaultChild() {
			return this.defaultExpr.node;
		}
		
		@Override
		public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
			visitor.visit(this);
		}
		
		@Override
		public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
			this.argName.acceptInR(visitor);
			if (this.withDefault) {
				this.defaultExpr.node.acceptInR(visitor);
			}
		}
		
		@Override
		public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			visitor.visit(this.argName);
			if (this.withDefault) {
				visitor.visit(this.defaultExpr.node);
			}
		}
		
		
		@Override
		final @Nullable Expression getExpr(final RAstNode child) {
			if (this.defaultExpr.node == child) {
				return this.defaultExpr;
			}
			return null;
		}
		
		@Override
		final @Nullable Expression getLeftExpr() {
			return null;
		}
		
		@Override
		final Expression getRightExpr() {
			return this.defaultExpr;
		}
		
		Expression addDefault() {
			this.withDefault= true;
			return this.defaultExpr;
		}
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.F_DEF_ARG == element.getNodeType());
		}
		
		
		@Override
		final int getMissingExprStatus(final Expression expr) {
			if (this.withDefault && this.defaultExpr == expr) {
				return TYPE123_SYNTAX_EXPR_AS_ARGVALUE_MISSING | CTX12_FDEF;
			}
			throw new IllegalArgumentException();
		}
		
	}
	
	
	int argsOpenOffset= NA_OFFSET;
	final Args args= new Args(this);
	int argsCloseOffset= NA_OFFSET;
	final Expression expr= new Expression();
	
	
	FDef() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.F_DEF;
	}
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	abstract int getOpLength();
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 2;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		switch (index) {
		case 0:
			return this.args;
		case 1:
			return this.expr.node;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.args == child) {
			return 0;
		}
		if (this.expr.node == child) {
			return 1;
		}
		return -1;
	}
	
	public final int getArgsOpenOffset() {
		return this.argsOpenOffset;
	}
	
	public final Args getArgsChild() {
		return this.args;
	}
	
	public final int getArgsCloseOffset() {
		return this.argsCloseOffset;
	}
	
	public final RAstNode getContChild() {
		return this.expr.node;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.args.acceptInR(visitor);
		this.expr.node.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.args);
		visitor.visit(this.expr.node);
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.expr.node == child) {
			return this.expr;
		}
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final Expression getRightExpr() {
		return this.expr;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.F_DEF == element.getNodeType());
	}
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (expr == this.expr) {
			return TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_FDEF;
		}
		throw new IllegalArgumentException();
	}
	
	@SuppressWarnings("unused")
	final void updateOffsets() {
		if (this.expr.node != null) {
			this.endOffset= this.expr.node.endOffset;
		}
		else if (this.argsCloseOffset != NA_OFFSET) {
			this.endOffset= this.argsCloseOffset + 1;
		}
		else if (this.args != null) {
			this.endOffset= this.args.endOffset;
		}
		else if (this.argsOpenOffset != NA_OFFSET) {
			this.endOffset= this.argsOpenOffset + 1;
		}
		else {
			this.endOffset= this.startOffset + getOpLength();
		}
	}
	
}
