/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AFTER_OP_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>+ §right§</code>
 * <code>- §right§</code>
 */
@NonNullByDefault
public abstract class Sign extends RAstNode {
	
	
	static final class PlusSign extends Sign {
		
		
		PlusSign() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SIGN;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.PLUS;
		}
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.SIGN == element.getNodeType());
		}
		
	}
	
	static final class MinusSign extends Sign {
		
		
		MinusSign() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SIGN;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.MINUS;
		}
		
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.SIGN == element.getNodeType());
		}
		
	}
	
	static final class Not extends Sign {
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.NOT;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.NOT;
		}
		
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.NOT == element.getNodeType());
		}
		
	}
	
	
	final Expression rightExpr= new Expression();
	
	
	Sign() {
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 1;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		if (index == 0) {
			return this.rightExpr.node;
		}
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.rightExpr.node == child) {
			return 0;
		}
		return -1;
	}
	
	public final RAstNode getRightChild() {
		return this.rightExpr.node;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.rightExpr.node.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.rightExpr.node);
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.rightExpr.node == child) {
			return this.rightExpr;
		}
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final Expression getRightExpr() {
		return this.rightExpr;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (this.rightExpr == expr) {
			return TYPE123_SYNTAX_EXPR_AFTER_OP_MISSING;
		}
		throw new IllegalArgumentException();
	}
	
	final void updateOffsets() {
		this.endOffset= this.rightExpr.node.endOffset;
	}
	
	
	@Override
	public boolean equalsValue(final RAstNode element) {
		if (getNodeType() == element.getNodeType()
				&& getOperator(0) == element.getOperator(0) ) {
			final Sign other= (Sign)element;
			return (this.rightExpr.node.equalsValue(other.rightExpr.node));
		}
		return false;
	}
	
}
