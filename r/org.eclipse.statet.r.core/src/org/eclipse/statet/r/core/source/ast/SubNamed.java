/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_REF_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§ref§ $ §subname§</code>
 */
@NonNullByDefault
public abstract class SubNamed extends RAstNode {
	
	
	static final class Named extends SubNamed {
		
		
		Named() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SUB_NAMED_PART;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.SUB_NAMED_PART;
		}
		
	}
	
	static final class Slot extends SubNamed {
		
		
		Slot() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SUB_NAMED_SLOT;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.SUB_NAMED_SLOT;
		}
		
	}
	
	
	final Expression expr= new Expression();
	SingleValue subname;
	int operatorOffset= NA_OFFSET;
	
	
	SubNamed() {
	}
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 2;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		switch (index) {
		case 0:
			return this.expr.node;
		case 1:
			return this.subname;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.expr.node == child) {
			return 0;
		}
		if (this.subname == child) {
			return 1;
		}
		return -1;
	}
	
	public final RAstNode getRefChild() {
		return this.expr.node;
	}
	
	public final RAstNode getSubnameChild() {
		return this.subname;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.expr.node.acceptInR(visitor);
		this.subname.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.expr.node);
		visitor.visit(this.subname);
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.expr.node == child) {
			return this.expr;
		}
		return null;
	}
	
	@Override
	final Expression getLeftExpr() {
		return this.expr;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		if (getNodeType() == element.getNodeType()) {
			final SubNamed other= (SubNamed)element;
			return ((this.expr.node == other.expr.node
							|| (this.expr.node != null && this.expr.node.equalsSingle(other.expr.node)) )
					&& (this.subname == other.subname
							|| (this.subname != null && this.subname.equalsSingle(other.subname)) )
					);
		}
		return false;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (this.expr == expr) {
			return TYPE123_SYNTAX_EXPR_AS_REF_MISSING;
		}
		throw new IllegalArgumentException();
	}
	
	final void updateOffsets() {
		this.startOffset= this.expr.node.startOffset;
		this.endOffset= this.subname.endOffset;
	}
	
}
