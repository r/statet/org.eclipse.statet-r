/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * 
 */
@NonNullByDefault
public abstract class Symbol extends SingleValue {
	
	
	static final class Std extends Symbol {
		
		
		Std() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.SYMBOL;
		}
		
		
		@Override
		public final @Nullable TextRegion getTextRegion() {
			return this;
		}
		
		@Override
		void setText(final @Nullable String text, final @Nullable TextRegion textRegion) {
			this.text= text;
		}
		
	}
	
	
	static final class G extends Symbol {
		
		
		private @Nullable TextRegion textRegion;
		
		
		G() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.SYMBOL_G;
		}
		
		
		@Override
		public final @Nullable TextRegion getTextRegion() {
			return this.textRegion;
		}
		
		@Override
		void setText(final @Nullable String text, final @Nullable TextRegion textRegion) {
			this.text= text;
			this.textRegion= textRegion;
		}
		
	}
	
	
	Symbol() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.SYMBOL;
	}
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		if (NodeType.SYMBOL == element.getNodeType()) {
			final Symbol other= (Symbol)element;
			return (Objects.equals(this.text, other.text));
		}
		return false;
	}
	
	@Override
	void appendPathElement(final StringBuilder s) {
//		if (parent != null) {
//			s.append(parent.getEqualsIndex(this));
//		}
		s.append('$');
		s.append(this.text);
	}
	
}
