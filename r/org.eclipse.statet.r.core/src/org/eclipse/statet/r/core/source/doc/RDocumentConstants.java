/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.doc;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;


@NonNullByDefault
public interface RDocumentConstants {
	
/* R *************************************************************************/
	
	/**
	 * The id of partitioning of R documents.
	 * 
	 * Value: {@value}
	 */
	String R_PARTITIONING= "org.eclipse.statet.R"; //$NON-NLS-1$
	
	
	/**
	 * The type of a default partition (R code) in R documents
	 * 
	 * Value: {@value}
	 */
	String R_DEFAULT_CONTENT_TYPE= "R.Default"; //$NON-NLS-1$
	
	/**
	 * The type of a symbol partition in R documents
	 * 
	 * Value: {@value}
	 */
	String R_QUOTED_SYMBOL_CONTENT_TYPE= "R.QuotedSymbol"; //$NON-NLS-1$
	
	/**
	 * The type of a infix operator partition in R documents
	 * 
	 * Value: {@value}
	 */
	String R_INFIX_OPERATOR_CONTENT_TYPE= "R.Op.Infix"; //$NON-NLS-1$
	
	/**
	 * The type of a string partition in R documents
	 * 
	 * Value: {@value}
	 */
	String R_STRING_CONTENT_TYPE= "R.String"; //$NON-NLS-1$
	
	/**
	 * The type of a comment partition in R documents.
	 * Value: {@value}
	 */
	String R_COMMENT_CONTENT_TYPE= "R.Comment"; //$NON-NLS-1$
	
	/**
	 * The type of a Roxygen comment partition in R documents.
	 * Value: {@value}
	 */
	String R_ROXYGEN_CONTENT_TYPE= "R.Roxygen"; //$NON-NLS-1$
	
	
	/**
	 * List with all partition content types of R documents.
	 */
	ImList<String> R_CONTENT_TYPES= ImCollections.newList(
			R_DEFAULT_CONTENT_TYPE, 
			R_QUOTED_SYMBOL_CONTENT_TYPE, 
			R_INFIX_OPERATOR_CONTENT_TYPE, 
			R_STRING_CONTENT_TYPE, 
			R_COMMENT_CONTENT_TYPE,
			R_ROXYGEN_CONTENT_TYPE );
	
	
	PartitionConstraint R_DEFAULT_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String partitionType) {
			return (partitionType == R_DEFAULT_CONTENT_TYPE);
		}
	};
	
	PartitionConstraint R_CODE_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String partitionType) {
			return (partitionType == R_DEFAULT_CONTENT_TYPE
					|| partitionType == R_STRING_CONTENT_TYPE
					|| partitionType == R_QUOTED_SYMBOL_CONTENT_TYPE
					|| partitionType == R_INFIX_OPERATOR_CONTENT_TYPE );
		}
	};
	
	PartitionConstraint R_ANY_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String partitionType) {
			return (partitionType == R_DEFAULT_CONTENT_TYPE
					|| partitionType == R_STRING_CONTENT_TYPE
					|| partitionType == R_COMMENT_CONTENT_TYPE
					|| partitionType == R_ROXYGEN_CONTENT_TYPE
					|| partitionType == R_INFIX_OPERATOR_CONTENT_TYPE
					|| partitionType == R_QUOTED_SYMBOL_CONTENT_TYPE );
		}
	};
	
	PartitionConstraint R_NO_COMMENT_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType != R_COMMENT_CONTENT_TYPE);
		}
	};
	
	
/* Rd ************************************************************************/
	
	/**
	 * The Id of partitioning of Rd documents.
	 * 
	 * Value: {@value}
	 */
	String RDOC_PARTITIONING= "org.eclipse.statet.Rd"; //$NON-NLS-1$
	
	
	/**
	 * The type of a default partition (Rd code) in Rd documents
	 * 
	 * Value: defined by {@link org.eclipse.jface.text.IDocument#DEFAULT_CONTENT_TYPE}
	 */
	String RDOC_DEFAULT_CONTENT_TYPE= IDocument.DEFAULT_CONTENT_TYPE;
	
	/**
	 * The type of a comment partition in Rd documents
	 * 
	 * Value: {@value}
	 */
	String RDOC_COMMENT_CONTENT_TYPE= "Rd.Comment"; //$NON-NLS-1$
	
	/**
	 * The type of a platform instruction partition in Rd documents
	 * 
	 * Value: {@value}
	 */
	
	String RDOC_PLATFORM_SPECIF= "__rd_platform"; //$NON-NLS-1$
	
	
	/**
	 * List with all partition content types of Rd documents.
	 */
	ImList<String> RDOC_CONTENT_TYPES= ImCollections.newList(
			RDOC_DEFAULT_CONTENT_TYPE, 
			RDOC_COMMENT_CONTENT_TYPE, 
			RDOC_PLATFORM_SPECIF );
	
}
