/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.doc;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.treepartitioner.BasicPartitionNodeType;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;


@NonNullByDefault
public abstract class RPartitionNodeType extends BasicPartitionNodeType {
	
	
	public static final RPartitionNodeType DEFAULT_ROOT= new RPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_DEFAULT_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_DEFAULT;
		}
		
		@Override
		public boolean prefereAtBegin(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
		@Override
		public boolean prefereAtEnd(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
	};
	
	public static final RPartitionNodeType STRING_S= new RPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_STRING_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_STRING_S;
		}
		
	};
	
	public static final RPartitionNodeType STRING_D= new RPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_STRING_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_STRING_D;
		}
		
	};
	
	public static final class StringR extends RPartitionNodeType {
		
		
		private final char @Nullable [] closingSequence;
		
		
		public StringR(final char @Nullable [] closingSequence) {
			this.closingSequence= closingSequence;
		}
		
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_STRING_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_STRING_R;
		}
		
		@Override
		protected char @Nullable [] getClosingSequence() {
			return this.closingSequence;
		}
		
	}
	
	public static final RPartitionNodeType QUOTED_SYMBOL= new RPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_QUOTED_SYMBOL;
		}
		
	};
	
	public static final RPartitionNodeType INFIX_OPERATOR= new RPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_INFIX_OPERATOR_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_INFIX_OPERATOR;
		}
		
	};
	
	public static abstract class Comment extends RPartitionNodeType {
		
		
		public Comment() {
		}
		
		
		@Override
		public boolean prefereAtEnd(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
	}
	
	public static final Comment COMMENT= new Comment() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_COMMENT_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_COMMENT;
		}
		
	};
	
	
	public static final Comment ROXYGEN= new Comment() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.R_ROXYGEN_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return RPartitionNodeScanner.S_ROXYGEN;
		}
		
	};
	
	
	protected RPartitionNodeType() {
	}
	
	
	public abstract byte getScannerState();
	
	protected char @Nullable [] getClosingSequence() {
		return null;
	}
	
}
