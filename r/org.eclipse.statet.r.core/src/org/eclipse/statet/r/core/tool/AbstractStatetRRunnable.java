/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.tool;

import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RReference;
import org.eclipse.statet.rj.services.FQRObject;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RGraphicCreator;
import org.eclipse.statet.rj.services.RPlatform;
import org.eclipse.statet.rj.ts.core.AbstractRToolRunnable;
import org.eclipse.statet.rj.ts.core.RTool;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class AbstractStatetRRunnable extends AbstractRToolRunnable {
	
	
	protected static void checkNewCommand(final IRConsoleService r,
			final ProgressMonitor m) throws StatusException {
		if (!r.acceptNewConsoleCommand()) {
			throw new StatusException(new ErrorStatus(RCore.BUNDLE_ID,
					"Operation cancelled because another command is already started" +
					"in the console.", null ));
		}
	}
	
	private static class RToolServiceWrapper implements IRConsoleService {
		
		
		private final RToolService service;
		
		
		public RToolServiceWrapper(final RToolService service) {
			this.service= service;
		}
		
		
		@Override
		public RTool getTool() {
			return this.service.getTool();
		}
		
		@Override
		public RPlatform getPlatform() {
			return this.service.getPlatform();
		}
		
		@Override
		public void evalVoid(final String expression,
				final ProgressMonitor m) throws StatusException {
			this.service.evalVoid(expression, m);
		}
		
		@Override
		public void evalVoid(final String expression, final @Nullable RObject envir,
				final ProgressMonitor m) throws StatusException {
			this.service.evalVoid(expression, m);
		}
		
		@Override
		public RObject evalData(final String expression,
				final ProgressMonitor m) throws StatusException {
			return this.service.evalData(expression, m);
		}
		
		@Override
		public RObject evalData(final String expression, final @Nullable String factoryId, final int options, final int depth,
				final ProgressMonitor m) throws StatusException {
			return this.service.evalData(expression, factoryId, options, depth, m);
		}
		
		@Override
		public RObject evalData(final String expression, final @Nullable RObject envir,
				final @Nullable String factoryId, final int options, final int depth,
				final ProgressMonitor m) throws StatusException {
			return this.service.evalData(expression, factoryId, options, depth, m);
		}
		
		@Override
		public RObject evalData(final RReference reference,
				final ProgressMonitor m) throws StatusException {
			return this.service.evalData(reference, m);
		}
		
		@Override
		public RObject evalData(final RReference reference, final @Nullable String factoryId, final int options, final int depth,
				final ProgressMonitor m) throws StatusException {
			return this.service.evalData(reference, factoryId, options, depth, m);
		}
		
		@Override
		public @Nullable FQRObject<? extends RTool> findData(final String symbol, final @Nullable RObject env, final boolean inherits,
				final @Nullable String factoryId, final int options, final int depth,
				final ProgressMonitor m) throws StatusException {
			return this.service.findData(symbol, env, inherits, factoryId, options, depth, m);
		}
		
		@Override
		public void assignData(final String expression, final RObject data,
				final ProgressMonitor m) throws StatusException {
			this.service.assignData(expression, data, m);
		}
		
		@Override
		public void uploadFile(final InputStream in, final long length, final String fileName, final int options,
				final ProgressMonitor m) throws StatusException {
			this.service.uploadFile(in, length, fileName, options, m);
		}
		
		@Override
		public void downloadFile(final OutputStream out, final String fileName, final int options,
				final ProgressMonitor m) throws StatusException {
			this.service.downloadFile(fileName, options, m);
		}
		
		@Override
		public byte[] downloadFile(final String fileName, final int options,
				final ProgressMonitor m) throws StatusException {
			return this.service.downloadFile(fileName, options, m);
		}
		
		@Override
		public FunctionCall createFunctionCall(final String name) throws StatusException {
			return this.service.createFunctionCall(name);
		}
		
		@Override
		public RGraphicCreator createRGraphicCreator(final int options) throws StatusException {
			return this.service.createRGraphicCreator(options);
		}
		
		@Override
		public boolean acceptNewConsoleCommand() {
			return true;
		}
		
		@Override
		public void submitToConsole(final String input,
				final ProgressMonitor m) throws StatusException {
			this.service.evalVoid("{\n" + input + "\n}", m); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		@Override
		public void briefAboutToChange() {
		}
		
		@Override
		public void briefChanged(final int o) {
		}
		
	}
	
	
	/**
	 * @param typeId
	 * @param label
	 */
	public AbstractStatetRRunnable(final String typeId, final String label) {
		super(typeId, label);
	}
	
	
	@Override
	protected void run(final RToolService r, final ProgressMonitor m) throws StatusException {
		if (r instanceof IRConsoleService) {
			run((IRConsoleService) r, m);
		}
		else {
			run(new RToolServiceWrapper(r), m);
		}
	}
	
	protected void run(final IRConsoleService r,
			final ProgressMonitor m) throws StatusException {
	}
	
}
