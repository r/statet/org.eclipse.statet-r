/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.tool;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public class TmpUtils {
	
	
	private static final String CREATE_ITEM= "rj:::tmp.createItem"; //$NON-NLS-1$
	private static final String REMOVE_ITEM= "rj:::tmp.removeItem"; //$NON-NLS-1$
	private static final String SET= "rj:::tmp.set"; //$NON-NLS-1$
	private static final String REMOVE= "rj:::tmp.remove"; //$NON-NLS-1$
	
	private static final String PREFIX_PAR= "prefix"; //$NON-NLS-1$
	private static final String ID_PAR= "id"; //$NON-NLS-1$
	private static final String NAME_PAR= "name"; //$NON-NLS-1$
	private static final String VALUE_PAR= "value"; //$NON-NLS-1$
	
	private static final RElementName PKG_ELEMENT_NAME= RElementName.create(RElementName.SCOPE_NS, "rj");
	private static final RElementName ENV_ELEMENT_NAME= RElementName.create(RElementName.MAIN_DEFAULT, ".rj.tmp");
	public static final RElementName ENV_FQ_ELEMENT_NAME= RElementName.create(ImCollections.newList(
			PKG_ELEMENT_NAME, ENV_ELEMENT_NAME ));
	public static final String ENV_FQ_NAME= "rj::.rj.tmp"; //$NON-NLS-1$
	
	
	public static boolean isTmp(final RElementName elementName) {
		return (PKG_ELEMENT_NAME.equals(elementName.getScope())
				&& ENV_ELEMENT_NAME.getSegmentName().equals(elementName.getSegmentName()) );
	}
	
	public static RElementName createFQElementName(final RElementName name) {
		return RElementName.create(ImCollections.newList(PKG_ELEMENT_NAME, ENV_ELEMENT_NAME, name));
	}
	
	
	public static class Item {
		
		
		private final String id;
		
		private final RService r;
		
		
		Item(final String id, final RService r) {
			this.id= id;
			this.r= r;
		}
		
		
		public String getId() {
			return this.id;
		}
		
		public void dispose(
				final ProgressMonitor m) throws StatusException {
			final FunctionCall call= this.r.createFunctionCall(REMOVE_ITEM);
			call.addChar(ID_PAR, this.id);
			call.evalVoid(m);
		}
		
		public void disposeChecked(final ProgressMonitor m) {
			try {
				dispose(m);
			}
			catch (final StatusException e) {
				RCorePlugin.log(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
						"An error occurred when cleaning up temporary objects.",
						e ));
			}
		}
		
		public String createSub(final String sub) {
			return getId() + sub;
		}
		
		public void set(final String sub, final RObject value,
				final ProgressMonitor m) throws StatusException {
			final FunctionCall call= this.r.createFunctionCall(SET);
			call.addChar(NAME_PAR, sub);
			call.add(VALUE_PAR, value);
			call.evalVoid(m);
		}
		
		public void set(final String sub, final String expression,
				final ProgressMonitor m) throws StatusException {
			final FunctionCall call= this.r.createFunctionCall(SET);
			call.addChar(NAME_PAR, sub);
			call.add(VALUE_PAR, expression);
			call.evalVoid(m);
		}
		
		public void remove(final String sub,
				final ProgressMonitor m) throws StatusException {
			final FunctionCall call= this.r.createFunctionCall(REMOVE);
			call.addChar(NAME_PAR, sub);
			call.evalVoid(m);
		}
		
	}
	
	
	public static Item newItem(final String prefix,
			final RService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final FunctionCall call= r.createFunctionCall(CREATE_ITEM);
		call.addChar(PREFIX_PAR, prefix);
		final String id= RDataUtils.checkSingleCharValue(call.evalData(m));
		return new Item(id, r);
	}
	
}
