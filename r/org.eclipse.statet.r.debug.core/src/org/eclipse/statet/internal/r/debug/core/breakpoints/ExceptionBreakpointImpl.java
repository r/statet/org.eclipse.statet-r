/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.breakpoints;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IBreakpoint;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.debug.core.RDebugModel;
import org.eclipse.statet.r.debug.core.breakpoints.RExceptionBreakpoint;


@NonNullByDefault
public class ExceptionBreakpointImpl extends BasicRBreakpoint implements RExceptionBreakpoint {
	
	
	public static final String R_EXCEPTION_BREAKPOINT_MARKER_TYPE= "org.eclipse.statet.r.resourceMarkers.RExceptionBreakpoint"; //$NON-NLS-1$
	
	public static final String EXCEPTION_ID_MARKER_ATTR= "org.eclipse.statet.r.resourceMarkers.ExceptionIdAttribute"; //$NON-NLS-1$
	
	
	public ExceptionBreakpointImpl(final IResource resource, final String exceptionId,
			final boolean temporary) throws DebugException {
		final Map<String, Object> attributes= new HashMap<>();
		attributes.put(IBreakpoint.ID, getModelIdentifier());
		attributes.put(IBreakpoint.ENABLED, Boolean.TRUE);
		attributes.put(EXCEPTION_ID_MARKER_ATTR, nonNullAssert(exceptionId));
		
		final IWorkspaceRunnable wr= new IWorkspaceRunnable() {
			@Override
			public void run(final IProgressMonitor monitor) throws CoreException {
				setMarker(resource.createMarker(R_EXCEPTION_BREAKPOINT_MARKER_TYPE, attributes));
				
				register(!temporary);
				if (temporary) {
					setPersisted(false);
				}
			}
		};
		run(ResourcesPlugin.getWorkspace().getRuleFactory().markerRule(resource), wr);
	}
	
	public ExceptionBreakpointImpl() {
	}
	
	
	@Override
	public String getBreakpointType() {
		return RDebugModel.R_EXCEPTION_BREAKPOINT_TYPE_ID;
	}
	
	
	@Override
	public String getExceptionId() throws DebugException {
		return nonNullAssert(ensureMarker().getAttribute(EXCEPTION_ID_MARKER_ATTR, null));
	}
	
}
