/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.breakpoints;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.breakpoints.RBreakpoint;
import org.eclipse.statet.r.debug.core.breakpoints.RBreakpointStatus;
import org.eclipse.statet.rj.server.dbg.TracepointEvent;


@NonNullByDefault
public class TracepointEventBreakpointStatus implements RBreakpointStatus {
	
	
	protected final TracepointEvent event;
	
	protected final @Nullable String label;
	protected final @Nullable RBreakpoint breakpoint;
	
	
	public TracepointEventBreakpointStatus(final TracepointEvent event,
			final @Nullable String label, final @Nullable RBreakpoint breakpoint) {
		this.event= event;
		this.label= label;
		this.breakpoint= breakpoint;
	}
	
	
	@Override
	public int getKind() {
		switch (this.event.getKind()) {
		case TracepointEvent.KIND_ABOUT_TO_HIT:
			return HIT;
		}
		return 0;
	}
	
	@Override
	public @Nullable String getLabel() {
		return this.label;
	}
	
	@Override
	public @Nullable RBreakpoint getBreakpoint() {
		return this.breakpoint;
	}
	
}
