/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.eval;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.model.IErrorReportingExpression;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.debug.core.model.RDebugElement;
import org.eclipse.statet.r.debug.core.RValue;


@NonNullByDefault
public class EvaluationExpression extends RDebugElement implements IErrorReportingExpression {
	
	
	private static final @NonNull String[] NO_MESSAGES= new String[0];
	
	
	private final EvaluationResult result;
	
	
	public EvaluationExpression(final EvaluationResult result) {
		super(result.getThread().getDebugTarget());
		this.result= result;
		
		this.result.getThread().getExpressionManager().register(this);
	}
	
	
	@Override
	public String getExpressionText() {
		return this.result.getExpressionText();
	}
	
	public EvaluationResult getResult() {
		return this.result;
	}
	
	@Override
	public boolean hasErrors() {
		return (this.result.getStatus() > IStatus.ERROR);
	}
	
	@Override
	public @NonNull String[] getErrorMessages() {
		if (this.result.getStatus() >= IStatus.ERROR) {
			final ImList<String> messages= this.result.getMessages();
			if (messages != null) {
				return messages.toArray(new @NonNull String[messages.size()]);
			}
		}
		return NO_MESSAGES;
	}
	
	@Override
	public @Nullable RValue getValue() {
		return this.result.getValue();
	}
	
	@Override
	public void dispose() {
		this.result.getThread().getExpressionManager().unregister(this);
		this.result.free();
	}
	
}
