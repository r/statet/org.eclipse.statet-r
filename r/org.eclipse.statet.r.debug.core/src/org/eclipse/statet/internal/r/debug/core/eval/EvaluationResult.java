/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.eval;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.internal.r.debug.core.RDebugCorePlugin;
import org.eclipse.statet.internal.r.debug.core.model.BasicRElementVariable;
import org.eclipse.statet.internal.r.debug.core.model.RMainThread;
import org.eclipse.statet.r.core.tool.TmpUtils;
import org.eclipse.statet.r.debug.core.REvaluationResult;
import org.eclipse.statet.r.debug.core.RValue;
import org.eclipse.statet.r.debug.core.RVariable;


@NonNullByDefault
public final class EvaluationResult implements REvaluationResult {
	
	
	private final String expression;
	
	private final RMainThread thread;
	
	private final int status;
	
	private final @Nullable BasicRElementVariable variable;
	private final TmpUtils. @Nullable Item tmpItem;
	
	private final @Nullable ImList<@NonNull String> errorMessages;
	
	private int lockCounter;
	
	
	public EvaluationResult(final String expression, final RMainThread thread,
			final BasicRElementVariable variable, final TmpUtils.Item tmpItem) {
		this.expression= expression;
		this.thread= thread;
		this.status= IStatus.OK;
		this.variable= variable;
		this.tmpItem= tmpItem;
		this.errorMessages= null;
		
		this.lockCounter= 1;
	}
	
	public EvaluationResult(final String expression, final RMainThread thread,
			final int status, final String errorMessage) {
		this.expression= expression;
		this.thread= thread;
		this.status= status;
		this.variable= null;
		this.tmpItem= null;
		this.errorMessages= ImCollections.newList(errorMessage);
		
		this.lockCounter= 1;
	}
	
	public EvaluationResult(final String expression, final RMainThread thread) {
		this.expression= expression;
		this.thread= thread;
		this.status= SKIPPED;
		this.variable= null;
		this.tmpItem= null;
		this.errorMessages= null;
		
		this.lockCounter= 1;
	}
	
	
	@Override
	public String getExpressionText() {
		return this.expression;
	}
	
	@Override
	public RMainThread getThread() {
		return this.thread;
	}
	
	@Override
	public int getStatus() {
		return this.status;
	}
	
	public @Nullable RVariable getVariable() {
		return this.variable;
	}
	
	@Override
	public @Nullable RValue getValue() {
		final var variable= this.variable;
		return (variable != null) ? variable.getCurrentValue() : null;
	}
	
	public TmpUtils. @Nullable Item getTmpItem() {
		return this.tmpItem;
	}
	
	@Override
	public @Nullable ImList<@NonNull String> getMessages() {
		return this.errorMessages;
	}
	
	
	public synchronized void lock() {
		this.lockCounter++;
	}
	
	@Override
	public synchronized void free() {
		this.lockCounter--;
		
		if (this.lockCounter < 0) {
			this.thread.getExpressionManager().scheduleClean();
		}
	}
	
	public synchronized boolean isLocked() {
		return (this.lockCounter > 0);
	}
	
	public void reset(final int stamp, final ProgressMonitor m) {
		final var variable= this.variable;
		if (variable != null) {
			try {
				variable.reset(stamp);
				variable.getValue(m);
			}
			catch (final DebugException e) {
				RDebugCorePlugin.log(new Status(IStatus.ERROR, RDebugCorePlugin.BUNDLE_ID,
						"An error occurred when refreshing expression result.", e ));
			}
		}
	}
	
}
