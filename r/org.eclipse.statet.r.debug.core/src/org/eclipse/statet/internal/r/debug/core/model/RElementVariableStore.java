/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import java.util.function.Consumer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RElementVariableStore {
	
	
	private static final int SEGMENT_LENGTH= 1000;
	
	
	private final long length;
	private final @Nullable BasicRElementVariable[] @Nullable [] arrays;
	
	
	public RElementVariableStore(final long length) {
		this.length= length;
		
		this.arrays= new @Nullable BasicRElementVariable[
				(this.length > 0) ?
						(int) ((this.length - 1) / SEGMENT_LENGTH) + 1 :
						0 ] @Nullable [];
	}
	
	
	public void set(final long idx, final BasicRElementVariable value) {
		final int idx0= (int) (idx / SEGMENT_LENGTH);
		@Nullable BasicRElementVariable[] segment= this.arrays[idx0];
		if (segment == null) {
			segment= this.arrays[idx0]= new @Nullable BasicRElementVariable[
					(idx0 == this.arrays.length - 1) ?
							(int) ((this.length - 1) % SEGMENT_LENGTH) + 1 :
							SEGMENT_LENGTH];
		}
		final int idx1= (int) (idx % SEGMENT_LENGTH);
		segment[idx1]= value;
	}
	
	public @Nullable BasicRElementVariable get(final long idx) {
		final int idx0= (int) (idx / SEGMENT_LENGTH);
		final @Nullable BasicRElementVariable[] segment= this.arrays[idx0];
		return (segment != null) ? segment[(int) (idx % SEGMENT_LENGTH)] : null;
	}
	
	public @Nullable BasicRElementVariable clear(final long idx) {
		final int idx0= (int) (idx / SEGMENT_LENGTH);
		final @Nullable BasicRElementVariable[] segment= this.arrays[idx0];
		if (segment == null) {
			return null;
		}
		final int idx1= (int) (idx % SEGMENT_LENGTH);
		final BasicRElementVariable value= segment[idx1];
		segment[idx1]= null;
		return value;
	}
	
//	public BasicRElementVariable[] get(final long firstIdx, final int length) {
//		final BasicRElementVariable[] array= new BasicRElementVariable[length];
//		int idx0= (int) (firstIdx / SEGMENT_LENGTH);
//		int idx1= (int) (firstIdx % SEGMENT_LENGTH);
//		for (int i= 0; i < length;) {
//			final BasicRElementVariable[] segment= this.arrays[idx0];
//			final int l= Math.min(length - i, SEGMENT_LENGTH - idx1);
//			System.arraycopy(segment, idx1, array, i, l);
//			idx0++;
//			idx1= 0;
//			i+= l;
//		}
//		return array;
//	}
	
	public void forEachSet(final Consumer<BasicRElementVariable> action) {
		for (int idx0= 0; idx0 < this.arrays.length; idx0++) {
			final @Nullable BasicRElementVariable[] segment= this.arrays[idx0];
			if (segment != null) {
				for (int idx1= 0; idx1 < segment.length; idx1++) {
					final BasicRElementVariable value= segment[idx1];
					if (value != null) {
						action.accept(value);
					}
				}
			}
		}
	}
	
}
