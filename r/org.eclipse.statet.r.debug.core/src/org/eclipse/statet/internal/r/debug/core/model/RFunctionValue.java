/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.rmodel.Parameters;
import org.eclipse.statet.r.core.rmodel.Parameters.Parameter;
import org.eclipse.statet.r.core.rmodel.RLangMethod;


@NonNullByDefault
public class RFunctionValue extends RElementVariableValue<CombinedRElement> {
	
	
	public RFunctionValue(final BasicRElementVariable variable) {
		super(variable);
	}
	
	
	@Override
	public String getValueString() throws DebugException {
		if (this.element instanceof RLangMethod) {
			final var lang= (RLangMethod<?>)this.element;
			final StringBuilder sb= new StringBuilder();
			final Parameters params= lang.getParameters();
			sb.append("function("); //$NON-NLS-1$
			if (params == null) {
				sb.append("<unknown>"); //$NON-NLS-1$
			}
			else if (params.size() > 0) {
				append(sb, params.get(0));
				{	for (int i= 1; i < params.size(); i++) {
						sb.append(", "); //$NON-NLS-1$
						append(sb, params.get(i));
					}
				}
			}
			sb.append(")"); //$NON-NLS-1$
			return sb.toString();
		}
		return super.getValueString();
	}
	
	private void append(final StringBuilder sb, final Parameter parameter) {
		final String name= parameter.getName();
		if (name != null) {
			sb.append(name);
		}
	}
	
}
