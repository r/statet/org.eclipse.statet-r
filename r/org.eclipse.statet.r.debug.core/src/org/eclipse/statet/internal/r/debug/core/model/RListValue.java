/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.core.model.VariablePartitionFactory;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.data.CombinedRList;
import org.eclipse.statet.r.debug.core.RVariable;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RList;


@NonNullByDefault
public class RListValue extends RElementVariableValue<CombinedRList> implements RIndexValueInternal {
	
	
	protected static class RListPartition extends RVariablePartition {
		
		
		public RListPartition(final RIndexElementValue value,
				final VariablePartitionFactory<RIndexElementValue>.PartitionHandle partition) {
			super(value, partition);
		}
		
		
		@Override
		protected @Nullable String getNameIndexLabel(final long idx) {
			return ((RList) this.value.getElement()).getName(idx);
		}
		
	}
	
	protected static final VariablePartitionFactory<RIndexElementValue> LIST_PARTITION_FACTORY= 
			new VariablePartitionFactory<>() {
		
		@Override
		protected RVariable createPartition(final RIndexElementValue value, final PartitionHandle partition) {
			return new RListPartition(value, partition);
		}
		
	};
	
	protected static class ByName extends RListValue {
		
		public ByName(final BasicRElementVariable variable) {
			super(variable);
		}
		
		
		@Override
		protected @Nullable BasicRElementVariable checkPreviousVariable(final RListValue previousValue,
				final long idx, final CombinedRElement element) {
			final RCharacterStore names= previousValue.element.getNames();
			if (names != null) {
				return super.checkPreviousVariable(previousValue,
						names.indexOf(this.element.getName(idx)), element);
			}
			return null;
		}
		
	}
	
	
	private final RElementVariableStore childVariables;
	
	
	public RListValue(final BasicRElementVariable variable) {
		super(variable);
		
		this.childVariables= new RElementVariableStore(this.element.getLength());
	}
	
	
	public final @Nullable RListValue getVariablePreviousValue() {
		return (RListValue) this.variable.getPreviousValue();
	}
	
	
	@Override
	public String getReferenceTypeName() throws DebugException {
		return this.element.getRClassName();
	}
	
	@Override
	public String getValueString() throws DebugException {
		final StringBuilder sb= new StringBuilder();
		sb.append('[');
		sb.append(this.element.getLength());
		sb.append(']');
		return sb.toString();
	}
	
	@Override
	public String getDetailString() {
		return ""; //$NON-NLS-1$
	}
	
	
	@Override
	public boolean hasVariables() throws DebugException {
		return this.element.hasModelChildren(null);
	}
	
	@Override
	public @NonNull IVariable[] getVariables() throws DebugException {
		return getPartitionFactory().getVariables(this);
	}
	
	
	@Override
	public final VariablePartitionFactory<RIndexElementValue> getPartitionFactory() {
		return LIST_PARTITION_FACTORY;
	}
	
	@Override
	public long getSize() throws DebugException {
		return this.element.getLength();
	}
	
	@Override
	public @NonNull RVariable[] getVariables(final long offset, final int length) {
		return getVariables(offset, length, this.variable);
	}
	
	@Override
	public @NonNull RVariable[] getVariables(final long offset, final int length,
			final RVariable parent) {
		final RListValue previousValue;
		synchronized (this.variable) {
			if (this != this.variable.getCurrentValue()) {
				return NO_VARIABLES;
			}
			previousValue= getVariablePreviousValue();
		}
		synchronized (this.childVariables) {
			final var variables= new @NonNull RVariable[length];
			final boolean direct= (parent == this.variable);
			for (int i= 0; i < length; i++) {
				final long idx= offset + i;
				BasicRElementVariable childVariable= this.childVariables.get(idx);
				if (childVariable == null) {
					final CombinedRElement childElement= this.element.get(idx);
					if (previousValue != null) {
						childVariable= checkPreviousVariable(previousValue, idx, childElement);
					}
					if (childVariable == null) {
						childVariable= new BasicRElementVariable(childElement,
								this.variable.getThread(), this.stamp, this.variable );
					}
					this.childVariables.set(idx, childVariable);
				}
				variables[i]= (direct) ? childVariable : RVariableProxy.create(childVariable, parent);
			}
			return variables;
		}
	}
	
	protected @Nullable BasicRElementVariable checkPreviousVariable(final RListValue previousValue,
			final long idx, final CombinedRElement element) {
		if (idx >= 0 && idx < previousValue.element.getLength()) {
			final BasicRElementVariable previousVariable;
			synchronized (previousValue) {
				previousVariable= previousValue.childVariables.clear(idx);
			}
			if (previousVariable != null
					&& previousVariable.update(element, this.stamp) ) {
				return previousVariable;
			}
		}
		return null;
	}
	
}
