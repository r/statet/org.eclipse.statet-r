/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.services.FQRObject;
import org.eclipse.statet.rj.services.FQRObjectRef;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RService;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore;


/**
 * org.eclipse.statet.rj.services.util.dataaccess.AbstractRDataAdapter<TRObject, TFragmentObject>
 * for simple objects
 */
@NonNullByDefault
public class RObjectAdapter<TRObject extends RObject> {
	
	
	// org.eclipse.statet.rj.services.util.dataaccess
	
	protected static final String API_R_PREFIX= "rj:::sda002"; //$NON-NLS-1$
	
	
	protected static void addXRef(final FunctionCall fcall, final FQRObjectRef ref) {
		fcall.add("x.env", ref.getEnv()); //$NON-NLS-1$
		fcall.add("x.expr", DefaultRObjectFactory.INSTANCE.createExpression( //$NON-NLS-1$
				"x.env$" + ((RLanguage)ref.getName()).getSource() )); //$NON-NLS-1$
	}
	
	// ----
	
	
	private final byte objectType;
	
	
	public RObjectAdapter(final byte objectType) {
		this.objectType= objectType;
	}
	
	
	public TRObject loadObject(final FQRObjectRef ref, final RObject referenceObject,
			final LazyRStore.Fragment<TRObject> fragment,
			final RService r,
			final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final RObject object;
		if (((RLanguage)ref.getName()).getLanguageType() == RLanguage.NAME) {
			final FQRObject fqrObject= r.findData(((RLanguage)ref.getName()).getSource(),
					ref.getEnv(), false,
					null, getLoadOptions(), RService.DEPTH_INFINITE,
					m );
			object= (fqrObject != null) ? fqrObject.getObject() : null;
		}
		else {
			final FunctionCall fcall= r.createFunctionCall(getLoadObjectFName());
			addXRef(fcall, ref);
			
			object= fcall.evalData(null, getLoadOptions(), RService.DEPTH_INFINITE,
					m );
		}
		
		return validateObject(object, referenceObject, fragment);
	}
	
	protected String getLoadObjectFName() {
		return API_R_PREFIX + ".getObject"; //$NON-NLS-1$
	}
	
	protected int getLoadOptions() {
		return RObjectFactory.F_WITH_DBG;
	}
	
	protected TRObject validateObject(final @Nullable RObject rObject, final RObject referenceObject,
			final LazyRStore.Fragment<TRObject> fragment)
			throws UnexpectedRDataException {
		return (TRObject) RDataUtils.checkType(rObject, this.objectType);
	}
	
}
