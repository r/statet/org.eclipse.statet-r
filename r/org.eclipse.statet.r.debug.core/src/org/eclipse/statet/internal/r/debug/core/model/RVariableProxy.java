/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.RDebugTarget;
import org.eclipse.statet.r.debug.core.RElementVariable;
import org.eclipse.statet.r.debug.core.RValue;
import org.eclipse.statet.r.debug.core.RVariable;


@NonNullByDefault
public class RVariableProxy implements RVariable {
	
	
	public static RVariableProxy create(final RVariable variable, final RVariable parent) {
		if (variable instanceof RElementVariable) {
			return new RElementVariableProxy((RElementVariable)variable, parent);
		}
		return new RVariableProxy(variable, parent);
	}
	
	public static @Nullable Object unproxy(final @Nullable Object obj) {
		if (obj instanceof RVariableProxy) {
			return ((RVariableProxy)obj).variable;
		}
		return obj;
	}
	
	
	protected final RVariable variable;
	
	protected final RVariable parent;
	
	
	protected RVariableProxy(final RVariable variable, final RVariable parent) {
		this.variable= variable;
		this.parent= parent;
	}
	
	
	@Override
	public final String getModelIdentifier() {
		return this.variable.getModelIdentifier();
	}
	
	@Override
	public final RDebugTarget getDebugTarget() {
		return this.variable.getDebugTarget();
	}
	
	@Override
	public final ILaunch getLaunch() {
		return this.variable.getLaunch();
	}
	
	@Override
	public final @Nullable RVariable getParent() {
		return this.parent;
	}
	
	
	@Override
	public final String getName() {
		return this.variable.getName();
	}
	
	@Override
	public final String getReferenceTypeName() throws DebugException {
		return this.variable.getReferenceTypeName();
	}
	
	@Override
	public final boolean hasValueChanged() throws DebugException {
		return this.variable.hasValueChanged();
	}
	
	@Override
	public final RValue getValue() throws DebugException {
		return RValueProxy.create(this.variable.getValue(), this);
	}
	
	@Override
	public final boolean supportsValueModification() {
		return this.variable.supportsValueModification();
	}
	
	@Override
	public final boolean verifyValue(final String expression) throws DebugException {
		return this.variable.verifyValue(expression);
	}
	
	@Override
	public final boolean verifyValue(final IValue value) throws DebugException {
		return this.variable.verifyValue(value);
	}
	
	@Override
	public final void setValue(final String expression) throws DebugException {
		this.variable.setValue(expression);
	}
	
	@Override
	public final void setValue(final IValue value) throws DebugException {
		this.variable.setValue(value);
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> type) {
		return this.variable.getAdapter(type);
	}
	
	
	@Override
	public final int hashCode() {
		return this.variable.hashCode();
	}
	
	@Override
	public final boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		return this.variable.equals(obj);
	}
	
	@Override
	public final String toString() {
		return this.variable.toString();
	}
	
}
