/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.sourcelookup;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.ISourcePathComputerDelegate;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.debug.core.sourcelookup.REnvLibraryPathSourceContainer;
import org.eclipse.statet.r.launching.core.RLaunching;
import org.eclipse.statet.rj.renv.core.REnv;


public class RSourcePathComputer implements ISourcePathComputerDelegate {
	
	
	/** Created via extension point */
	public RSourcePathComputer() {
	}
	
	
	@Override
	public ISourceContainer[] computeSourceContainers(final ILaunchConfiguration configuration,
			final IProgressMonitor monitor) throws CoreException {
		final REnv rEnv= RLaunching.readREnv(configuration);
		if (rEnv == null) {
			throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					Messages.RSourcePathComputer_error_REnvNotFound_message, null)); 
		}
		return new ISourceContainer[] {
				new REnvLibraryPathSourceContainer(rEnv),
		};
	}
	
}
