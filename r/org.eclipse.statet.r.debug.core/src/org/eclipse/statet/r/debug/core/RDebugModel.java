/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointManager;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IErrorReportingExpression;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.debug.core.breakpoints.ExceptionBreakpointImpl;
import org.eclipse.statet.internal.r.debug.core.breakpoints.LineBreakpointImpl;
import org.eclipse.statet.internal.r.debug.core.breakpoints.MethodBreakpointImpl;
import org.eclipse.statet.internal.r.debug.core.eval.EvaluationExpression;
import org.eclipse.statet.internal.r.debug.core.eval.EvaluationResult;
import org.eclipse.statet.internal.r.debug.core.model.RDebugTargetImpl;
import org.eclipse.statet.r.debug.core.breakpoints.RExceptionBreakpoint;
import org.eclipse.statet.r.debug.core.breakpoints.RLineBreakpoint;
import org.eclipse.statet.r.debug.core.breakpoints.RMethodBreakpoint;
import org.eclipse.statet.r.nico.AbstractRDbgController;


@NonNullByDefault
public class RDebugModel {
	
	/**
	 * Identifier of the R debug model
	 */
	public static final String IDENTIFIER= "org.eclipse.statet.r.debugModels.R"; //$NON-NLS-1$
	
	
	public static final String R_LINE_BREAKPOINT_TYPE_ID=
			"org.eclipse.statet.r.debugBreakpoints.RLineBreakpoint"; //$NON-NLS-1$
	public static final String R_METHOD_BREAKPOINT_TYPE_ID=
			"org.eclipse.statet.r.debugBreakpoints.RMethodBreakpoint"; //$NON-NLS-1$
	public static final String R_EXCEPTION_BREAKPOINT_TYPE_ID=
			"org.eclipse.statet.r.debugBreakpoints.RExceptionBreakpoint"; //$NON-NLS-1$
	
	private static final List<RLineBreakpoint> NO_R_LINE_BREAKPOINTS= Collections.emptyList();
	
	
	/**
	 * Creates a new R line breakpoint.
	 * 
	 * @param resource
	 * @param lineNumber (1-based)
	 * @param charStart
	 * @param charEnd
	 * @param elementLabel
	 * @param path 
	 * @param temporary
	 * @return the new breakpoint
	 * @throws DebugException
	 */
	public static RLineBreakpoint createLineBreakpoint(
			final IFile resource,
			final int lineNumber, final int charStart, final int charEnd,
			final int elementType, final String elementId, final String elementLabel, final String subLabel,
			final boolean temporary) throws CoreException {
		return new LineBreakpointImpl(resource, lineNumber, charStart, charEnd,
				elementType, elementId, elementLabel, subLabel,
				temporary );
	}
	
	/**
	 * Creates a new R method breakpoint.
	 * 
	 * @param resource
	 * @param lineNumber (1-based)
	 * @param charStart
	 * @param charEnd
	 * @param elementLabel
	 * @param temporary
	 * @return the new breakpoint
	 * @throws DebugException
	 */
	public static RMethodBreakpoint createMethodBreakpoint(
			final IFile resource,
			final int lineNumber, final int charStart, final int charEnd,
			final int elementType, final String elementId, final String elementLabel, final String subLabel,
			final boolean temporary) throws CoreException {
		return new MethodBreakpointImpl(resource, lineNumber, charStart, charEnd,
				elementType, elementId, elementLabel, subLabel,
				temporary );
	}
	
	public static RExceptionBreakpoint createExceptionBreakpoint(final String exceptionId,
			final boolean temporary) throws CoreException {
		return new ExceptionBreakpointImpl(ResourcesPlugin.getWorkspace().getRoot(), exceptionId,
				temporary );
	}
	
	/**
	 * Returns all existing R line breakpoint in the specified source file.
	 * 
	 * @param file
	 * @param enabled if breakpoint must be enabled
	 * @return list of the list
	 * @throws CoreException
	 */
	public static List<RLineBreakpoint> getLineBreakpoints(
			final IFile file)
			throws CoreException {
		final IBreakpointManager manager= DebugPlugin.getDefault().getBreakpointManager();
		final IBreakpoint[] breakpoints= manager.getBreakpoints(RDebugModel.IDENTIFIER);
		List<RLineBreakpoint> matches= null;
		for (int i= 0; i < breakpoints.length; i++) {
			if (breakpoints[i] instanceof RLineBreakpoint) {
				final RLineBreakpoint breakpoint= (RLineBreakpoint)breakpoints[i];
				final IMarker marker= breakpoint.getMarker();
				if (marker != null && marker.exists()
						&& file.equals(marker.getResource()) ) {
					if (matches == null) {
						matches= new ArrayList<>(4);
					}
					matches.add(breakpoint);
				}
			}
		}
		return (matches != null) ? matches : NO_R_LINE_BREAKPOINTS;
	}
	
	/**
	 * Returns all existing R line breakpoint in the specified source line.
	 * 
	 * @param file
	 * @param lineNumber (1-based) line number
	 * @param enabled if breakpoint must be enabled
	 * @return list of the list
	 * @throws CoreException
	 */
	public static List<RLineBreakpoint> getLineBreakpoints(
			final IFile file,
			final int lineNumber)
			throws CoreException {
		final IBreakpointManager manager= DebugPlugin.getDefault().getBreakpointManager();
		final IBreakpoint[] breakpoints= manager.getBreakpoints(RDebugModel.IDENTIFIER);
		List<RLineBreakpoint> matches= null;
		for (int i= 0; i < breakpoints.length; i++) {
			if (breakpoints[i] instanceof RLineBreakpoint) {
				final RLineBreakpoint breakpoint= (RLineBreakpoint)breakpoints[i];
				final IMarker marker= breakpoint.getMarker();
				if (marker != null && marker.exists()
						&& file.equals(marker.getResource())
						&& lineNumber == breakpoint.getLineNumber() ) {
					if (matches == null) {
						matches= new ArrayList<>(4);
					}
					matches.add(breakpoint);
				}
			}
		}
		return (matches != null) ? matches : NO_R_LINE_BREAKPOINTS;
	}
	
	public static @Nullable RExceptionBreakpoint getExpressionBreakpoint(
			final String expressionId)
			throws DebugException {
		final IBreakpointManager manager= DebugPlugin.getDefault().getBreakpointManager();
		final IBreakpoint[] breakpoints= manager.getBreakpoints(RDebugModel.IDENTIFIER);
		for (int i= 0; i < breakpoints.length; i++) {
			if (breakpoints[i] instanceof RExceptionBreakpoint) {
				final RExceptionBreakpoint breakpoint= (RExceptionBreakpoint)breakpoints[i];
				if (breakpoint.getExceptionId().equals(expressionId)) {
					return breakpoint;
				}
			}
		}
		return null;
	}
	
	
	public static IErrorReportingExpression createExpression(
			final REvaluationResult result) {
		return new EvaluationExpression((EvaluationResult)result);
	}
	
//	/**
//	 * Returns all existing R line breakpoint in the specified source line.
//	 * 
//	 * @param file
//	 * @param lineNumber (1-based)
//	 * @return list of the list
//	 * @throws CoreException
//	 */
//	public static List<RLineBreakpoint> getRLineBreakpoints(final IFile file,
//			I) throws CoreException {
//		final IBreakpointManager manager= DebugPlugin.getDefault().getBreakpointManager();
//		final IBreakpoint[] breakpoints= manager.getBreakpoints(RDebugModel.IDENTIFIER);
//		List<RLineBreakpoint> matches= null;
//		for (int i= 0; i < breakpoints.length; i++) {
//			if (breakpoints[i] instanceof RLineBreakpoint) {
//				final RLineBreakpoint breakpoint= (RLineBreakpoint)breakpoints[i];
//				final IMarker marker= breakpoint.getMarker();
//				if (marker != null && marker.exists()
//						&& file.equals(marker.getResource())
//						&& lineNumber == breakpoint.getLineNumber() ) {
//					if (matches == null) {
//						matches= new ArrayList<RLineBreakpoint>(4);
//					}
//					matches.add(breakpoint);
//				}
//			}
//		}
//		return (matches != null) ? matches : NO_R_LINE_BREAKPOINTS;
//	}
	
	
	/**
	 * Creates a new R debug target for a controller based on {@link AbstractRDbgController}.
	 * The debug target initializes the debug mode of the controller.
	 * 
	 * @param controller the controller
	 * @return a debug target for the controller
	 */
	public static RDebugTarget createRDebugTarget(
			final AbstractRDbgController controller) {
		return new RDebugTargetImpl(controller);
	}
	
}
