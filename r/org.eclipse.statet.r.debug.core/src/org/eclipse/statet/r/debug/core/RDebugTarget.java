/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core;

import org.eclipse.debug.core.model.IDebugTarget;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.console.core.RProcess;


/**
 * Represents the R engine in the Eclipse debug model for R.
 */
@NonNullByDefault
public interface RDebugTarget extends IDebugTarget {
	
	
	@Override
	RProcess getProcess();
	
	@Override
	RDebugTarget getDebugTarget();
	
	
	@Override
	<T> @Nullable T getAdapter(final Class<T> type);
	
}
