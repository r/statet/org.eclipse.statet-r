/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.breakpoints;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.model.ILineBreakpoint;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RLineBreakpoint extends RBreakpoint, ILineBreakpoint {
	
	int R_COMMON_FUNCTION_ELEMENT_TYPE= 1;
	int R_S4_METHOD_ELEMENT_TYPE= 2;
	
	int R_TOPLEVEL_COMMAND_ELEMENT_TYPE= 9;
	
	
	void setConditionEnabled(boolean enabled) throws CoreException;
	boolean isConditionEnabled() throws CoreException;
	void setConditionExpr(String code) throws CoreException;
	String getConditionExpr() throws CoreException;
	
	int getElementType() throws CoreException;
//	String getElementId() throws CoreException;
	@Nullable String getElementLabel() throws CoreException;
	@Nullable String getSubLabel() throws CoreException;
	
}
