/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.sourcelookup;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.ISourceContainerType;
import org.eclipse.debug.core.sourcelookup.containers.CompositeSourceContainer;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.internal.r.debug.core.sourcelookup.Messages;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.RLibGroup;
import org.eclipse.statet.rj.renv.core.RLibLocation;


public class REnvLibraryPathSourceContainer extends CompositeSourceContainer {
	
	
	public static final String TYPE_ID= "org.eclipse.statet.r.debugSourceContainers.REnvLibraryPathType"; //$NON-NLS-1$
	
	
	private final REnv rEnv;
	
	
	public REnvLibraryPathSourceContainer(final REnv rEnv) {
		if (rEnv == null) {
			throw new NullPointerException("rEnv"); //$NON-NLS-1$
		}
		this.rEnv= rEnv;
	}
	
	
	@Override
	public ISourceContainerType getType() {
		return getSourceContainerType(TYPE_ID);
	}
	
	@Override
	public String getName() {
		return NLS.bind(Messages.REnvLibraryPathSourceContainer_name, this.rEnv.getName());
	}
	
	public REnv getREnv() {
		return this.rEnv;
	}
	
	@Override
	protected ISourceContainer[] createSourceContainers() throws CoreException {
		final List<ISourceContainer> list= new ArrayList<>();
		final REnvConfiguration rEnvConfig= this.rEnv.get(REnvConfiguration.class);
		if (rEnvConfig == null) {
			abort(Messages.REnvLibraryPathSourceContainer_error_REnvNotAvailable_message, null);
		}
		final List<? extends RLibGroup> libraryGroups= rEnvConfig.getRLibGroups();
		for (final RLibGroup group : libraryGroups) {
			final List<? extends RLibLocation> libraries= group.getLibLocations();
			for (final RLibLocation lib : libraries) {
				final Path path= lib.getDirectoryPath();
				if (path != null) {
					final RLibrarySourceContainer container= new RLibrarySourceContainer(
							path.toString(), path );
					container.init(getDirector());
					list.add(container);
				}
			}
		}
		return list.toArray(new ISourceContainer[list.size()]);
	}
	
	
	@Override
	public int hashCode() {
		return this.rEnv.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		return (obj instanceof REnvLibraryPathSourceContainer
				&& this.rEnv.equals(obj));
	}
	
}
