/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.sourcelookup;

import java.net.URI;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.ISourceContainerType;
import org.eclipse.debug.core.sourcelookup.containers.CompositeSourceContainer;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RProjectSourceContainer extends CompositeSourceContainer implements RSourceContainer {
	
	
	public static final String TYPE_ID= "org.eclipse.statet.r.debugSourceContainers.RProjectType"; //$NON-NLS-1$
	
	
	private final IProject project;
	
	private boolean referenced;
	
	
	public RProjectSourceContainer(final IProject project, final boolean referenced) {
		this.project= project;
	}
	
	
	@Override
	public ISourceContainerType getType() {
		return getSourceContainerType(TYPE_ID);
	}
	
	@Override
	public String getName() {
		return this.project.getName();
	}
	
	@Override
	public boolean isComposite() {
		return this.referenced;
	}
	
	/**
	 * @return the project
	 */
	public IProject getProject() {
		return this.project;
	}
	
	@Override
	protected @NonNull ISourceContainer[] createSourceContainers() throws CoreException {
		// TODO: project dependencies
		return new ISourceContainer[0];
	}
	
	
	@Override
	public @NonNull Object[] findSourceElements(final String name) throws CoreException {
		return EMPTY;
	}
	
	@Override
	public @Nullable IFile findSourceElement(final @Nullable URI fileUri, final IFile[] fileInWorkspace) {
		if (this.project.isOpen() && this.project.exists()) {
			for (final IFile workspaceFile : fileInWorkspace) {
				if (this.project.equals(workspaceFile.getProject())
						&& workspaceFile.exists() ) {
					return workspaceFile;
				}
			}
		}
		return null;
	}
	
	@Override
	public void findSourceElement(final IPath path, final List<Object> elements) {
		if (this.project.isOpen() && this.project.exists()) {
			final IFile file= this.project.getFile(path);
			if (file.exists()) {
				elements.add(file);
			}
		}
	}
	
	@Override
	public int hashCode() {
		return this.project.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof RProjectSourceContainer
						&& this.project.equals(((RProjectSourceContainer)obj).project) ));
	}
	
}
