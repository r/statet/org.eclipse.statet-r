/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.sourcelookup;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.text.core.util.ImmutableDocument;

import org.eclipse.statet.ltk.core.input.BasicSourceFragment;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.rj.renv.core.REnv;


/**
 * Source fragment of an R process
 */
@NonNullByDefault
public class RRuntimeSourceFragment extends BasicSourceFragment {
	
	
	private static String createId(final RProcess process, final String name, 
			final String fullName, final String source) {
		return "r:" + process.getLabel(Tool.DEFAULT_LABEL) + '-'+  //$NON-NLS-1$
				process.getStartupTimestamp() + '/' +
				fullName + '-' + source.hashCode();
	}
	
	
	private final RProcess process;
	
	
	public RRuntimeSourceFragment(final RProcess process,
			final String name, final String fullName, final String source) {
		super(createId(process, name, fullName, source), name, fullName,
				new ImmutableDocument(source, System.currentTimeMillis()) );
		this.process= process;
	}
	
	
	public RProcess getProcess() {
		return this.process;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == ToolProcess.class) {
			return (T) this.process;
		}
		if (adapterType == REnv.class) {
			return this.process.getAdapter((Class<T>)REnv.class);
		}
		return null;
	}
	
}
