/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.debug.ui.IDebugView;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.debug.core.RDebugTarget;
import org.eclipse.statet.r.debug.core.RElementVariable;
import org.eclipse.statet.r.debug.core.RVariable;
import org.eclipse.statet.r.ui.dataeditor.RDataEditor;
import org.eclipse.statet.r.ui.dataeditor.RLiveDataEditorInput;


public class OpenInEditorHandler extends AbstractDebugHandler {
	
	
	public OpenInEditorHandler() {
	}
	
	
	@Override
	public void setEnabled(final Object evaluationContext) {
		final IWorkbenchPart part= WorkbenchUIUtils.getActivePart(evaluationContext);
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(evaluationContext);
		if (part != null && selection != null && !selection.isEmpty()) {
			if (selection instanceof IStructuredSelection) {
				final IStructuredSelection structSelection= (IStructuredSelection) selection;
				if (structSelection.size() == 1) {
					final Object obj= structSelection.getFirstElement();
					if (obj instanceof RVariable) {
						final RVariable rVariable= (RVariable) obj;
						final RElementVariable elementVariable= getElementVariable(rVariable);
						if (elementVariable != null) {
							final CombinedRElement element= elementVariable.getElement();
							setBaseEnabled(element != null
									&& elementVariable.getFQElementName() != null
									&& RLiveDataEditorInput.isSupported(element) );
							return;
						}
					}
				}
			}
		}
		setBaseEnabled(false);
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart part= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		if (part instanceof IDebugView && selection != null && !selection.isEmpty()) {
			if (selection instanceof IStructuredSelection) {
				final IStructuredSelection structSelection= (IStructuredSelection) selection;
				if (structSelection.size() == 1) {
					final Object obj= structSelection.getFirstElement();
					if (obj instanceof RVariable) {
						final RVariable rVariable= (RVariable) obj;
						final RElementVariable elementVariable= getElementVariable(rVariable);
						if (elementVariable != null) {
							final RDebugTarget debugTarget= elementVariable.getDebugTarget();
							final RElementName elementName= elementVariable.getFQElementName();
							if (elementName != null) {
								RDataEditor.open(part.getSite().getPage(), debugTarget.getProcess(),
										elementName, getVariableItemIndex(rVariable) );
								return null;
							}
						}
					}
				}
			}
		}
		return null;
	}
	
}
