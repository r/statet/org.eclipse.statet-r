/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.breakpoints;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.debug.ui.IDetailPane;
import org.eclipse.debug.ui.IDetailPaneFactory;
import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.statet.internal.r.debug.ui.Messages;
import org.eclipse.statet.r.debug.core.RDebugModel;
import org.eclipse.statet.r.debug.core.breakpoints.RBreakpoint;


public class BreakpointDetailFactory implements IDetailPaneFactory {
	
	
	public BreakpointDetailFactory() {
	}
	
	
	@Override
	public Set getDetailPaneTypes(final IStructuredSelection selection) {
		final Set<String> types= new HashSet<>();
		if (selection.size() == 1 && selection.getFirstElement() instanceof RBreakpoint) {
			final String breakpointType= ((RBreakpoint)selection.getFirstElement()).getBreakpointType();
			if (breakpointType == RDebugModel.R_LINE_BREAKPOINT_TYPE_ID) {
				types.add(RLineBreakpointDetailPane.ID);
			}
			else if (breakpointType == RDebugModel.R_METHOD_BREAKPOINT_TYPE_ID) {
				types.add(RMethodBreakpointDetailPane.ID);
			}
		}
		return types;
	}
	
	@Override
	public String getDefaultDetailPane(final IStructuredSelection selection) {
		if (selection.size() == 1 && selection.getFirstElement() instanceof RBreakpoint) {
			final String breakpointType= ((RBreakpoint)selection.getFirstElement()).getBreakpointType();
			if (breakpointType == RDebugModel.R_LINE_BREAKPOINT_TYPE_ID) {
				return RLineBreakpointDetailPane.ID;
			}
			else if (breakpointType == RDebugModel.R_METHOD_BREAKPOINT_TYPE_ID) {
				return RMethodBreakpointDetailPane.ID;
			}
		}
		return null;
	}
	
	@Override
	public String getDetailPaneName(final String paneID) {
		if (paneID.equals(RLineBreakpointDetailPane.ID)
				|| paneID.equals(RMethodBreakpointDetailPane.ID) ) { 
			return Messages.Breakpoint_DefaultDetailPane_name;
		}
		return null;
	}
	
	@Override
	public String getDetailPaneDescription(final String paneID) {
		if (paneID.equals(RLineBreakpointDetailPane.ID)
				|| paneID.equals(RMethodBreakpointDetailPane.ID) ) { 
			return Messages.Breakpoint_DefaultDetailPane_description;
		}
		return null;
	}
	
	@Override
	public IDetailPane createDetailPane(final String paneID) {
		if (paneID.equals(RLineBreakpointDetailPane.ID)) {
			return new RLineBreakpointDetailPane();
		}
		else if (paneID.equals(RMethodBreakpointDetailPane.ID)) {
			return new RMethodBreakpointDetailPane();
		}
		return null;
	}
	
}
