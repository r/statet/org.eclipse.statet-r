/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.breakpoints;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.debug.ui.breakpoints.AbstractBreakpointDetailEditor;
import org.eclipse.statet.ecommons.debug.ui.breakpoints.AbstractBreakpointDetailPane;

import org.eclipse.statet.internal.r.debug.ui.Messages;


public class RLineBreakpointDetailPane extends AbstractBreakpointDetailPane {
	
	
	public static final String ID= "org.eclipse.statet.r.debugDetailPane.RLineBreakpointDefault"; //$NON-NLS-1$
	
	
	public RLineBreakpointDetailPane() {
		super(ID, Messages.Breakpoint_DefaultDetailPane_name, Messages.Breakpoint_DefaultDetailPane_description);
	}
	
	
	@Override
	protected AbstractBreakpointDetailEditor createEditor(final Composite parent) {
		return new RLineBreakpointDetailEditor(false, true, getPropertyListeners());
	}
	
}
