/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.lauching;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;

import org.eclipse.statet.r.console.core.AbstractRController;
import org.eclipse.statet.r.debug.core.RDebugModel;
import org.eclipse.statet.r.launching.core.ILaunchDelegateAddon;
import org.eclipse.statet.r.nico.AbstractRDbgController;


public class DebugLaunchDelegateAddon implements ILaunchDelegateAddon {
	
	
	public DebugLaunchDelegateAddon() {
	}
	
	
	@Override
	public void init(final ILaunchConfiguration configuration, final String mode,
			final AbstractRController controller, final IProgressMonitor monitor) {
		if (mode.equals(ILaunchManager.DEBUG_MODE)
				&& controller instanceof AbstractRDbgController) {
			RDebugModel.createRDebugTarget((AbstractRDbgController) controller);
		}
	}
	
}
