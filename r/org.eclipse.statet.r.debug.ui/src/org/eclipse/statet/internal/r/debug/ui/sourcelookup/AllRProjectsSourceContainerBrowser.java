/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.sourcelookup;

import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.ISourceLookupDirector;
import org.eclipse.debug.ui.sourcelookup.AbstractSourceContainerBrowser;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.r.debug.core.sourcelookup.AllRProjectsSourceContainer;


public class AllRProjectsSourceContainerBrowser extends AbstractSourceContainerBrowser {
	
	
	/** Created via extension point */
	public AllRProjectsSourceContainerBrowser() {
	}
	
	
	@Override
	public boolean canAddSourceContainers(final ISourceLookupDirector director) {
		final ISourceContainer[] containers= director.getSourceContainers();
		for (final ISourceContainer container : containers) {
			if (container.getType().getId().equals(AllRProjectsSourceContainer.TYPE_ID)) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public ISourceContainer[] addSourceContainers(final Shell shell,
			final ISourceLookupDirector director) {
		final AllRProjectsSourceContainer container= new AllRProjectsSourceContainer();
		container.init(director);
		return new ISourceContainer[] { container };
	}
	
}
