/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.sourcelookup;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IContributorResourceAdapter;
import org.eclipse.ui.model.IWorkbenchAdapter;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.internal.r.debug.ui.RDebugUIPlugin;
import org.eclipse.statet.r.debug.core.sourcelookup.RRuntimeSourceFragment;
import org.eclipse.statet.r.debug.core.sourcelookup.RSourceLookupMatch;


public class RSourceLookupAdapterFactory implements IAdapterFactory,
		IWorkbenchAdapter, IContributorResourceAdapter {
	
	
	private static final @NonNull Class<?>[] ADAPTERS= new @NonNull Class<?>[] {
		IWorkbenchAdapter.class,
		IContributorResourceAdapter.class,
	};
	
	
	/** Created via extension point */
	public RSourceLookupAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == IWorkbenchAdapter.class
				|| adapterType == IContributorResourceAdapter.class) {
			return (T) this;
		}
		return null;
	}
	
	
	@Override
	public @NonNull Object[] getChildren(final Object o) {
		return null;
	}
	
	@Override
	public @Nullable Object getParent(final Object o) {
		return null;
	}
	
	@Override
	public @Nullable ImageDescriptor getImageDescriptor(final Object obj) {
		if (obj instanceof RSourceLookupMatch) {
			final Object element= ((RSourceLookupMatch)obj).getElement();
			if (element instanceof RRuntimeSourceFragment) {
				return RDebugUIPlugin.getInstance().getImageRegistry().getDescriptor(
						RDebugUIPlugin.IMG_OBJ_R_SOURCE_FROM_RUNTIME );
			}
			if (element instanceof IAdaptable) {
				final IWorkbenchAdapter adapter= Platform.getAdapterManager()
						.getAdapter(element, IWorkbenchAdapter.class);
				if (adapter != null) {
					return adapter.getImageDescriptor(element);
				}
			}
		}
		return null;
	}
	
	@Override
	public String getLabel(final Object obj) {
		if (obj instanceof RSourceLookupMatch) {
			final Object element= ((RSourceLookupMatch)obj).getElement();
			if (element instanceof RRuntimeSourceFragment) {
				final RRuntimeSourceFragment fragment= (RRuntimeSourceFragment) element;
				return fragment.getName() + "  \u2012  " + fragment.getProcess().getLabel(Tool.DEFAULT_LABEL); //$NON-NLS-1$
			}
			if (element instanceof IAdaptable) {
				final IWorkbenchAdapter adapter= Platform.getAdapterManager()
						.getAdapter(element, IWorkbenchAdapter.class);
				if (adapter != null) {
					return adapter.getLabel(element);
				}
			}
			return element.toString();
		}
		return obj.toString();
	}
	
	@Override
	public @Nullable IResource getAdaptedResource(final IAdaptable obj) {
		if (obj instanceof RSourceLookupMatch) {
			final Object element= ((RSourceLookupMatch)obj).getElement();
			if (element instanceof IResource) {
				return (IResource)element;
			}
		}
		return null;
	}
	
}
