<?xml version="1.0" encoding="UTF-8"?>
<!--
 #=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Tobias Verbeke - initial implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - commands, adaption to updates, intro
 #=============================================================================#
-->
<cheatsheet title="Configure and Launch R Console">
   <intro>
      <description>
         This cheat sheet shows you how to configure StatET to launch an
         R console from within Eclipse.
         <br/><br/>
         Note that the R console in StatET requires the R package 'rj'. Please check
         the homepage for more information and installation instructions.
         <br/><br/>
         In StatET you have to create a configuration for the R environment and the
         R console as shown here. 
      </description>
   </intro>
   <item
         title="Add R Environment"
         dialog="true"
         skip="false"
         href="/org.eclipse.statet.r.doc/reference/r_env.xhtml">
      <description>
         To add an <b>R environment</b> to StatET, perform the following steps:
      </description>
      <subitem
            label="Select the &apos;R Environments&apos; preference page.">
         <command
               required="false"
               serialization="org.eclipse.ui.window.preferences(preferencePageId=org.eclipse.statet.r.preferencePages.REnvironmentPage)"/>
      </subitem>
      <subitem
            label="Clicking on the &apos;Add...&apos; button opens the dialog to specify the new R environment.">
      </subitem>
      <subitem
            label="Indicate a &apos;Name&apos; you would like to give to the configuration, e.g. &apos;R-2.13&apos;.">
      </subitem>
      <subitem
            label="Specify the &apos;R_HOME&apos; directory to which the R version you would like to use was installed.">
      </subitem>
      <subitem
            label="Click the &apos;Detect Default Properties/Settings&apos; button to load the default values for the other options.">
      </subitem>
      <subitem
            label="Click on &apos;OK&apos; to close the dialog.">
      </subitem>
      <subitem
            label="Click on &apos;OK&apos; to save the new R environment and close the preferences dialog.">
      </subitem>
   </item>
   <item
         title="R Console Launch Configuration"
         dialog="true"
         skip="false"
         href="/org.eclipse.statet.r.doc/reference/r_console-launch.xhtml">
      <description>
         Next, you need a <b>launch configuration</b> for the R console:
      </description>
      <subitem
            label="Go to &apos;Run &gt; Run Configurations...&apos;">
         <command
               required="false"
               serialization="org.eclipse.debug.ui.commands.OpenRunConfigurations"/>
      </subitem>
      <subitem
            label="Double-click on &apos;R Console&apos; to create a new configuration.">
      </subitem>
      <subitem
            label="Specify a &apos;Name&apos; and edit all tabs of the new launch configuration.">
      </subitem>
      <subitem
            label="Add suitable Options in the Main tab if desired, such as --no-save or --no-restore, to be used when invoking R." skip="true">
      </subitem>
      <subitem
            label="Specify the R Environment to be used in the R Config tab. This can be the default R Environment or any other R Environment that has been defined." skip="false">
      </subitem>
      <subitem
            label="Optionally check the box in the Common tab to list this R Launch configuration in the Favorites menu" skip="true">
      </subitem>
      <subitem
            label="Click on &apos;Apply&apos; to save the R Launch Configuration" skip="false">
      </subitem>
   </item>
   <item title="Launch the R console" dialog="true" skip="false">
      <description>
         Click on '<b>Run</b>' in the Run Dialog for the R Launch configuration you created.
         <br/><br/>
         The R Console will be launched and ready for use from within Eclipse.
      </description>
   </item>
</cheatsheet>
