/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.launching.winrgui;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Plugin;


public class WinRGuiConnectorPlugin extends Plugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.debug.winrgui"; //$NON-NLS-1$
	
	
	private static WinRGuiConnectorPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static WinRGuiConnectorPlugin getInstance() {
		return instance;
	}
	
	
	/**
	 * The constructor.
	 */
	public WinRGuiConnectorPlugin() {
		instance = this;
	}
	
	
	/**
	 * This method is called upon plug-in activation
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
	}
	
	/**
	 * This method is called when the plug-in is stopped
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		instance = null;
		super.stop(context);
	}
	
}
