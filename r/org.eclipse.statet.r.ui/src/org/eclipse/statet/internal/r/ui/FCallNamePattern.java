/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import java.util.List;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RLangMethod;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.ui.sourceediting.RFrameSearchPath;
import org.eclipse.statet.r.ui.sourceediting.RFrameSearchPath.RFrameIterator;


public class FCallNamePattern {
	
	
	private final RElementName elementName;
	
	private final String packageName;
	
	private final String assignName;
	private final int assignLength;
	
	
	public FCallNamePattern(final RElementName name) {
		this.elementName= name;
		
		this.packageName= (name.getScope() != null
						&& RElementName.isPackageFacetScopeType(name.getScope().getType()) ) ?
				name.getScope().getSegmentName() : null;
		
		if (this.elementName.getNextSegment() == null) {
			this.assignName= this.elementName.getSegmentName();
			this.assignLength= this.assignName.length();
		}
		else {
			this.assignName= null;
			this.assignLength= 0;
		}
	}
	
	
	public final RElementName getElementName() {
		return this.elementName;
	}
	
	public boolean matches(final RElementName candidateName) {
		String candidate0;
		return (candidateName != null
				&& (this.elementName.equals(candidateName)
						|| (this.assignName != null && candidateName.getNextSegment() == null
								&& this.assignLength == (candidate0= candidateName.getSegmentName()).length() - 2
								&& this.elementName.getType() == candidateName.getType()
								&& candidate0.charAt(this.assignLength) == '<' && candidate0.charAt(this.assignLength + 1) == '-'
								&& candidate0.regionMatches(false, 0, this.assignName, 0, this.assignLength) )));
	}
	
	
	public void searchFDef(final RFrameSearchPath searchPath) {
		ITER_FRAMES: for (final RFrameIterator iter= searchPath.iterator(); iter.hasNext(); ) {
			final RFrame frame= iter.next();
			if (this.packageName != null) {
				final RElementName frameName= frame.getElementName();
				if (!(frameName != null
						&& RElementName.isPackageFacetScopeType(frameName.getType())
						&& this.packageName.equals(frameName.getSegmentName()) )) {
					continue ITER_FRAMES;
				}
			}
			final List<? extends LtkModelElement> elements= frame.getModelChildren(null);
			for (final LtkModelElement candidate : elements) {
				if (candidate.getModelTypeId() == RModel.R_TYPE_ID
						&& (candidate.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_METHOD
						&& matches((RElementName) candidate.getElementName()) ) {
					handleMatch((RLangMethod)candidate, frame, iter);
				}
			}
		}
	}
	
	protected void handleMatch(final RLangMethod element, final RFrame frame,
			final RFrameIterator iterator) {
	}
	
}
