/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.texteditor.IDocumentProvider;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.ElementLabelProvider;
import org.eclipse.statet.r.ui.RLabelProvider;


@NonNullByDefault
public class RAdapterFactory implements IAdapterFactory {
	
	
	private static final @NonNull Class<?>[] ADAPTERS= new @NonNull Class<?>[] {
		ElementLabelProvider.class,
		IDocumentProvider.class,
	};
	
	
	public RAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == ElementLabelProvider.class) {
			return (T)new RLabelProvider();
		}
		if (adapterType == IDocumentProvider.class) {
			return (T)RUIPlugin.getInstance().getRDocumentProvider();
		}
		return null;
	}
	
}
