/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.search.ui.ISearchPageScoreComputer;


/**
 * Implements basic UI support for R editor input.
 */
public class REditorInputAdapterFactory implements IAdapterFactory {
	
	
	private static Class<?>[] PROPERTIES = new Class<?>[] {
//		RElement.class,
	};
	
	/*
	 * Do not use real type since this would cause
	 * the Search plug-in to be loaded.
	 */
	private Class<?> fSearchPageScoreComputerClass;
	private Object fSearchPageScoreComputer;
	
	
	public REditorInputAdapterFactory() {
	}
	
	
	@Override
	public Class<?>[] getAdapterList() {
		updateLazyLoadedAdapters();
		return PROPERTIES;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		updateLazyLoadedAdapters();
		if (fSearchPageScoreComputerClass != null
				&& adapterType == fSearchPageScoreComputerClass) {
			return (T) fSearchPageScoreComputer;
		}
		return null;
	}
	
	
	private synchronized void updateLazyLoadedAdapters() {
		if (fSearchPageScoreComputerClass == null && RUIPlugin.isSearchPlugInActivated()) {
			createSearchPageScoreComputer();
		}
	}
	
	private void createSearchPageScoreComputer() {
		fSearchPageScoreComputerClass = ISearchPageScoreComputer.class;
		fSearchPageScoreComputer = new RSearchPageScoreComputer();
		final Class<?>[] newProperties = new Class[PROPERTIES.length+1];
		System.arraycopy(PROPERTIES, 0, newProperties, 0, PROPERTIES.length);
		newProperties[PROPERTIES.length] = fSearchPageScoreComputerClass;
		PROPERTIES = newProperties;
	}
	
}
