/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.ui.impl.GenericEditorWorkspaceSourceUnitWorkingCopy2;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.rmodel.build.RSourceUnitModelContainer;


/**
 * R source unit working copy which can be processed by the model manager.
 */
@NonNullByDefault
public class REditorWorkingCopy
		extends GenericEditorWorkspaceSourceUnitWorkingCopy2<RSourceUnitModelContainer>
		implements RWorkspaceSourceUnit {
	
	
	public REditorWorkingCopy(final RWorkspaceSourceUnit from) {
		super(from);
	}
	
	@Override
	protected RSourceUnitModelContainer createModelContainer() {
		return new RSourceUnitModelContainer(this,
				RUIPlugin.getInstance().getRDocumentProvider() );
	}
	
	
	@Override
	protected final void register() {
		super.register();
		
		if (!getModelTypeId().equals(RModel.R_TYPE_ID)) {
			RModel.getRModelManager().registerDependentUnit(this);
		}
	}
	
	@Override
	protected final void unregister() {
		super.unregister();
		
		if (!getModelTypeId().equals(RModel.R_TYPE_ID)) {
			RModel.getRModelManager().deregisterDependentUnit(this);
		}
	}
	
	@Override
	public RCoreAccess getRCoreAccess() {
		return ((RSourceUnit)getUnderlyingUnit()).getRCoreAccess();
	}
	
	
}
