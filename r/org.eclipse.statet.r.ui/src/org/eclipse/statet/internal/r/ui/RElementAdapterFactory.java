/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.search.ui.ISearchPageScoreComputer;
import org.eclipse.ui.IContributorResourceAdapter;

import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.r.core.rmodel.RElement;


/**
 * Implements basic UI support for R elements.
 */
public class RElementAdapterFactory implements IAdapterFactory, IContributorResourceAdapter {
	
	
	private static Class<?>[] PROPERTIES= new Class<?>[] {
		IResource.class,
		IContributorResourceAdapter.class,
	};
	
	
	/*
	 * Do not use real type since this would cause
	 * the Search plug-in to be loaded.
	 */
	private Class<?> fSearchPageScoreComputerClass;
	private Object fSearchPageScoreComputer;
	
	
	@Override
	public Class<?>[] getAdapterList() {
		updateLazyLoadedAdapters();
		return PROPERTIES;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		updateLazyLoadedAdapters();
		final RElement element= getRElement(adaptableObject);
//		if (IPropertySource.class.equals(key)) {
//			return getProperties(java);
//		}
		if (adapterType == IResource.class) {
			return (T) getResource(element);
		}
		if (fSearchPageScoreComputerClass != null
				&& adapterType == fSearchPageScoreComputerClass) {
			return (T) fSearchPageScoreComputer;
		}
//		if (IWorkbenchAdapter.class.equals(key)) {
//			return getRWorkbenchAdapter();
//		}
//		if (IPersistableElement.class.equals(key)) {
//			return new PersistableRElementFactory(java);
//		}
		if (adapterType == IContributorResourceAdapter.class) {
			return (T) this;
		}
//		if (IContributorResourceAdapter2.class.equals(key)) {
//			return this;
//		}
//		if (ITaskListResourceAdapter.class.equals(key)) {
//			return getTaskListAdapter();
//		}
//		if (IContainmentAdapter.class.equals(key)) {
//			return getRElementContainmentAdapter();
//		}
		return null;
	}
	
	private IResource getResource(final RElement element) {
		switch (element.getElementType() & RElement.MASK_C1) {
//		case RElement.C1_CLASS:
//		case RElement.C1_METHOD:
//			// top level types behave like the CU
//			if (element instanceof SourceStructElement) {
//				final SourceStructElement parent= ((SourceStructElement)element).getSourceParent();
//				if ((parent.getElementType() & RElement.MASK_C1) == RElement.C1_SOURCE) {
//					final SourceUnit su= parent.getSourceUnit();
//					if (su instanceof WorkspaceSourceUnit) {
//						return ((WorkspaceSourceUnit)su).getResource();
//					}
//				}
//			}
//			return null;
		case RElement.C1_SOURCE:
			if (element instanceof SourceElement) {
				final SourceUnit su= ((SourceElement)element).getSourceUnit();
				if (su instanceof WorkspaceSourceUnit) {
					return ((WorkspaceSourceUnit)su).getResource();
				}
			}
			return null;
		default:
			return null;
		}
	}
	
	@Override
	public IResource getAdaptedResource(final IAdaptable adaptable) {
		final RElement element= getRElement(adaptable);
		if (element != null) {
			return getResource(element);
		}
		return null;
	}
	
//	public IResourceMapping getAdaptedResourceMapping(final IAdaptable adaptable) {
//		final RElement element= getRElement(adaptable);
//		if (element != null) {
//			return
//		}
//		return null;
//	}
	
	private RElement getRElement(final Object element) {
		if (element instanceof RElement) {
			return (RElement)element;
		}
		return null;
	}
	
	private synchronized void updateLazyLoadedAdapters() {
		if (fSearchPageScoreComputerClass == null && RUIPlugin.isSearchPlugInActivated()) {
			createSearchPageScoreComputer();
		}
	}
	
	private void createSearchPageScoreComputer() {
		fSearchPageScoreComputerClass= ISearchPageScoreComputer.class;
		fSearchPageScoreComputer= new RSearchPageScoreComputer();
		final Class<?>[] newProperties= new Class[PROPERTIES.length+1];
		System.arraycopy(PROPERTIES, 0, newProperties, 0, PROPERTIES.length);
		newProperties[PROPERTIES.length]= fSearchPageScoreComputerClass;
		PROPERTIES= newProperties;
	}
	
//	private RWorkbenchAdapter getRWorkbenchAdapter() {
//		if (fWorkbenchAdapter == null)
//			fWorkbenchAdapter=;
//		return fWorkbenchAdapter;
//	}
	
}
