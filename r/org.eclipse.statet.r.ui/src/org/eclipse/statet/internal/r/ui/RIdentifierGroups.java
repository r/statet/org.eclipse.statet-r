/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.AbstractPreferencesModelObject;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.StringArrayPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.r.core.RSymbolComparator;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.RUIPreferenceConstants;


/**
 * 
 */
@NonNullByDefault
public class RIdentifierGroups extends AbstractPreferencesModelObject {
	
	
	public final static String GROUP_ID= "r.editor/identifiergroups"; //$NON-NLS-1$
	
	
	private ImList<String> identifiersItemsAssignment= ImCollections.emptyList();
	private ImList<String> identifiersItemsLogical= ImCollections.emptyList();
	private ImList<String> identifiersItemsFlowcontrol= ImCollections.emptyList();
	private ImList<String> identifiersItemsCustom1= ImCollections.emptyList();
	private ImList<String> identifiersItemsCustom2= ImCollections.emptyList();
	
	
	public RIdentifierGroups() {
		installLock();
	}
	
	@Override
	public String[] getNodeQualifiers() {
		return new String[0];
	}
	
	@Override
	public void loadDefaults() {
	}
	
	@Override
	public void load(final PreferenceAccess prefs) {
		final RSymbolComparator comparator= new RSymbolComparator();
		this.identifiersItemsAssignment= ImCollections.newList(
				loadValues(prefs, RUIPreferenceConstants.R.TS_IDENTIFIER_SUB_ASSIGNMENT_ITEMS),
				comparator );
		this.identifiersItemsLogical= ImCollections.newList(
				loadValues(prefs, RUIPreferenceConstants.R.TS_IDENTIFIER_SUB_LOGICAL_ITEMS),
				comparator );
		this.identifiersItemsFlowcontrol= ImCollections.newList(
				loadValues(prefs, RUIPreferenceConstants.R.TS_IDENTIFIER_SUB_FLOWCONTROL_ITEMS),
				comparator );
		this.identifiersItemsCustom1= ImCollections.newList(
				loadValues(prefs, RUIPreferenceConstants.R.TS_IDENTIFIER_SUB_CUSTOM1_ITEMS),
				comparator );
		this.identifiersItemsCustom2= ImCollections.newList(
				loadValues(prefs, RUIPreferenceConstants.R.TS_IDENTIFIER_SUB_CUSTOM2_ITEMS),
				comparator );
	}
	
	@Override
	public Map<Preference<?>, Object> deliverToPreferencesMap(final Map<Preference<?>, Object> map) {
		return map;
	}
	
	private final @NonNull String[] loadValues(final PreferenceAccess prefs, final String key) {
		final Preference<String[]> pref= new StringArrayPref(RUI.BUNDLE_ID, key);
		return prefs.getPreferenceValue(pref);
	}
	
	
	public ImList<String> getAssignmentIdentifiers() {
		return this.identifiersItemsAssignment;
	}
	
	public ImList<String> getLogicalIdentifiers() {
		return this.identifiersItemsLogical;
	}
	
	public ImList<String> getFlowcontrolIdentifiers() {
		return this.identifiersItemsFlowcontrol;
	}
	
	public ImList<String> getCustom1Identifiers() {
		return this.identifiersItemsCustom1;
	}
	
	public ImList<String> getCustom2Identifiers() {
		return this.identifiersItemsCustom2;
	}
	
}
