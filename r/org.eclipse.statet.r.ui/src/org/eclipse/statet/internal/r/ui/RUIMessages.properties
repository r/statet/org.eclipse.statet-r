 #=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#

ChooseREnv_None_label = &None / Disabled
ChooseREnv_WorkbenchDefault_label = &Workbench Default:
ChooseREnv_Selected_label = &Selected Configuration:
ChooseREnv_Configure_label = C&onfigure...
ChooseREnv_error_InvalidPreferences_message = The R Environment preferences of the Workspace are invalid.
ChooseREnv_error_IncompleteSelection_message = Selection of R Environment is incomplete.
ChooseREnv_error_InvalidSelection_message = Selection of R Environment is invalid.

StripComments_task_label = stripping comments
CorrectIndent_task_label = correcting line indentation

RProject_ConfigureTask_label= Configuring R project ''{0}''...
RPkgProject_ConvertTask_error_message= An error occurred when configuring R package nature for projects.

RProject_NeedsBuild_title= R Project Configuration Changed
RProject_NeedsBuild_Full_message= The configuration for one or several R projects have changed. A full rebuild is required for changes to take effect. Do the full build now?
RProject_NeedsBuild_Project_message= The configuration for one or several R projects have changed. A rebuild of the project is required for changes to take effect. Build the project now?

Proposal_RenameInFile_label = Rename in file
Proposal_RenameInFile_description = Link all references for a local rename in the current file
Proposal_RenameInChunk_label = Rename in R chunk
Proposal_RenameInChunk_description = Link all references in the current R chunk for a local rename in the current file
Proposal_RenameInFilePrecending_label = Rename in file this one and preceding
Proposal_RenameInFilePrecending_description = Link current and preceding references for a local rename in the current file
Proposal_RenameInFileFollowing_label = Rename in file this one and following
Proposal_RenameInFileFollowing_description = Link current and following references for a local rename in the current file

Proposal_RenameInRegion_label= Rename in selected region...
Proposal_RenameInRegion_description= Start refactoring to rename variables in the selected region
Proposal_RenameInWorkspace_label= Rename in workspace...
Proposal_RenameInWorkspace_description= Start refactoring to rename the selected variable in the workspace
Proposal_ConvertFCallToPipeForward_label= Convert function call to forward pipe

Outline_HideGeneralVariables_name = Hide general variables
Outline_HideLocalElements_name = Hide local elements

EditorTemplates_RCodeContext_label = R Code
EditorTemplates_RoxygenContext_label = Roxygen

GenerateRoxygenElementComment_label = Generate Element Comment (Roxygen)
GenerateRoxygenElementComment_error_message = An error occurred when generating element Roxygen comment(s)

Templates_Variable_ElementName_description= Element name
Templates_Variable_RoxygenParamTags_description= Roxygen @param tags
Templates_Variable_RoxygenSlotTags_description= Roxygen @slot tags
Templates_Variable_RoxygenSigList_description= Method signature (comma separated)
Templates_Variable_RPkgName_description= R package name
