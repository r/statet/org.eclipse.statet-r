/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.buildpaths;

import org.eclipse.core.resources.IProject;

import org.eclipse.statet.ltk.buildpath.core.BuildpathElementType;
import org.eclipse.statet.ltk.buildpath.ui.BuildpathListElement;
import org.eclipse.statet.ltk.buildpath.ui.BuildpathsUIDescription;


public class RBuildpathsUIDescription extends BuildpathsUIDescription {
	
	
	public RBuildpathsUIDescription() {
	}
	
	
	@Override
	public String getDefaultExt(final BuildpathListElement element) {
		return "R"; //$NON-NLS-1$
	}
	
	
	@Override
	public boolean getAllowAdd(final IProject project, final BuildpathElementType type) {
		return false;
	}
	
	@Override
	public boolean getAllowEdit(final BuildpathListElement element) {
		return false;
	}
	
}
