/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.correction;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.ltk.ui.LtkActions;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickRefactoringAssistProposal;
import org.eclipse.statet.r.core.refactoring.FCallToPipeForwardRefactoring;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class ConvertFCallToPipeForwardAssistProposal
		extends QuickRefactoringAssistProposal<RAssistInvocationContext> {
	
	
	public ConvertFCallToPipeForwardAssistProposal(final RAssistInvocationContext invocationContext,
			final FCallToPipeForwardRefactoring refactoring) {
		super(invocationContext, LtkActions.QUICK_ASSIST_CONVERT_TO_PIPE_FORWARD,
				RUIMessages.Proposal_ConvertFCallToPipeForward_label,
				refactoring );
	}
	
	
	@Override
	public Image getImage() {
		return RUI.getImage(RUI.IMG_TOOL_ASSIST_TO_PIPE_FORWARD);
	}
	
	
}
