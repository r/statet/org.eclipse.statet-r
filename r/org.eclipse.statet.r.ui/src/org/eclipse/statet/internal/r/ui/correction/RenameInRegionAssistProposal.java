/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.correction;

import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.internal.r.ui.refactoring.RenameInRegionWizard;
import org.eclipse.statet.ltk.ui.refactoring.RefactoringSaveHelper;
import org.eclipse.statet.ltk.ui.refactoring.RefactoringWizardExecutionHelper;
import org.eclipse.statet.ltk.ui.sourceediting.assist.CommandAssistProposal;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;
import org.eclipse.statet.r.core.refactoring.RenameInRegionRefactoring;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class RenameInRegionAssistProposal
		extends CommandAssistProposal<RAssistInvocationContext> {
	
	
	public RenameInRegionAssistProposal(final RAssistInvocationContext invocationContext) {
		super(invocationContext, "org.eclipse.statet.ltk.commands.RefactorRenameInSelectedRegion", //$NON-NLS-1$
				RUIMessages.Proposal_RenameInRegion_label,
				RUIMessages.Proposal_RenameInRegion_description );
	}
	
	
	@Override
	public void apply(final ITextViewer viewer, final char trigger, final int stateMask, final int offset) {
		final var context= getInvocationContext();
		
		RenameInRegionRefactoring refactoring;
		{	final ITextSelection textSelection= (ITextSelection)viewer.getSelectionProvider().getSelection();
			refactoring= new RenameInRegionRefactoring(context.getSourceUnit(),
					LTKSelectionUtils.toTextRegion(textSelection) );
		}
		if (refactoring != null) {
			final RefactoringWizardExecutionHelper executionHelper= new RefactoringWizardExecutionHelper(
					new RenameInRegionWizard(refactoring), RefactoringSaveHelper.SAVE_NOTHING);
			executionHelper.perform(viewer.getTextWidget().getShell());
		}
	}
	
}
