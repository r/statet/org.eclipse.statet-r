/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.r.ui.dataeditor.RDataTableContentDescription;
import org.eclipse.statet.r.ui.dataeditor.RDataTableVariable;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public final class ContentDescription extends RDataTableContentDescription {
	
	
	private static final @NonNull RDataTableVariable[] NO_COLUMNS_ARRAY= new RDataTableVariable[0];
	
	
	private final @NonNull RDataTableVariable[] dataVariables;
	
	private final @Nullable RDataFormatter defaultDataFormat;
	
	
	public ContentDescription(final RElementName elementName, final RObject struct,
			final RTool rHandle,
			final ImList<RDataTableColumn> columnHeaderRows, final ImList<RDataTableColumn> rowHeaderColumns,
			final ImList<RDataTableColumn> dataColumns,
			final @NonNull RDataTableVariable @Nullable [] dataVariables,
			final @Nullable RDataFormatter defaultDataFormat) {
		super(elementName, struct, rHandle,
				columnHeaderRows, rowHeaderColumns, dataColumns );
		this.dataVariables= (dataVariables != null) ? dataVariables : NO_COLUMNS_ARRAY;
		this.defaultDataFormat= defaultDataFormat;
	}
	
	
	@NonNull RDataTableVariable[] getVariables() {
		return this.dataVariables;
	}
	
	
	public @Nullable RDataFormatter getDefaultDataFormat() {
		return this.defaultDataFormat;
	}
	
	
	@Override
	public int hashCode() {
		return 986986;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof RDataTableContentDescription);
	}
	
}
