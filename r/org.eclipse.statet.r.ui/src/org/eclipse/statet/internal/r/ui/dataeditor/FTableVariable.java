/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.statet.r.ui.dataeditor.RDataTableVariable;
import org.eclipse.statet.rj.data.RStore;


public class FTableVariable implements RDataTableVariable {
	
	
	private final int fPresentation;
	
	private final String fName;
	
	private final RStore fDataStore;
	
	
	public FTableVariable(final int orientation, final String name, final RStore dataStore) {
		this.fPresentation= orientation;
		this.fName= name;
		this.fDataStore= dataStore;
	}
	
	
	@Override
	public int getVarPresentation() {
		return this.fPresentation;
	}
	
	@Override
	public String getName() {
		return this.fName;
	}
	
	@Override
	public int getVarType() {
		return FACTOR;
	}
	
	public RStore getLevelStore() {
		return this.fDataStore;
	}
	
	
	@Override
	public int hashCode() {
		return this.fName.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof FTableVariable)) {
			return false;
		}
		final FTableVariable other= (FTableVariable) obj;
		return this.fName.equals(other.fName);
	}
	
}
