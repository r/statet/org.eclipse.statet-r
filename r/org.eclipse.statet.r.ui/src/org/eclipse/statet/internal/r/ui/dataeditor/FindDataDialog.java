/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.ecommons.ui.dialogs.ExtStatusDialog;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.r.ui.dataeditor.RDataTableViewer;


@NonNullByDefault
public class FindDataDialog extends ExtStatusDialog {
	
	
	private static final Map<IWorkbenchWindow, FindDataDialog> gDialogs= new HashMap<>();
	
	
	public static @Nullable FindDataDialog get(final IWorkbenchWindow window) {
		return gDialogs.get(window);
	}
	
	public static FindDataDialog getCreate(final IWorkbenchWindow window) {
		FindDataDialog dialog= gDialogs.get(window);
		if (dialog == null) {
			dialog= new FindDataDialog(window);
			dialog.create();
			gDialogs.put(window, dialog);
		}
		return dialog;
	}
	
	
	private static final int FIND_NEXT_ID= 101;
	private static final int FIND_PREVIOUS_ID= 102;
	
	
	private class PartListener implements IWindowListener, IPartListener {
		
		@Override
		public void windowOpened(final IWorkbenchWindow window) {
		}
		@Override
		public void windowClosed(final IWorkbenchWindow window) {
			if (FindDataDialog.this.window == window) {
				close();
			}
		}
		@Override
		public void windowActivated(final IWorkbenchWindow window) {
		}
		@Override
		public void windowDeactivated(final IWorkbenchWindow window) {
		}
		
		@Override
		public void partOpened(final IWorkbenchPart part) {
		}
		@Override
		public void partClosed(final IWorkbenchPart part) {
			if (FindDataDialog.this.part == part) {
				FindDataDialog.this.part= null;
			}
		}
		@Override
		public void partActivated(final IWorkbenchPart part) {
			FindDataDialog.this.part= part;
			update(part);
		}
		@Override
		public void partDeactivated(final IWorkbenchPart part) {
		}
		@Override
		public void partBroughtToTop(final IWorkbenchPart part) {
		}
		
	}
	
	
	private final IWorkbenchWindow window;
	private @Nullable IWorkbenchPart part;
	
	private final PartListener partListener= new PartListener();
	
	private @Nullable RDataTableViewer table;
	
	private Combo textControl= nonNullLateInit();
	private Button rExpressionModeControl= nonNullLateInit();
	private Button isNaModeControl= nonNullLateInit();
	private Button directionFirstInColumnControl= nonNullLateInit();
	private Button directionFirstInRowControl= nonNullLateInit();
	private Button selectedOnlyControl= nonNullLateInit();
	
	private final Map<String, String> historyModeMap= new HashMap<>();
	
	
	private final FindListener findListener= new FindListener() {
		@Override
		public void handleFindEvent(final FindListener.FindEvent event) {
			updateStatus(event.status);
		}
	};
	
	
	protected FindDataDialog(final IWorkbenchWindow window) {
		super(window.getShell());
		
		this.window= window;
		this.window.getWorkbench().addWindowListener(this.partListener);
		this.window.getActivePage().addPartListener(this.partListener);
		this.part= this.window.getActivePage().getActivePart();
		
		setTitle("Find");
		setShellStyle((getShellStyle() & ~SWT.APPLICATION_MODAL) | SWT.MODELESS);
		setBlockOnOpen(false);
		setStatusLineAboveButtons(false);
		setHelpAvailable(false);
	}
	
	
	protected IDialogSettings getDialogSettings() {
		return DialogUtils.getDialogSettings(RUIPlugin.getInstance(), "RDataTable.FindDialog"); //$NON-NLS-1$
	}
	
	@Override
	protected IDialogSettings getDialogBoundsSettings() {
		return getDialogSettings();
	}
	
	@Override
	public void create() {
		super.create();
		
		loadSettings();
	}
	
	@Override
	public boolean close() {
		this.window.getWorkbench().removeWindowListener(this.partListener);
		final IWorkbenchPage page= this.window.getActivePage();
		if (page != null) {
			page.removePartListener(this.partListener);
		}
		
		gDialogs.remove(this.window);
		saveSettings();
		
		return super.close();
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newDialogGrid(2));
		
		final Composite textInput= createTextInput(composite);
		textInput.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		
		final Composite additionalOptions= createAdditionalOptions(composite);
		additionalOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		
		final Composite directionOptions= createDirectionOptions(composite);
		directionOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		
//		final Composite scopeOptions= createScopeOptions(composite);
//		scopeOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
//		
		LayoutUtils.addSmallFiller(composite, false);
		final Composite navigateButtons= createNavigateButtons(composite);
		navigateButtons.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 2, 1));
		
		applyDialogFont(composite);
		return composite;
	}
	
	protected Composite createTextInput(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(2));
		
		final Label label= new Label(composite, SWT.NONE);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		label.setText("Find:");
		
		this.textControl= new Combo(composite, SWT.DROP_DOWN | SWT.BORDER);
		this.textControl.setLayoutData(LayoutUtils.hintWidth(
				new GridData(SWT.FILL, SWT.CENTER, true, false), this.textControl, 25 ));
		
		this.textControl.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				clearStatus();
				updateState();
			}
		});
		this.textControl.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				final int selectionIdx;
				if (!FindDataDialog.this.textControl.getListVisible()
						&& (selectionIdx= FindDataDialog.this.textControl.getSelectionIndex()) >= 0) {
					loadQuery(FindDataDialog.this.textControl.getItem(selectionIdx));
				}
			}
		});
		
		return composite;
	}
	
	protected Composite createDirectionOptions(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setText("Direction");
		composite.setLayout(LayoutUtils.newGroupGrid(2, true));
		
		{	final Button button= new Button(composite, SWT.RADIO);
			button.setText("&Column, Row");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			button.setSelection(true);
			this.directionFirstInColumnControl= button;
		}
		{	final Button button= new Button(composite, SWT.RADIO);
			button.setText("&Row, Column");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.directionFirstInRowControl= button;
		}
		{	final Button button= new Button(composite, SWT.CHECK);
			button.setText("Only Selec&ted Cells");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			this.selectedOnlyControl= button;
		}
		
		return composite;
	}
	
	protected Composite createScopeOptions(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setText("Scope");
		composite.setLayout(LayoutUtils.newGroupGrid(1));
		
		{	final Button button= new Button(composite, SWT.RADIO);
			button.setText("A&ll");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			button.setSelection(true);
		}
		{	final Button button= new Button(composite, SWT.CHECK);
			button.setText("Selec&ted Cells");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.selectedOnlyControl= button;
		}
		return composite;
	}
	
	protected Composite createAdditionalOptions(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setText("Options");
		composite.setLayout(LayoutUtils.newGroupGrid(2, true));
		
		{	final Button button= new Button(composite, SWT.CHECK);
			button.setText("R expression");
			button.setToolTipText("Use x to reference the object itself, e.g. 'x >= 1'");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.rExpressionModeControl= button;
			
			this.rExpressionModeControl.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					if (FindDataDialog.this.rExpressionModeControl.getSelection()) {
						FindDataDialog.this.isNaModeControl.setSelection(false);
					}
				}
			});
		}
		
		{	final Button button= new Button(composite, SWT.CHECK);
			button.setText("Is NA");
			button.setToolTipText("The search expression is ignored");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.isNaModeControl= button;
			
			this.isNaModeControl.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					if (FindDataDialog.this.isNaModeControl.getSelection()) {
						FindDataDialog.this.rExpressionModeControl.setSelection(false);
					}
				}
			});
		}
		
		return composite;
	}
	
	protected Composite createNavigateButtons(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(0));
		
		createButton(composite, FIND_PREVIOUS_ID, "&Previous", false);
		createButton(composite, FIND_NEXT_ID, "&Next", true);
		
		return composite;
	}
	
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, false);
	}
	
	@Override
	protected void buttonPressed(final int buttonId) {
		switch (buttonId) {
		case IDialogConstants.CLOSE_ID:
			close();
			return;
		case FIND_NEXT_ID:
			doFind(true);
			return;
		case FIND_PREVIOUS_ID:
			doFind(false);
			return;
		}
	}
	
	private void loadQuery(final String text) {
		if (text == null || text.isEmpty()) {
			return;
		}
		
		final String mode= this.historyModeMap.get(text);
		if (mode != null) {
			if ("r_expression".equals(mode)) {
				this.rExpressionModeControl.setSelection(true);
				this.isNaModeControl.setSelection(false);
			}
			else {
				this.rExpressionModeControl.setSelection(false);
			}
		}
	}
	
	private void doFind(final boolean forward) {
		final var table= nonNullAssert(this.table);
		final String expression= getExpression();
		if (!expression.equals("is.na(x)")) {
			final String text= this.textControl.getText();
			final int idx= this.textControl.indexOf(text);
			if (idx != 0) {
				if (idx > 0) {
					this.textControl.remove(idx);
				}
				this.textControl.add(text, 0);
				this.textControl.setText(text);
			}
			this.historyModeMap.put(text,
					this.rExpressionModeControl.getSelection() ? "r_expression" : "default");
		}
		table.find(expression,
				this.selectedOnlyControl.getSelection(),
				this.directionFirstInRowControl.getSelection(),
				forward);
	}
	
	protected String getExpression() {
		if (this.rExpressionModeControl.getSelection()) {
			return this.textControl.getText();
		}
		else if (this.isNaModeControl.getSelection()) {
			return "is.na(x)";
		}
		else {
			return "x == " + this.textControl.getText();
		}
	}
	
	protected void loadSettings() {
		final IDialogSettings settings= getDialogSettings();
		final @NonNull String[] history= DialogUtils.noNull(settings.getArray("SearchText.history"));
		this.textControl.setItems(history);
		final @NonNull String[] historyModes= DialogUtils.noNull(settings.getArray("SearchMode.history"));
		if (history.length == historyModes.length) {
			for (int i= 0; i < history.length; i++) {
				this.historyModeMap.put(history[i], historyModes[i]);
			}
		}
		if (history.length > 0) {
			this.textControl.setText(history[0]);
			loadQuery(history[0]);
		}
		if (settings.getBoolean("Direction.firstInRow")) {
			this.directionFirstInColumnControl.setSelection(false);
			this.directionFirstInRowControl.setSelection(true);
		}
		else {
			this.directionFirstInColumnControl.setSelection(true);
			this.directionFirstInRowControl.setSelection(false);
		}
	}
	
	protected void saveSettings() {
		final IDialogSettings settings= getDialogSettings();
		final String[] history= DialogUtils.combineHistoryItems(this.textControl.getItems(), null);
		settings.put("SearchText.history", history);
		final String[] historyModes= new String[history.length];
		for (int i= 0; i < history.length; i++) {
			String mode= this.historyModeMap.get(history[i]);
			if (mode == null) {
				mode= "default";
			}
			historyModes[i]= mode;
		}
		settings.put("SearchMode.history", historyModes);
		settings.put("Direction.firstInRow", this.directionFirstInRowControl.getSelection());
	}
	
	
	public void update(final IWorkbenchPart part) {
		if (part != null && this.part == part) {
			final var currentTable= this.table;
			final var newTable= part.getAdapter(RDataTableViewer.class);
			if (currentTable != null && currentTable != newTable) {
				currentTable.removeFindListener(this.findListener);
				clearStatus();
			}
			
			this.table= newTable;
			if (newTable != null) {
				newTable.addFindListener(this.findListener);
			}
			updateState();
		}
	}
	
	private void updateState() {
		final boolean enabled= (this.table != null && this.textControl.getText().length() > 0);
		getButton(FIND_NEXT_ID).setEnabled(enabled);
		getButton(FIND_PREVIOUS_ID).setEnabled(enabled);
	}
	
}
