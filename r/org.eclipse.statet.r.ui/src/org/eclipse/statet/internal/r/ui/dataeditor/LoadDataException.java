/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;


public class LoadDataException extends Exception {
	
	
	private static final long serialVersionUID= -2599418116314887064L;
	
	
	private final boolean isRecoverable;
	
	
	public LoadDataException(final boolean isRecoverable) {
		this.isRecoverable= isRecoverable;
	}
	
	public boolean isRecoverable() {
		return this.isRecoverable;
	}
	
}
