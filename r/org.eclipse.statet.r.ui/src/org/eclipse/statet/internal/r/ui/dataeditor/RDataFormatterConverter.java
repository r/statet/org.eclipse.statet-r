/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import java.util.List;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionId;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.data.convert.IDisplayConverter;

import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;


public class RDataFormatterConverter implements IDisplayConverter {
	
	
	public static class RowHeader extends RDataFormatterConverter {
		
		public RowHeader(final AbstractRDataProvider<?> dataProvider) {
			super(dataProvider);
		}
		
		@Override
		protected RDataFormatter getFormatter(final ContentDescription description,
				final LayerCell cell) {
			if ((cell.getDim(Orientation.VERTICAL).getId() & PositionId.CAT_MASK) == PositionId.BODY_CAT) {
				final List<RDataTableColumn> columns= description.getRowHeaderColumns();
				final long index= getColumnIndex(cell);
				if (columns != null && index >= 0 && index < columns.size()) {
					return columns.get((int) index).getDefaultFormat();
				}
			}
			return null;
		}
		
	}
	
	
	private final AbstractRDataProvider<?> fDataProvider;
	
	
	RDataFormatter fFallbackFormatter;
	
	
	public RDataFormatterConverter(final AbstractRDataProvider<?> dataProvider) {
		this.fDataProvider= dataProvider;
	}
	
	
	private RDataFormatter getFormatter(final LayerCell cell) {
		final ContentDescription description= this.fDataProvider.getDescription();
		if (description != null) {
			final RDataFormatter formatter= getFormatter(description, cell);
			if (formatter != null) {
				return formatter;
			}
		}
		if (this.fFallbackFormatter == null) {
			this.fFallbackFormatter= new RDataFormatter();
		}
		return this.fFallbackFormatter;
	}
	
	protected @Nullable RDataFormatter getFormatter(final ContentDescription description,
			final LayerCell cell) {
		if ((cell.getDim(Orientation.HORIZONTAL).getId() & PositionId.CAT_MASK) == PositionId.BODY_CAT
				&& (cell.getDim(Orientation.VERTICAL).getId() & PositionId.CAT_MASK) == PositionId.BODY_CAT) {
			final List<RDataTableColumn> columns= description.getDataColumns();
			final long index= getColumnIndex(cell);
			if (index >= 0 && index < columns.size()) {
				return columns.get((int) index).getDefaultFormat();
			}
			return description.getDefaultDataFormat();
		}
		return null;
	}
	
	protected long getColumnIndex(final LayerCell cell) {
		return (cell.getDim(Orientation.HORIZONTAL).getId() & PositionId.NUM_MASK);
	}
	
	
	@Override
	public Object canonicalToDisplayValue(final LayerCell cell, final ConfigRegistry configRegistry,
			final Object canonicalValue) {
		return getFormatter(cell).modelToDisplayValue(canonicalValue);
	}
	
	@Override
	public Object displayToCanonicalValue(final LayerCell cell, final ConfigRegistry configRegistry,
			final Object displayValue) {
		throw new UnsupportedOperationException();
	}
	
}
