/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.r.ui.dataeditor.RDataTableInput;
import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore;
import org.eclipse.statet.rj.services.util.dataaccess.RDataFrameDataAdapter;
import org.eclipse.statet.rj.ts.core.RToolService;


public class RDataFrameDataProvider extends AbstractRDataProvider<RDataFrame> {
	
	
	public RDataFrameDataProvider(final RDataTableInput input, final RDataFrame struct) throws CoreException {
		super(input, new RDataFrameDataAdapter(), struct);
		
		reset();
	}
	
	
	@Override
	protected ContentDescription loadDescription(final RElementName name,
			final RDataFrame struct,
			final RToolService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final int columnCount= (int) getColumnCount();
		
		final RDataTableColumn rowHeaderColumn= createNamesColumn(
				"attr(" + getInput().getFullName() + ", 'row.names', exact= TRUE)", //$NON-NLS-1$ //$NON-NLS-2$
				struct.getRowCount(), r, m );
		final @NonNull RDataTableColumn[] dataColumns= new @NonNull RDataTableColumn[columnCount];
		for (int i= 0; i < columnCount; i++) {
			final String columnName= struct.getColumnNames().getChar(i);
			final RElementName elementName= RElementName.create(ImCollections.newList(BASE_NAME,
					RElementName.create(RElementName.SUB_NAMEDPART, columnName, i+1 )));
			dataColumns[i]= createColumn(struct.getColumn(i),
					getInput().getFullName() + "[[" + (i+1) + "]]", elementName, i, columnName,
					r, m );
		}
		
		return new ContentDescription(name, struct, r.getTool(),
				ImCollections.emptyList(), ImCollections.newList(rowHeaderColumn),
				ImCollections.newList(dataColumns), dataColumns,
				null );
	}
	
	@Override
	protected void appendOrderCmd(final StringBuilder cmd, final SortColumn sortColumn) {
		cmd.append("order(");
		cmd.append(getInput().getFullName());
		cmd.append("[[");
		cmd.append((sortColumn.getIdx() + 1));
		cmd.append("]], decreasing=");
		cmd.append(sortColumn.decreasing ? "TRUE" : "FALSE");
		cmd.append(')');
	}
	
	
	@Override
	protected Object getDataValue(final LazyRStore.Fragment<RDataFrame> fragment,
			final long rowIdx, final long columnIdx) {
		return fragment.getRObject().getColumn(fragment.toLocalColumnIdx(columnIdx))
				.get(fragment.toLocalRowIdx(rowIdx) );
	}
	
	@Override
	public DataProvider createColumnDataProvider() {
		return new ColumnDataProvider() {
			@Override
			public Object getDataValue(final long columnIndex, final long rowIndex,
					final int flags, final IProgressMonitor monitor) {
				return getRObject().getName(columnIndex);
			}
		};
	}
	
	@Override
	protected Object getColumnName(final LazyRStore.Fragment<RDataFrame> fragment, final long columnIdx) {
		return fragment.getRObject().getName(fragment.toLocalColumnIdx(columnIdx));
	}
	
}
