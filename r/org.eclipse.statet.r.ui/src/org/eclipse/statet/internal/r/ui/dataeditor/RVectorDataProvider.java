/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.r.ui.dataeditor.RDataTableInput;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore;
import org.eclipse.statet.rj.services.util.dataaccess.RVectorDataAdapter;
import org.eclipse.statet.rj.ts.core.RToolService;


public class RVectorDataProvider extends AbstractRDataProvider<RVector<?>> {
	
	
	public RVectorDataProvider(final RDataTableInput input, final RVector<?> struct) throws CoreException {
		super(input, new RVectorDataAdapter(), struct);
		
		reset();
	}
	
	
	@Override
	protected ContentDescription loadDescription(final RElementName name,
			final RVector<?> struct,
			final RToolService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final RDataTableColumn rowHeaderColumn= createNamesColumn(
				"names(" + getInput().getFullName() + ")",
				getAdapter().getRowCount(struct),
				r, m );
		
		final RDataTableColumn dataColumn= createColumn(struct.getData(),
				getInput().getFullName(), BASE_NAME, 0, getInput().getName(),
				r, m );
		
		return new ContentDescription(name, struct, r.getTool(),
				ImCollections.newList(), ImCollections.newList(rowHeaderColumn),
				ImCollections.newList(dataColumn), new @NonNull RDataTableColumn[] { dataColumn },
				dataColumn.getDefaultFormat() );
	}
	
	@Override
	protected void appendOrderCmd(final StringBuilder cmd, final SortColumn sortColumn) {
		cmd.append("order(");
		cmd.append(getInput().getFullName());
		cmd.append(",decreasing=");
		cmd.append(sortColumn.decreasing ? "TRUE" : "FALSE");
		cmd.append(')');
	}
	
	
	@Override
	protected Object getDataValue(final LazyRStore.Fragment<RVector<?>> fragment,
			final long rowIdx, final long columnIdx) {
		return fragment.getRObject().getData().get(fragment.toLocalRowIdx(rowIdx));
	}
	
	
	@Override
	public boolean hasRealColumns() {
		return false;
	}
	
	@Override
	public DataProvider createColumnDataProvider() {
		return new ColumnDataProvider() {
			@Override
			public Object getDataValue(final long columnIndex, final long rowIndex,
					final int flags, final IProgressMonitor monitor) {
				return getInput().getName();
			}
		};
	}
	
	@Override
	protected Object getColumnName(final LazyRStore.Fragment<RVector<?>> fragment, final long columnIdx) {
		throw new UnsupportedOperationException();
	}
	
}
