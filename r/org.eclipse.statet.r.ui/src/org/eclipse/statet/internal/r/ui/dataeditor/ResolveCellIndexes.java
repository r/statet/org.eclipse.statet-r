/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.SystemRunnable;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.r.core.tool.TmpUtils;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.ts.core.RToolService;


public class ResolveCellIndexes {
	
	
	private final ToolRunnable findRunnable= new SystemRunnable() {
		
		@Override
		public String getTypeId() {
			return "r/dataeditor/find"; //$NON-NLS-1$
		}
		
		@Override
		public String getLabel() {
			return "Find Data (" + ResolveCellIndexes.this.dataProvider.getInput().getName() + ")";
		}
		
		@Override
		public boolean canRunIn(final Tool tool) {
			return true; // TODO
		}
		
		@Override
		public boolean changed(final int event, final Tool process) {
			switch (event) {
			case MOVING_FROM:
				return false;
			case REMOVING_FROM:
			case BEING_ABANDONED:
				synchronized (this) {
					ResolveCellIndexes.this.findScheduled= false;
				}
				break;
			default:
				break;
			}
			return true;
		}
		
		@Override
		public void run(final ToolService service, final ProgressMonitor m) throws StatusException {
			final long[] coord;
			synchronized (this) {
				coord= ResolveCellIndexes.this.index;
				ResolveCellIndexes.this.findScheduled= false;
			}
			if (coord == null
					|| ResolveCellIndexes.this.dataProvider.getLockState() > Lock.LOCAL_PAUSE_STATE) {
				return;
			}
			try {
				coord[1]= resolveRowIdx(coord[1], (RToolService) service, m);
				execute(coord[0], coord[1]);
			}
			catch (final StatusException | UnexpectedRDataException e) {
				throw new StatusException(new ErrorStatus(RUI.BUNDLE_ID,
						"An error occurred when resolving indexes for data viewer.", e ));
			}
		}
		
	};
	
	private boolean findScheduled;
	
	private final AbstractRDataProvider<?> dataProvider;
	
	private long[] index;
	
	
	public ResolveCellIndexes(final AbstractRDataProvider<?> dataProvider) {
		this.dataProvider= dataProvider;
	}
	
	
	public AbstractRDataProvider<?> getDataProvider() {
		return this.dataProvider;
	}
	
	public void resolve(final long columnIdx, final long rowIdx) {
		synchronized (this.findRunnable) {
			this.index= null;
			if (this.dataProvider.getFilter() != null
					|| this.dataProvider.getSortColumn() != null) {
				this.index= new long[] { columnIdx, rowIdx };
				if (!this.findScheduled) {
					this.findScheduled= true;
					this.dataProvider.schedule(this.findRunnable);
				}
				return;
			}
		}
		
		execute(columnIdx, rowIdx);
	}
	
	private long resolveRowIdx(final long rowIdx,
			final RToolService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final String revIndexName= this.dataProvider.checkRevIndex(r, m);
		if (revIndexName != null) {
			final StringBuilder cmd= this.dataProvider.getRCmdStringBuilder();
			cmd.append(TmpUtils.ENV_FQ_NAME+'$').append(revIndexName)
					.append('[').append(rowIdx + 1).append(']');
			final RVector<?> vector= RDataUtils.checkRVector(r.evalData(cmd.toString(), m));
			final RStore<?> data= RDataUtils.checkLengthEqual(vector, 1).getData();
			if (data.isNA(0)) {
				return -1;
			}
			return ((data.getStoreType() == RStore.INTEGER) ?
							(long) data.getInt(0) : (long) data.getNum(0) )
					- 1;
		}
		return rowIdx;
	}
	
	protected void execute(final long columnIndex, final long rowIndex) {
	}
	
}
