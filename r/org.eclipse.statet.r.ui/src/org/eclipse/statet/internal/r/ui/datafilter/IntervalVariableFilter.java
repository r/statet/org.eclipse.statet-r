/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilter;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class IntervalVariableFilter extends VariableFilter {
	
	
	public static abstract class Data {
		
		
		private final boolean hasNA;
		private final boolean hasMinMax;
		
		
		protected Data(final boolean hasMinMax, final boolean hasNA) {
			this.hasMinMax= hasMinMax;
			this.hasNA= hasNA;
		}
		
		
		public boolean hasMinMax() {
			return this.hasMinMax;
		}
		
		public abstract Number getMin();
		public abstract Number getMax();
		
		public abstract boolean isValidValue(Number value);
		public abstract boolean isGreaterThanMin(Number value);
		public abstract boolean isSmallerThanMax(Number value);
		
		public boolean hasNA() {
			return this.hasNA;
		}
		
	}
	
	public static class IntData extends Data {
		
		private final int min;
		private final int max;
		
		private IntData(final int min, final int max,
				final boolean hasNA) {
			super(true, hasNA);
			this.min= min;
			this.max= max;
		}
		
		private IntData(final boolean hasNA) {
			super(false, hasNA);
			this.min= 0;
			this.max= 0;
		}
		
		public int getMinInt() {
			return this.min;
		}
		
		@Override
		public Integer getMin() {
			return this.min;
		}
		
		public int getMaxInt() {
			return this.max;
		}
		
		@Override
		public Integer getMax() {
			return this.max;
		}
		
		@Override
		public boolean isValidValue(final Number value) {
			if (value instanceof Integer) {
				final int v= value.intValue();
				return (this.min <= v && v <= this.max);
			}
			return false;
		}
		
		@Override
		public boolean isGreaterThanMin(final Number value) {
			return (value != null && this.min < value.intValue());
		}
		
		@Override
		public boolean isSmallerThanMax(final Number value) {
			return (value != null && value.intValue() < this.max);
		}
		
	}
	
	public static class NumData extends Data {
		
		private final double min;
		private final double max;
		
		private NumData(final double min, final double max,
				final boolean hasNA) {
			super(true, hasNA);
			this.min= min;
			this.max= max;
		}
		
		private NumData(final boolean hasNA) {
			super(false, hasNA);
			this.min= 0;
			this.max= 0;
		}
		
		public double getMinDouble() {
			return this.min;
		}
		
		@Override
		public Double getMin() {
			return this.min;
		}
		
		public double getMaxDouble() {
			return this.max;
		}
		
		@Override
		public Double getMax() {
			return this.max;
		}
		
		@Override
		public boolean isValidValue(final Number value) {
			if (value instanceof Double) {
				final double v= value.doubleValue();
				return (this.min <= v && v <= this.max);
			}
			return false;
		}
		
		@Override
		public boolean isGreaterThanMin(final Number value) {
			return (value != null && this.min < value.doubleValue());
		}
		
		@Override
		public boolean isSmallerThanMax(final Number value) {
			return (value != null && value.doubleValue() < this.max);
		}
		
	}
	
	private static final int MIN_IDX= 0;
	private static final int MAX_IDX= 1;
	private static final int NA_IDX= 2;
	
	
	private @Nullable Data data;
	
	private final IObservableValue<Number> selectedLowerValue;
	private final IObservableValue<Number> selectedUpperValue;
	private final IObservableValue<Boolean> selectedNA;
	
	
	protected IntervalVariableFilter(final FilterSet set, final RDataTableColumn column) {
		super(set, column);
		
		this.selectedLowerValue= new WritableValue<>(set.getRealm());
		this.selectedUpperValue= new WritableValue<>(set.getRealm());
		this.selectedNA= new WritableValue<>(set.getRealm(), true, Boolean.TYPE);
		registerObservable(this.selectedLowerValue);
		registerObservable(this.selectedUpperValue);
		registerObservable(this.selectedNA);
	}
	
	
	@Override
	public FilterType getType() {
		return FilterType.INTERVAL;
	}
	
	@Override
	public void load(final VariableFilter filter) {
		if (filter.getType() == FilterType.INTERVAL
				&& filter.getColumn().getDataStore().getStoreType() == getColumn().getDataStore().getStoreType() ) {
			final IntervalVariableFilter intervalFilter= (IntervalVariableFilter)filter;
			runInRealm(() -> {
				var minMaxData= this.data;
				if (minMaxData != null || (minMaxData= intervalFilter.data) == null) {
					return;
				}
				this.data= minMaxData;
				this.selectedLowerValue.setValue(intervalFilter.getSelectedLowerValue().getValue());
				this.selectedUpperValue.setValue(intervalFilter.getSelectedUpperValue().getValue());
				this.selectedNA.setValue(intervalFilter.getSelectedNA().getValue());
			});
		}
	}
	
	@Override
	public void reset() {
		runInRealm(() -> {
			final var data= this.data;
			if (data == null) {
				return;
			}
			this.selectedLowerValue.setValue(data.getMin());
			this.selectedUpperValue.setValue(data.getMax());
			this.selectedNA.setValue(Boolean.TRUE);
		});
	}
	
	@Override
	protected void updateData(final RToolService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final RDataTableColumn column= getColumn();
		{	final FunctionCall fcall= r.createFunctionCall("rj:::.getDataIntervalValues"); //$NON-NLS-1$
			fcall.add(column.getRExpression());
			
			final RObject rData= fcall.evalData(m);
			RDataUtils.checkRVector(rData);
			final RStore<Number> rStore= (RStore<Number>)RDataUtils.checkData(
					rData.getData(), column.getDataStore().getStoreType() );
			RDataUtils.checkLengthGreaterOrEqual(rStore, 3);
			final boolean hasMinMax= !rStore.isMissing(MIN_IDX) && !rStore.isMissing(MAX_IDX);
			final boolean hasNA= rStore.getLogi(NA_IDX);
			if (rStore.getStoreType() == RStore.NUMERIC) {
				setFilterData((hasMinMax) ?
						new NumData(rStore.getNum(MIN_IDX), rStore.getNum(MAX_IDX), hasNA) :
						new NumData(hasNA) );
			}
			else {
				setFilterData((hasMinMax) ?
						new IntData(rStore.getInt(MIN_IDX), rStore.getInt(MAX_IDX), hasNA) :
						new IntData(hasNA) );
			}
		}
	}
	
	@Override
	protected void setStatus(final IStatus status) {
		runInRealm(() -> {
			this.data= null;
			doSetStatus(status);
			updateFilter(SCHEDULE);
			notifyListeners();
		});
	}
	
	protected void setFilterData(final Data data) {
		runInRealm(() -> {
			final var prevData= this.data;
			this.data= data;
			{	final Number value= this.selectedLowerValue.getValue();
				if (!data.isValidValue(value)
						|| prevData == null || value.equals(prevData.getMin()) ) {
					this.selectedLowerValue.setValue(data.getMin());
				}
			}
			{	final Number value= this.selectedLowerValue.getValue();
				if (!data.isValidValue(value)
						|| prevData == null || value.equals(prevData.getMax()) ) {
					this.selectedUpperValue.setValue(data.getMax());
				}
			}
			clearStatus();
			updateFilter(SCHEDULE);
			notifyListeners();
		});
	}
	
	@Override
	protected @Nullable String createFilter(final String varExpression) {
		final var data= this.data;
		final Number lower;
		final Number upper;
		if (data == null
				|| (lower= this.selectedLowerValue.getValue()) == null
				|| (upper= this.selectedUpperValue.getValue()) == null) {
			return null;
		}
		final StringBuilder sb= new StringBuilder();
		sb.append('(');
		
		int num= 0;
		int na= 0;
		
		if (data.hasMinMax()) {
			if (data.isGreaterThanMin(lower)) {
				sb.append(varExpression);
				sb.append(" >= "); //$NON-NLS-1$
				sb.append(lower);
				num++;
			}
			if (data.isSmallerThanMax(upper)) {
				if (sb.length() > 1) {
					sb.append(" & "); //$NON-NLS-1$
				}
				sb.append(varExpression);
				sb.append(" <= "); //$NON-NLS-1$
				sb.append(upper);
				num++;
			}
		}
		if (data.hasNA()) {
			na= (this.selectedNA.getValue()) ? 1 : -1;
		}
		if (num > 0 || na < 0) {
			if (na >= 0) {
				if (num > 1) {
					sb.insert(0, '(');
					sb.append(')');
				}
				if (num > 0) {
					sb.append(" | "); //$NON-NLS-1$
				}
				sb.append("is.na(").append(varExpression).append(')'); //$NON-NLS-1$
			}
			else {
				if (num > 0) {
					sb.append(" & "); //$NON-NLS-1$
				}
				sb.append("!is.na(").append(varExpression).append(')'); //$NON-NLS-1$
			}
		}
		sb.append(')');
		return (sb.length() <= 2) ? "" : sb.toString(); //$NON-NLS-1$
	}
	
	
	public @Nullable Data getData() {
		return this.data;
	}
	
	
	public IObservableValue<Number> getSelectedLowerValue() {
		return this.selectedLowerValue;
	}
	
	public IObservableValue<Number> getSelectedUpperValue() {
		return this.selectedUpperValue;
	}
	
	public IObservableValue<Boolean> getSelectedNA() {
		return this.selectedNA;
	}
	
}
