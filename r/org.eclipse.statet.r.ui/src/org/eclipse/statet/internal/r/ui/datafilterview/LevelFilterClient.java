/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.databinding.UpdateSetStrategy;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckable;
import org.eclipse.swt.SWT;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.ui.actions.ControlServicesUtil;
import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;
import org.eclipse.statet.ecommons.ui.util.AutoCheckController;
import org.eclipse.statet.ecommons.ui.workbench.ContextHandlers;

import org.eclipse.statet.internal.r.ui.datafilter.LevelVariableFilter;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class LevelFilterClient extends FilterClient<LevelVariableFilter> {
	
	
	private RStore<?> availableValues;
	
	private final IObservableSet<Object> selectedValues;
	
	/*- GUI/Widgets -*/
	
	private CheckboxTableViewer valueListViewer= nonNullLateInit();
	
	private ContextHandlers valueListHandlers= nonNullLateInit();
	private MenuManager valueListMenuManager= nonNullLateInit();
	
	
	public LevelFilterClient(final VariableComposite parent, final LevelVariableFilter filter) {
		super(parent, filter);
		
		this.availableValues= filter.getAvailableValues();
		this.selectedValues= filter.getSelectedValues();
		init(1);
	}
	
	@Override
	protected void onDispose() {
		if (this.valueListMenuManager != null) {
			this.valueListMenuManager.dispose();
			this.valueListMenuManager= null;
		}
		if (this.valueListHandlers != null) {
			this.valueListHandlers.dispose();
			this.valueListHandlers= null;
		}
		
		super.onDispose();
	}
	
	
	@Override
	public LevelVariableFilter getFilter() {
		return this.filter;
	}
	
	@Override
	protected void addWidgets() {
		addStatusInfoLine();
		
		this.valueListViewer= CheckboxTableViewer.newCheckList(this, SWT.MULTI | SWT.FLAT | SWT.FULL_SELECTION);
		this.valueListViewer.setContentProvider(new RStoreContentProvider());
		this.valueListViewer.setLabelProvider(new ColumnLabelProvider(this.filter.getColumn()));
	}
	
	@Override
	protected void initActions(final IServiceLocator serviceLocator) {
		final ControlServicesUtil servicesUtil= new ControlServicesUtil(serviceLocator,
				getClass().getName() + "/ValueList#" + hashCode(), this ); //$NON-NLS-1$
		servicesUtil.addControl(this.valueListViewer.getTable());
		final var handlers= new ContextHandlers(serviceLocator);
		handlers.setDefaultActivationExpression(servicesUtil.getExpression());
		handlers.setDeactivateOnDisposal(true);
		this.valueListHandlers= handlers;
		
		final AutoCheckController<?> autoCheckController= new AutoCheckController<>(
				this.valueListViewer, this.filter.getSelectedValues() );
		{	final IHandler2 handler= autoCheckController.createSelectAllHandler();
			handlers.addActivate(SELECT_ALL_COMMAND_ID, handler);
		}
		
		this.valueListMenuManager= new MenuManager();
		this.valueListMenuManager.add(new HandlerContributionItem(new CommandContributionItemParameter(serviceLocator,
						null, SELECT_ALL_COMMAND_ID, HandlerContributionItem.STYLE_PUSH),
				nonNullAssert(this.valueListHandlers.get(SELECT_ALL_COMMAND_ID)) ));
		this.valueListViewer.getTable().setMenu(
				this.valueListMenuManager.createContextMenu(this.valueListViewer.getControl()) );
		
//		this.valueListViewer.addSelectionChangedListener(new ISelectionChangedListener() {
//			@Override
//			public void selectionChanged(final SelectionChangedEvent event) {
//				updateActions();
//			}
//		});
//		updateActions();
	}
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		db.getContext().bindSet(
				ViewerProperties.checkedElements(Object.class)
						.observe((ICheckable)this.valueListViewer),
				this.selectedValues,
				new UpdateSetStrategy<>()
						.setConverter(new UI2RStoreConverter<>()),
				new UpdateSetStrategy<>()
						.setConverter(new RStore2UIConverter<>()) );
	}
	
	@Override
	protected void updateInput() {
		this.availableValues= this.filter.getAvailableValues();
		this.valueListViewer.setInput(this.availableValues);
		
		checkLayout();
	}
	
	@Override
	protected boolean updateLayout() {
		return updateLayout(this.valueListViewer, (int)this.availableValues.getLength());
	}
	
}
