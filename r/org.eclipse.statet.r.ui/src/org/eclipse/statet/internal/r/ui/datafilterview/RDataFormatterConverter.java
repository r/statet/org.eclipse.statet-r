/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.FIELD;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.PARAMETER;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.RETURN_TYPE;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.core.conversion.ClassTypedConverter;

import org.eclipse.statet.internal.r.ui.dataeditor.RDataFormatter;


@NonNullByDefault({ RETURN_TYPE, FIELD })
public class RDataFormatterConverter<S> implements ClassTypedConverter<S, String> {
	
	
	private final Class<S> fromType;
	
	private final RDataFormatter formatter;
	
	
	@NonNullByDefault({ PARAMETER })
	public RDataFormatterConverter(final Class<S> fromType, final RDataFormatter formatter) {
		this.fromType= fromType;
		this.formatter= formatter;
	}
	
	
	@Override
	public Class<S> getFromType() {
		return this.fromType;
	}
	
	@Override
	public Class<String> getToType() {
		return String.class;
	}
	
	@Override
	public String convert(final S fromObject) {
		return this.formatter.modelToDisplayValue(fromObject).toString();
	}
	
}
