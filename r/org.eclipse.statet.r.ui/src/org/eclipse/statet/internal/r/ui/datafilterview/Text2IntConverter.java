/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.core.conversion.ClassTypedConverter;


@NonNullByDefault
public class Text2IntConverter implements ClassTypedConverter<String, Integer> {
	
	
	public Text2IntConverter() {
	}
	
	
	@Override
	public Class<String> getFromType() {
		return String.class;
	}
	
	@Override
	public Class<Integer> getToType() {
		return Integer.TYPE;
	}
	
	@Override
	public Integer convert(final String fromObject) {
		String s= fromObject;
		if (s == null || (s= s.trim()).length() == 0) {
			throw new IllegalArgumentException();
		}
		if (s.charAt(s.length() - 1) == 'L') {
			s= s.substring(0, s.length()-1);
		}
		if (s.startsWith("0x")) { //$NON-NLS-1$
			return Integer.parseInt(s.substring(2), 16);
		}
		return Integer.parseInt(s);
	}
	
}
