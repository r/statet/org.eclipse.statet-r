/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import org.eclipse.core.databinding.conversion.IConverter;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.dataeditor.AbstractRDataProvider;


@NonNullByDefault
public class UI2RStoreConverter<D> implements IConverter<Object, @Nullable D> {
	
	
	public UI2RStoreConverter() {
	}
	
	
	@Override
	public Object getFromType() {
		return Object.class;
	}
	
	@Override
	public Object getToType() {
		return Object.class;
	}
	
	@Override
	public @Nullable D convert(final Object fromObject) {
		if (fromObject == AbstractRDataProvider.NA) {
			return null;
		}
		return (D)fromObject;
	}
	
}
