/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;
import org.eclipse.statet.ecommons.ui.swt.expandable.ExpandableComposite;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.internal.r.ui.datafilter.FilterSet;
import org.eclipse.statet.internal.r.ui.datafilter.FilterType;
import org.eclipse.statet.internal.r.ui.datafilter.VariableFilter;
import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;


@NonNullByDefault
public class VariableComposite extends ExpandableComposite {
	
	
	private final VariableContainer container;
	private RDataTableColumn column;
	
	private final MenuManager menuManager;
	
	private boolean active;
	
	
	public VariableComposite(final Composite parent, final VariableContainer container,
			final RDataTableColumn column) {
		super(parent, SWT.NONE, TWISTIE | CLIENT_INDENT | IMAGE);
		
		this.container= container;
		this.column= nonNullLateInit();
		setColumn(column);
		
		this.menuManager= new MenuManager();
		this.menuManager.setRemoveAllWhenShown(true);
		this.menuManager.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(final IMenuManager manager) {
				fillMenu(manager);
			}
		});
		setMenu(this.menuManager.createContextMenu(this));
	}
	
	
	@Override
	public void setClient(final Control client) {
		if (!(client instanceof FilterClient)) {
			throw new IllegalArgumentException();
		}
		super.setClient(client);
		updateImage(false);
	}
	
	@Override
	public void layout(final @NonNull Control @Nullable [] changed, final int flags) {
		super.layout(changed, flags);
		getParent().layout(new @NonNull Control[] { this });
		if (isExpanded()) {
			this.container.getComposite().reflow(true);
		}
	}
	
	
	public VariableContainer getContainer() {
		return this.container;
	}
	
	@Override
	public FilterClient<?> getClient() {
		return (FilterClient<?>)super.getClient();
	}
	
	public void setColumn(final RDataTableColumn column) {
		final String columnName= nonNullAssert(column.getName());
		
		this.column= column;
		
		updateImage(true);
		setText(columnName);
	}
	
	protected void updateImage(final boolean force) {
		final boolean isActive= isFilterActive();
		if (!force && this.active == isActive) {
			return;
		}
		Image image= this.container.getLabelProvider().getImage(this.column);
		if (isActive) {
			final DecorationOverlayIcon descriptor= new DecorationOverlayIcon(image,
					new ImageDescriptor[] { null, SharedUIResources.getImages()
							.getDescriptor(SharedUIResources.OVR_YELLOW_LIGHT_IMAGE_ID)
				});
			image= RUIPlugin.getInstance().getImageDescriptorRegistry().get(descriptor);
			this.active= true;
		}
		else {
			this.active= false;
		}
		setImage(image);
	}
	
	private boolean isFilterActive() {
		final FilterClient<?> client= getClient();
		if (client != null) {
			final String rExpression= client.getFilter().getFilterRExpression();
			return (rExpression != null && !rExpression.isEmpty());
		}
		return false;
	}
	
	public RDataTableColumn getColumn() {
		return this.column;
	}
	
	protected void fillMenu(final IMenuManager menu) {
		final VariableFilter currentFilter= getClient().getFilter();
		final FilterSet filterSet= currentFilter.getSet();
		final List<FilterType> filters= filterSet.getAvailableFilters(this.column);
		for (int i= 0; i < filters.size(); i++) {
			final FilterType filterType= filters.get(i);
			final SimpleContributionItem item= new SimpleContributionItem(filterType.getLabel(), null,
					SimpleContributionItem.STYLE_RADIO) {
				@Override
				protected void execute() throws ExecutionException {
					setFilterType(filterType);
				}
			};
			item.setChecked(currentFilter.getType() == filterType);
			menu.add(item);
		}
		menu.add(new Separator());
		menu.add(new SimpleContributionItem(Messages.Variable_Clear_label, null) {
			@Override
			protected void execute() throws ExecutionException {
				getClient().getFilter().reset();
			}
		});
	}
	
	protected void setFilterType(final FilterType type) {
		final VariableFilter currentFilter= getClient().getFilter();
		if (currentFilter.getType() == type) {
			return;
		}
		final FilterSet filterSet= currentFilter.getSet();
		filterSet.replace(currentFilter, type);
		layout(new @NonNull Control[] { getClient() });
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(super.toString());
		sb.append(' ', this.column.toString());
		return sb.toString();
	}
	
}
