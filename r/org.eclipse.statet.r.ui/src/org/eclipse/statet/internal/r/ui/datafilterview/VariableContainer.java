/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.swt.expandable.ExpandableRowsList;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.internal.r.ui.dataeditor.RDataLabelProvider;
import org.eclipse.statet.internal.r.ui.datafilter.FilterListener;
import org.eclipse.statet.internal.r.ui.datafilter.FilterSet;
import org.eclipse.statet.internal.r.ui.datafilter.IntervalVariableFilter;
import org.eclipse.statet.internal.r.ui.datafilter.LevelVariableFilter;
import org.eclipse.statet.internal.r.ui.datafilter.TextVariableFilter;
import org.eclipse.statet.internal.r.ui.datafilter.VariableFilter;
import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.r.ui.dataeditor.RDataTableContentDescription;


/**
 * Container for a list of VariableComposite
 */
@NonNullByDefault
public class VariableContainer {
	
	
	private final IServiceLocator serviceLocator;
	
	private final ExpandableRowsList composite;
	
	private final List<VariableComposite> variables;
	
	private final RDataLabelProvider labelProvider;
	
	private final FilterSet filterSet;
	
	private @Nullable RDataTableContentDescription description;
	
	
	public VariableContainer(final IServiceLocator serviceLocator, final ExpandableRowsList composite) {
		this.serviceLocator= serviceLocator;
		this.composite= composite;
		this.variables= new ArrayList<>();
		
		this.composite.getContent().setLayout(LayoutUtils.newContentGrid(1));
		
		this.labelProvider= new RDataLabelProvider();
		
		this.filterSet= new FilterSet(nonNullAssert(Realm.getDefault())) {
			@Override
			protected void updateFilter(final int idx,
					final @Nullable VariableFilter oldFilter, final @Nullable VariableFilter newFilter) {
				if (newFilter == null) {
					removeVariable(idx);
					return;
				}
				final int oldIdx= getVariable(oldFilter);
				if (oldIdx == idx) {
					updateVariable(idx, newFilter);
				}
				else if (oldIdx > idx) {
					moveVariable(oldIdx, idx);
					updateVariable(idx, newFilter);
				}
				else {
					if (!isInputFilterUpdate() && oldFilter != null) {
						removeVariable(idx);
					}
					addVariable(idx, newFilter);
				}
			}
			@Override
			protected void completeInputFilterUpdate(final ImList<VariableFilter> filter) {
				removeVariables(filter.size());
			}
		};
		
		this.filterSet.addPostListener(new FilterListener() {
			@Override
			public void filterChanged() {
				for (final VariableComposite variable : VariableContainer.this.variables) {
					variable.updateImage(false);
				}
			}
		});
	}
	
	
	public void updateInput(final RDataTableContentDescription description) {
		this.description= description;
		
		this.composite.setRedraw(false);
		this.composite.setDelayedReflow(true);
		try {
			this.filterSet.updateInput(description);
			
			if (this.variables.size() == 1) {
				this.variables.get(0).setExpanded(true);
			}
		}
		finally {
			this.composite.setDelayedReflow(false);
			this.composite.setRedraw(true);
			this.composite.reflow(true);
		}
	}
	
	public @Nullable RDataTableContentDescription getDescription() {
		return this.description;
	}
	
	
	private void addVariable(final int idx, final VariableFilter filter) {
		final boolean insert= (idx < this.variables.size());
		final VariableComposite composite= createVariable(filter.getColumn());
		createFilterClient(composite, filter);
		if (insert) {
			composite.moveAbove(this.variables.get(idx));
		}
		this.variables.add(idx, composite);
	}
	
	private void moveVariable(final int fromIdx, final int toIdx) {
		if (fromIdx <= toIdx) {
			throw new UnsupportedOperationException();
		}
		this.variables.get(fromIdx).moveAbove(this.variables.get(toIdx));
		this.variables.add(toIdx, this.variables.remove(fromIdx));
	}
	
	private void updateVariable(final int idx, final VariableFilter filter) {
		final VariableComposite composite= this.variables.get(idx);
		composite.setColumn(filter.getColumn());
		final FilterClient<?> oldClient= composite.getClient();
		createFilterClient(composite, filter);
		if (oldClient != null) {
			oldClient.dispose();
		}
		composite.layout(new @NonNull Control[] { composite.getClient() });
	}
	
	private void removeVariable(final int idx) {
		if (idx >= this.variables.size()) {
			return;
		}
		final VariableComposite composite= this.variables.remove(idx);
		composite.dispose();
	}
	
	private void removeVariables(final int startIdx) {
		int size;
		while ((size= this.variables.size()) > startIdx) {
			removeVariable(size - 1);
		}
	}
	
	
	protected @Nullable FilterClient<?> createFilterClient(final VariableComposite composite,
			final @Nullable VariableFilter filter) {
		if (filter == null) {
			return null;
		}
		FilterClient<?> client;
		switch (filter.getType().getId()) {
		case 0:
			client= new LevelFilterClient(composite, (LevelVariableFilter)filter);
			break;
		case 1:
			client= new IntervalFilterClient(composite, (IntervalVariableFilter)filter);
			break;
		case 2:
			client= new TextFilterClient(composite, (TextVariableFilter)filter);
			break;
		default:
			throw new IllegalStateException(filter.toString());
		}
		this.composite.adaptChild(client);
		
		return client;
	}
	
	protected VariableComposite createVariable(final RDataTableColumn column) {
		final VariableComposite expandable= new VariableComposite(
				this.composite.getContent(), this, column);
		expandable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
//		expandable.setExpanded(true);
		this.composite.adaptChild(expandable);
		
		return expandable;
	}
	
	
	public FilterSet getFilterSet() {
		return this.filterSet;
	}
	
	public IServiceLocator getServiceLocator() {
		return this.serviceLocator;
	}
	
	public ExpandableRowsList getComposite() {
		return this.composite;
	}
	
	public List<VariableComposite> getVariables() {
		return this.variables;
	}
	
	protected int getVariable(final @Nullable VariableFilter filter) {
		if (filter != null) {
			for (int i= 0; i < this.variables.size(); i++) {
				final var client= this.variables.get(i).getClient();
				if (client != null && client.getFilter() == filter) {
					return i;
				}
			}
		}
		return -1;
	}
	
	public RDataLabelProvider getLabelProvider() {
		return this.labelProvider;
	}
	
	public void dispose() {
		this.labelProvider.dispose();
	}
	
}
