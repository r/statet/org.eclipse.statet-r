/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.IntPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;

import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.editors.RDefaultFoldingProvider;


/**
 * Preferences for {@link RDefaultFoldingProvider}
 */
@NonNullByDefault
public class DefaultRFoldingPreferences {
	
	public static final String GROUP_ID = "r.editor/folding/default"; //$NON-NLS-1$
	
	public static final String NODE = RUI.BUNDLE_ID + "/editor.r/folding.default"; //$NON-NLS-1$
	
	public static final BooleanPref PREF_OTHERBLOCKS_ENABLED = new BooleanPref(
			DefaultRFoldingPreferences.NODE, "other_blocks.enabled"); //$NON-NLS-1$
	public static final IntPref PREF_MINLINES_NUM = new IntPref(
			DefaultRFoldingPreferences.NODE, "min_lines.num"); //$NON-NLS-1$
	
	public static final BooleanPref PREF_ROXYGEN_ENABLED = new BooleanPref(
			DefaultRFoldingPreferences.NODE, "roxygen.enabled"); //$NON-NLS-1$
	public static final IntPref PREF_ROXYGEN_MINLINES_NUM = new IntPref(
			DefaultRFoldingPreferences.NODE, "roxygen.min_lines.num"); //$NON-NLS-1$
	public static final BooleanPref PREF_ROXYGEN_COLLAPSE_INITIALLY_ENABLED = new BooleanPref(
			DefaultRFoldingPreferences.NODE, "roxygen.collapse_initially.enabled"); //$NON-NLS-1$
	
	
	/**
	 * Initializes the default values.
	 */
	public static void initializeDefaultValues(final IScopeContext context) {
		PreferenceUtils.setPrefValue(context, DefaultRFoldingPreferences.PREF_MINLINES_NUM, 4);
		PreferenceUtils.setPrefValue(context, DefaultRFoldingPreferences.PREF_OTHERBLOCKS_ENABLED, false);
		PreferenceUtils.setPrefValue(context, DefaultRFoldingPreferences.PREF_ROXYGEN_ENABLED, true);
		PreferenceUtils.setPrefValue(context, DefaultRFoldingPreferences.PREF_ROXYGEN_MINLINES_NUM, 2);
		PreferenceUtils.setPrefValue(context, DefaultRFoldingPreferences.PREF_ROXYGEN_COLLAPSE_INITIALLY_ENABLED, false);
	}
	
}
