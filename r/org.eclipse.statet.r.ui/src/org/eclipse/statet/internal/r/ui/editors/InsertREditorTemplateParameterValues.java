/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InsertEditorTemplateParameterValues;
import org.eclipse.statet.ltk.ui.templates.EnhTemplateStore;


@NonNullByDefault
public class InsertREditorTemplateParameterValues extends InsertEditorTemplateParameterValues {
	
	
	/** plugin.xml */
	public InsertREditorTemplateParameterValues() {
	}
	
	
	@Override
	protected EnhTemplateStore getTemplateStore() {
		return RUIPlugin.getInstance().getREditorTemplateStore();
	}
	
}
