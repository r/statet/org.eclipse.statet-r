/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;
import org.eclipse.statet.ecommons.ui.SharedUIResources;

import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.rmodel.RElementAccess;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.ui.sourceediting.ROpenDeclaration;


public class OpenRElementHyperlink implements IHyperlink {
	
	
	private final SourceEditor editor;
	private final IRegion region;
	
	private final RSourceUnit su;
	private final RElementAccess access;
	
	
	public OpenRElementHyperlink(final SourceEditor editor, final RSourceUnit su, final RElementAccess access) {
		assert (su != null);
		assert (access != null);
		
		this.editor= editor;
		this.region= JFaceTextRegion.toJFaceRegion(
				nonNullAssert(access.getNameNode()) );
		this.su= su;
		this.access= access;
	}
	
	
	@Override
	public String getTypeLabel() {
		return null;
	}
	
	@Override
	public IRegion getHyperlinkRegion() {
		return this.region;
	}
	
	@Override
	public String getHyperlinkText() {
		return NLS.bind(Messages.Hyperlinks_OpenDeclaration_label, this.access.getDisplayName());
	}
	
	@Override
	public void open() {
		try {
			final List<SourceElement> list= RModel.searchDeclaration(this.access, this.su);
			final ROpenDeclaration open= new ROpenDeclaration();
			final SourceElement element= open.selectElement(list, this.editor.getWorkbenchPart());
			if (element != null) {
				open.open(element, true);
			}
			else {
				Display.getCurrent().beep();
			}
		}
		catch (final PartInitException e) {
			Display.getCurrent().beep();
			StatusManager.getManager().handle(new Status(IStatus.INFO, SharedUIResources.BUNDLE_ID, -1,
					NLS.bind("An error occurred when following the hyperlink and opening the editor for the declaration of ''{0}''", this.access.getDisplayName()), e));
		}
		catch (final CoreException e) {
			// cancelled
		}
	}
	
}
