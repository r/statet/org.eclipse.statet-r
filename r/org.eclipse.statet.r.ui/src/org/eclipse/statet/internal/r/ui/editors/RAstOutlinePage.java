/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IElementComparer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.TreeViewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1OutlinePage;
import org.eclipse.statet.ltk.ui.util.ExtModelContentProvider;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.ui.RLabelProvider;


/**
 * Shows the AST in the outline - for debugging purposes
 */
@NonNullByDefault
public class RAstOutlinePage extends SourceEditor1OutlinePage {
	
	
	public RAstOutlinePage(final REditor editor) {
		super(editor, RModel.R_TYPE_ID, null);
	}
	
	
	@Override
	protected IDialogSettings getDialogSettings() {
		return DialogUtils.getDialogSettings(RUIPlugin.getInstance(), "RAstOutlineView"); //$NON-NLS-1$
	}
	
	@Override
	protected ExtModelContentProvider createContentProvider() {
		return new AstContentProvider();
	}
	
	@Override
	protected void configureViewer(final TreeViewer viewer) {
		super.configureViewer(viewer);
		
		viewer.setComparer(new IElementComparer() {
			@Override
			public int hashCode(final Object element) {
				return ((RAstNode)element).hashCodeIgnoreAst();
			}
			@Override
			public boolean equals(final Object a, final Object b) {
				return ((RAstNode)a).equalsIgnoreAst(b);
			}
		});
	}
	
	@Override
	protected ILabelProvider createLabelProvider() {
		return new RLabelProvider();
	}
	
}
