/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.source.IAnnotationModel;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;
import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.ui.sourceediting.SourceAnnotationModel;
import org.eclipse.statet.ltk.ui.sourceediting.SourceDocumentProvider;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.source.doc.RDocumentSetupParticipant;
import org.eclipse.statet.r.ui.editors.REditorBuild;


@NonNullByDefault
public class RDocumentProvider extends SourceDocumentProvider<RSourceUnit> implements Disposable {
	
	
	private class ThisAnnotationModel extends SourceAnnotationModel {
		
		public ThisAnnotationModel(final IResource resource) {
			super(resource, RDocumentProvider.this.getIssueTypeSet());
		}
		
		@Override
		protected boolean isHandlingTemporaryProblems(final IssueTypeSet.ProblemCategory issueCategory) {
			return RDocumentProvider.this.handleTemporaryProblems;
		}
		
	}
	
	
	private SettingsChangeNotifier. @Nullable ChangeListener editorPrefListener;
	
	private boolean handleTemporaryProblems;
	
	
	public RDocumentProvider() {
		super(RModel.R_TYPE_ID, new RDocumentSetupParticipant(),
				REditorBuild.R_ISSUE_TYPE_SET );
		
		{	final var editorPrefListener = new SettingsChangeNotifier.ChangeListener() {
				@Override
				public void settingsChanged(final Set<String> groupIds) {
					if (groupIds.contains(REditorBuild.GROUP_ID)) {
						updateEditorPrefs();
					}
				}
			};
			this.editorPrefListener= editorPrefListener;
			PreferencesUtil.getSettingsChangeNotifier().addChangeListener(editorPrefListener);
		}
		final PreferenceAccess access = EPreferences.getInstancePrefs();
		this.handleTemporaryProblems = access.getPreferenceValue(REditorBuild.PROBLEMCHECKING_ENABLED_PREF);
	}
	
	
	@Override
	public void dispose() {
		{	final var editorPrefListener= this.editorPrefListener;
			if (editorPrefListener != null) {
				this.editorPrefListener= null;
				PreferencesUtil.getSettingsChangeNotifier().removeChangeListener(editorPrefListener);
			}
		}
	}
	
	private void updateEditorPrefs() {
		final PreferenceAccess access = EPreferences.getInstancePrefs();
		final boolean newHandleTemporaryProblems = access.getPreferenceValue(REditorBuild.PROBLEMCHECKING_ENABLED_PREF);
		if (this.handleTemporaryProblems != newHandleTemporaryProblems) {
			this.handleTemporaryProblems = newHandleTemporaryProblems;
			RModel.getRModelManager().refresh(Ltk.EDITOR_CONTEXT);
		}
	}
	
	@Override
	protected IAnnotationModel createAnnotationModel(final IFile file) {
		return new ThisAnnotationModel(file);
	}
	
}
