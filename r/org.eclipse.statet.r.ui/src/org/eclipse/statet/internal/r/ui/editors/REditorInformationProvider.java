/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHover.MODE_FOCUS;
import static org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHover.MODE_TOOLTIP;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Region;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.rhelp.RHelpHover;
import org.eclipse.statet.ltk.ui.sourceediting.EditorInformationProvider;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.r.core.source.RHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class REditorInformationProvider extends EditorInformationProvider {
	
	
	private @Nullable RHeuristicTokenScanner scanner;
	
	
	public REditorInformationProvider(final RSourceEditor editor) {
		super(editor, ImCollections.newList(
				new RHelpHover(MODE_TOOLTIP | MODE_FOCUS) ));
	}
	
	
	@Override
	public RSourceEditor getEditor() {
		return (RSourceEditor)super.getEditor();
	}
	
	private RHeuristicTokenScanner getScanner() {
		var scanner= this.scanner;
		if (scanner == null) {
			scanner= RHeuristicTokenScanner.create(getEditor().getDocumentContentInfo());
			this.scanner= scanner;
		}
		return scanner;
	}
	
	@Override
	public IRegion getSubject(final ITextViewer textViewer, final int offset) {
		try {
			final var scanner= getScanner();
			final IDocument document= nonNullAssert(textViewer.getDocument());
			scanner.configure(document);
			final IRegion word= scanner.findRWord(offset, false, true);
			if (word != null) {
				final ITypedRegion partition= scanner.getPartition(word.getOffset());
				if (RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT.matches(partition.getType())
						|| partition.getType() == RDocumentConstants.R_STRING_CONTENT_TYPE
						|| partition.getType() == RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE) {
					return word;
				}
			}
		}
		catch (final Exception e) {
		}
		return new Region(offset, 0);
	}
	
	@Override
	protected @Nullable AssistInvocationContext createContext(final IRegion region,
			final String contentType,
			final IProgressMonitor monitor) {
		final var scanner= getScanner();
		final RAssistInvocationContext context= new RAssistInvocationContext(getEditor(),
				region, contentType, scanner, monitor );
		if (context.getAstInfo() == null) {
			return null;
		}
		return context;
	}
	
}
