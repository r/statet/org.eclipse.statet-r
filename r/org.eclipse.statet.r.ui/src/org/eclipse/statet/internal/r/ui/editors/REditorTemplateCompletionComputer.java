/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.templates.TemplateContextType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;
import org.eclipse.statet.ltk.ui.templates.SourceEditorTemplateContext;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.editors.templates.REditorTemplateContext;
import org.eclipse.statet.r.ui.editors.templates.REditorTemplateContextType;


@NonNullByDefault
public class REditorTemplateCompletionComputer extends TemplateCompletionComputer {
	
	
	public REditorTemplateCompletionComputer() {
		super(RUIPlugin.getInstance().getREditorTemplateStore(),
				RUIPlugin.getInstance().getREditorTemplateContextRegistry() );
	}
	
	
	@Override
	protected @Nullable String extractPrefix(final AssistInvocationContext context) {
		try {
			final IDocument document= context.getSourceViewer().getDocument();
			final int end= context.getInvocationOffset();
			final int start= Math.max(end - 50, 0);
			final String text= document.get(start, end-start);
			int i= text.length() - 1;
			while (i >= 0) {
				final char c= text.charAt(i);
				if (Character.isLetterOrDigit(c) || c == '.' || c == '_') {
					i--;
					continue;
				}
				if (c == '\\' || c == '@') {
					return text.substring(i);
				}
				return text.substring(i + 1);
			}
			return ""; //$NON-NLS-1$
		}
		catch (final BadLocationException e) {
			return null;
		}
	}
	
	@Override
	protected @Nullable TemplateContextType getContextType(
			final AssistInvocationContext context, final TextRegion region) {
		try {
			final SourceEditor editor= context.getEditor();
			final AbstractDocument document= (AbstractDocument) context.getSourceViewer().getDocument();
			final ITypedRegion partition= document.getPartition(
					editor.getDocumentContentInfo().getPartitioning(), region.getStartOffset(), true );
			if (partition.getType() == RDocumentConstants.R_ROXYGEN_CONTENT_TYPE) {
				return getTypeRegistry().getContextType(REditorTemplateContextType.ROXYGEN_CONTEXTTYPE_ID);
			}
			else {
				return getTypeRegistry().getContextType(REditorTemplateContextType.RCODE_CONTEXTTYPE_ID);
			}
		}
		catch (final BadPartitioningException | BadLocationException e) {}
		return null;
	}
	
	@Override
	protected @Nullable SourceEditorTemplateContext createTemplateContext(
			final AssistInvocationContext context, final TextRegion region,
			final int flags) {
		final TemplateContextType contextType= getContextType(context, region);
		if (contextType != null) {
			return new REditorTemplateContext(contextType, context.getDocument(), region,
					context.getEditor(), flags );
		}
		return null;
	}
	
}
