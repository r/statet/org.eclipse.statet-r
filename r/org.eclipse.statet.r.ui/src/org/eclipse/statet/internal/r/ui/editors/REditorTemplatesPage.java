/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.ui.texteditor.templates.TemplatesView;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;
import org.eclipse.statet.ltk.ui.templates.config.AbstractEditorTemplatesPage;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RPartitionNodeType;
import org.eclipse.statet.r.ui.editors.templates.REditorTemplateContextType;
import org.eclipse.statet.r.ui.sourceediting.RTemplateSourceViewerConfigurator;


/**
 * Page for {@link REditor} / {@link TemplatesView}
 */
@NonNullByDefault
public class REditorTemplatesPage extends AbstractEditorTemplatesPage {
	
	
	private @Nullable SourceEditorViewerConfigurator rPreviewConfigurator;
	
	
	public REditorTemplatesPage(final REditor editor) {
		super(RUIPlugin.getInstance().getREditorTemplateStore(), editor, editor);
	}
	
	
	@Override
	protected String getPreferencePageId() {
		return "org.eclipse.statet.r.preferencePages.REditorTemplates"; //$NON-NLS-1$
	}
	
	@Override
	protected IPreferenceStore getTemplatePreferenceStore() {
		return RUIPlugin.getInstance().getPreferenceStore();
	}
	
	
	@Override
	protected String[] getContextTypeIds(final IDocument document, final int offset) {
		try {
			final String partitionType= TextUtilities.getContentType(document,
					getSourceEditor().getDocumentContentInfo().getPartitioning(), offset, true );
			if (partitionType == RDocumentConstants.R_ROXYGEN_CONTENT_TYPE) {
				return new String[] { REditorTemplateContextType.ROXYGEN_CONTEXTTYPE_ID };
			}
		}
		catch (final BadLocationException e) {}
		return new String[] { REditorTemplateContextType.RCODE_CONTEXTTYPE_ID };
	}
	
	@Override
	protected @Nullable TemplateCompletionComputer getComputer(
			final AssistInvocationContext context, final Template template) {
		return (TemplateCompletionComputer)RUIPlugin.getInstance().getREditorContentAssistRegistry()
				.getComputer("org.eclipse.statet.r.contentAssistComputers.RTemplateCompletion"); //$NON-NLS-1$
	}
	
	
	@Override
	protected SourceEditorViewerConfigurator getTemplatePreviewConfig(final Template template, final TemplateVariableProcessor templateProcessor) {
		SourceEditorViewerConfigurator configurator= this.rPreviewConfigurator;
		if (configurator == null) {
			configurator= new RTemplateSourceViewerConfigurator(
					RCore.WORKBENCH_ACCESS,
					templateProcessor );
			this.rPreviewConfigurator= configurator;
		}
		return configurator;
	}
	
	@Override
	protected SourceEditorViewerConfigurator getTemplateEditConfig(final Template template, final TemplateVariableProcessor templateProcessor) {
		return new RTemplateSourceViewerConfigurator(
				RCore.WORKBENCH_ACCESS,
				templateProcessor );
	}
	
	@Override
	protected void configureDocument(final AbstractDocument document, final TemplateContextType contextType, final SourceEditorViewerConfigurator configurator) {
		final String partitioning= configurator.getDocumentContentInfo().getPartitioning();
		final TreePartitioner partitioner = (TreePartitioner) document.getDocumentPartitioner(partitioning);
		if (contextType.getId().equals(REditorTemplateContextType.ROXYGEN_CONTEXTTYPE_ID)) {
			partitioner.setStartType(RPartitionNodeType.ROXYGEN);
		}
		else {
			partitioner.setStartType(RPartitionNodeType.DEFAULT_ROOT);
		}
		partitioner.disconnect();
		partitioner.connect(document);
		document.setDocumentPartitioner(partitioning, partitioner);
	}
	
}
