/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.assist.SimpleCompletionProposal;
import org.eclipse.statet.r.core.source.RHeuristicTokenScanner;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class RSimpleCompletionProposal extends SimpleCompletionProposal<RAssistInvocationContext> {
	
	
	public RSimpleCompletionProposal(final ProposalParameters<RAssistInvocationContext> parameters,
			final String replacementString) {
		super(parameters, replacementString);
	}
	
	
	@Override
	protected int computeReplacementLength(final int replacementOffset, final Point selection,
			final int caretOffset, final boolean overwrite) {
		// keep in synch with RElementCompletionProposal
		final int end= Math.max(caretOffset, selection.x + selection.y);
		if (overwrite) {
			final RAssistInvocationContext context= getInvocationContext();
			final RHeuristicTokenScanner scanner= context.getRHeuristicTokenScanner();
			scanner.configure(context.getDocument());
			final IRegion word= scanner.findRWord(end, false, true);
			if (word != null) {
				return (word.getOffset() + word.getLength() - replacementOffset);
			}
		}
		return (end - replacementOffset);
	}
	
	
	@Override
	protected @Nullable String getValidationPrefix(final int offset) throws BadLocationException {
		// keep in synch with RElementCompletionProposal
		final int startOffset= Math.max(getReplacementOffset(), 0);
		if (offset >= startOffset) {
			final RAssistInvocationContext context= getInvocationContext();
			final IDocument document= context.getDocument();
			int nameStartOffset= startOffset;
			int nameEndOffset= offset;
			if (nameEndOffset > nameStartOffset && document.getChar(nameStartOffset) == '`') {
				nameStartOffset++;
			}
			if (nameEndOffset > nameStartOffset && document.getChar(nameEndOffset - 1) == '`') {
				nameEndOffset--;
			}
			if (nameEndOffset >= nameStartOffset) {
				return context.getIdentifierSegmentName(
						document.get(startOffset, offset - startOffset) );
			}
		}
		return null;
	}
	
}
