/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.actions.ToggleBooleanPreferenceHandler;

import org.eclipse.statet.r.ui.editors.REditorBuild;


/**
 * Toggles activation of report problems when typing in R editors.
 */
@NonNullByDefault
public class RToggleLiveReportProblemsHandler extends ToggleBooleanPreferenceHandler {
	
	
	public RToggleLiveReportProblemsHandler() {
		super(	REditorBuild.PROBLEMCHECKING_ENABLED_PREF,
				"org.eclipse.statet.ltk.commands.ToggleLiveReportProblems"); //$NON-NLS-1$
	}
	
}
