/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.resize.core.DimPositionResizeCommand;


@NonNullByDefault
public class DimPositionResizeCommandHandler extends AbstractLayerCommandHandler<DimPositionResizeCommand> {
	
	
	private final RDataLayer dataLayer;
	
	
	public DimPositionResizeCommandHandler(final RDataLayer dataLayer) {
		this.dataLayer= dataLayer;
	}
	
	
	@Override
	public Class<DimPositionResizeCommand> getCommandClass() {
		return DimPositionResizeCommand.class;
	}
	
	
	@Override
	protected boolean doCommand(final DimPositionResizeCommand command) {
		if (command.getOrientation() == HORIZONTAL) {
			this.dataLayer.setColumnWidth(command.getPosition(), command.getNewSize());
		}
		return true;
	}
	
}
