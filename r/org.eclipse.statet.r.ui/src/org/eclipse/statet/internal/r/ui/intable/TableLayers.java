/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.command.LayerCommandUtil;
import org.eclipse.statet.ecommons.waltable.coordinate.ColumnPositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportLayer;


public class TableLayers {
	
	
	public RDataLayer dataLayer;
	
	public SelectionLayer selectionLayer;
	
	public ViewportLayer viewportLayer;
	
	public Layer topBodyLayer;
	
	public DataLayer dataColumnHeaderLayer;
	public Layer topColumnHeaderLayer;
	public DataLayer dataRowHeaderLayer;
	public Layer topRowHeaderLayer;
	
	public NatTable table;
	
	
	public void setAnchor(long columnPosition, final long rowPosition,
			final boolean moveIntoViewport) {
		if (columnPosition < 0) {
			final ColumnPositionCoordinate colCoordinate= LayerCommandUtil.convertColumnPositionToTargetContext(
					new ColumnPositionCoordinate(this.viewportLayer, 0), this.selectionLayer);
			if (colCoordinate != null) {
				columnPosition= colCoordinate.getColumnPosition();
			}
		}
		if (columnPosition < 0 || columnPosition > this.selectionLayer.getColumnCount()
				|| rowPosition < 0 || rowPosition > this.selectionLayer.getRowCount() ) {
			return;
		}
		this.selectionLayer.setSelectionAnchor(columnPosition, rowPosition, moveIntoViewport);
	}
	
}
