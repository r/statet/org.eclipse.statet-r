/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable.css.dom;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.css.dom.VirtualStylableElement;


@NonNullByDefault
@SuppressWarnings("restriction")
public class TableCellElement extends VirtualStylableElement<TableRegionElement> {
	
	
	private final ImList<String> labels;
	
	
	public TableCellElement(final TableRegionElement parent,
			final @Nullable String label,
			final CSSEngine engine) {
		super(nonNullAssert(parent), "Cell", label, engine); //$NON-NLS-1$
		
		this.labels= (label != null) ?
				ImCollections.newIdentityList(label) :
				ImCollections.emptyIdentityList();
	}
	
	
	public ImList<String> getLabels() {
		return this.labels;
	}
	
	
}
