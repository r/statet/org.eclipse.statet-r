/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable.css.dom;

import java.util.ArrayList;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.css.dom.VirtualCompositeStylableElement;
import org.eclipse.statet.ecommons.waltable.grid.AlternatingRowLabelContributor;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.style.SelectionStyleLabels;

import org.eclipse.statet.internal.r.ui.intable.DataTable;


@NonNullByDefault
@SuppressWarnings("restriction")
public class TableElement extends VirtualCompositeStylableElement<DataTable> {
	
	
	private final static @Nullable String NO_CLASS= null;
	
	
	public TableElement(final DataTable dataTable, final CSSEngine engine) {
		super(dataTable, engine);
	}
	
	@Override
	protected String computeLocalName() {
		return "RDataTable"; //$NON-NLS-1$
	}
	
	@Override
	protected ImList<TableRegionElement> createChildren(final DataTable control) {
		final var list= new ArrayList<TableRegionElement>();
		list.add(new TableRegionElement(this, TableRegionElement.BODY_NAME, null,
				ImCollections.newList(
						NO_CLASS,
						AlternatingRowLabelContributor.EVEN_ROW_CONFIG_TYPE,
						AlternatingRowLabelContributor.ODD_ROW_CONFIG_TYPE,
						SelectionStyleLabels.SELECTION_ANCHOR_STYLE ),
				ImCollections.newList("selected"),
				this.engine ));
		list.add(new TableRegionElement(this, TableRegionElement.HEADER_NAME, GridLabels.CORNER,
				ImCollections.newList(NO_CLASS),
				ImCollections.newList(),
				this.engine ));
		list.add(new TableRegionElement(this, TableRegionElement.HEADER_NAME, GridLabels.COLUMN_HEADER,
				ImCollections.newList(NO_CLASS),
				ImCollections.newList("selected"),
				this.engine ));
		list.add(new TableRegionElement(this, TableRegionElement.HEADER_NAME, GridLabels.ROW_HEADER,
				ImCollections.newList(NO_CLASS),
				ImCollections.newList("selected"),
//				ImCollections.newList("selected", "fully-selected"),
				this.engine ));
		list.add(new TableRegionElement(this, TableRegionElement.HEADER_NAME, GridLabels.COLUMN_HEADER_LABEL,
				ImCollections.newList(NO_CLASS),
				ImCollections.newList(),
				this.engine ));
		list.add(new TableRegionElement(this, TableRegionElement.HEADER_NAME, GridLabels.ROW_HEADER_LABEL,
				ImCollections.newList(NO_CLASS),
				ImCollections.newList(),
				this.engine ));
		return ImCollections.toList(list);
	}
	
	
}
