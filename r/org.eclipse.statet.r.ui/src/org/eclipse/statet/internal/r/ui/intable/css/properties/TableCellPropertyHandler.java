/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable.css.properties;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.swt.graphics.Color;

import org.w3c.dom.css.CSSValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.style.SelectionStyleLabels;

import org.eclipse.statet.internal.r.ui.intable.css.dom.TableCellElement;


@NonNullByDefault
@SuppressWarnings("restriction")
public class TableCellPropertyHandler extends AbstractDataTablePropertyHandler {
	
	
	public TableCellPropertyHandler() {
	}
	
	
	@Override
	public @Nullable String retrieveCSSProperty(final Object element,
			final String property, final @Nullable String pseudo,
			final CSSEngine engine) throws Exception {
		if (!(element instanceof TableCellElement)) {
			return null;
		}
		final var cellElement= (TableCellElement)element;
		final var displayMode= getDisplayMode(cellElement, pseudo);
		final var label= getOverrideLabel(cellElement, pseudo);
		
		if (displayMode != null) {
			switch (property) {
			case "background-color":
				return receiveCellStyleAttribute(cellElement, displayMode, label,
						CellStyling.BACKGROUND_COLOR, Color.class,
						engine );
			case "color":
				return receiveCellStyleAttribute(cellElement, displayMode, label,
						CellStyling.FOREGROUND_COLOR, Color.class,
						engine );
			default:
				break;
			}
		}
		return null;
	}
	
	@Override
	public boolean applyCSSProperty(final Object element,
			final String property, final CSSValue value, final @Nullable String pseudo,
			final CSSEngine engine) throws Exception {
		if (!(element instanceof TableCellElement)) {
			return false;
		}
		if (DEBUG) {
			print(element, pseudo, property, value);
		}
		final var cellElement= (TableCellElement)element;
		final var displayMode= getDisplayMode(cellElement, pseudo);
		final var label= getOverrideLabel(cellElement, pseudo);
		
		if (displayMode != null) {
			switch (property) {
			case "background-color":
				applyCellStyleAttribute(cellElement, displayMode, label,
						CellStyling.BACKGROUND_COLOR, Color.class, value,
						engine );
				return true;
			case "color":
				applyCellStyleAttribute(cellElement, displayMode, label,
						CellStyling.FOREGROUND_COLOR, Color.class, value,
						engine );
				return true;
			case "border-color":
				applyCellStyleBorderAttribute(cellElement, displayMode, label,
						CellStyling.BORDER_STYLE, Color.class, value,
						engine );
				return true;
			default:
				break;
			}
		}
		return true;
	}
	
	
	private static @Nullable DisplayMode getDisplayMode(final TableCellElement cellElement,
			final @Nullable String pseudo) {
		if (pseudo == null) {
			return DisplayMode.NORMAL;
		}
		switch (pseudo) {
		case "selected":
		case "fully-selected":
			return DisplayMode.SELECTED;
		default:
			return null;
		}
	}
	
	private static @Nullable String getOverrideLabel(final TableCellElement cellElement,
			final @Nullable String pseudo) {
		if (pseudo == null) {
			return null;
		}
		if (pseudo.equals("fully-selected")) {
			final String cssClass= cellElement.getParentNode().getCSSClass();
			if (cssClass != null) {
				switch (cssClass) {
				case GridLabels.COLUMN_HEADER_LABEL:
					return SelectionStyleLabels.COLUMN_FULLY_SELECTED_STYLE;
				case GridLabels.ROW_HEADER_LABEL:
					return SelectionStyleLabels.ROW_FULLY_SELECTED_STYLE;
				}
			}
		}
		return null;
	}
	
	
	private static <T> @Nullable String receiveCellStyleAttribute(final TableCellElement cellElement,
			final DisplayMode displayMode, @Nullable String label,
			final ConfigAttribute<T> attribute, final Class<T> valueType,
			final CSSEngine engine) throws Exception {
		final var regionElement= cellElement.getParentNode();
		final var dataTable= regionElement.getParentNode().getNativeWidget();
		if (label == null) {
			label= cellElement.getCSSClass();
		}
		if (label == null) {
			label= regionElement.getCSSClass();
		}
		final Style style= dataTable.getConfigRegistry().getSpecificAttribute(
				CellConfigAttributes.CELL_STYLE, displayMode, label );
		if (style != null) {
			final var value= style.getAttributeValue(attribute);
			return engine.convert(value, valueType, null);
		}
		return null;
	}
	
	private static <T> void applyCellStyleAttribute(final TableCellElement cellElement,
			final DisplayMode displayMode, @Nullable String label,
			final ConfigAttribute<T> attribute, final Class<T> valueType, final CSSValue cssValue,
			final CSSEngine engine) throws Exception {
		final var regionElement= cellElement.getParentNode();
		final var dataTable= regionElement.getParentNode().getNativeWidget();
		if (label == null) {
			label= cellElement.getCSSClass();
		}
		if (label == null) {
			label= regionElement.getCSSClass();
		}
		final Style style= dataTable.getConfigRegistry().getSpecificAttribute(
				CellConfigAttributes.CELL_STYLE, displayMode, label );
		final @Nullable T oldValue;
		if (style != null
				&& (oldValue= style.getAttributeValue(attribute)) != null) {
			@SuppressWarnings("unchecked")
			final var value= (T)engine.convert(cssValue, valueType, dataTable.getDisplay());
			if (value != null && !value.equals(oldValue)) {
				style.setAttributeValue(attribute, value);
			}
		}
	}
	
	private static <T> void applyCellStyleBorderAttribute(final TableCellElement cellElement,
			final DisplayMode displayMode, @Nullable String label,
			final ConfigAttribute<BorderStyle> attribute, final Class<T> valueType, final CSSValue cssValue,
			final CSSEngine engine) throws Exception {
		final var regionElement= cellElement.getParentNode();
		final var dataTable= regionElement.getParentNode().getNativeWidget();
		if (label == null) {
			label= cellElement.getCSSClass();
		}
		if (label == null) {
			label= regionElement.getCSSClass();
		}
		final Style style= dataTable.getConfigRegistry().getSpecificAttribute(
				CellConfigAttributes.CELL_STYLE, displayMode, label );
		final @Nullable BorderStyle oldBorderStyle;
		if (style != null
				&& (oldBorderStyle= style.getAttributeValue(CellStyling.BORDER_STYLE)) != null) {
			final var value= toSWT(cssValue, valueType, engine, dataTable);
			BorderStyle borderStyle= null;
			if (valueType == Color.class) {
				if (value != null && !value.equals(oldBorderStyle.getColor())) {
					borderStyle= new BorderStyle(oldBorderStyle.getThickness(), (Color)value,
									oldBorderStyle.getLineStyle(), oldBorderStyle.getOffset() );
				}
			}
			if (borderStyle != null) {
				style.setAttributeValue(CellStyling.BORDER_STYLE, borderStyle);
			}
		}
	}
	
}
