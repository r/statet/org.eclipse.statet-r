/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable.css.properties;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.swt.graphics.Color;

import org.w3c.dom.css.CSSValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.intable.css.dom.TableElement;


@NonNullByDefault
@SuppressWarnings("restriction")
public class TablePropertyHandler extends AbstractDataTablePropertyHandler {
	
	
	public TablePropertyHandler() {
	}
	
	
	@Override
	public @Nullable String retrieveCSSProperty(final Object element,
			final String property, final @Nullable String pseudo,
			final CSSEngine engine) throws Exception {
		if (!(element instanceof TableElement)) {
			return null;
		}
		final var tableElement= (TableElement)element;
		
		switch (property) {
		case "background-color":
			if (pseudo == null) {
				final var color= tableElement.getNativeWidget().getBackground();
				return engine.convert(color, Color.class, null);
			}
			return null;
		default:
			break;
		}
		return null;
	}
	
	@Override
	public boolean applyCSSProperty(final Object element,
			final String property, final CSSValue value, final @Nullable String pseudo,
			final CSSEngine engine) throws Exception {
		if (!(element instanceof TableElement)) {
			return false;
		}
		if (DEBUG) {
			print(element, pseudo, property, value);
		}
		final var tableElement= (TableElement)element;
		final var dataTable= tableElement.getNativeWidget();
		
		switch (property) {
		case "background-color":
			if (pseudo == null) {
				final var color= (Color)engine.convert(value, Color.class, dataTable.getDisplay());
				dataTable.setBackground(color);
			}
			return true;
		default:
			break;
		}
		return true;
	}
	
	
}
