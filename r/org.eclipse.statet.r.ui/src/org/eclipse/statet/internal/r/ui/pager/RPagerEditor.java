/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pager;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.MultiPageEditorPart;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.util.ImmutableDocument;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.internal.r.ui.pager.RPagerEditorInput.TextFile;
import org.eclipse.statet.ltk.core.input.BasicSourceFragment;
import org.eclipse.statet.ltk.ui.input.BasicSourceFragmentEditorInput;
import org.eclipse.statet.r.ui.RUI;


@NonNullByDefault
public class RPagerEditor extends MultiPageEditorPart {
	
	
	public RPagerEditor() {
	}
	
	
	@Override
	protected Image getDefaultImage() {
		return RUI.getImage(RUIPlugin.PAGER_VIEW_IMAGE_ID);
	}
	
	
	@Override
	protected void setInputWithNotify(final IEditorInput input) {
		super.setInput(input);
		
		setPartName(input.getName());
		
		if (getContainer() != null) {
			updatePages();
		}
		
		firePropertyChange(IEditorPart.PROP_INPUT);
	}
	
	@Override
	protected void setInput(final IEditorInput input) {
		setInputWithNotify(input);
	}
	
	
	protected void updatePages() {
		while (true) {
			final int pageCount= getPageCount();
			if (pageCount == 0) {
				break;
			}
			removePage(pageCount - 1);
		}
		
		final IEditorInput editorInput= getEditorInput();
		if (editorInput instanceof RPagerEditorInput) {
			final var pagerInput= (RPagerEditorInput)editorInput;
			final ImList<TextFile> files= pagerInput.getFiles();
			int id= 0;
			for (final var textFile : files) {
				try {
					final var sourceFragment= new BasicSourceFragment("RPagerFile#" + id++,
							textFile.getName(), textFile.getName(),
							new ImmutableDocument(textFile.getContent(), 0) );
					final TextFileEditorPage page= new TextFileEditorPage();
					addPage(page, new BasicSourceFragmentEditorInput(sourceFragment));
				}
				catch (final Exception e) {
					RUIPlugin.logError(String.format("An error occurred when creating R Pager editor page for '%1$s'.",
							textFile.getName() ),
							e );
				}
			}
		}
	}
	
	@Override
	protected void createPages() {
		updatePages();
	}
	
	@Override
	public void addPage(final int index, final IEditorPart editor, final IEditorInput input)
			throws PartInitException {
		super.addPage(index, editor, input);
		setPageText(index, input.getName());
	}
	
	
	@Override
	public boolean isSaveAsAllowed() {
		final int pageIndex= getActivePage();
		if (pageIndex >= 0) {
			final IEditorPart editor= getEditor(pageIndex);
			if (editor != null) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void doSave(final IProgressMonitor monitor) {
	}
	
	@Override
	public void doSaveAs() {
		final int pageIndex= getActivePage();
		if (pageIndex >= 0) {
			final IEditorPart editor= getEditor(pageIndex);
			if (editor != null) {
				editor.doSaveAs();
				return;
			}
		}
	}
	
}
