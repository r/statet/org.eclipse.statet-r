/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pager;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.source.AnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.texteditor.AbstractDocumentProvider;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.util.ImmutableDocument;

import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.ui.input.SourceFragmentEditorInput;


@NonNullByDefault
public class TextFileDocumentProvider extends AbstractDocumentProvider {
	
	
	public class TextFileElementInfo extends ElementInfo {
		
		private final ImList<ITypedRegion> formatRegions;
		
		public TextFileElementInfo(final IDocument document, final ImList<ITypedRegion> formatRegions,
				final IAnnotationModel model) {
			super(document, model);
			this.formatRegions= formatRegions;
		}
		
	}
	
	
	final TextFileParser parser= new TextFileParser();
	
	
	public TextFileDocumentProvider() {
	}
	
	
	@Override
	protected @Nullable ElementInfo createElementInfo(final Object element) throws CoreException {
		AbstractDocument document= null;
		if (document == null) {
			document= createDocument(element);
		}
		if (document != null) {
			final ImList<ITypedRegion> formatRegions;
			final String orgText= document.get();
			synchronized (this.parser) {
				this.parser.parse(orgText);
				if (this.parser.getCleanText() != orgText) {
					document= new ImmutableDocument(this.parser.getCleanText(), document.getModificationStamp());
				}
				formatRegions= this.parser.getFormatRegions();
			}
			
			setupDocument(document);
			final ElementInfo info= new TextFileElementInfo(document, formatRegions,
					createAnnotationModel(element) );
			return info;
		}
		return null;
	}
	
	@Override
	protected @Nullable AbstractDocument createDocument(final Object element) throws CoreException {
		if (element instanceof SourceFragmentEditorInput) {
			final SourceFragment fragment= ((SourceFragmentEditorInput)element).getSourceFragment();
			return fragment.getDocument();
		}
		return null;
	}
	
	protected void setupDocument(final AbstractDocument document) {
		// we use the default partitioner
	}
	
	@Override
	protected IAnnotationModel createAnnotationModel(final Object element) throws CoreException {
		return new AnnotationModel();
	}
	
	@Override
	protected @Nullable IRunnableContext getOperationRunner(final @Nullable IProgressMonitor monitor) {
		return null;
	}
	
	@Override
	protected void doSaveDocument(final @Nullable IProgressMonitor monitor, final Object element,
			final IDocument document, final boolean overwrite) throws CoreException {
	}
	
	public ImList<ITypedRegion> getTextFormatRegions(final @Nullable Object element) {
		if (element instanceof SourceFragmentEditorInput) {
			final var elementInfo= getElementInfo(element);
			if (elementInfo instanceof TextFileElementInfo) {
				return ((TextFileElementInfo)elementInfo).formatRegions;
			}
		}
		return ImCollections.emptyList();
	}
	
}
