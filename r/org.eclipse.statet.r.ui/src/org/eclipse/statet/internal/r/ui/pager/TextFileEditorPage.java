/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pager;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.source.CompositeRuler;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.texteditor.StatusTextEditor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class TextFileEditorPage extends StatusTextEditor {
	
	
	private static final TextFileDocumentProvider DOCUMENT_PROVIDER= new TextFileDocumentProvider();
	
	
	private final TextFileSourceViewerConfiguration sourceViewerConfiguration;
	
	
	public TextFileEditorPage() {
		setDocumentProvider(TextFileEditorPage.DOCUMENT_PROVIDER);
		this.sourceViewerConfiguration= new TextFileSourceViewerConfiguration();
		setSourceViewerConfiguration(this.sourceViewerConfiguration);
		setRulerContextMenuId(""); //$NON-NLS-1$
	}
	
	
	@Override
	@SuppressWarnings("null")
	public TextFileDocumentProvider getDocumentProvider() {
		return (TextFileDocumentProvider)super.getDocumentProvider();
	}
	
	
	@Override
	protected void doSetInput(final @Nullable IEditorInput input) throws CoreException {
		final var documentProvider= getDocumentProvider();
		documentProvider.connect(input);
		try {
			this.sourceViewerConfiguration.setStyleRegions(
					TextFileEditorPage.DOCUMENT_PROVIDER.getTextFormatRegions(input) );
			super.doSetInput(input);
		}
		finally {
			documentProvider.disconnect(input);
		}
	}
	
	
	@Override
	public boolean isDirty() {
		return false;
	}
	
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	@Override
	protected void performSaveAs(final @Nullable IProgressMonitor progressMonitor) {
		// for save as: implement and change isSaveAsAllowed to true
	}
	
	
	@Override
	protected IVerticalRuler createVerticalRuler() {
		return new CompositeRuler();
	}
	
}
