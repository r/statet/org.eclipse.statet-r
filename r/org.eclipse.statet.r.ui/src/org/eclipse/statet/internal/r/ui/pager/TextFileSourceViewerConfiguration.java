/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pager;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.ui.presentation.FixTokenScanner;
import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager;


@NonNullByDefault
public class TextFileSourceViewerConfiguration extends SourceViewerConfiguration {
	
	
	private static final TextStyleManager<TextAttribute> TEXT_STYLE_MANAGER= new TextFilesTextStyleManager();
	
	
	private final TextStyleManager<TextAttribute> textStyles;
	
	private @Nullable IPresentationReconciler presentationReconciler;
	private final FixTokenScanner scanner;
	
	
	public TextFileSourceViewerConfiguration() {
		this.textStyles= TEXT_STYLE_MANAGER;
		
		this.scanner= new FixTokenScanner(this.textStyles, TextFileParser.DEFAULT_FORMAT_TYPE);
	}
	
	
	public void setStyleRegions(final ImList<ITypedRegion> regions) {
		this.scanner.setStyleRegions(regions);
	}
	
	
	@Override
	public IPresentationReconciler getPresentationReconciler(final ISourceViewer sourceViewer) {
		var presentationReconciler= this.presentationReconciler;
		if (presentationReconciler == null) {
			presentationReconciler= createPresentationReconciler();
			this.presentationReconciler= presentationReconciler;
		}
		return presentationReconciler;
	}
	
	protected IPresentationReconciler createPresentationReconciler() {
		final PresentationReconciler reconciler= new PresentationReconciler();
		reconciler.setDocumentPartitioning(getConfiguredDocumentPartitioning(null));
		
		final DefaultDamagerRepairer dr= new DefaultDamagerRepairer(this.scanner);
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);
		
		return reconciler;
	}
	
}
