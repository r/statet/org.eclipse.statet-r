/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pager;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.swt.SWT;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.presentation.BasicTextStyleManager;


@NonNullByDefault
public class TextFilesTextStyleManager extends BasicTextStyleManager<TextAttribute> {
	
	
	public TextFilesTextStyleManager() {
	}
	
	
	@Override
	protected TextAttribute createStyleData(final String key) {
		int style= 0;
		if (key == TextFileParser.UNDERLINE_FORMAT_TYPE) {
			style= SWT.BOLD;
		}
		return new TextAttribute(null, null, style);
	}
	
}
