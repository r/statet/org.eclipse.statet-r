/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.ViewerCell;

import org.eclipse.statet.r.core.pkgmanager.RRepo;


public class RRepoLabelProvider extends StyledCellLabelProvider {
	
	
	@Override
	public void update(final ViewerCell cell) {
		final RRepo repo= (RRepo) cell.getElement();
		
		update(cell, repo);
		
		super.update(cell);
	}
	
	protected void update(final ViewerCell cell, final RRepo repo) {
		final StyledString sb= new StyledString(repo.getName());
		sb.append(" - ", StyledString.QUALIFIER_STYLER); //$NON-NLS-1$
		sb.append(repo.getURL(), StyledString.QUALIFIER_STYLER);
		
		cell.setText(sb.getString());
		cell.setStyleRanges(sb.getStyleRanges());
	}
	
}
