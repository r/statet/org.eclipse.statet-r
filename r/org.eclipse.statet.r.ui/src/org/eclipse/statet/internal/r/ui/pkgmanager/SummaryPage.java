/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils.TableComposite;

import org.eclipse.statet.r.core.pkgmanager.IRPkgManager;
import org.eclipse.statet.r.core.pkgmanager.RPkgAction;
import org.eclipse.statet.r.core.pkgmanager.RPkgResolver;
import org.eclipse.statet.r.core.pkgmanager.RRepo;
import org.eclipse.statet.r.ui.REnvLabelProvider;


abstract class SummaryPage extends WizardPage {
	
	
	private final IRPkgManager.Ext pkgManager;
	private final RPkgResolver resolver;
	
	private TableComposite table;
	
	
	public SummaryPage(final IRPkgManager.Ext pkgManager, final RPkgResolver resolver,
			final String title) {
		super("InstallPkgsSummaryPage"); //$NON-NLS-1$
		this.pkgManager= pkgManager;
		this.resolver= resolver;
		
		setTitle(title);
		setDescription("Summary packages to install/update.");
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		initializeDialogUnits(parent);
		
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newContentGrid(1));
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			label.setText("Packages to install:");
		}
		
		this.table= new TableComposite(composite, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION
				| SWT.VIRTUAL );
		this.table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		this.table.viewer.setContentProvider(ArrayContentProvider.getInstance());
		{	final TableViewerColumn column= this.table.addColumn("Name", SWT.LEFT,
					new ColumnWeightData(60, true) );
			column.setLabelProvider(new CellLabelProvider() {
				@Override
				public void update(final ViewerCell cell) {
					final RPkgAction action= (RPkgAction) cell.getElement();
					cell.setText(action.getPkg().getName());
				}
			});
		}
		{	final TableViewerColumn column= this.table.addColumn("", SWT.LEFT,
					new ColumnPixelData(LayoutUtils.hintColWidth(this.table.table, 10), true, true) );
			column.setLabelProvider(new CellLabelProvider() {
				@Override
				public void update(final ViewerCell cell) {
					final RPkgAction action= (RPkgAction) cell.getElement();
					cell.setText(SummaryPage.this.resolver.getReason(action.getPkg()));
				}
			});
		}
		{	final TableViewerColumn column= this.table.addColumn("Version", SWT.LEFT,
					new ColumnPixelData(LayoutUtils.hintColWidth(this.table.table, 10), true, true) );
			column.setLabelProvider(new CellLabelProvider() {
				@Override
				public void update(final ViewerCell cell) {
					final RPkgAction action= (RPkgAction) cell.getElement();
					cell.setText(action.getPkg().getVersion().toString());
				}
			});
		}
		{	final TableViewerColumn column= this.table.addColumn("From", SWT.LEFT,
					new ColumnWeightData(40, true) );
			column.setLabelProvider(new CellLabelProvider() {
				@Override
				public void update(final ViewerCell cell) {
					final RPkgAction action= (RPkgAction) cell.getElement();
					final RRepo repo= SummaryPage.this.pkgManager.getRepo(action.getRepoId());
					if (repo.getPkgType() != null) {
						final StringBuilder sb= new StringBuilder(repo.getName());
						sb.append(" (");
						sb.append(repo.getPkgType().getLabel());
						sb.append(")");
						cell.setText(sb.toString());
					}
					else {
						cell.setText(repo.getName());
					}
				}
			});
		}
		{	final TableViewerColumn column= this.table.addColumn("To", SWT.LEFT,
					new ColumnWeightData(40, true) );
			column.setLabelProvider(new CellLabelProvider() {
				@Override
				public void update(final ViewerCell cell) {
					cell.setText(REnvLabelProvider.getSafeLabel(
							((RPkgAction) cell.getElement()).getLibLocation() ));
				}
			});
		}
		this.table.table.setHeaderVisible(true);
		this.table.table.setLinesVisible(true);
		
		Dialog.applyDialogFont(composite);
		setControl(composite);
	}
	
	@Override
	public void setVisible(final boolean visible) {
		super.setVisible(visible);
		if (visible) {
			updateInput();
		}
	}
	
	public abstract void updateInput();
	
	protected void setActions(final List<? extends RPkgAction> list) {
		this.table.viewer.setInput(list);
	}
	
}
