/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.preferences;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.templates.TemplateContextType;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.templates.config.AbstractTemplatesPreferencePage;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.source.doc.RPartitionNodeType;
import org.eclipse.statet.r.ui.editors.templates.REditorTemplateContextType;
import org.eclipse.statet.r.ui.sourceediting.RTemplateSourceViewerConfigurator;


public class REditorTemplatesPreferencePage extends AbstractTemplatesPreferencePage {
	
	
	public REditorTemplatesPreferencePage() {
		super(RUIPlugin.getInstance().getPreferenceStore(),
				RUIPlugin.getInstance().getREditorTemplateStore() );
	}
	
	@Override
	protected SourceEditorViewerConfigurator createSourceViewerConfigurator(
			final TemplateVariableProcessor templateProcessor) {
		return new RTemplateSourceViewerConfigurator(
				RCore.WORKBENCH_ACCESS,
				templateProcessor );
	}
	
	
	@Override
	protected void configureContext(final AbstractDocument document,
			final TemplateContextType contextType, final SourceEditorViewerConfigurator configurator) {
		final String partitioning= configurator.getDocumentContentInfo().getPartitioning();
		final TreePartitioner partitioner= (TreePartitioner) document.getDocumentPartitioner(partitioning);
		if (contextType.getId().equals(REditorTemplateContextType.ROXYGEN_CONTEXTTYPE_ID)) {
			partitioner.setStartType(RPartitionNodeType.ROXYGEN);
		}
		else {
			partitioner.setStartType(RPartitionNodeType.DEFAULT_ROOT);
		}
		partitioner.disconnect();
		partitioner.connect(document);
		document.setDocumentPartitioner(partitioning, partitioner);
	}
	
}
