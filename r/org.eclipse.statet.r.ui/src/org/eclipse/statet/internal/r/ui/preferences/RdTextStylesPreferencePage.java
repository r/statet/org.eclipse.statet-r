/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.preferences;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.AbstractTextStylesConfigurationBlock;
import org.eclipse.statet.ltk.ui.util.CombinedPreferenceStore;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.ui.RUIPreferenceConstants;
import org.eclipse.statet.r.ui.editors.RdDocumentSetupParticipant;
import org.eclipse.statet.r.ui.editors.RdSourceViewerConfiguration;


@NonNullByDefault
public class RdTextStylesPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public RdTextStylesPreferencePage() {
		setPreferenceStore(RUIPlugin.getInstance().getPreferenceStore());
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() {
		return new RdTextStylesConfigurationBlock();
	}
	
}


@NonNullByDefault
class RdTextStylesConfigurationBlock extends AbstractTextStylesConfigurationBlock {
	
	
	public RdTextStylesConfigurationBlock() {
	}
	
	
	@Override
	protected String getSettingsGroup() {
		return RUIPreferenceConstants.Rd.TS_GROUP_ID;
	}
	
	@Override
	protected ImList<CategoryNode> createItems() {
		return ImCollections.newList(
			new CategoryNode(Messages.RdSyntaxColoring_CodeCategory_label,
				new StyleNode(Messages.RdSyntaxColoring_Default_label, Messages.RdSyntaxColoring_Default_description,
						RUIPreferenceConstants.Rd.TS_DEFAULT_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
//				new StyleNode(Messages.RdSyntaxColoring_Verbatim_label, Messages.RdSyntaxColoring_Verbatim_description,
//						RUIPreferenceConstants.Rd.TS_VERBATIM_ROOT, ),
				new StyleNode(Messages.RdSyntaxColoring_SectionTag_label, Messages.RdSyntaxColoring_SectionTag_description,
						RUIPreferenceConstants.Rd.TS_SECTION_TAG_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.RdSyntaxColoring_SubSectionTag_label, Messages.RdSyntaxColoring_SubSectionTag_description,
						RUIPreferenceConstants.Rd.TS_SUBSECTION_TAG_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.RdSyntaxColoring_OtherTag_label, Messages.RdSyntaxColoring_OtherTag_description,
						RUIPreferenceConstants.Rd.TS_OTHER_TAG_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.RdSyntaxColoring_UnlistedTag_label, Messages.RdSyntaxColoring_UnlistedTag_description,
						RUIPreferenceConstants.Rd.TS_UNLISTED_TAG_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.RdSyntaxColoring_Brackets_label, Messages.RdSyntaxColoring_Brackets_description,
						RUIPreferenceConstants.Rd.TS_BRACKETS_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
					
				new StyleNode(Messages.RdSyntaxColoring_PlatformSpecif_label, Messages.RdSyntaxColoring_PlatformSpecif_description,
						RUIPreferenceConstants.Rd.TS_PLATFORM_SPECIF_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ))
			),
			new CategoryNode(Messages.RdSyntaxColoring_CommentsCategory_label,
				new StyleNode(Messages.RdSyntaxColoring_Comment_label, Messages.RdSyntaxColoring_Comment_description,
						RUIPreferenceConstants.Rd.TS_COMMENT_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.RdSyntaxColoring_TaskTag_label, Messages.RdSyntaxColoring_TaskTag_description,
						RUIPreferenceConstants.Rd.TS_TASK_TAG_ROOT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ))
			)
		);
	}
	
	@Override
	protected String getPreviewFileName() {
		return "RdSyntaxColoringPreviewCode.txt"; //$NON-NLS-1$
	}
	
	@Override
	protected IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new RdDocumentSetupParticipant();
	}
	
	@Override
	protected SourceEditorViewerConfiguration getSourceEditorViewerConfiguration(
			final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		return new RdSourceViewerConfiguration(0, null, RCore.getDefaultsAccess(),
				CombinedPreferenceStore.createStore(
						preferenceStore,
						LtkUIPreferences.getPreferenceStore(),
						EditorsUI.getPreferenceStore()),
				textStyles );
	}
	
}
