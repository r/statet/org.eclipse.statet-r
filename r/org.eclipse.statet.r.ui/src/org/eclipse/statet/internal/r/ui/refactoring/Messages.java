/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.refactoring;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String RenameInWorkspace_Wizard_title;
	public static String RenameInWorkspace_Wizard_header;
	public static String RenameInWorkspace_Wizard_VariableName_label;
	public static String RenameInWorkspace_error_message;
	
	public static String RenameInRegion_Wizard_title;
	public static String RenameInRegion_Wizard_header;
	public static String RenameInRegion_error_message;
	
	public static String InlineTemp_Wizard_title;
	public static String InlineTemp_Wizard_header;
	public static String InlineTemp_error_message;
	
	public static String ExtractTemp_Wizard_title;
	public static String ExtractTemp_Wizard_header;
	public static String ExtractTemp_Wizard_VariableName_label;
	public static String ExtractTemp_Wizard_ReplaceAll_label;
	public static String ExtractTemp_error_message;
	
	public static String ExtractFunction_Wizard_title;
	public static String ExtractFunction_Wizard_header;
	public static String ExtractFunction_Wizard_VariableName_label;
	public static String ExtractFunction_error_message;
	
	public static String FunctionToS4Method_Wizard_title;
	public static String FunctionToS4Method_Wizard_header;
	public static String FunctionToS4Method_Wizard_VariableName_label;
	public static String FunctionToS4Method_Wizard_GenerateGeneric_label;
	public static String FunctionToS4Method_error_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
