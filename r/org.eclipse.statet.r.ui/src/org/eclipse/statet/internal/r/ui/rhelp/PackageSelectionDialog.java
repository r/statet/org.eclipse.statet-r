/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.eclipse.ui.internal.IWorkbenchHelpContextIds;

import org.eclipse.statet.ecommons.ui.components.SearchText;
import org.eclipse.statet.ecommons.ui.content.SearchTextBinding;
import org.eclipse.statet.ecommons.ui.content.TableFilterController;
import org.eclipse.statet.ecommons.ui.content.TextElementFilter;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils.CheckboxTableComposite;

import org.eclipse.statet.rhelp.core.RPkgHelp;


public class PackageSelectionDialog extends SelectionDialog {
	
	
	private final List<RPkgHelp> input;
	
	private SearchText filterText;
	private CheckboxTableViewer viewer;
	private TableFilterController filterController;
	
	private final List<RPkgHelp> selection;
	
	
	
	protected PackageSelectionDialog(final Shell parentShell,
			final List<RPkgHelp> packages, final List<RPkgHelp> initialSelection) {
		super(parentShell);
		this.input= packages;
		this.selection= initialSelection;
		setTitle(Messages.PackageSelection_title);
		setMessage(Messages.PackageSelection_message);
	}
	
	
	@Override
	protected void configureShell(final Shell shell) {
		super.configureShell(shell);
		PlatformUI.getWorkbench().getHelpSystem().setHelp(shell,
				IWorkbenchHelpContextIds.LIST_SELECTION_DIALOG);
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newDialogGrid(1));
		
		initializeDialogUnits(composite);
		
		createMessageArea(composite);
		
		this.filterText= new SearchText(composite);
		this.filterText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		this.filterText.setMessage("Filter");
		
		final CheckboxTableComposite tableComposite= new CheckboxTableComposite(composite,
				SWT.BORDER | SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.VIRTUAL);
		final GridData data= new GridData(SWT.FILL, SWT.FILL, true, true);
		data.heightHint= LayoutUtils.hintHeight(tableComposite.table, 10);
		data.widthHint= LayoutUtils.hintWidth(tableComposite.table, 40);
		tableComposite.setLayoutData(data);
		this.viewer= tableComposite.viewer;
		
		final TableViewerColumn column= tableComposite.addColumn("Name", SWT.LEFT, new ColumnWeightData(1));
		column.setLabelProvider(new RHelpLabelProvider());
		ColumnViewerToolTipSupport.enableFor(tableComposite.viewer);
		
		this.viewer.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(final CheckStateChangedEvent event) {
				final Object element= event.getElement();
				if (element instanceof RPkgHelp) {
					final RPkgHelp pkg= (RPkgHelp) element;
					if (!PackageSelectionDialog.this.selection.remove(pkg)) {
						PackageSelectionDialog.this.selection.add(pkg);
					}
				}
			}
		});
		this.viewer.setCheckStateProvider(new ICheckStateProvider() {
			@Override
			public boolean isGrayed(final Object element) {
				return false;
			}
			@Override
			public boolean isChecked(final Object element) {
				return PackageSelectionDialog.this.selection.contains(element);
			}
		});
		
		this.filterController= new TableFilterController(this.viewer);
		
		{	final TextElementFilter filter= new TextElementFilter();
			this.filterController.setFilter(0, filter);
			new SearchTextBinding(this.filterText, this.filterController, filter);
		}
		this.filterController.setInput(this.input);
		
		ViewerUtils.installSearchTextNavigation(this.viewer, this.filterText, true);
		
		final Button clearAllControl= new Button(composite, SWT.PUSH);
		final GridData gd= new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gd.widthHint= LayoutUtils.hintWidth(clearAllControl);
		clearAllControl.setLayoutData(gd);
		clearAllControl.setText(Messages.PackageSelection_ClearAll_label);
		clearAllControl.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				PackageSelectionDialog.this.selection.clear();
				PackageSelectionDialog.this.viewer.refresh();
			}
		});
		
		Dialog.applyDialogFont(composite);
		
		return composite;
	}
	
	@Override
	protected void okPressed() {
		super.okPressed();
		setSelectionResult(this.selection.toArray(new RPkgHelp[this.selection.size()]));
	}
	
	@Override
	public RPkgHelp[] getResult() {
		return (RPkgHelp[]) super.getResult();
	}
	
}
