/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import static org.eclipse.statet.r.ui.rhelp.RHelpUI.RHELP_SEARCH_OBJ_IMAGE_ID;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.ecommons.workbench.search.ui.ElementMatchComparator;
import org.eclipse.statet.ecommons.workbench.search.ui.ExtTextSearchResult;

import org.eclipse.statet.r.ui.rhelp.RHelpUI;
import org.eclipse.statet.rhelp.core.RPkgHelp;
import org.eclipse.statet.rj.renv.core.REnv;


public class RHelpSearchResult extends ExtTextSearchResult<RPkgHelp, RHelpSearchUIMatch> {
	
	
	public static final ElementMatchComparator<RPkgHelp, RHelpSearchUIMatch> COMPARATOR= new ElementMatchComparator<>(
			new RPkgHelp[0], null,
			new RHelpSearchUIMatch[0], null );
	
	
	private final RHelpSearchUIQuery query;
		
	private REnv rEnv;
	
	
	public RHelpSearchResult(final RHelpSearchUIQuery query) {
		super(COMPARATOR);
		
		this.query= query;
	}
	
	
	@Override
	public ImageDescriptor getImageDescriptor() {
		return RHelpUI.getUIResources().getImageDescriptor(RHELP_SEARCH_OBJ_IMAGE_ID);
	}
	
	@Override
	public String getLabel() {
		final String searchLabel= this.query.getSearchLabel();
		
		final String matchLabel;
		{	final Object[] data= new Object[3]; // count, pkg-count, renv
			
			data[2]= this.rEnv.getName();
			if (data[2] == null) {
				data[2]= "-"; //$NON-NLS-1$
			}
			
			int count;
			synchronized (this) {
				count= getMatchCount();
				data[0]= count;
				data[1]= getElementCount();
			}
			
			if (count == 1) {
				matchLabel= NLS.bind(Messages.Search_SingleMatch_label, data[2]);
			}
			else {
				matchLabel= NLS.bind(Messages.Search_MultipleMatches_label, data);
			}
		}
		
		return searchLabel + " – " + matchLabel; //$NON-NLS-1$
	}
	
	@Override
	public RHelpSearchUIQuery getQuery() {
		return this.query;
	}
	
	
	public void init(final REnv renv) {
		this.rEnv= renv;
		removeAll();
	}
	
}
