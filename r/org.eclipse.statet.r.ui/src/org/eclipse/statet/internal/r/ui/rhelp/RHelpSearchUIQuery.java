/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;

import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rhelp.core.RHelpSearchMatch;
import org.eclipse.statet.rhelp.core.RHelpSearchQuery;
import org.eclipse.statet.rhelp.core.RHelpSearchRequestor;


public class RHelpSearchUIQuery implements ISearchQuery {
	
	
	private final RHelpSearchQuery query;
	
	private RHelpSearchResult result;
	
	
	public RHelpSearchUIQuery(final RHelpSearchQuery coreQuery) {
		if (coreQuery == null) {
			throw new NullPointerException();
		}
		this.query= coreQuery;
	}
	
	
	@Override
	public IStatus run(final IProgressMonitor monitor) throws OperationCanceledException {
		synchronized (this) {
			if (this.result == null) {
				this.result= new RHelpSearchResult(this);
			}
			this.result.init(this.query.getREnv());
		}
		
		final RHelpSearchRequestor requestor= new RHelpSearchRequestor() {
			@Override
			public int getMaxFragments() {
				return EPreferences.getInstancePrefs().getPreferenceValue(
						RHelpPreferences.SEARCH_PREVIEW_FRAGMENTS_MAX_PREF );
			}
			@Override
			public void matchFound(final RHelpSearchMatch match) {
				RHelpSearchUIQuery.this.result.addMatch(new RHelpSearchUIMatch(match));
			}
		};
		try {
			RCore.getRHelpManager().search(this.query, requestor, EStatusUtils.convert(monitor, 1));
			return Status.OK_STATUS;
		}
		catch (final StatusException e) {
			if (e.getStatus().getSeverity() == IStatus.CANCEL) {
				return EStatusUtils.convert(e.getStatus());
			}
			return new Status(IStatus.ERROR, RUI.BUNDLE_ID,
					String.format("An error occurred when performing R help search: %1$s",
							getSearchLabel() ),
					EStatusUtils.convert(e) );
		}
		finally {
			if (monitor != null) {
				monitor.done();
			}
		}
	}
	
	@Override
	public String getLabel() {
		return Messages.Search_Query_label;
	}
	
	public String getSearchLabel() {
		final String searchString= this.query.getSearchString();
		if (searchString.length() > 0) {
			switch (this.query.getSearchType()) {
			case RHelpSearchQuery.TOPIC_SEARCH:
				return NLS.bind(Messages.Search_PatternInTopics_label, searchString);
			case RHelpSearchQuery.FIELD_SEARCH:
				return NLS.bind(Messages.Search_PatternInFields_label, searchString);
			}
		}
		return NLS.bind(Messages.Search_Pattern_label, searchString);
	}
	
	@Override
	public boolean canRerun() {
		return (this.query.getREnv().get(REnvHelpConfiguration.class) != null);
	}
	
	@Override
	public boolean canRunInBackground() {
		return true;
	}
	
	@Override
	public ISearchResult getSearchResult() {
		synchronized (this) {
			if (this.result == null) {
				this.result= new RHelpSearchResult(this);
				this.result.init(this.query.getREnv());
			}
			return this.result;
		}
	}
	
	
	public RHelpSearchQuery getRHelpQuery() {
		return this.query;
	}
	
}
