/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rtools;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.common.NotDefinedException;

import org.eclipse.statet.r.core.RUtil;
import org.eclipse.statet.r.launching.AbstractRCommandHandler;


/**
 * Command handler for help.search("...")
 */
public class RunHelpSearchInR extends AbstractRCommandHandler {
	
	
	public static final String COMMAND_ID= "org.eclipse.statet.r.commands.RunHelpSearchInR"; //$NON-NLS-1$
	private static final String PAR_TEXT= "text"; //$NON-NLS-1$
	
	
	public static String createCommandString(final String text) throws NotDefinedException {
		return createCommandString(COMMAND_ID, new String[][] {{ PAR_TEXT, text }});
	}
	
	
	public RunHelpSearchInR() {
		super(Messages.HelpCommand_name);
	}
	
	
	@Override
	public Object execute(final ExecutionEvent arg) throws ExecutionException {
		String text= arg.getParameter(PAR_TEXT);
		if (text == null) {
			text= getRSelection();
			if (text == null) {
				return null;
			}
		}
		runCommand("help.search(\""+RUtil.escapeForDQuote(text)+"\")", false); //$NON-NLS-1$ //$NON-NLS-2$
		return null;
	}
	
}
