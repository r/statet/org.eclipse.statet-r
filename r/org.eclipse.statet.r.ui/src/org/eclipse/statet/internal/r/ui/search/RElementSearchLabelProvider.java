/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.search;

import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.workbench.search.ui.LineElement;
import org.eclipse.statet.ecommons.workbench.search.ui.TextSearchLabelUtil;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.ui.RLabelProvider;


public class RElementSearchLabelProvider extends RLabelProvider implements IStyledLabelProvider {
	
	
	private final RElementSearchResultPage page;
	
	private final TextSearchLabelUtil util;
	private final WorkbenchLabelProvider workbenchLabelProvider;
	
	
	public RElementSearchLabelProvider(final RElementSearchResultPage page, final int style) {
		super(style);
		this.page= page;
		this.util= new TextSearchLabelUtil();
		this.workbenchLabelProvider= new WorkbenchLabelProvider();
	}
	
	
	@Override
	public void dispose() {
		super.dispose();
		this.workbenchLabelProvider.dispose();
	}
	
	@Override
	public void addListener(final ILabelProviderListener listener) {
		super.addListener(listener);
		this.workbenchLabelProvider.addListener(listener);
	}
	
	@Override
	public void removeListener(final ILabelProviderListener listener) {
		super.removeListener(listener);
		this.workbenchLabelProvider.removeListener(listener);
	}
	
	@Override
	public boolean isLabelProperty(final Object element, final String property) {
		if (element instanceof LtkModelElement) {
			return super.isLabelProperty(element, property);
		}
		if (element instanceof RElementMatch || element instanceof LineElement<?>) {
			return true;
		}
		return this.workbenchLabelProvider.isLabelProperty(element, property);
	}
	
	
	@Override
	public Image getImage(final Object element) {
		if (element instanceof LtkModelElement) {
			if (element instanceof SourceUnit) {
				return this.workbenchLabelProvider.getImage(((SourceUnit) element).getResource());
			}
			return super.getImage((LtkModelElement)element);
		}
		if (element instanceof RElementMatch || element instanceof LineElement<?>) {
			return SharedUIResources.getImages().get(SharedUIResources.OBJ_LINE_MATCH_IMAGE_ID);
		}
		return this.workbenchLabelProvider.getImage(element);
	}
	
	@Override
	public StyledString getStyledText(final Object element) {
		if (element instanceof LtkModelElement) {
			final StyledString text= super.getStyledText((LtkModelElement)element);
			if (element instanceof RSourceUnit) {
				final int count= this.page.getDisplayedMatchCount(element);
				final StringBuilder sb= getTextBuilder();
				sb.append(" ("); //$NON-NLS-1$
				sb.append(count);
				sb.append(' ');
				sb.append((count == 1) ? Messages.Search_Match_sing_label : Messages.Search_Match_plural_label);
				sb.append(')');
				text.append(sb.toString(), StyledString.COUNTER_STYLER);
			}
			return text;
		}
		if (element instanceof LineElement<?>) {
			final LineElement<?> lineElement= (LineElement<?>) element;
			return this.util.getStyledText(lineElement, this.page.getDisplayedMatches(lineElement));
		}
		return this.workbenchLabelProvider.getStyledText(element);
	}
	
}
