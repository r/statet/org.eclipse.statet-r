/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.ui.dialogs.WizardNewProjectReferencePage;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.wizards.LTKWizardsMessages;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizard;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.r.ui.RUI;


public class NewRProjectWizard extends NewElementWizard {
	
	
	private NewRProjectWizardPage firstPage;
	private WizardNewProjectReferencePage referencePage;
	
	private NewProject newRProject;
	
	
	public NewRProjectWizard() {
		setDialogSettings(DialogUtils.getDialogSettings(RUIPlugin.getInstance(), "NewElementWizard")); //$NON-NLS-1$
		setDefaultPageImageDescriptor(RUI.getImageDescriptor(RUIPlugin.IMG_WIZBAN_NEW_RPROJECT));
		setWindowTitle(Messages.NewRProjectWizard_title);
	}
	
	@Override
	public void addPages() {
		super.addPages();
		this.firstPage= new NewRProjectWizardPage(getSelection(), null);
		addPage(this.firstPage);
		
		// only add page if there are already projects in the workspace
		if (ResourcesPlugin.getWorkspace().getRoot().getProjects().length > 0) {
			this.referencePage= new WizardNewProjectReferencePage("BasicProjectReferencePage"); //$NON-NLS-1$
			this.referencePage.setTitle(LTKWizardsMessages.NewProjectReferencePage_title);
			this.referencePage.setDescription(LTKWizardsMessages.NewProjectReferencePage_description);
			addPage(this.referencePage);
		}
		
	}
	
//	protected ISchedulingRule getSchedulingRule() { // root-rule required to change project description
	
	@Override
	public boolean performFinish() {
		this.newRProject= new NewProject(
				this.firstPage.getProjectHandle(),
				(this.firstPage.useDefaults()) ? null : this.firstPage.getLocationPath(),
				(this.referencePage != null) ? this.referencePage.getReferencedProjects() : null,
				this.firstPage.getSelectedWorkingSets()
				) {
			@Override
			protected void doConfigProject(final IProject project, final IProgressMonitor monitor) throws CoreException {
				RProjects.setupRProject(NewRProjectWizard.this.newRProject.getResource(), monitor);
			}
		};
		
		final boolean result= super.performFinish();
		
		if (result && this.newRProject.getResource() != null) {
			updatePerspective();
			selectAndReveal(this.newRProject.getResource());
		}
		
		return result;
	}
	
	@Override
	protected void performOperations(final IProgressMonitor monitor) throws InterruptedException, CoreException, InvocationTargetException {
		final SubMonitor m= SubMonitor.convert(monitor, "Create new R project...", 10);
		
		this.newRProject.createProject(m.newChild(10));
	}
	
}
