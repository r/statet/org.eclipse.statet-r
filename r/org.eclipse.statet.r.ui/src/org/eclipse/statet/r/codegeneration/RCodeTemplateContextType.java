/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.codegeneration;

import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateVariableResolver;
import org.eclipse.text.templates.ContextTypeRegistry;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.ltk.ui.templates.SourceEditorContextType;


@NonNullByDefault
public class RCodeTemplateContextType extends SourceEditorContextType {
	
	
/* context types **************************************************************/
	public static final String NEW_RSCRIPTFILE_CONTEXTTYPE= "r_NewRScriptFile_context"; //$NON-NLS-1$
	
	public static final String ROXYGEN_COMMONFUNCTION_CONTEXTTYPE= "roxygen_CommonFunctionDef_context"; //$NON-NLS-1$
	public static final String ROXYGEN_CLASS_CONTEXTTYPE= "roxygen_ClassDef_context"; //$NON-NLS-1$
	public static final String ROXYGEN_METHOD_CONTEXTTYPE= "roxygen_MethodDef_context"; //$NON-NLS-1$
	
/* templates ******************************************************************/
	public static final String NEW_RSCRIPTFILE= "r_NewRScriptFile"; //$NON-NLS-1$
	
	public static final String ROXYGEN_COMMONFUNCTION_TEMPLATE_ID= "roxygen_CommonFunctionDef"; //$NON-NLS-1$
	public static final String ROXYGEN_S4CLASS_TEMPLATE_ID= "roxygen_S4ClassDef"; //$NON-NLS-1$
	public static final String ROXYGEN_S4METHOD_TEMPLATE_ID= "roxygen_S4MethodDef"; //$NON-NLS-1$
	
/* variables ******************************************************************/
	public static final String ELEMENT_NAME_VAR_NAME= "element_name"; //$NON-NLS-1$;
	
	public static final String ROXYGEN_PARAM_TAGS_VAR_NAME= "param_tags"; //$NON-NLS-1$
	public static final String ROXYGEN_SLOT_TAGS_VAR_NAME= "slot_tags"; //$NON-NLS-1$
	public static final String ROXYGEN_SIG_LIST_VAR_NAME= "sig_list"; //$NON-NLS-1$
	
	
	public static void registerContextTypes(final ContextTypeRegistry registry) {
		registry.addContextType(new RCodeTemplateContextType(NEW_RSCRIPTFILE_CONTEXTTYPE));
		
		registry.addContextType(new RCodeTemplateContextType(ROXYGEN_COMMONFUNCTION_CONTEXTTYPE));
		registry.addContextType(new RCodeTemplateContextType(ROXYGEN_CLASS_CONTEXTTYPE));
		registry.addContextType(new RCodeTemplateContextType(ROXYGEN_METHOD_CONTEXTTYPE));
	}
	
	
	private static class RoxygenParamTagsVariableResolver extends TemplateVariableResolver {
		
		public RoxygenParamTagsVariableResolver() {
			super(ROXYGEN_PARAM_TAGS_VAR_NAME, RUIMessages.Templates_Variable_RoxygenParamTags_description);
		}
		
		@Override
		protected String resolve(final TemplateContext context) {
			return "@param \u2026"; //$NON-NLS-1$
		}
	}
	
	private static class RoxygenSlotTagsVariableResolver extends TemplateVariableResolver {
		
		public RoxygenSlotTagsVariableResolver() {
			super(ROXYGEN_SLOT_TAGS_VAR_NAME, RUIMessages.Templates_Variable_RoxygenSlotTags_description);
		}
		
		@Override
		protected String resolve(final TemplateContext context) {
			return "@slot \u2026"; //$NON-NLS-1$
		}
	}
	
	private static class RoxygenSigListVariableResolver extends TemplateVariableResolver {
		
		public RoxygenSigListVariableResolver() {
			super(ROXYGEN_SIG_LIST_VAR_NAME, RUIMessages.Templates_Variable_RoxygenSigList_description);
		}
		
		@Override
		protected String resolve(final TemplateContext context) {
			return ""; //$NON-NLS-1$
		}
	}
	
	private static class RElementNameVariableResolver extends TemplateVariableResolver {
		
		protected RElementNameVariableResolver() {
			super(ELEMENT_NAME_VAR_NAME, RUIMessages.Templates_Variable_ElementName_description);
		}
		
	}
	
	
	RCodeTemplateContextType(final String contextName) {
		super(contextName);
		
		addCommonVariables();
		if (NEW_RSCRIPTFILE_CONTEXTTYPE.equals(contextName)) {
			addSourceUnitGenerationVariables();
		}
		else if (ROXYGEN_CLASS_CONTEXTTYPE.equals(contextName)) {
			addSourceUnitGenerationVariables();
			addResolver(new RElementNameVariableResolver());
			addResolver(new RoxygenSlotTagsVariableResolver()); 
		}
		else if (ROXYGEN_COMMONFUNCTION_CONTEXTTYPE.equals(contextName)) {
			addSourceUnitGenerationVariables();
			addResolver(new RElementNameVariableResolver());
			addResolver(new RoxygenParamTagsVariableResolver()); 
		}
		else if (ROXYGEN_METHOD_CONTEXTTYPE.equals(contextName)) {
			addSourceUnitGenerationVariables();
			addResolver(new RElementNameVariableResolver());
			addResolver(new RoxygenParamTagsVariableResolver()); 
			addResolver(new RoxygenSigListVariableResolver()); 
		}
	}
	
}
