/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.internal.r.ui.RUIPlugin;


/**
 * 
 */
public class RUI {
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.ui"; //$NON-NLS-1$
	
	public static final String R_EDITOR_ID = "org.eclipse.statet.r.editors.R"; //$NON-NLS-1$
	public static final String RD_EDITOR_ID = "org.eclipse.statet.r.editors.Rd"; //$NON-NLS-1$
	
	public static final String R_HELP_VIEW_ID = "org.eclipse.statet.r.views.RHelp"; //$NON-NLS-1$
	public static final String R_HELP_SEARCH_PAGE_ID = "org.eclipse.statet.r.searchPages.RHelpPage"; //$NON-NLS-1$
	
	public static final String SOURCE_EDITORS_PREF_PAGE_ID= "org.eclipse.statet.r.preferencePages.SourceEditors"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_R_SCRIPT = RUI.BUNDLE_ID + "/images/obj/r_script"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_RUNTIME_ENV = RUI.BUNDLE_ID + "/images/obj/r_environment"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_REMOTE_ENV = RUI.BUNDLE_ID + "/images/obj/r_environment.remote"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_PACKAGE = RUI.BUNDLE_ID + "/images/obj/r_package"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_PACKAGE_NA = RUI.BUNDLE_ID + "/images/obj/r_package-notavail"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_METHOD = RUI.BUNDLE_ID + "/images/obj/method"; //$NON-NLS-1$
	public static final String IMG_OBJ_COMMON_FUNCTION = RUI.BUNDLE_ID + "/images/obj/function.common"; //$NON-NLS-1$
	public static final String IMG_OBJ_COMMON_LOCAL_FUNCTION = RUI.BUNDLE_ID + "/images/obj/function.common.local"; //$NON-NLS-1$
	public static final String IMG_OBJ_GENERIC_FUNCTION = RUI.BUNDLE_ID + "/images/obj/function.generic"; //$NON-NLS-1$
	public static final String IMG_OBJ_GENERAL_VARIABLE = RUI.BUNDLE_ID + "/images/obj/variable.common"; //$NON-NLS-1$
	public static final String IMG_OBJ_GENERAL_LOCAL_VARIABLE = RUI.BUNDLE_ID + "/images/obj/variable.common.local"; //$NON-NLS-1$
	public static final String IMG_OBJ_SLOT = RUI.BUNDLE_ID + "/images/obj/variable.slot"; //$NON-NLS-1$
	public static final String IMG_OBJ_PACKAGEENV = RUI.BUNDLE_ID + "/images/obj/packageenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_GLOBALENV = RUI.BUNDLE_ID + "/images/obj/globalenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_EMPTYENV = RUI.BUNDLE_ID + "/images/obj/emptyenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_OTHERENV = RUI.BUNDLE_ID + "/images/obj/otherenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_LIST = RUI.BUNDLE_ID + "/images/obj/list"; //$NON-NLS-1$
	public static final String IMG_OBJ_DATAFRAME = RUI.BUNDLE_ID + "/images/obj/dataframe"; //$NON-NLS-1$
	public static final String IMG_OBJ_DATAFRAME_COLUMN = RUI.BUNDLE_ID + "/images/obj/datastore"; //$NON-NLS-1$
	public static final String IMG_OBJ_VECTOR = RUI.BUNDLE_ID + "/images/obj/vector"; //$NON-NLS-1$
	public static final String IMG_OBJ_ARRAY = RUI.BUNDLE_ID + "/images/obj/array"; //$NON-NLS-1$
	public static final String IMG_OBJ_S4OBJ = RUI.BUNDLE_ID + "/images/obj/s4obj"; //$NON-NLS-1$
	public static final String IMG_OBJ_S4OBJ_VECTOR = RUI.BUNDLE_ID + "/images/obj/s4obj.vector"; //$NON-NLS-1$
	public static final String IMG_OBJ_S4OBJ_DATAFRAME_COLUMN = RUI.BUNDLE_ID + "/images/obj/s4obj.dataframe_col"; //$NON-NLS-1$
	public static final String IMG_OBJ_NULL = RUI.BUNDLE_ID + "/images/obj/null"; //$NON-NLS-1$
	public static final String IMG_OBJ_MISSING = RUI.BUNDLE_ID + "/images/obj/missing"; //$NON-NLS-1$
	public static final String IMG_OBJ_PROMISE = RUI.BUNDLE_ID + "/images/obj/promise"; //$NON-NLS-1$
	public static final String IMG_OBJ_ARGUMENT_ASSIGN = RUI.BUNDLE_ID + "/images/obj/argument.assign"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_COL_LOGI = RUI.BUNDLE_ID + "/images/obj/col.logi"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_INT = RUI.BUNDLE_ID + "/images/obj/col.int"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_NUM = RUI.BUNDLE_ID + "/images/obj/col.num"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_CPLX = RUI.BUNDLE_ID + "/images/obj/col.cplx"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_CHAR = RUI.BUNDLE_ID + "/images/obj/col.char"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_RAW = RUI.BUNDLE_ID + "/images/obj/col.raw"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_FACTOR = RUI.BUNDLE_ID + "/images/obj/col.factor"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_DATE = RUI.BUNDLE_ID + "/images/obj/col.date"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_DATETIME = RUI.BUNDLE_ID + "/images/obj/col.datetime"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_LIBRARY_GROUP = BUNDLE_ID + "/images/obj/library.group"; //$NON-NLS-1$
	public static final String IMG_OBJ_LIBRARY_LOCATION = BUNDLE_ID + "/images/obj/library.location"; //$NON-NLS-1$
	
	public static final String IMG_TOOL_ASSIST_TO_PIPE_FORWARD= "/images/tool/assist-to_pipe_forward"; //$NON-NLS-1$
	
	public static final String IMG_LOCTOOL_SORT_PACKAGE = BUNDLE_ID + "/images/obj/sort.package"; //$NON-NLS-1$
	
	
	public static ImageDescriptor getImageDescriptor(final String key) {
		return RUIPlugin.getInstance().getImageRegistry().getDescriptor(key);
	}
	
	public static Image getImage(final String key) {
		return RUIPlugin.getInstance().getImageRegistry().get(key);
	}
	
}
