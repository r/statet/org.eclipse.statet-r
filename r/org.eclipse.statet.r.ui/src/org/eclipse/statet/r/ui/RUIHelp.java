/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui;

import org.eclipse.help.IContextProvider;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.IWorkbenchPart3;

import org.eclipse.statet.internal.r.ui.help.EnrichedRHelpContext;


/**
 * 
 */
public class RUIHelp {
	
	
	public static IContextProvider createEnrichedRHelpContextProvider(final IWorkbenchPart3 part, final String contextId) {
		return new EnrichedRHelpContext.Provider(part, contextId);
	}
	
	public static IContextProvider createEnrichedRHelpContextProvider(final ISourceViewer viewer, final String contextId) {
		return new EnrichedRHelpContext.Provider(viewer, contextId);
	}
	
}
