/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.statet.jcommons.collections.ImLongList;
import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class DataViewDescription implements Immutable {
	
	
	private final ImLongList dataDim;
	
	private final ImLongList viewDim;
	
	
	public DataViewDescription(final ImLongList dataDim, final ImLongList viewDim) {
		this.dataDim= dataDim;
		this.viewDim= viewDim;
	}
	
	
	/**
	 * Returns the dimension of the data.
	 * 
	 * @return the dimension
	 */
	public ImLongList getDataDimension() {
		return this.dataDim;
	}
	
	/**
	 * Returns the dimension of the (filtered) view data.
	 * 
	 * @return the dimension
	 */
	public ImLongList getViewDataDimension() {
		return this.viewDim;
	}
	
}
