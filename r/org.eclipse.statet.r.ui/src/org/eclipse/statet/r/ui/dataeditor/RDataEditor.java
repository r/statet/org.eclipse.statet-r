/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.impl.RLanguageImpl;
import org.eclipse.statet.rj.services.BasicFQRObjectRef;
import org.eclipse.statet.rj.services.FQRObjectRef;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public class RDataEditor {
	
	
	public static final String RDATA_EDITOR_ID= "org.eclipse.statet.r.editors.RData"; //$NON-NLS-1$
	
	
	private static FQRObjectRef<? extends RTool> createFQRef(final RTool tool, final RElementName elementName) {
		RElementName envName= elementName.getScope();
		RElementName objName= elementName;
		if (envName == null && RElementName.isScopeType(elementName.getType())) {
			envName= elementName;
			objName= elementName.getNextSegment();
		}
		if (envName == null) {
			throw new IllegalArgumentException("elementName not FQ.");
		}
		
		RElementName next= objName.getNextSegment();
		while (next != null) {
			if (next.getType() == RElementName.MAIN_DEFAULT) {
				objName= next;
			}
			next= next.getNextSegment();
		}
		
		envName= RElementName.create(envName, objName, true);
		objName= RElementName.create(objName, null, false);
		return new BasicFQRObjectRef<>(tool,
				new RLanguageImpl(RLanguage.CALL, envName.getDisplayName(RElementName.DISPLAY_FQN | RElementName.DISPLAY_EXACT), null),
				new RLanguageImpl(RLanguage.CALL, objName.getDisplayName(RElementName.DISPLAY_EXACT), null) );
	}
	
	public static void open(final IWorkbenchPage page, final RTool tool,
			final RElementName elementName,
			final long @Nullable [] indexes) {
		open(page, elementName, createFQRef(tool, elementName), indexes);
	}
	
	public static void open(final IWorkbenchPage page,
			final RElementName elementName, final FQRObjectRef<? extends RTool> elementRef,
			final long @Nullable [] indexes) {
		try {
			final IEditorPart editor= IDE.openEditor(page,
					new RLiveDataEditorInput(elementName, elementRef),
					RDATA_EDITOR_ID, true );
			
			final RDataTableViewer table= editor.getAdapter(RDataTableViewer.class);
			if (indexes != null && table != null) {
				switch (indexes.length) {
				case 1:
					table.setAnchorDataIdxs(0, indexes[0]);
					break;
				case 2:
					table.setAnchorDataIdxs(indexes[1], indexes[0]);
					break;
				default:
					break;
				}
			}
		}
		catch (final PartInitException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
					"Failed to open the R element in the data editor.",
					e ));
		}
	}
	
}
