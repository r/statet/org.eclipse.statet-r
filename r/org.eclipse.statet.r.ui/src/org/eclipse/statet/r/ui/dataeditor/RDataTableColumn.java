/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.dataeditor.RDataFormatter;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RDataTableColumn implements RDataTableVariable {
	
	
	private final long index;
	private final @Nullable String name;
	
	private final @Nullable String rExpression;
	private final @Nullable RElementName elementName;
	
	private final int columnType;
	private final RStore<?> dataStore;
	
	private final List<String> classNames;
	
	private final RDataFormatter defaultFormat;
	
	
	public RDataTableColumn(final long columnIndex, final @Nullable String name,
			final @Nullable String rExpression, final @Nullable RElementName elementName,
			final int columnType, final RStore<?> dataStore, final List<String> classNames,
			final RDataFormatter defaultFormat) {
		this.index= columnIndex;
		this.name= name;
		this.rExpression= rExpression;
		this.elementName= elementName;
		this.columnType= columnType;
		this.dataStore= dataStore;
		this.classNames= classNames;
		
		this.defaultFormat= defaultFormat;
	}
	
	
	@Override
	public int getVarPresentation() {
		return COLUMN;
	}
	
	public long getIndex() {
		return this.index;
	}
	
	@Override
	public @Nullable String getName() {
		return this.name;
	}
	
	public @Nullable String getRExpression() {
		return this.rExpression;
	}
	
	public @Nullable RElementName getElementName() {
		return this.elementName;
	}
	
	@Override
	public int getVarType() {
		return this.columnType;
	}
	
	public RStore<?> getDataStore() {
		return this.dataStore;
	}
	
	public List<String> getClassNames() {
		return this.classNames;
	}
	
	public RDataFormatter getDefaultFormat() {
		return this.defaultFormat;
	}
	
	
	@Override
	public int hashCode() {
		final int h= (int)(this.index ^ (this.index >>> 32));
		return h ^ (h >>> 7);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RDataTableColumn) {
			final RDataTableColumn other= (RDataTableColumn)obj;
			return (this.index == other.index);
		}
		return false;
	}
	
}
