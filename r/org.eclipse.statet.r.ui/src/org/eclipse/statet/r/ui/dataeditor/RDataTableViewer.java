/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.CopyOnWriteList;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.SystemRunnable;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.BasicCustomViewer;
import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.config.LayoutSizeConfig;
import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.copy.CopyToClipboardCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionId;
import org.eclipse.statet.ecommons.waltable.core.data.ControlData;
import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;
import org.eclipse.statet.ecommons.waltable.core.data.SpanningDataProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerListener;
import org.eclipse.statet.ecommons.waltable.core.layer.events.DimGeneralStrucuralChangeEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.events.DimPositionsVisualChangeEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.events.GeneralVisualChangeEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.events.VisualChangeEvent;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;
import org.eclipse.statet.ecommons.waltable.data.core.SpanningDataLayer;
import org.eclipse.statet.ecommons.waltable.freeze.FreezeLayer;
import org.eclipse.statet.ecommons.waltable.freeze.core.CompositeFreezeLayer;
import org.eclipse.statet.ecommons.waltable.grid.AlternatingRowLabelContributor;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLayer;
import org.eclipse.statet.ecommons.waltable.grid.core.data.DefaultCornerDataProvider;
import org.eclipse.statet.ecommons.waltable.grid.core.labeled.ExtColumnHeaderLayer;
import org.eclipse.statet.ecommons.waltable.grid.core.labeled.ExtGridLayer;
import org.eclipse.statet.ecommons.waltable.grid.core.labeled.ExtRowHeaderLayer;
import org.eclipse.statet.ecommons.waltable.grid.core.labeled.LabelCornerLayer;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.ColumnHeaderLayer;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.CornerLayer;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.RowHeaderLayer;
import org.eclipse.statet.ecommons.waltable.layer.cell.AggregrateConfigLabelAccumulator;
import org.eclipse.statet.ecommons.waltable.resize.core.InitializeAutoResizeCommandHandler;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectAllCommand;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectDimPositionsCommand;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectRelativeCommandHandler;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.sort.core.ClearSortCommand;
import org.eclipse.statet.ecommons.waltable.sort.core.ClearSortCommandHandler;
import org.eclipse.statet.ecommons.waltable.sort.core.SortDimPositionCommand;
import org.eclipse.statet.ecommons.waltable.sort.core.SortDirection;
import org.eclipse.statet.ecommons.waltable.sort.core.SortHeaderLayer;
import org.eclipse.statet.ecommons.waltable.sort.core.SortModel;
import org.eclipse.statet.ecommons.waltable.sort.core.SortPositionCommandHandler;
import org.eclipse.statet.ecommons.waltable.tickupdate.config.DefaultTickUpdateConfiguration;
import org.eclipse.statet.ecommons.waltable.ui.ITableUIContext;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportLayer;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportLayerDim;

import org.eclipse.statet.internal.r.ui.dataeditor.AbstractRDataProvider;
import org.eclipse.statet.internal.r.ui.dataeditor.FTableDataProvider;
import org.eclipse.statet.internal.r.ui.dataeditor.FindFilter;
import org.eclipse.statet.internal.r.ui.dataeditor.FindListener;
import org.eclipse.statet.internal.r.ui.dataeditor.FindTask;
import org.eclipse.statet.internal.r.ui.dataeditor.RDataFormatter;
import org.eclipse.statet.internal.r.ui.dataeditor.RDataFormatterConverter;
import org.eclipse.statet.internal.r.ui.dataeditor.RDataFrameDataProvider;
import org.eclipse.statet.internal.r.ui.dataeditor.RMatrixDataProvider;
import org.eclipse.statet.internal.r.ui.dataeditor.RVectorDataProvider;
import org.eclipse.statet.internal.r.ui.dataeditor.ResolveCellIndexes;
import org.eclipse.statet.internal.r.ui.intable.DataTable;
import org.eclipse.statet.internal.r.ui.intable.PresentationConfig;
import org.eclipse.statet.internal.r.ui.intable.RDataLayer;
import org.eclipse.statet.internal.r.ui.intable.TableLayers;
import org.eclipse.statet.internal.r.ui.intable.UIBindings;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class RDataTableViewer extends BasicCustomViewer<RDataTableSelection> {
	
	
	private static final RDataTableSelection NO_SELECTION= new RDataTableSelection(null,
			null, null, null, null);
	
	private static final int CHECK_LABEL= 1 << 0;
	
	
	private class SelectionFindFilter implements FindFilter {
		
		@Override
		public boolean match(final long rowIdx, final long columnIdx) {
			final var tableLayers= RDataTableViewer.this.tableLayers;
			if (tableLayers != null) {
				if (columnIdx >= 0) {
					return tableLayers.selectionLayer.isCellPositionSelected(columnIdx, rowIdx);
				}
				else {
					return tableLayers.selectionLayer.isRowPositionSelected(rowIdx);
				}
			}
			return false;
		}
		
	}
	
	private class SetAnchorByDataIndexes extends ResolveCellIndexes {
		
		public SetAnchorByDataIndexes(final AbstractRDataProvider<?> dataProvider) {
			super(dataProvider);
		}
		
		
		@Override
		protected void execute(final long columnIndex, final long rowIndex) {
			RDataTableViewer.this.display.asyncExec(new Runnable() {
				@Override
				public void run() {
					if (getDataProvider() != RDataTableViewer.this.dataProvider) {
						return;
					}
					setAnchorViewIdxs(columnIndex, rowIndex);
				}
			});
		}
		
	}
	
	private class RUIContext implements ITableUIContext {
		
		
		public RUIContext() {
		}
		
		
		@Override
		public void run(final boolean fork, final boolean cancelable,
				final IRunnableWithProgress runnable)
				throws InvocationTargetException, InterruptedException {
			final IRunnableContext runnableContext= RDataTableViewer.this
					.callbacks.getServiceLocator().getService(IProgressService.class);
			runnableContext.run(fork, cancelable, new IRunnableWithProgress() {
				@Override
				public void run(final IProgressMonitor monitor)
						throws InvocationTargetException, InterruptedException {
					RDataTableViewer.this.dataProvider.beginOperation(this);
					try {
						runnable.run(monitor);
					}
					finally {
						RDataTableViewer.this.dataProvider.endOperation(this);
					}
				}
			});
		}
		
		@Override
		public void show(final IStatus status) {
			StatusManager.getManager().handle(status, StatusManager.SHOW);
		}
		
	}
	
	
	private final Display display;
	
	private final Composite composite;
	private final StackLayout layout;
	
	private final Label messageControl;
	
	private @Nullable Composite reloadControl;
	
	private final RDataTableCallbacks callbacks;
	
	private RDataTableInput input;
	
	private AbstractRDataProvider<?> dataProvider;
	private @Nullable TableLayers tableLayers;
	private boolean tableInitialized;
	
	private @Nullable DataViewDescription dataViewDescription;
	
	private PositionCoordinate currentAnchor;
	private @Nullable PositionCoordinate currentLastSelectedCell;
	private final CopyOnWriteList<FindListener> findListeners= new CopyOnWriteList<>();
	private final CopyOnWriteList<RDataTableListener> tableListeners= new CopyOnWriteList<>();
	
	private final RDataFormatter formatter= new RDataFormatter();
	
	private @Nullable ResolveCellIndexes setAnchorByData;
	
	
	/**
	 * @param parent a widget which will be the parent of the new instance (cannot be null)
	 */
	public RDataTableViewer(final Composite parent, final RDataTableCallbacks callbacks) {
		super(parent.getDisplay(), NO_SELECTION);
		this.composite= new Composite(parent, SWT.NONE);
		
		if (callbacks == null) {
			throw new NullPointerException("callbacks"); //$NON-NLS-1$
		}
		
		this.display= this.composite.getDisplay();
		this.callbacks= callbacks;
		
		this.layout= new StackLayout();
		this.composite.setLayout(this.layout);
		
		this.messageControl= new Label(this.composite, SWT.NONE);
		showDummy("Preparing...");
		
		initSelectionController(NO_SELECTION, 50);
	}
	
	
	@Override
	public final Control getControl() {
		return this.composite;
	}
	
	@Override
	public final boolean isDisposed() {
		return this.composite.isDisposed();
	}
	
	
	@Override
	public RDataTableInput getInput() {
		return this.input;
	}
	
	@Override
	public void setInput(final @Nullable Object input) {
		throw new UnsupportedOperationException();
	}
	
	protected void initTable(final RDataTableInput input,
			final AbstractRDataProvider<? extends RObject> dataProvider) {
		this.input= input;
		this.dataProvider= dataProvider;
		
		final PresentationConfig presentation= PresentationConfig.getInstance(this.display);
		
		final TableLayers layers= new TableLayers();
		
		layers.dataLayer= new RDataLayer(dataProvider, presentation.getBaseSizeConfig());
		
		if (!this.dataProvider.getAllColumnsEqual()) {
//			final ColumnOverrideLabelAccumulator columnLabelAccumulator=
//					new ColumnOverrideLabelAccumulator(dataLayer);
//			for (long i= 0; i < this.dataProvider.getColumnCount(); i++) {
//				columnLabelAccumulator.registerColumnOverrides(i, ColumnLabelAccumulator.COLUMN_LABEL_PREFIX + i);
//			}
			final AggregrateConfigLabelAccumulator aggregateLabelAccumulator=
					new AggregrateConfigLabelAccumulator();
//			aggregateLabelAccumulator.add(columnLabelAccumulator);
//			dataLayer.setConfigLabelAccumulator(aggregateLabelAccumulator);
		}
		
//		final WColumnReorderLayer columnReorderLayer= new WColumnReorderLayer(dataLayer);
//		final ColumnHideShowLayer columnHideShowLayer= new ColumnHideShowLayer(columnReorderLayer);
		
		layers.selectionLayer= new SelectionLayer(layers.dataLayer, false);
		layers.selectionLayer.addConfiguration(new UIBindings.SelectionConfiguration());
		layers.selectionLayer.addConfiguration(new DefaultTickUpdateConfiguration());
		
		layers.viewportLayer= new ViewportLayer(layers.selectionLayer);
		
		{	final FreezeLayer freezeLayer= new FreezeLayer(layers.selectionLayer);
			final CompositeFreezeLayer compositeFreezeLayer= new CompositeFreezeLayer(freezeLayer,
					layers.viewportLayer, layers.selectionLayer, true);
			layers.topBodyLayer= compositeFreezeLayer;
		}
		
		{	final DataProvider headerDataProvider= dataProvider.getColumnDataProvider();
			layers.dataColumnHeaderLayer= (headerDataProvider instanceof SpanningDataProvider) ?
				new SpanningDataLayer((SpanningDataProvider) headerDataProvider,
						PositionId.BODY_CAT, 10,
						PositionId.HEADER_CAT, 10 ) :
				new DataLayer(headerDataProvider,
						PositionId.BODY_CAT, 10,
						PositionId.HEADER_CAT, 10 );
			
			final ColumnHeaderLayer layer= new ColumnHeaderLayer(
					layers.dataColumnHeaderLayer,
					layers.topBodyLayer, layers.selectionLayer,
					false, presentation.getHeaderLayerPainter() );
			layer.addConfiguration(new UIBindings.ColumnHeaderConfiguration());
			layers.topColumnHeaderLayer= layer;
		}
		final SortModel sortModel= dataProvider.getSortModel();
		if (sortModel != null) {
			final SortHeaderLayer<?> sortHeaderLayer= new SortHeaderLayer<>(
					layers.topColumnHeaderLayer, sortModel, false);
			sortHeaderLayer.addConfiguration(new UIBindings.SortConfiguration());
			layers.topColumnHeaderLayer= sortHeaderLayer;
		}
		{	final DataProvider headerDataProvider= dataProvider.getRowDataProvider();
			layers.dataRowHeaderLayer= (headerDataProvider instanceof SpanningDataProvider) ?
					new SpanningDataLayer((SpanningDataProvider) headerDataProvider,
							PositionId.HEADER_CAT, 10,
							PositionId.BODY_CAT, 10 ) :
					new DataLayer(headerDataProvider,
							PositionId.HEADER_CAT, 10,
							PositionId.BODY_CAT, 10 );
			
			layers.topRowHeaderLayer= new RowHeaderLayer(
					layers.dataRowHeaderLayer,
					layers.topBodyLayer, layers.selectionLayer,
					false, presentation.getHeaderLayerPainter() );
		}
		final DataProvider cornerDataProvider= new DefaultCornerDataProvider(
				layers.dataColumnHeaderLayer.getDataProvider(),
				layers.dataRowHeaderLayer.getDataProvider() );
		
		final GridLayer gridLayer;
		if (dataProvider.getColumnLabelProvider() != null || dataProvider.getRowLabelProvider() != null) {
			layers.topColumnHeaderLayer= new ExtColumnHeaderLayer(layers.topColumnHeaderLayer);
			layers.topRowHeaderLayer= new ExtRowHeaderLayer(layers.topRowHeaderLayer);
			final CornerLayer cornerLayer= new LabelCornerLayer(
					new DataLayer(cornerDataProvider,
							PositionId.HEADER_CAT, 10,
							PositionId.HEADER_CAT, 10 ),
					layers.topRowHeaderLayer, layers.topColumnHeaderLayer,
					dataProvider.getColumnLabelProvider(), dataProvider.getRowLabelProvider(),
					false, presentation.getHeaderLabelLayerPainter() );
			gridLayer= new ExtGridLayer(layers.topBodyLayer,
					layers.topColumnHeaderLayer, layers.topRowHeaderLayer,
					cornerLayer, false );
		}
		else {
			final CornerLayer cornerLayer= new CornerLayer(
					new DataLayer(cornerDataProvider, PositionId.HEADER_CAT),
					layers.topRowHeaderLayer, layers.topColumnHeaderLayer,
					false, presentation.getHeaderLayerPainter() );
			gridLayer= new GridLayer(layers.topBodyLayer,
					layers.topColumnHeaderLayer, layers.topRowHeaderLayer, cornerLayer, false);
		}
		gridLayer.addCellLabelContributor(GridLabels.BODY, new AlternatingRowLabelContributor());
		
		final Runnable configRunnable= new Runnable() {
			@Override
			public void run() {
				final TableLayers layers= RDataTableViewer.this.tableLayers;
				if (layers == null) {
					return;
				}
				
				final LayoutSizeConfig sizeConfig= presentation.getBaseSizeConfig();
				
				layers.dataLayer.setSizeConfig(sizeConfig);
				layers.dataColumnHeaderLayer.setDefaultRowHeight(sizeConfig.getRowHeight());
				layers.dataRowHeaderLayer.setDefaultColumnWidth(sizeConfig.getCharWidth() * 8 + sizeConfig.getDefaultSpace() * 2);
				if (layers.topColumnHeaderLayer instanceof ExtColumnHeaderLayer) {
					((ExtColumnHeaderLayer) layers.topColumnHeaderLayer).setSpaceSize(sizeConfig.getRowHeight());
					((ExtRowHeaderLayer) layers.topRowHeaderLayer).setSpaceSize(sizeConfig.getRowHeight());
				}
				
				presentation.configureRegistry(layers.table.getConfigRegistry());
				
				layers.table.updateResize();
			}
		};
		presentation.addListener(configRunnable);
		
		final ITableUIContext uiContext= new RUIContext();
//		{	final ILayerCommandHandler<?> commandHandler= new ScrollCommandHandler(fTableLayers.viewportLayer);
//			fTableLayers.viewportLayer.registerCommandHandler(commandHandler);
//		}
		{	final LayerCommandHandler<?> commandHandler= new SelectRelativeCommandHandler(
					layers.selectionLayer );
			layers.viewportLayer.registerCommandHandler(commandHandler);
			layers.selectionLayer.registerCommandHandler(commandHandler);
		}
		{	final LayerCommandHandler<?> commandHandler= new CopyToClipboardCommandHandler(
					layers.selectionLayer, uiContext );
			layers.selectionLayer.registerCommandHandler(commandHandler);
		}
		{	final LayerCommandHandler<?> commandHandler= new InitializeAutoResizeCommandHandler(
					layers.selectionLayer );
			gridLayer.registerCommandHandler(commandHandler);
		}
		if (sortModel != null) {
			final LayerCommandHandler<?> commandHandler= new SortPositionCommandHandler(sortModel);
			layers.dataLayer.registerCommandHandler(commandHandler);
		}
		if (sortModel != null) {
			final LayerCommandHandler<?> commandHandler= new ClearSortCommandHandler(sortModel);
			layers.dataLayer.registerCommandHandler(commandHandler);
		}
		
		layers.table= new DataTable(this.composite, gridLayer);
		layers.table.addConfiguration(new UIBindings.HeaderContextMenuConfiguration(layers.table,
				this.callbacks.getWorkbenchPart() ));
		layers.table.addConfiguration(new UIBindings.BodyContextMenuConfiguration(layers.table,
				this.callbacks.getServiceLocator() ));
		
		layers.table.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				dataProvider.dispose();
			}
		});
		
		final ConfigRegistry registry= layers.table.getConfigRegistry();
		registry.registerAttribute(CellConfigAttributes.DISPLAY_CONVERTER,
				new RDataFormatterConverter(dataProvider));
		registry.registerAttribute(CellConfigAttributes.DISPLAY_CONVERTER,
				new RDataFormatterConverter.RowHeader(dataProvider),
				DisplayMode.NORMAL, GridLabels.ROW_HEADER);
		
		this.tableLayers= layers;
		this.tableInitialized= false;
		
		configRunnable.run();
		
		layers.table.addLayerListener(new LayerListener() {
			@Override
			public void handleLayerEvent(final LayerEvent event) {
				if (event instanceof org.eclipse.statet.ecommons.waltable.selection.core.SelectionEvent) {
					refreshSelection(0, UpdateType.DEFAULT_DELAYED_POST, true);
					return;
				}
				if (event instanceof VisualChangeEvent) {
					refreshSelection(CHECK_LABEL, UpdateType.DEFAULT_DELAYED_POST, true);
					return;
				}
			}
		});
		dataProvider.addDataChangedListener(new AbstractRDataProvider.DataProviderListener() {
			@Override
			public void onInputInitialized(final boolean structChanged) {
				if (layers != RDataTableViewer.this.tableLayers || layers.table.isDisposed()) {
					clearDataViewDescription();
					return;
				}
				
				if (layers.table != RDataTableViewer.this.layout.topControl) {
					layers.table.configure();
				}
				
				if (layers.table != RDataTableViewer.this.layout.topControl) {
					RDataTableViewer.this.layout.topControl= layers.table;
					RDataTableViewer.this.composite.layout();
				}
				final var viewDescription= refreshDataViewDescription(layers);
				if (!RDataTableViewer.this.tableInitialized) {
					layers.selectionLayer.setSelectionToCell(0, 0, true);
					RDataTableViewer.this.tableInitialized= true;
				}
				else if (structChanged) {
					layers.dataLayer.fireLayerEvent(new DimGeneralStrucuralChangeEvent(
							layers.dataLayer.getDim(VERTICAL) ));
				}
				else {
					layers.dataLayer.fireLayerEvent(new GeneralVisualChangeEvent(
							layers.dataLayer ));
				}
				
				for (final RDataTableListener listener : RDataTableViewer.this.tableListeners) {
					listener.onInputChanged(input, dataProvider.getDescription(), viewDescription);
				}
				
				refreshSelection(CHECK_LABEL, UpdateType.DIRECT, false);
			}
			
			@Override
			public void onInputFailed(final int error) {
				if (error == ERROR_STRUCT_CHANGED) {
					showReload();
				}
				else {
					showDummy("An error occurred when loading the table input.");
					for (final RDataTableListener listener : RDataTableViewer.this.tableListeners) {
						listener.onInputChanged(null, null, null);
					}
				}
			}
			
			@Override
			public void onRowsChanged() {
				if (layers != RDataTableViewer.this.tableLayers || layers.table.isDisposed()) {
					return;
				}
				
				final DataViewDescription dataViewDescription= refreshDataViewDescription(layers);
				layers.dataLayer.fireLayerEvent(new DimGeneralStrucuralChangeEvent(
						layers.dataLayer.getDim(VERTICAL) ));
				
				for (final RDataTableListener listener : RDataTableViewer.this.tableListeners) {
					listener.onDataViewChanged(dataViewDescription);
				}
				
				refreshSelection(CHECK_LABEL, UpdateType.DIRECT, false);
			}
			
			@Override
			public void onRowsChanged(final long beginIdx, final long endIdx) {
				RDataTableViewer.this.display.asyncExec(new Runnable() {
					@Override
					public void run() {
						if (layers != RDataTableViewer.this.tableLayers || layers.table.isDisposed()) {
							return;
						}
						layers.dataLayer.fireLayerEvent(new DimPositionsVisualChangeEvent(
								layers.dataLayer.getDim(VERTICAL), new LRange(beginIdx, endIdx) ));
					}
				});
			}
			
		});
		dataProvider.addFindListener(this::handleFindEvent);
	}
	
	
	public @Nullable NatTable getTable() {
		final var tableLayers= this.tableLayers;
		return (tableLayers != null) ? tableLayers.table : null;
	}
	
	
	@Override
	protected @Nullable RDataTableSelection fetchSelectionFromWidget(final int flags,
			final RDataTableSelection prevSelection) {
		final var tableLayers= this.tableLayers;
		final var viewDescription= this.dataViewDescription;
		if (tableLayers == null || viewDescription == null) {
			return NO_SELECTION;
		}
		else {
			final var selectionLayer= tableLayers.selectionLayer;
			final PositionCoordinate anchor= selectionLayer.getSelectionAnchor();
			PositionCoordinate lastSelected= selectionLayer.getLastSelectedCellPosition();
			if (lastSelected.equals(anchor)) {
				lastSelected= null;
			}
			
			final boolean anchorChanged;
			if ((anchorChanged= !anchor.equals(this.currentAnchor))) {
				this.currentAnchor= new PositionCoordinate(anchor);
			}
			final boolean lastSelectedChanged;
			if ((lastSelectedChanged= !((lastSelected != null) ?
					lastSelected.equals(this.currentLastSelectedCell) : null == this.currentLastSelectedCell ))) {
				this.currentLastSelectedCell= (lastSelected != null) ? new PositionCoordinate(lastSelected) : null;
			}
			
			if (!anchorChanged && !lastSelectedChanged
					&& viewDescription == prevSelection.getDataViewDescription()
					&& (flags & CHECK_LABEL) == 0) {
				return null;
			}
			
			String anchorRowLabel= null;
			String anchorColumnLabel= null;
			if (this.currentAnchor.columnPosition >= 0 && this.currentAnchor.rowPosition >= 0) {
				if (anchorChanged || (flags & CHECK_LABEL) != 0) {
					anchorRowLabel= getRowLabel(this.currentAnchor.rowPosition);
					if (anchorRowLabel != null) {
						anchorColumnLabel= getColumnLabel(this.currentAnchor.columnPosition);
						if (anchorColumnLabel == null) {
							anchorRowLabel= null;
						}
					}
				}
				else {
					anchorRowLabel= prevSelection.getAnchorRowLabel();
					anchorColumnLabel= prevSelection.getAnchorColumnLabel();
				}
			}
			
			String lastSelectedRowLabel= null;
			String lastSelectedColumnLabel= null;
			if (anchorRowLabel != null) {
				final PositionCoordinate lastSelectedCell= this.currentLastSelectedCell;
				if (lastSelectedCell != null
						&& lastSelectedCell.columnPosition >= 0 && lastSelectedCell.rowPosition >= 0) {
					if (lastSelectedChanged || (flags & CHECK_LABEL) != 0) {
						lastSelectedRowLabel= getRowLabel(lastSelectedCell.rowPosition);
						if (lastSelectedRowLabel != null) {
							lastSelectedColumnLabel= getColumnLabel(lastSelectedCell.columnPosition);
							if (lastSelectedColumnLabel == null) {
								lastSelectedRowLabel= null;
							}
						}
					}
					else {
						lastSelectedRowLabel= prevSelection.getLastSelectedCellRowLabel();
						lastSelectedColumnLabel= prevSelection.getLastSelectedCellColumnLabel();
					}
				}
			}
			return new RDataTableSelection(viewDescription,
					anchorRowLabel, anchorColumnLabel,
					lastSelectedRowLabel, lastSelectedColumnLabel);
		}
	}
	
	protected @Nullable String getRowLabel(final long row) {
		if (!this.dataProvider.hasRealRows()) {
			return ""; //$NON-NLS-1$
		}
		final DataProvider dataProvider= this.dataProvider.getRowDataProvider();
		if (dataProvider.getColumnCount() <= 1) {
			return getHeaderLabel(dataProvider.getDataValue(0, row, 0, null));
		}
		final StringBuilder sb= new StringBuilder();
		for (long i= 0; i < dataProvider.getColumnCount(); i++) {
			final String label= getHeaderLabel(dataProvider.getDataValue(i, row, 0, null));
			if (label == null) {
				return null;
			}
			sb.append(label);
			sb.append(", "); //$NON-NLS-1$
		}
		return sb.substring(0, sb.length() - 2);
	}
	
	protected @Nullable String getColumnLabel(final long column) {
		if (!this.dataProvider.hasRealColumns()) {
			return ""; //$NON-NLS-1$
		}
		final DataProvider dataProvider= this.dataProvider.getColumnDataProvider();
		if (dataProvider.getRowCount() <= 1) {
			return getHeaderLabel(dataProvider.getDataValue(column, 0, 0, null));
		}
		final StringBuilder sb= new StringBuilder();
		for (long i= 0; i < dataProvider.getRowCount(); i++) {
			final String label= getHeaderLabel(dataProvider.getDataValue(column, i, 0, null));
			if (label == null) {
				return null;
			}
			sb.append(label);
			sb.append(", "); //$NON-NLS-1$
		}
		return sb.substring(0, sb.length() - 2);
	}
	
	private @Nullable String getHeaderLabel(final @Nullable Object value) {
		if (value != null) {
			if (value instanceof ControlData && value != AbstractRDataProvider.NA) {
				return null;
			}
			final Object displayValue= this.formatter.modelToDisplayValue(value);
			if (displayValue.getClass() == String.class) {
				return (String) displayValue;
			}
			if (displayValue == AbstractRDataProvider.DUMMY) {
				return ""; //$NON-NLS-1$
			}
			return null;
		}
		else {
			return this.formatter.modelToDisplayValue(null).toString();
		}
	}
	
	protected void showDummy(final String message) {
		if (isDisposed()) {
			return;
		}
		this.messageControl.setText(message);
		this.layout.topControl= this.messageControl;
		this.composite.layout();
	}
	
	
	public @Nullable DataViewDescription getDataView() {
		return this.dataViewDescription;
	}
	
	private DataViewDescription refreshDataViewDescription(final TableLayers tableLayers) {
		final var viewDescription= new DataViewDescription(
				ImCollections.newLongList(
						this.dataProvider.getFullRowCount(), this.dataProvider.getColumnCount() ),
				ImCollections.newLongList(
						this.dataProvider.getRowCount(), this.dataProvider.getColumnCount() ));
		this.dataViewDescription= viewDescription;
		return viewDescription;
	}
	
	private void clearDataViewDescription() {
		this.dataViewDescription= null;
	}
	
	public boolean isOK() {
		final var tableLayers= this.tableLayers;
		return (tableLayers != null && this.layout.topControl == tableLayers.table);
	}
	
	public ViewportLayerDim getViewport(final Orientation orientation) {
		return this.tableLayers.viewportLayer.getDim(orientation);
	}
	
	
	@Override
	public void setSelection(final ISelection selection, final boolean reveal) {
	}
	
	
	public void find(final String expression, final boolean selectedOnly, final boolean firstInRow, final boolean forward) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			final PositionCoordinate anchor= tableLayers.selectionLayer.getSelectionAnchor();
			final FindTask task= new FindTask(expression,
					anchor.getRowPosition(), anchor.getColumnPosition(), firstInRow, forward,
					(selectedOnly) ? new SelectionFindFilter() : null );
			this.dataProvider.find(task);
		}
	}
	
	public void addFindListener(final FindListener listener) {
		this.findListeners.add(listener);
	}
	
	public void removeFindListener(final FindListener listener) {
		this.findListeners.remove(listener);
	}
	
	private void handleFindEvent(final FindListener.FindEvent event) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			if (event.rowIdx >= 0) {
				tableLayers.setAnchor(event.colIdx, event.rowIdx, true);
			}
			for (final FindListener listener : RDataTableViewer.this.findListeners) {
				listener.handleFindEvent(event);
			}
		}
	}
	
	
	public void setInput(final RDataTableInput input) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			showDummy(""); //$NON-NLS-1$
			tableLayers.table.dispose();
			tableLayers.table= null;
			this.dataProvider= null;
			this.setAnchorByData= null;
		}
		if (input != null) {
			showDummy("Preparing (" + input.getName() + ")...");
			try {
				final ToolRunnable runnable= new SystemRunnable() {
					
					@Override
					public String getTypeId() {
						return "r/dataeditor/init"; //$NON-NLS-1$
					}
					
					@Override
					public String getLabel() {
						return "Prepare Data Viewer (" + input.getName() + ")";
					}
					
					@Override
					public boolean canRunIn(final Tool tool) {
						return true; // TODO
					}
					
					@Override
					public boolean changed(final int event, final Tool process) {
						if (event == MOVING_FROM) {
							return false;
						}
						return true;
					}
					
					@Override
					public void run(final ToolService service,
							final ProgressMonitor m) throws StatusException {
						final RToolService r= (RToolService) service;
						
						final AtomicReference<@Nullable AbstractRDataProvider<?>> dataProvider= new AtomicReference<>();
						Exception error= null;
						try {
							final RObject struct= r.evalData(input.getFullName(),
									null, RObjectFactory.F_ONLY_STRUCT, 1, m );
							RCharacterStore classNames= null;
							{	final FunctionCall call= r.createFunctionCall("class"); //$NON-NLS-1$
								call.add(input.getFullName());
								classNames= RDataUtils.checkRCharVector(call.evalData(m)).getData();
							}
							
							switch (struct.getRObjectType()) {
							case RObject.TYPE_VECTOR:
								dataProvider.set(new RVectorDataProvider(input, (RVector<?>)struct));
								break;
							case RObject.TYPE_ARRAY: {
								final RArray<?> array= (RArray<?>) struct;
								if (array.getDim().getLength() == 2) {
									if (classNames.contains("ftable")) { //$NON-NLS-1$
										dataProvider.set(new FTableDataProvider(input, array));
										break;
									}
									dataProvider.set(new RMatrixDataProvider(input, array));
									break;
								}
								break; }
							case RObject.TYPE_DATAFRAME:
								dataProvider.set(new RDataFrameDataProvider(input, (RDataFrame)struct));
								break;
							default:
								break;
							}
						}
						catch (final CoreException | UnexpectedRDataException e) {
							error= e;
						}
						final IStatus status;
						if (error != null) {
							status= new org.eclipse.core.runtime.Status(IStatus.ERROR, RUI.BUNDLE_ID,
									"An error occurred when preparing the R data viewer.", error );
							StatusManager.getManager().handle(status);
						}
						else if (dataProvider.get() == null) {
							status= new org.eclipse.core.runtime.Status(IStatus.ERROR, RUI.BUNDLE_ID,
									"This R element type is not supported.", null );
						}
						else {
							status= null;
						}
						RDataTableViewer.this.display.asyncExec(() -> {
							RDataTableViewer.this.dataProvider= null;
							if (isDisposed()) {
								return;
							}
							if (status == null) {
								initTable(input, dataProvider.get());
							}
							else {
								showDummy(status.getMessage());
							}
						});
					}
				};
				final Status status= input.getTool().getQueue().add(runnable);
				if (status.getSeverity() >= Status.ERROR) {
					throw new StatusException(status);
				}
			}
			catch (final StatusException e) {
				showDummy(e.getLocalizedMessage());
			}
		}
	}
	
	protected void showReload() {
		Composite reloadControl= this.reloadControl;
		if (reloadControl == null) {
			reloadControl= new Composite(this.composite, SWT.NONE);
			reloadControl.setLayout(LayoutUtils.newCompositeGrid(4));
			
			final Label label= new Label(reloadControl, SWT.WRAP);
			label.setText("The structure of the R element is changed (columns / data type).");
			label.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 4, 1));
			
			{	final Button button= new Button(reloadControl, SWT.PUSH);
				button.setLayoutData(LayoutUtils.hintWidth(new GridData(
						SWT.FILL, SWT.CENTER, false, false), button));
				button.setText("Refresh");
				button.setToolTipText("Refresh table with old structure");
				button.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(final SelectionEvent e) {
						refresh();
					}
				});
			}
			{	final Button button= new Button(reloadControl, SWT.PUSH);
				button.setLayoutData(LayoutUtils.hintWidth(new GridData(
						SWT.FILL, SWT.CENTER, false, false), button));
				button.setText("Reopen");
				button.setToolTipText("Reopen table with new structure");
				button.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(final SelectionEvent e) {
						setInput(RDataTableViewer.this.input);
					}
				});
			}
			if (this.callbacks.isCloseSupported()) {
				final Button button= new Button(reloadControl, SWT.PUSH);
				button.setLayoutData(LayoutUtils.hintWidth(new GridData(
						SWT.FILL, SWT.CENTER, false, false), button));
				button.setText("Close");
				button.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(final SelectionEvent e) {
						RDataTableViewer.this.callbacks.close();
					}
				});
			}
//			
			LayoutUtils.addSmallFiller(reloadControl, true);
			
			this.reloadControl= reloadControl;
		}
		this.layout.topControl= reloadControl;
		this.composite.layout(true);
	}
	
	
	public void setFilter(final @Nullable String filter) {
		this.dataProvider.setFilter(filter);
	}
	
	
	public RDataTableContentDescription getDescription() {
		return this.dataProvider.getDescription();
	}
	
	public void addTableListener(final RDataTableListener listener) {
		this.tableListeners.add(listener);
		final var tableLayers= this.tableLayers;
		final var dataView= this.dataViewDescription;
		if (tableLayers != null && dataView != null) {
			listener.onInputChanged(this.input, this.dataProvider.getDescription(), dataView);
		}
	}
	
	public void removeTableListener(final RDataTableListener listener) {
		this.tableListeners.remove(listener);
	}
	
	
	public boolean setFocus() {
		Control control;
		return ((this.layout != null && (control= this.layout.topControl) != null
						&& control.forceFocus() )
				|| this.composite.forceFocus() );
	}
	
	public void revealColumn(final long index) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			tableLayers.viewportLayer.getDim(HORIZONTAL).movePositionIntoViewport(index);
		}
	}
	
	public void selectColumns(final Collection<LRange> indexes) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			final LRangeList columns= LRangeList.toRangeList(indexes);
//			final long rowIndex= this.tableBodyLayerStack.getViewportLayer().getRowIndexByPosition(0);
			final long rowIndex= 0;
			tableLayers.table.doCommand(new SelectDimPositionsCommand(
					tableLayers.selectionLayer.getDim(HORIZONTAL),
					0, columns, rowIndex,
					0,
					!(columns.isEmpty()) ? columns.values().first() : SelectionLayer.NO_SELECTION ));
		}
	}
	
	public void setAnchorViewIdxs(final long columnIndex, final long rowIndex) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			tableLayers.selectionLayer.setSelectionAnchor(columnIndex, rowIndex, true);
		}
	}
	
	public long @Nullable [] getAnchorDataIdxs() {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			final PositionCoordinate coordinate= tableLayers.selectionLayer.getSelectionAnchor();
			if (coordinate.columnPosition < 0 || coordinate.rowPosition < 0) {
				return null;
			}
			return this.dataProvider.toDataIdxs(coordinate.columnPosition, coordinate.rowPosition);
		}
		return null;
	}
	
	public void setAnchorDataIdxs(final long columnIdx, final long rowIdx) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			ResolveCellIndexes data= this.setAnchorByData;
			if (data == null) {
				data= new SetAnchorByDataIndexes(this.dataProvider);
				this.setAnchorByData= data;
			}
			data.resolve(columnIdx, rowIdx);
		}
	}
	
	public void selectAll() {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			tableLayers.table.doCommand(new SelectAllCommand());
		}
	}
	
	public void sortByColumn(final long index, final boolean increasing) {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			tableLayers.dataLayer.doCommand(new SortDimPositionCommand(
					tableLayers.dataLayer.getDim(HORIZONTAL), index,
					increasing ? SortDirection.ASC : SortDirection.DESC, false ));
//			final ISortModel sortModel= this.fDataProvider.getSortModel();
//			if (sortModel != null) {
//				sortModel.sort(
//						PositionId.BODY_CAT | index, increasing ? SortDirection.ASC : SortDirection.DESC, false );
//				this.fTableLayers.topColumnHeaderLayer.fireLayerEvent(
//						new SortColumnEvent(this.fTableLayers.topColumnHeaderLayer.getDim(HORIZONTAL), 0) );
//			}
		}
	}
	
	public void clearSorting() {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			tableLayers.table.doCommand(new ClearSortCommand());
		}
	}
	
	@Override
	public void refresh() {
		final var tableLayers= this.tableLayers;
		if (tableLayers != null) {
			this.dataProvider.reset();
			tableLayers.table.redraw();
		}
	}
	
}
