/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.SystemRunnable;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.r.console.core.RProcessREnvironment;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.services.FQRObject;
import org.eclipse.statet.rj.services.RService;
import org.eclipse.statet.rj.ts.core.AbstractRToolCommandHandler;
import org.eclipse.statet.rj.ts.core.AbstractRToolRunnable;
import org.eclipse.statet.rj.ts.core.RTool;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class ShowRElementCommandHandler extends AbstractRToolCommandHandler {
	
	
	private static class CheckRunnable extends AbstractRToolRunnable implements SystemRunnable {
		
		
		private final RTool tool;
		
		private final RElementName elementName;
		
		
		public CheckRunnable(final RTool tool, final RElementName elementName) {
			super("r/dataeditor/open", "Show R Element"); //$NON-NLS-1$
			this.tool= tool;
			this.elementName= elementName;
		}
		
		
		@Override
		public boolean canRunIn(final Tool tool) {
			return (tool == this.tool);
		}
		
		@Override
		public boolean changed(final int event, final Tool tool) {
			switch (event) {
			case MOVING_FROM:
				return false;
			default:
				return true;
			}
		}
		
		@Override
		public void run(final RToolService service, final ProgressMonitor m) throws StatusException {
			final FQRObject<?> data= service.findData(this.elementName.getSegmentName(),
					null, true, "combined", RObjectFactory.F_ONLY_STRUCT, RService.DEPTH_REFERENCE,
					m );
			if (data == null) {
				return;
			}
			final RProcessREnvironment foundEnv= (RProcessREnvironment) data.getEnv();
			final List<RElementName> segments= new ArrayList<>();
			segments.add(foundEnv.getElementName());
			RElementName.addSegments(segments, this.elementName);
			doOpen(this.tool, RElementName.create(segments));
		}
		
	}
	
	private static void doOpen(final RTool tool, final RElementName name) {
		UIAccess.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(true);
				if (page == null) {
					return;
				}
				RDataEditor.open(page, tool, name, null);
			}
		});
	}
	
	
	public static final String SHOW_ELEMENT_COMMAND_ID= "r/showElement"; //$NON-NLS-1$
	@Deprecated
	public static final String SHOW_ELEMENT_COMMAND_OLD_ID= "showElement"; //$NON-NLS-1$
	
	
	public ShowRElementCommandHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final RToolService r, final ToolCommandData data,
			final ProgressMonitor m) throws StatusException {
		switch (id) {
		case SHOW_ELEMENT_COMMAND_ID:
		case SHOW_ELEMENT_COMMAND_OLD_ID:
			{	final RTool tool= r.getTool();
				final String elementName= data.getStringRequired("elementName");
				final RElementName rElementName= RElementName.parseDefault(elementName);
				if (rElementName != null) {
					if (rElementName.getScope() != null) {
						doOpen(tool, rElementName);
					}
					else {
						tool.getQueue().addHot(new CheckRunnable(tool, rElementName));
					}
				}
				return Status.OK_STATUS;
			}
			
		default:
			throw new UnsupportedOperationException();
		}
	}
	
}
