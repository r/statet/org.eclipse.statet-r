/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.editors;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;

import org.eclipse.statet.internal.r.ui.RUIPreferenceInitializer;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.r.core.project.RIssues;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.ui.RUI;


@NonNullByDefault
public class REditorBuild {
	
	
	public static final String GROUP_ID = "r/r.editor/build.options"; //$NON-NLS-1$
	
	
	public static final BooleanPref PROBLEMCHECKING_ENABLED_PREF = new BooleanPref(
			RUIPreferenceInitializer.REDITOR_NODE, "ProblemChecking.enabled"); //$NON-NLS-1$
	
	
	public static final String ERROR_ANNOTATION_TYPE=       "org.eclipse.statet.r.editorAnnotations.ErrorProblem"; //$NON-NLS-1$
	public static final String WARNING_ANNOTATION_TYPE=     "org.eclipse.statet.r.editorAnnotations.WarningProblem"; //$NON-NLS-1$
	public static final String INFO_ANNOTATION_TYPE=        "org.eclipse.statet.r.editorAnnotations.InfoProblem"; //$NON-NLS-1$
	
	private static final IssueTypeSet.ProblemTypes PROBLEM_ANNOTATION_TYPES= new IssueTypeSet.ProblemTypes(
			ERROR_ANNOTATION_TYPE, WARNING_ANNOTATION_TYPE, INFO_ANNOTATION_TYPE );
	
	public static final IssueTypeSet.ProblemCategory R_MODEL_PROBLEM_CATEGORY= new IssueTypeSet.ProblemCategory(
			RModel.R_TYPE_ID,
			RIssues.R_MODEL_PROBLEM_MARKER_TYPES, PROBLEM_ANNOTATION_TYPES );
	
	public static final IssueTypeSet R_ISSUE_TYPE_SET= new IssueTypeSet(RUI.BUNDLE_ID,
			RIssues.TASK_CATEGORY,
			ImCollections.newList(
					R_MODEL_PROBLEM_CATEGORY ));
	
}
