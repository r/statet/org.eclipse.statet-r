/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.editors;

import java.util.List;
import java.util.Map;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.menus.IWorkbenchContribution;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.ecommons.ui.actions.ListContributionItem;

import org.eclipse.statet.internal.r.ui.search.Messages;
import org.eclipse.statet.ltk.ui.LtkActions;


public class RElementSearchContributionItem extends ListContributionItem
		implements IWorkbenchContribution {
	
	
	public static class All extends RElementSearchContributionItem {
		
		public All() {
			super(LtkActions.SEARCH_ALL_ELEMENT_ACCESS_COMMAND_ID);
		}
		
		
	}
	
	public static class Write extends RElementSearchContributionItem {
		
		public Write() {
			super(LtkActions.SEARCH_WRITE_ELEMENT_ACCESS_COMMAND_ID);
		}
		
		
	}
	
	
	private final String commandId;
	
	private IServiceLocator serviceLocator;
	
	
	public RElementSearchContributionItem(final String commandId) {
		super();
		
		this.commandId= commandId;
	}
	
	
	@Override
	public void initialize(final IServiceLocator serviceLocator) {
		this.serviceLocator= serviceLocator;
	}
	
	@Override
	public void createContributionItems(final List<IContributionItem> items) {
		items.add(new CommandContributionItem(
				new CommandContributionItemParameter(this.serviceLocator,
						null, this.commandId, Map.of(
								LtkActions.SEARCH_SCOPE_PARAMETER_ID, LtkActions.SEARCH_SCOPE_WORKSPACE_PARAMETER_VALUE ),
						null, null, null,
						Messages.menus_Scope_Workspace_name, Messages.menus_Scope_Workspace_mnemonic, null,
						CommandContributionItem.STYLE_PUSH, null, false )));
		items.add(new CommandContributionItem(
				new CommandContributionItemParameter(this.serviceLocator,
						null, this.commandId, Map.of(
								LtkActions.SEARCH_SCOPE_PARAMETER_ID, LtkActions.SEARCH_SCOPE_PROJECT_PARAMETER_VALUE ),
						null, null, null,
						Messages.menus_Scope_Project_name, Messages.menus_Scope_Project_mnemonic, null,
						CommandContributionItem.STYLE_PUSH, null, false )));
		items.add(new CommandContributionItem(
				new CommandContributionItemParameter(this.serviceLocator,
						null, this.commandId, Map.of(
								LtkActions.SEARCH_SCOPE_PARAMETER_ID, LtkActions.SEARCH_SCOPE_FILE_PARAMETER_VALUE ),
						null, null, null,
						Messages.menus_Scope_File_name, Messages.menus_Scope_File_mnemonic, null,
						CommandContributionItem.STYLE_PUSH, null, false )));
	}
	
}
