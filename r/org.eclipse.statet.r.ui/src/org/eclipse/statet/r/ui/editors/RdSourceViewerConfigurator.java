/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.editors;

import java.util.Set;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.base.core.preferences.TaskTagsPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.rj.renv.core.REnv;


/**
 * Configurator for Rd source viewers.
 */
public class RdSourceViewerConfigurator extends SourceEditorViewerConfigurator
		implements RCoreAccess {
	
	
	private static final Set<String> RESET_GROUPS_IDS= ImCollections.newSet(
			TaskTagsPreferences.GROUP_ID );
	
	
	private RCoreAccess fSourceCoreAccess;
	
	
	public RdSourceViewerConfigurator(final RCoreAccess core,
			final RdSourceViewerConfiguration config) {
		super(config);
		config.setCoreAccess(this);
		setSource(core);
	}
	
	
	@Override
	public IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new RdDocumentSetupParticipant();
	}
	
	@Override
	protected Set<String> getResetGroupIds() {
		return RESET_GROUPS_IDS;
	}
	
	
	public void setSource(RCoreAccess newAccess) {
		if (newAccess == null) {
			newAccess = RCore.getWorkbenchAccess();
		}
		if (this.fSourceCoreAccess != newAccess) {
			this.fSourceCoreAccess = newAccess;
			handleSettingsChanged(null, null);
		}
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.fSourceCoreAccess.getPrefs();
	}
	
	@Override
	public REnv getREnv() {
		return this.fSourceCoreAccess.getREnv();
	}
	
	@Override
	public @NonNull RSourceConfig getRSourceConfig() {
		return this.fSourceCoreAccess.getRSourceConfig();
	}
	
	@Override
	public RCodeStyleSettings getRCodeStyle() {
		return null;
	}
	
}
