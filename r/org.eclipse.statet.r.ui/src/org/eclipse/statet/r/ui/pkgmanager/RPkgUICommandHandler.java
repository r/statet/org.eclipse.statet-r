/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.pkgmanager;

import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.rj.ts.core.AbstractRToolCommandHandler;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class RPkgUICommandHandler extends AbstractRToolCommandHandler {
	
	
	public static final String OPEN_PACKAGE_MANAGER_COMMAND_ID= "openPackageManager"; //$NON-NLS-1$
	
	
	public RPkgUICommandHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final RToolService r, final ToolCommandData data,
			final ProgressMonitor m) {
		switch (id) {
		case OPEN_PACKAGE_MANAGER_COMMAND_ID:
			final IWorkbenchPage page= NicoUI.getToolRegistry().findWorkbenchPage(r.getTool());
			
			return RPkgManagerUI.openDialog((RProcess)r.getTool(),
					page.getWorkbenchWindow().getShell(), 0, null );
			
		default:
			throw new UnsupportedOperationException();
		}
	}
	
}
