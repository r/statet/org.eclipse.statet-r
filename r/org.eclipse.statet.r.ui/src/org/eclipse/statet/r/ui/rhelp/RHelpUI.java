/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rhelp;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIResources;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.r.ui.RUI;


@NonNullByDefault
public class RHelpUI {
	
	
	private static final String NS= "org.eclipse.statet.r.ui.rhelp"; //$NON-NLS-1$
	
	
	public static final String RHELP_VIEW_IMAGE_ID=                NS + "/image/view/RHelp"; //$NON-NLS-1$
	
	public static final String RPACKAGE_OBJ_IMAGE_ID=              RUI.IMG_OBJ_R_PACKAGE;
	
	public static final String RHELP_PAGE_OBJ_IMAGE_ID=            NS + "/image/obj/Page"; //$NON-NLS-1$
	public static final String RHELP_PAGE_INTERNAL_OBJ_IMAGE_ID =  NS + "/image/obj/Page.internal"; //$NON-NLS-1$
	public static final String RHELP_TOPIC_OBJ_IMAGE_ID=           NS + "/image/obj/Topic"; //$NON-NLS-1$
	public static final String RHELP_TOPIC_INTERNAL_OBJ_IMAGE_ID=  NS + "/image/obj/Topic.internal"; //$NON-NLS-1$
	
	public static final String RHELP_SEARCH_OBJ_IMAGE_ID=          NS + "/image/obj/Search"; //$NON-NLS-1$
	
	
	private static class UIInstance {
		private static final UIResources RESOURCES= new UIResources(RUIPlugin.getInstance().getImageRegistry());
	}
	
	public static UIResources getUIResources() {
		return UIInstance.RESOURCES;
	}
	
	
	private RHelpUI() {
	}
	
}
