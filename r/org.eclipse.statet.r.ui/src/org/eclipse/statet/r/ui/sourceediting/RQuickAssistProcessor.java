/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.quickassist.IQuickAssistInvocationContext;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.editors.RQuickRefactoringComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistProcessor;
import org.eclipse.statet.r.core.source.RHeuristicTokenScanner;
import org.eclipse.statet.r.ui.editors.RSourceEditor;


@NonNullByDefault
public class RQuickAssistProcessor extends QuickAssistProcessor {
	
	
	private final QuickAssistComputer computer= new RQuickRefactoringComputer();
	
	private @Nullable RHeuristicTokenScanner scanner;
	
	
	public RQuickAssistProcessor(final RSourceEditor editor) {
		super(editor);
	}
	
	
	@Override
	protected AssistInvocationContext createContext(final IQuickAssistInvocationContext invocationContext,
			final String contentType,
			final IProgressMonitor monitor) {
//		if (this.scanner == null) {
//			this.scanner= RHeuristicTokenScanner.create(getEditor().getDocumentContentInfo());
//		}
		return new RAssistInvocationContext((RSourceEditor)getEditor(),
				invocationContext.getOffset(), contentType, true, this.scanner, monitor );
	}
	
	@Override
	protected void addModelAssistProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		this.computer.computeAssistProposals(context, proposals, monitor);
	}
	
}
