/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import java.util.Map;
import java.util.Set;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.base.core.preferences.TaskTagsPreferences;
import org.eclipse.statet.internal.r.ui.RUIPreferenceInitializer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.doc.RDocumentSetupParticipant;
import org.eclipse.statet.r.core.util.RCoreAccessWrapper;
import org.eclipse.statet.r.ui.editors.REditorOptions;


/**
 * Configurator for R code source viewers.
 */
public class RSourceViewerConfigurator extends SourceEditorViewerConfigurator {
	
	
	private static final Set<String> RESET_GROUP_IDS= ImCollections.newSet(
			RCodeStyleSettings.INDENT_GROUP_ID,
			TaskTagsPreferences.GROUP_ID );
	
	
	private RCoreAccessWrapper rCoreAccess;
	
	
	public RSourceViewerConfigurator(final RCoreAccess coreAccess,
			final RSourceViewerConfiguration config) {
		super(config);
		this.rCoreAccess= new RCoreAccessWrapper(coreAccess) {
			private final RCodeStyleSettings codeStyle= new RCodeStyleSettings(1);
			@Override
			public RCodeStyleSettings getRCodeStyle() {
				return this.codeStyle;
			}
		};
		config.setCoreAccess(this.rCoreAccess);
		
		this.rCoreAccess.getRCodeStyle().load(this.rCoreAccess.getParent().getRCodeStyle());
		this.rCoreAccess.getRCodeStyle().resetDirty();
		this.rCoreAccess.getRCodeStyle().addPropertyChangeListener(this);
	}
	
	
	public final RCoreAccess getRCoreAccess() {
		return this.rCoreAccess;
	}
	
	@Override
	public IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new RDocumentSetupParticipant();
	}
	
	
	@Override
	protected Set<String> getResetGroupIds() {
		return RESET_GROUP_IDS;
	}
	
	
	public void setSource(final RCoreAccess rCoreAccess) {
		if (rCoreAccess != null) {
			this.rCoreAccess.setParent(rCoreAccess);
			handleSettingsChanged(null, null);
		}
	}
	
	
	@Override
	public void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		
		this.rCoreAccess.getRCodeStyle().resetDirty();
	}
	
	@Override
	protected void checkSettingsChanges(final Set<String> groupIds, final Map<String, Object> options) {
		super.checkSettingsChanges(groupIds, options);
		
		if (groupIds.contains(RCodeStyleSettings.INDENT_GROUP_ID)
				|| groupIds.contains(RCodeStyleSettings.WS_GROUP_ID)) {
			this.rCoreAccess.getRCodeStyle().load(
					this.rCoreAccess.getParent().getRCodeStyle() );
		}
		if (groupIds.contains(REditorOptions.GROUP_ID)) {
			this.updateCompleteConfig = true;
		}
		if (groupIds.contains(RUIPreferenceInitializer.REDITOR_HOVER_GROUP_ID)) {
			this.updateInfoHovers = true;
		}
	}
	
}
