/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.text.r;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * Groups of terminals and keys (suffix KEY) for text tokens recognized by text-parser
 * (syntax-highlighting)
 */
public interface IRTextTokens {
	
	static final ImList<RTerminal> SYMBOL= ImCollections.newList(
			RTerminal.SYMBOL );
//			RTerminal.SYMBOL_G ); // colored like a string because of partitioning
	
	static final ImList<RTerminal> STRING= ImCollections.newList(
			RTerminal.STRING_D, RTerminal.STRING_S,
			RTerminal.STRING_R );
	
	static final ImList<RTerminal> NUM= ImCollections.newList(
			RTerminal.NUM_NUM );
	
	static final ImList<RTerminal> NUM_SUB_INT= ImCollections.newList(
			RTerminal.NUM_INT );
	
	static final ImList<RTerminal> NUM_SUB_CPLX= ImCollections.newList(
			RTerminal.NUM_CPLX );
	
	static final ImList<RTerminal> SPECIALCONST= ImCollections.newList(
			RTerminal.NULL,
			RTerminal.NA,
			RTerminal.NA_REAL,
			RTerminal.NA_INT,
			RTerminal.NA_CPLX,
			RTerminal.NA_CHAR,
			RTerminal.INF,
			RTerminal.NAN );
	
	static final ImList<RTerminal> LOGICALCONST= ImCollections.newList(
			RTerminal.TRUE,
			RTerminal.FALSE );
	
	static final ImList<RTerminal> FLOWCONTROL= ImCollections.newList(
			RTerminal.IF, RTerminal.ELSE,
			RTerminal.FOR, RTerminal.IN,
			RTerminal.WHILE,
			RTerminal.REPEAT,
			RTerminal.NEXT, RTerminal.BREAK,
			RTerminal.FUNCTION, RTerminal.FUNCTION_B );
	
	static final ImList<RTerminal> GROUPING= ImCollections.newList(
			RTerminal.BLOCK_OPEN, RTerminal.BLOCK_CLOSE,
			RTerminal.GROUP_OPEN, RTerminal.GROUP_CLOSE );
	
	static final ImList<RTerminal> SEPARATOR= ImCollections.newList(
			RTerminal.COMMA,
			RTerminal.SEMICOLON );
	
	static final ImList<RTerminal> NSGET= ImCollections.newList(
			RTerminal.NS_GET,
			RTerminal.NS_GET_INT );
	
	static final ImList<RTerminal> SUBACCESS= ImCollections.newList(
			RTerminal.SUB_INDEXED_S_OPEN, RTerminal.SUB_INDEXED_D_OPEN, RTerminal.SUB_INDEXED_CLOSE,
			RTerminal.SUB_NAMED_PART,
			RTerminal.SUB_NAMED_SLOT );
	
	static final ImList<RTerminal> ASSIGN= ImCollections.newList(
			RTerminal.ARROW_LEFT_S, RTerminal.ARROW_LEFT_D,
			RTerminal.ARROW_RIGHT_S, RTerminal.ARROW_RIGHT_D,
			RTerminal.COLON_EQUAL );
			
	static final ImList<RTerminal> ASSIGN_SUB_EQUAL= ImCollections.newList(
			RTerminal.EQUAL );
	
	static final ImList<RTerminal> ASSIGN_SUB_PIPE= ImCollections.newList(
			RTerminal.PIPE_RIGHT );
	
	static final ImList<RTerminal> OP= ImCollections.newList(
			RTerminal.PLUS,
			RTerminal.MINUS,
			RTerminal.MULT,
			RTerminal.DIV,
			RTerminal.POWER,
			RTerminal.SEQ,
			RTerminal.TILDE,
			RTerminal.QUESTIONMARK );
	
	static final ImList<RTerminal> OP_SUB_LOGICAL= ImCollections.newList(
			RTerminal.NOT,
			RTerminal.AND, RTerminal.AND_D,
			RTerminal.OR, RTerminal.OR_D );
	
	static final ImList<RTerminal> OP_SUB_RELATIONAL= ImCollections.newList(
			RTerminal.REL_EQ, RTerminal.REL_NE,
			RTerminal.REL_GT, RTerminal.REL_GE,
			RTerminal.REL_LT, RTerminal.REL_LE );
	
	public static final ImList<RTerminal> UNDEFINED= ImCollections.newList(
			RTerminal.UNKNOWN );
	
	public static final ImList<RTerminal> COMMENT= ImCollections.newList(
			RTerminal.COMMENT,
			RTerminal.ROXYGEN_COMMENT );
	
	
	public static final String ROOT= "text_R_"; //$NON-NLS-1$
	
	public static final String SYMBOL_KEY= ROOT+"rDefault"; //$NON-NLS-1$
	public static final String SYMBOL_SUB_ASSIGN_KEY= SYMBOL_KEY + ".Assignment"; //$NON-NLS-1$
	public static final String SYMBOL_SUB_LOGICAL_KEY= SYMBOL_KEY + ".Logical"; //$NON-NLS-1$
	public static final String SYMBOL_SUB_FLOWCONTROL_KEY= SYMBOL_KEY + ".Flowcontrol"; //$NON-NLS-1$
	public static final String SYMBOL_SUB_CUSTOM1_KEY= SYMBOL_KEY + ".Custom2"; //$NON-NLS-1$
	public static final String SYMBOL_SUB_CUSTOM2_KEY= SYMBOL_KEY + ".Custom1"; //$NON-NLS-1$
	
	public static final String STRING_KEY= ROOT+"rString"; //$NON-NLS-1$
	public static final String NUM_KEY= ROOT+"rNumbers"; //$NON-NLS-1$
	public static final String NUM_SUB_INT_KEY= NUM_KEY+".Integer"; //$NON-NLS-1$
	public static final String NUM_SUB_CPLX_KEY= NUM_KEY+".Complex"; //$NON-NLS-1$
	
	public static final String SPECIALCONST_KEY= ROOT+"rSpecialConstants"; //$NON-NLS-1$
	public static final String LOGICALCONST_KEY= ROOT+"rLogicalConstants"; //$NON-NLS-1$
	public static final String GROUPING_KEY= ROOT+"rGrouping"; //$NON-NLS-1$
	public static final String SEPARATOR_KEY= ROOT+"rSeparators"; //$NON-NLS-1$
	public static final String SUBACCESS_KEY= ROOT+"rIndexing";	 //$NON-NLS-1$
	public static final String ASSIGN_KEY= ROOT+"rAssignment"; //$NON-NLS-1$
	public static final String ASSIGN_SUB_EQUAL_KEY= ASSIGN_KEY + ".Equalsign"; //$NON-NLS-1$
	public static final String ASSIGN_SUB_PIPE_KEY= ASSIGN_KEY + ".Pipe"; //$NON-NLS-1$
	public static final String FLOWCONTROL_KEY= ROOT+"rFlowcontrol"; //$NON-NLS-1$
	public static final String OP_KEY= ROOT+"rOtherOperators"; //$NON-NLS-1$
	public static final String OP_SUB_LOGICAL_KEY= OP_KEY + ".Logical"; //$NON-NLS-1$
	public static final String OP_SUB_RELATIONAL_KEY= OP_KEY + ".Relational"; //$NON-NLS-1$
	public static final String OP_SUB_USERDEFINED_KEY= OP_KEY + ".Userdefined"; //$NON-NLS-1$
	
	public static final String COMMENT_KEY= ROOT+"rComment"; //$NON-NLS-1$
	public static final String TASK_TAG_KEY= ROOT+"taskTag"; //$NON-NLS-1$
	public static final String ROXYGEN_KEY= ROOT + "rRoxygen"; //$NON-NLS-1$
	public static final String ROXYGEN_TAG_KEY= ROOT + "rRoxygenTag"; //$NON-NLS-1$
	public static final String UNDEFINED_KEY= ROOT+"rUndefined"; //$NON-NLS-1$
	
}
