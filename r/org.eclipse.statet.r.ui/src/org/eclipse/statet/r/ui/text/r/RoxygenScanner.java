/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.text.r;

import java.util.List;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager;

import org.eclipse.statet.ltk.ui.sourceediting.presentation.CommentScanner;


/**
 * Scanner for Roxygen comments.
 */
@NonNullByDefault
public class RoxygenScanner extends CommentScanner {
	
	
	private static class RoxygenTagRule implements IPredicateRule {
		
		private final IToken tagToken;
		private final IToken defaultToken;
		
		public RoxygenTagRule(final IToken tagToken, final IToken defaultToken) {
			this.tagToken = tagToken;
			this.defaultToken = defaultToken;
		}
		
		@Override
		public IToken getSuccessToken() {
			return this.tagToken;
		}
		
		@Override
		public IToken evaluate(final ICharacterScanner scanner) {
			return evaluate(scanner, false);
		}
		
		@Override
		public IToken evaluate(final ICharacterScanner scanner, final boolean resume) {
			int c;
			if (!resume) {
				final int c0 = scanner.read();
				if (c0 != '@') {
					if (c0 != ICharacterScanner.EOF) { 
						scanner.unread();
					}
					return Token.UNDEFINED;
				}
				c = scanner.read();
				if (c == '@') {
					return this.defaultToken;
				}
			}
			else {
				c = scanner.read();
			}
			while (c != ICharacterScanner.EOF) {
				if (c == '@' || !isRoxygenTagChar(c)) {
					scanner.unread();
					break;
				}
				c = scanner.read();
			}
			return this.tagToken;
		}
		
		private boolean isRoxygenTagChar(final int c) {
			if ((c >= 0x41 && c <= 0x5A) || (c >= 0x61 && c <= 0x7A)) {
				return true;
			}
			final int type = Character.getType(c);
			return (type > 0) && (type < 12 || type > 19);
		}
		
	}
	
	
	public RoxygenScanner(final TextStyleManager<?> textStyles, final PreferenceAccess corePrefs) {
		super(textStyles, IRTextTokens.ROXYGEN_KEY, IRTextTokens.TASK_TAG_KEY, corePrefs);
	}
	
	
	@Override
	protected void createRules(final List<IRule> rules) {
		super.createRules(rules);
		
		rules.add(new RoxygenTagRule(getToken(IRTextTokens.ROXYGEN_TAG_KEY), this.fDefaultReturnToken));
	}
	
}
