/*=============================================================================#
 # Copyright (c) 2019, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.util;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.ISources;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistry;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public abstract class AbstractREnvHandler extends AbstractHandler {
	
	
	public AbstractREnvHandler() {
	}
	
	
	protected REnv getREnv(final @Nullable Object evaluationContext) {
		REnv rEnv= null;
		final IWorkbenchPart part= (IWorkbenchPart)HandlerUtil.getVariable(evaluationContext,
				ISources.ACTIVE_PART_NAME );
		final Tool tool= (Tool)HandlerUtil.getVariable(evaluationContext,
				WorkbenchToolRegistry.ACTIVE_TOOL_SOURCE_NAME );
		if (part != null) {
			rEnv= part.getAdapter(REnv.class);
		}
		if (rEnv == null && tool != null && tool.getMainType() == RTool.TYPE) {
			rEnv= ((RTool)tool).getREnv();
		}
		if (rEnv == null) {
			rEnv= RCore.getREnvManager().getDefault();
		}
		return rEnv;
	}
	
	
	@Override
	public void setEnabled(@Nullable final Object evaluationContext) {
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final REnv rEnv= getREnv(event.getApplicationContext());
		return execute(rEnv, event);
	}
	
	protected abstract @Nullable Object execute(final REnv rEnv,
			final ExecutionEvent event) throws ExecutionException;
	
}
