/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.TextConsole;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.ui.console.NIConsole;
import org.eclipse.statet.r.launching.IRCodeSubmitConnector;
import org.eclipse.statet.r.ui.RUI;


/**
 * Connector for classic Eclipse console.
 */
public class TextConsoleConnector implements IRCodeSubmitConnector {
	
	
	public static final String ID = "org.eclipse.statet.r.rCodeLaunchConnector.EclipseTextConsole"; //$NON-NLS-1$
	
	
	public TextConsoleConnector() {
	}
	
	@Override
	public boolean submit(final List<String> lines, final boolean gotoConsole) throws CoreException {
		if (lines == null) {
			throw new NullPointerException();
		}
		
		UIAccess.checkedSyncExec(new UIAccess.CheckedRunnable() {
			@Override
			public void run() throws CoreException {
				final IWorkbenchPage page = UIAccess.getActiveWorkbenchPage(true);
				IWorkbenchPart activePart = page.getActivePart();
				
				try {
					final TextConsole console = getAndShowConsole();
					if (console == null || console instanceof NIConsole) {
						handleNoConsole();
					}
					
					final IDocument doc = console.getDocument();
					try {
						for (int i = 0; i < lines.size(); i++) {
							doc.replace(doc.getLength(), 0, lines.get(i)+'\n');
						}
						if (gotoConsole) {
							activePart = null;
						}
					}
					catch (final BadLocationException e) {
						throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID, 0,
								RLaunchingMessages.TextConsoleConnector_error_Other_message,
								e ));
					}
				}
				finally {
					if (activePart != null) {
						page.activate(activePart);
					}
				}
			}
		});
		return true; // otherwise, we throw exception
	}
	
	@Override
	public void gotoConsole() throws CoreException {
		UIAccess.checkedSyncExec(new UIAccess.CheckedRunnable() {
			@Override
			public void run() throws CoreException {
				final TextConsole console = getAndShowConsole();
				if (console == null || console instanceof NIConsole) {
					handleNoConsole();
				}
			}
		});
	}
	
	private void handleNoConsole() throws CoreException {
		throw new CoreException(new Status(IStatus.WARNING, RUI.BUNDLE_ID, 0,
				RLaunchingMessages.TextConsoleConnector_error_NoConsole_message,
				null));
	}
	
	private TextConsole getAndShowConsole() throws CoreException {
		final IConsoleView view = getConsoleView(true);
		final IConsole console = view.getConsole();
		if (console instanceof TextConsole) {
			return ((TextConsole) console);
		}
		return null;
	}
	
	private IConsoleView getConsoleView(final boolean activateConsoleView) throws PartInitException {
		final IWorkbenchPage page = UIAccess.getActiveWorkbenchPage(false);
		final IConsoleView view = (IConsoleView) page.showView(IConsoleConstants.ID_CONSOLE_VIEW);
		if (activateConsoleView) {
			page.activate(view);
		}
		return view;
	}
	
}
