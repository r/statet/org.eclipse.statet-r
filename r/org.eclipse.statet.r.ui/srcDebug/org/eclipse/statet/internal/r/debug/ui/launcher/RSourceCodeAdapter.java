/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import java.util.List;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;

import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.BasicHeuristicTokenScanner;

import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.r.core.refactoring.RRefactoringAdapter;
import org.eclipse.statet.r.core.rmodel.RChunkElement;
import org.eclipse.statet.r.core.rmodel.RLangElement;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


public class RSourceCodeAdapter extends RRefactoringAdapter {
	
	
	public RSourceCodeAdapter() {
	}
	
	
	@Override
	protected void getSourceCode(final SourceElement element, final AbstractDocument doc,
			final BasicHeuristicTokenScanner scanner, final List<String> codeFragments)
			throws BadLocationException, BadPartitioningException {
		if (element instanceof RLangElement) {
			if (element instanceof RChunkElement) {
				final List<SourceComponent> components = (List<SourceComponent>)element.getAdapter(SourceComponent.class);
				for (final SourceComponent component : components) {
					final TextRegion range = expandSourceRange(
							component.getStartOffset(), component.getEndOffset(), doc, scanner );
					if (range != null && range.getLength() > 0) {
						codeFragments.add(doc.get(range.getStartOffset(), range.getLength()));
					}
				}
				return;
			}
			super.getSourceCode(element, doc, scanner, codeFragments);
			return;
		}
		if (element instanceof SourceStructElement) {
			final List<? extends SourceStructElement> children = ((SourceStructElement) element).getSourceChildren(null);
			for (final SourceStructElement child : children) {
				getSourceCode(child, doc, scanner, codeFragments);
			}
		}
	}
	
}
