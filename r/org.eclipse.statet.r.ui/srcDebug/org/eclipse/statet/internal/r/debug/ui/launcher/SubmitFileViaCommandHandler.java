/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import java.net.URI;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.editors.text.IEncodingSupport;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.internal.r.debug.ui.RLaunchingMessages;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.util.LTKWorkbenchUIUtil;
import org.eclipse.statet.r.launching.RCodeLaunching;


/**
 * Handler running a file in R using a command like source(...)
 * 
 * @see RCodeLaunchRegistry
 * @see RCodeLaunching#getPreferredFileCommand
 */
public class SubmitFileViaCommandHandler extends AbstractHandler implements IElementUpdater {
	
	
	private final boolean fGotoConsole;
	
	
	public SubmitFileViaCommandHandler() {
		this(false);
	}
	
	public SubmitFileViaCommandHandler(final boolean gotoConsole) {
		fGotoConsole = gotoConsole;
	}
	
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		// TODO
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final String fileCommandId = event.getParameter(RCodeLaunching.FILE_COMMAND_ID_PARAMTER_ID);
		
		final IWorkbenchPart activePart = HandlerUtil.getActivePart(event);
		
		String contentTypeId= LTKWorkbenchUIUtil.getContentTypeId(activePart);
		
		try {
			IAdaptable encodingAdaptable = null;
			SourceUnit su = null;
			IFile file = null;
			URI uri = null;
			if (activePart instanceof IEditorPart) {
				encodingAdaptable = activePart;
				final SourceEditor sourceEditor = activePart.getAdapter(SourceEditor.class);
				if (sourceEditor != null) {
					su = sourceEditor.getSourceUnit();
					if (su != null) {
						encodingAdaptable = sourceEditor;
					}
				}
				if (su == null) {
					su = activePart.getAdapter(SourceUnit.class);
				}
				if (su == null) {
					final IEditorPart editor = (IEditorPart) activePart;
					final IEditorInput input = editor.getEditorInput();
					file = ResourceUtil.getFile(input);
					if (file == null && input instanceof IURIEditorInput) {
						uri = ((IURIEditorInput) input).getURI();
					}
				}
			}
			if (su == null && file == null && uri == null) {
				final ISelection selection = WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
				if (selection instanceof IStructuredSelection) {
					final IStructuredSelection sel = (IStructuredSelection) selection;
					if (sel.size() == 1) {
						final Object object = sel.getFirstElement();
						if (object instanceof SourceUnit) {
							su = (SourceUnit) object;
						}
						else if (object instanceof SourceElement) {
							su = ((SourceElement) object).getSourceUnit();
						}
						else if (object instanceof IAdaptable) {
							su = ((IAdaptable) object).getAdapter(SourceUnit.class);
						}
						if (su == null) {
							if (object instanceof IFile) {
								file = (IFile) object;
							}
							else if (object instanceof IAdaptable) {
								file = ((IAdaptable) object).getAdapter(IFile.class);
							}
						}
					}
				}
			}
			
			if (su != null && file == null) {
				if (su.getResource() instanceof IFile) {
					file = (IFile) su.getResource();
				}
				else {
					final FileUtil fileUtil = FileUtil.getFileUtil(su.getResource());
					if (fileUtil != null) {
						uri = fileUtil.getURI();
					}
				}
			}
			else if (su == null && file != null) {
				su = LtkModels.getSourceUnitManager().getSourceUnit(
						Ltk.PERSISTENCE_CONTEXT, file, null, true, null );
			}
			if (file != null && uri == null) {
				uri = file.getLocationURI();
			}
			
			if (uri != null) {
				if (su != null) {
					while (su != null && su.getWorkingContext() != Ltk.PERSISTENCE_CONTEXT) {
						su = su.getUnderlyingUnit();
					}
				}
				
				String command = null;
				if (file != null) {
					if (contentTypeId == null) {
						contentTypeId= LaunchShortcutUtil.getContentTypeId(file);
					}
					command = (fileCommandId != null) ?
							RCodeLaunching.getFileCommand(fileCommandId) :
							RCodeLaunching.getPreferredFileCommand(contentTypeId);
				}
				else { // uri
					if (contentTypeId == null) {
						contentTypeId= LaunchShortcutUtil.getContentTypeId(uri);
					}
					command = (fileCommandId != null) ?
							RCodeLaunching.getFileCommand(fileCommandId) :
							RCodeLaunching.getPreferredFileCommand(contentTypeId);
				}
				
				if (command != null) {
					RCodeLaunching.runFileUsingCommand(command, uri, su,
							getEncoding(encodingAdaptable, file), fGotoConsole );
					return null;
				}
			}
		}
		catch (final Exception e) {
			LaunchShortcutUtil.handleRLaunchException(e,
					RLaunchingMessages.RScriptLaunch_error_message, event);
			return null;
		}
		
		LaunchShortcutUtil.handleUnsupportedExecution(event);
		return null;
	}
	
	private String getEncoding(final IAdaptable adaptable, final IFile file) throws CoreException {
		if (adaptable != null) {
			final IEncodingSupport encodingSupport = adaptable.getAdapter(IEncodingSupport.class);
			if (encodingSupport != null) {
				return encodingSupport.getEncoding();
			}
		}
		if (file != null) {
			return file.getCharset(true); 
		}
		return null;
	}
	
}
