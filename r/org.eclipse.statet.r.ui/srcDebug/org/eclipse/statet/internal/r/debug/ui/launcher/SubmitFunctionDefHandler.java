/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.internal.r.debug.ui.RLaunchingMessages;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;
import org.eclipse.statet.r.launching.RCodeLaunching.SourceRegion;
import org.eclipse.statet.r.ui.RUI;


/**
 * Launch shortcut, which submits the lowest enclosing function (assign of fdef)
 * and does not change the focus.
 * 
 * Supports only text editors with input supporting R AST.
 */
public class SubmitFunctionDefHandler extends SubmitEntireCommandHandler {
	
	
	public static class AndGotoConsole extends SubmitFunctionDefHandler {
		
		
		public AndGotoConsole() {
			super(true);
		}
		
	}
	
	
	public SubmitFunctionDefHandler() {
		super(false);
	}
	
	protected SubmitFunctionDefHandler(final boolean gotoConsole) {
		super(gotoConsole);
	}
	
	
	@Override
	protected String getErrorMessage() {
		return RLaunchingMessages.RFunctionLaunch_error_message;
	}
	
	@Override
	protected IStatus getRegions(final Data data) throws CoreException {
		final RAstNode node= RAsts.findLowestFDefAssignment(data.ast.getRoot(),
				data.selection.getOffset() );
		if (node == null) {
			return LaunchShortcutUtil.createUnsupported();
		}
		try {
			if (RAsts.hasErrors(node)) {
				return new Status(IStatus.ERROR, RUI.BUNDLE_ID,
						RLaunchingMessages.SubmitCode_info_SyntaxError_message );
			}
			
			data.nodes= ImCollections.newList(node);
			final List<SourceRegion> list= new ArrayList<>(1);
			{	final SourceRegion region= new SourceRegion(data.su, data.document);
				region.setBegin(checkStart(data.document, node.getStartOffset()));
				region.setEnd(node.getEndOffset());
				region.setCode(data.document.get(region.getOffset(), region.getLength()));
				region.setNode(node);
				list.add(region);
			}
			data.regions= list;
			return Status.OK_STATUS;
		}
		catch (final BadLocationException e) {
			throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID, -1,
					RLaunchingMessages.SubmitCode_error_WhenAnalyzingAndCollecting_message, e));
		}
	}
	
}
