/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.ui.actions.ToggleBooleanPreferenceHandler;

import org.eclipse.statet.r.launching.RCodeLaunching;


@NonNullByDefault
public class ToggleRunEchoHandler extends ToggleBooleanPreferenceHandler {
	
	
	/** Created via extension point */
	public ToggleRunEchoHandler() {
		super(RCodeLaunching.ECHO_ENABLED_PREF, EPreferences.getInstancePrefs(),
				LaunchShortcutUtil.TOGGLE_ECHO_COMMAND_ID );
	}
	
}
