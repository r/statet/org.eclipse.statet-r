/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.preferences;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.ecommons.ui.SharedUIResources;

import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;


public class REnvLabelProvider extends ColumnLabelProvider {
	
	
	private final IObservableValue<? extends REnvConfiguration> defaultValue;
	
	private Image envIcon;
	private Image envDefaultIcon;
	private Image envRemoteIcon;
	private Image envRemoteDefaultIcon;
	
	
	public REnvLabelProvider(final IObservableValue<? extends REnvConfiguration> defaultValue) {
		this.defaultValue= defaultValue;
	}
	
	
	private Image createIcon(final Image baseImage) {
		final Rectangle bounds= baseImage.getBounds();
		return new DecorationOverlayIcon(baseImage, new ImageDescriptor[] {
				null, null, null, null, null},
				new Point(bounds.width + 4, bounds.height)).createImage();
	}
	
	private Image createDefaultIcon(final Image baseImage) {
		final Rectangle bounds= baseImage.getBounds();
		return new DecorationOverlayIcon(baseImage, new ImageDescriptor[] {
				null, null, null, SharedUIResources.getImages().getDescriptor(SharedUIResources.OVR_DEFAULT_MARKER_IMAGE_ID), null},
				new Point(bounds.width + 4, bounds.height)).createImage();
	}
	
	
	@Override
	public void dispose() {
		if (this.envIcon != null) {
			this.envIcon.dispose();
			this.envIcon= null;
		}
		if (this.envDefaultIcon != null) {
			this.envDefaultIcon.dispose();
			this.envDefaultIcon= null;
		}
		if (this.envRemoteIcon != null) {
			this.envRemoteIcon.dispose();
			this.envRemoteIcon= null;
		}
		if (this.envRemoteDefaultIcon != null) {
			this.envRemoteDefaultIcon.dispose();
			this.envRemoteDefaultIcon= null;
		}
	}
	
	@Override
	public Image getImage(final Object element) {
		final REnvConfiguration config= (REnvConfiguration) element;
		if (config.isRemote()) {
			if (this.defaultValue.getValue() == config) {
				if (this.envRemoteDefaultIcon == null) {
					this.envRemoteDefaultIcon= createDefaultIcon(RUI.getImage(RUI.IMG_OBJ_R_REMOTE_ENV));
				}
				return this.envRemoteDefaultIcon;
			}
			else {
				if (this.envRemoteIcon == null) {
					this.envRemoteIcon= createIcon(RUI.getImage(RUI.IMG_OBJ_R_REMOTE_ENV));
				}
				return this.envRemoteIcon;
			}
		}
		else {
			if (this.defaultValue.getValue() == config) {
				if (this.envDefaultIcon == null) {
					this.envDefaultIcon= createDefaultIcon(RUI.getImage(RUI.IMG_OBJ_R_RUNTIME_ENV));
				}
				return this.envDefaultIcon;
			}
			else {
				if (this.envIcon == null) {
					this.envIcon= createIcon(RUI.getImage(RUI.IMG_OBJ_R_RUNTIME_ENV));
				}
				return this.envIcon;
			}
		}
	}
	
	@Override
	public String getText(final Object element) {
		final REnvConfiguration config= (REnvConfiguration) element;
		return config.getName();
	}
	
	
}
