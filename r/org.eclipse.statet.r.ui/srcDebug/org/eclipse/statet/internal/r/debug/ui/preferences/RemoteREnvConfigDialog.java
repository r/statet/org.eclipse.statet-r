/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.preferences;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.typed.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.ui.dialogs.ExtStatusDialog;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.VariableFilterUtils;
import org.eclipse.statet.ecommons.ui.workbench.ResourceInputComposite;

import org.eclipse.statet.internal.r.ui.help.IRUIHelpContextIds;
import org.eclipse.statet.r.core.renv.IREnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;


/**
 * Dialog for a remote {@link IREnvConfiguration} (<code>user-remote</code>)
 */
public class RemoteREnvConfigDialog extends ExtStatusDialog {
	
	
	private final IREnvConfiguration.WorkingCopy configModel;
	private final boolean isNewConfig;
	private final Set<String> existingNames;
	
	private Text nameControl;
	
	private Button indexDirectorySelectionControl;
	private ResourceInputComposite indexDirectorResourceControl;
	private Button indexServerSelectionControl;
	private Text indexServerUrlControl;
	
	
	public RemoteREnvConfigDialog(final Shell parent,
			final IREnvConfiguration.WorkingCopy config, final boolean isNewConfig,
			final Collection<IREnvConfiguration> existingConfigs) {
		super(parent, WITH_RUNNABLE_CONTEXT | ((isNewConfig) ? WITH_DATABINDING_CONTEXT :
				(WITH_DATABINDING_CONTEXT | SHOW_INITIAL_STATUS)) );
		
		this.configModel= config;
		this.isNewConfig= isNewConfig;
		this.existingNames= new HashSet<>();
		for (final IREnvConfiguration ec : existingConfigs) {
			this.existingNames.add(ec.getName());
		}
		setTitle(this.isNewConfig ?
				Messages.REnv_Detail_AddDialog_title : 
				Messages.REnv_Detail_Edit_Dialog_title );
	}
	
	
	@Override
	public void create() {
		super.create();
		
		updateEditable();
		
		PlatformUI.getWorkbench().getHelpSystem().setHelp(getShell(), IRUIHelpContextIds.R_ENV);
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite area= new Composite(parent, SWT.NONE);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		area.setLayout(LayoutUtils.newDialogGrid(2));
		
		{	final Label label= new Label(area, SWT.LEFT);
			label.setText("R Environment configuration for remote R installations (consoles).");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		}
		
		LayoutUtils.addSmallFiller(area, false);
		
		{	// Name:
			final Label label= new Label(area, SWT.LEFT);
			label.setText(Messages.REnv_Detail_Name_label+':');
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			
			final Text text= new Text(area, SWT.BORDER);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, true, false);
			gd.widthHint= LayoutUtils.hintWidth(text, 60);
			text.setLayoutData(gd);
			this.nameControl= text;
		}
		
		LayoutUtils.addSmallFiller(area, false);
		
		{	final Composite composite= createStateOptions(area);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		}
		
		LayoutUtils.addSmallFiller(area, true);
		
		applyDialogFont(area);
		
		return area;
	}
	
	protected Composite createStateOptions(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setText("R-Help / Index:");
		composite.setLayout(LayoutUtils.newGroupGrid(1));
		
		{	final Button button= new Button(composite, SWT.RADIO);
			button.setText("&Directory (empty for default location):");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.indexDirectorySelectionControl= button;
			
			final ResourceInputComposite text= new ResourceInputComposite(composite, ResourceInputComposite.STYLE_TEXT,
					(ResourceInputComposite.MODE_DIRECTORY | ResourceInputComposite.MODE_OPEN), "R_DOC_DIR");
			text.setShowInsertVariable(true, VariableFilterUtils.DEFAULT_NON_ITERACTIVE_FILTERS, null);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, true, false);
			gd.horizontalIndent= LayoutUtils.defaultIndent();
			text.setLayoutData(gd);
			this.indexDirectorResourceControl= text;
		}
		{	final Button button= new Button(composite, SWT.RADIO);
			button.setText("R-Help &Server:");
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.indexServerSelectionControl= button;
			
			final Text text= new Text(composite, SWT.LEFT | SWT.SINGLE | SWT.BORDER);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, true, false);
			gd.horizontalIndent= LayoutUtils.defaultIndent();
			text.setLayoutData(gd);
			this.indexServerUrlControl= text;
		}
		
		return composite;
	}
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		// don't specify IREnvConfiguration.WorkingCopy for BeanProperties (missing with getter)
		db.getContext().bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.nameControl),
				BeanProperties.value(IREnvConfiguration.PROP_NAME, String.class)
						.observe(this.configModel),
				new UpdateValueStrategy<String, String>()
						.setAfterGetValidator((final String text) -> {
							final String s= text.trim();
							if (s.isEmpty()) {
								return ValidationStatus.error(Messages.REnv_Detail_Name_error_Missing_message);
							}
							if (RemoteREnvConfigDialog.this.existingNames.contains(s)) {
								return ValidationStatus.error(Messages.REnv_Detail_Name_error_Duplicate_message);
							}
							if (s.contains("/")) {  //$NON-NLS-1$
								return ValidationStatus.error(Messages.REnv_Detail_Name_error_InvalidChar_message);
							}
							return ValidationStatus.ok();
						}),
				null );
		
		final IObservableValue<Boolean> indexDirectoryValue=
				WidgetProperties.buttonSelection()
						.observe(this.indexDirectorySelectionControl);
		final IObservableValue<Boolean> indexServerValue=
				WidgetProperties.buttonSelection()
						.observe(this.indexServerSelectionControl);
		
		final SelectObservableValue<String> typeValue= new SelectObservableValue<>();
		typeValue.addOption(REnvConfiguration.SHARED_DIRECTORY, indexDirectoryValue);
		typeValue.addOption(REnvConfiguration.SHARED_SERVER, indexServerValue);
		db.getContext().bindValue(typeValue,
				BeanProperties.value(IREnvConfiguration.PROP_STATE_SHARED_TYPE, String.class)
						.observe(this.configModel) );
		
		db.getContext().bindValue(
				WidgetProperties.enabled()
						.observe(this.indexDirectorResourceControl),
				indexDirectoryValue );
		db.getContext().bindValue(
				this.indexDirectorResourceControl.getObservable(),
				BeanProperties.value(IREnvConfiguration.PROP_STATE_SHARED_DIRECTORY, String.class)
						.observe(this.configModel) );
		
		db.getContext().bindValue(
				WidgetProperties.enabled()
						.observe(this.indexServerUrlControl),
				indexServerValue );
		db.getContext().bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.indexServerUrlControl),
				BeanProperties.value(IREnvConfiguration.PROP_STATE_SHARED_SERVER, String.class)
						.observe(this.configModel) );
	}
	
	private void updateEditable() {
		final boolean isEditable= this.configModel.isEditable();
		this.nameControl.setEditable(isEditable);
		this.indexDirectorySelectionControl.setEnabled(isEditable
				|| this.indexDirectorySelectionControl.getSelection() );
		this.indexDirectorResourceControl.setEditable(isEditable);
		this.indexServerSelectionControl.setEnabled(isEditable
				|| this.indexServerSelectionControl.getSelection() );
		this.indexServerUrlControl.setEditable(isEditable);
	}
	
}
