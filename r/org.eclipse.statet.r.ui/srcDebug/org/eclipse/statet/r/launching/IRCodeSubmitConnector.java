/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.launching;

import java.util.List;

import org.eclipse.core.runtime.CoreException;


public interface IRCodeSubmitConnector {
	
	
	/**
	 * Submit commands to R.
	 * 
	 * @param lines array with commands
	 * @param gotoConsole if <code>true</code>, switch focus console, else does not change the focus.
	 * @return <code>false</code>, if not successful, otherwise <code>true</code> (hint)
	 * 
	 * @throws CoreException if a technical error occured
	 */
	boolean submit(List<String> lines, boolean gotoConsole) throws CoreException;
	
	void gotoConsole() throws CoreException;
	
}
