/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.launching;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.nico.core.runtime.ConsoleRunnable;
import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.r.console.core.AbstractRController;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.core.RWorkspace;
import org.eclipse.statet.r.nico.IRSrcref;


public class SubmitEntireCommandRunnable implements ConsoleRunnable {
	
	
	private final String[] fLines;
	
	private final String fLabel;
	private final IRSrcref fSrcref;
	
	
	public SubmitEntireCommandRunnable(final String[] lines, final IRSrcref srcref) {
		fLines = lines;
		fLabel = createLabel();
		fSrcref = srcref;
		if (fSrcref instanceof RCodeLaunching.SourceRegion) {
			((RCodeLaunching.SourceRegion) fSrcref).installMarker();
		}
	}
	
	
	private String createLabel() {
		final String label = fLines[0];
		if (fLines.length > 0) {
			return label + " \u2026";
		}
		return label;
	}
	
	
	@Override
	public String getTypeId() {
		return "r/console/runCommand"; //$NON-NLS-1$
	}
	
	@Override
	public SubmitType getSubmitType() {
		return SubmitType.EDITOR;
	}
	
	@Override
	public String getLabel() {
		return fLabel;
	}
	
	@Override
	public boolean canRunIn(final Tool tool) {
		return (tool.isProvidingFeatureSet(RConsoleTool.R_BASIC_FEATURESET_ID));
	}
	
	@Override
	public boolean changed(final int event, final Tool process) {
		switch (event) {
		case REMOVING_FROM:
		case BEING_ABANDONED:
		case FINISHING_OK:
		case FINISHING_ERROR:
		case FINISHING_CANCEL:
			if (fSrcref instanceof RCodeLaunching.SourceRegion) {
				((RCodeLaunching.SourceRegion) fSrcref).disposeMarker();
			}
		}
		return true;
	}
	
	@Override
	public void run(final ToolService service, final ProgressMonitor m) throws StatusException {
		final AbstractRController r = (AbstractRController) service;
		r.briefAboutToChange();
		try {
			r.submitCommandToConsole(fLines, fSrcref, m);
		}
		finally {
			r.briefChanged(RWorkspace.REFRESH_AUTO);
		}
	}
	
}
