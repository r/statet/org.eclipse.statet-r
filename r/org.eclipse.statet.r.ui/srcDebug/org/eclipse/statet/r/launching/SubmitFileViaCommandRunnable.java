/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.launching;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.ecommons.ts.ui.ToolRunnableDecorator;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.nico.core.runtime.ConsoleRunnable;
import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.r.console.core.AbstractRController;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.core.RWorkspace;


public class SubmitFileViaCommandRunnable implements ConsoleRunnable, ToolRunnableDecorator {
	
	
	private final String fCommand;
	private final SourceUnit fSourceUnit;
	
	private final Image fImage;
	private final String fLabel;
	
	
	public SubmitFileViaCommandRunnable(final Image image, final String label,
			final String command, final SourceUnit su) {
		fImage = image;
		fLabel = label;
		fCommand = command;
		fSourceUnit = su;
	}
	
	
	@Override
	public String getTypeId() {
		return "r/console/runFileCommand"; //$NON-NLS-1$
	}
	
	@Override
	public SubmitType getSubmitType() {
		return SubmitType.EDITOR;
	}
	
	@Override
	public Image getImage() {
		return fImage;
	}
	
	@Override
	public String getLabel() {
		return fLabel;
	}
	
	@Override
	public boolean canRunIn(final Tool tool) {
		return (tool.isProvidingFeatureSet(RConsoleTool.R_BASIC_FEATURESET_ID));
	}
	
	@Override
	public boolean changed(final int event, final Tool tool) {
		return true;
	}
	
	@Override
	public void run(final ToolService service, final ProgressMonitor m) throws StatusException {
		final AbstractRController r = (AbstractRController) service;
		r.briefAboutToChange();
		try {
			r.submitFileCommandToConsole(new String[] { fCommand }, fSourceUnit, m);
		}
		finally {
			r.briefChanged(RWorkspace.REFRESH_AUTO);
		}
	}
	
}
