/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rtool;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.models.core.util.ElementPartition;
import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.util.ViewActionUtil;

import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.actions.AbstractToolHandler;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.ui.util.RElementInputContentProvider;
import org.eclipse.statet.r.ui.util.RElementInputUtils;
import org.eclipse.statet.rj.data.RObject;


@NonNullByDefault
public class PrintRElementHandler extends AbstractToolHandler<RProcess> {
	
	
	private final ViewActionUtil actionUtil;
	
	
	public PrintRElementHandler(final ViewActionUtil actionUtil) {
		super(RConsoleTool.TYPE, RConsoleTool.R_BASIC_FEATURESET_ID,
				(ToolProvider) actionUtil.getWorkbenchPart(), actionUtil.getWorkbenchPart().getSite() );
		this.actionUtil= actionUtil;
	}
	
	
	private ITreeSelection getSelection() {
		final ISelectionProvider selectionProvider= this.actionUtil.getSelectionProvider();
		return (ITreeSelection) selectionProvider.getSelection();
	}
	
	private boolean isValidSelection(final ITreeSelection selection) {
		if (selection == null || selection.size() != 1) {
			return false;
		}
		final Object element= selection.getFirstElement();
		if (element instanceof ElementPartition) {
			final CombinedRElement rElement= RElementInputContentProvider.getCombinedRElement(element);
			if (rElement == null || rElement.getRObjectType() != RObject.TYPE_LIST) {
				return false;
			}
		}
		return true;
	}
	
	
	@Override
	protected boolean evaluateIsEnabled(final RProcess tool, final @Nullable Object evaluationContext) {
		return (super.evaluateIsEnabled(tool, evaluationContext)
				&& isValidSelection(getSelection()) );
	}
	
	@Override
	protected @Nullable Object execute(final RProcess tool, final ExecutionEvent event) {
		if (!UIAccess.isOkToUse(this.actionUtil.getControl())) {
			return null;
		}
		final ITreeSelection selection= getSelection();
		if (!isValidSelection(selection)) {
			return null;
		}
		final TreePath treePath= selection.getPaths()[0];
		final RElementName elementName= RElementInputUtils.getRElementName(treePath, selection);
		if (elementName != null) {
			String cmd= elementName.getDisplayName(RElementName.DISPLAY_FQN | RElementName.DISPLAY_EXACT);
			if (treePath.getLastSegment() instanceof ElementPartition) {
				final ElementPartition partition= (ElementPartition) treePath.getLastSegment();
				cmd= cmd + '[' + (partition.getPartitionStart() + 1) + ':' + (partition.getPartitionStart() + partition.getPartitionLength()) + ']';
			}
			
			try {
				final ToolController controller= NicoUITools.accessController(RConsoleTool.TYPE, tool);
				controller.submit(cmd, SubmitType.TOOLS);
			}
			catch (final CoreException e) {
			}
		}
		
		return null;
	}
	
}
