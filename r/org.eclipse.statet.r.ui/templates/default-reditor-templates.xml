<?xml version="1.0" encoding="UTF-8"?>
<!--
 #=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<templates>

<template id="RCode.if" context="r-code"
		name="if" description="if statement"
		autoinsert="false"
>if (${condition}) {
	${line_selection}${cursor}
}</template>
<template id="RCode.ifelse" context="r-code"
		name="ifelse" description="if else statement"
		autoinsert="false"
>if (${condition}) {
	${line_selection}${cursor}
} else {
	
}</template>
<template id="RCode.else" context="r-code"
		name="else" description="else statement"
		autoinsert="false"
>else {
	${line_selection}${cursor}
}</template>
<template id="RCode.elseif" context="r-code"
		name="elseif" description="else if statement"
		autoinsert="false"
>else if (${condition}) {
	${line_selection}${cursor}
}</template>

<template id="RCode.for" context="r-code"
		name="foreach" description="iterate over a vector"
		autoinsert="false"
>for (${x} in ${vector}) {
	${line_selection}${cursor}
}</template>
<template id="RCode.for.index" context="r-code"
		name="for" description="iterate with index over a vector"
		autoinsert="false"
>for (${i} in seq(along = ${vector})) {
	${line_selection}${cursor}
}</template>

<template id="RCode.function" context="r-code"
		name="function" description="function definition"
		autoinsert="false"
>function(${parameter}) {
	${line_selection}${cursor}
}</template>

<template id="RCode.repeat" context="r-code"
		name="repeat" description="repeat loop statement"
		autoinsert="false"
>repeat {
	${line_selection}${cursor}
}</template>

<template id="RCode.while" context="r-code"
		name="while" description="while loop statement"
		autoinsert="false"
>while (${condition}) {
	${line_selection}${cursor}
}</template>

<template id="RCode.pipe.forward" context="r-code"
		name="pipe" description="pipe"
		autoinsert="false"
>|></template>

<template id="RCode.magrittr.pipe" context="r-code"
		name="pipe" description="magrittr pipe"
		autoinsert="false"
>%InfixGt</template>

<template id="RRoxygen.code" context="roxygen"
		name="\code" description="\code{...}"
		autoinsert="true"
>\code{${word_selection}${}}${cursor}</template>

<template id="RRoxygen.na" context="roxygen"
		name="NA" description="\code{NA}"
		autoinsert="true"
>\code{NA}${cursor}</template>
<template id="RRoxygen.nan" context="roxygen"
		name="NaN" description="\code{NaN}"
		autoinsert="true"
>\code{NaN}${cursor}</template>
<template id="RRoxygen.dots" context="roxygen"
		name="..." description="\code{\dots}"
		autoinsert="true"
>\code{\dots}${cursor}</template>

<template id="RRoxygen.link" context="roxygen"
		name="\link" description="\link{...}"
		autoinsert="true"
>\link{${word_selection}${}}${cursor}</template>

<template id="RRoxygen.item" context="roxygen"
		name="\item" description="\item{...}{...}"
		autoinsert="true"
>\item{${header}}{${word_selection}${}}${cursor}</template>

</templates>
