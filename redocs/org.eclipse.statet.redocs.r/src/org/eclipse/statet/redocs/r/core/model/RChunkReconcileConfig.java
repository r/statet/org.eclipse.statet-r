/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.TaskIssueConfig;
import org.eclipse.statet.ltk.model.core.build.ReconcileConfig;
import org.eclipse.statet.ltk.project.core.LtkProject;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.RSourceConfig;


@NonNullByDefault
public class RChunkReconcileConfig extends ReconcileConfig<RSourceConfig>{
	
	
	private final TaskIssueConfig taskIssueConfig;
	
	
	public RChunkReconcileConfig(final RSourceConfig sourceConfig,
			final TaskIssueConfig taskIssueConfig) {
		super(sourceConfig);
		this.taskIssueConfig= taskIssueConfig;
	}
	
	public RChunkReconcileConfig(final RCoreAccess coreAccess,
			final @Nullable LtkProject ltkProject) {
		this(coreAccess.getRSourceConfig(),
				TaskIssueConfig.getConfig((ltkProject != null) ? ltkProject : coreAccess.getPrefs()) );
	}
	
	
	public TaskIssueConfig getTaskIssueConfig() {
		return this.taskIssueConfig;
	}
	
}
