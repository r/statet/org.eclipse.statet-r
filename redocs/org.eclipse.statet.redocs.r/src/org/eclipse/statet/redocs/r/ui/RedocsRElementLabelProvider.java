/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.EmbeddingForeignElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.ElementLabelProvider;
import org.eclipse.statet.r.core.rmodel.RModel;


@NonNullByDefault
public class RedocsRElementLabelProvider implements ElementLabelProvider {
	
	
	public RedocsRElementLabelProvider() {
	}
	
	
	@Override
	public @Nullable Image getImage(final LtkModelElement<?> element) {
		if ((element.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_EMBEDDED) {
			final var embeddedElement= ((EmbeddingForeignElement<?, ?>)element).getForeignElement();
			if (embeddedElement != null && embeddedElement.getModelTypeId() == RModel.R_TYPE_ID) {
				return RedocsRUIResources.INSTANCE.getImage(RedocsRUIResources.OBJ_RCHUNK_IMAGE_ID);
			}
		}
		return null;
	}
	
}
