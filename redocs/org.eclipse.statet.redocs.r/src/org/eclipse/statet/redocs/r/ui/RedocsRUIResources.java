/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.internal.redocs.r.Plugin;


public final class RedocsRUIResources {
	
	
	private static final String NS= "org.eclipse.statet.redocs.r"; //$NON-NLS-1$
	
	
	public static final String OBJ_RCHUNK_IMAGE_ID= NS + "/image/obj/rchunk"; //$NON-NLS-1$
	
	public static final String TOOL_RWEAVE_IMAGE_ID= NS + "/image/tool/rweave"; //$NON-NLS-1$
	public static final String TOOL_BUILDTEX_IMAGE_ID= NS + "/image/tool/build-tex"; //$NON-NLS-1$
	
	public static final String LOCTOOL_FILTERCHUNKS_IMAGE_ID= NS + "/image/loctool/filter-r_chunks"; //$NON-NLS-1$
	
	
	public static final RedocsRUIResources INSTANCE= new RedocsRUIResources();
	
	
	private final ImageRegistry registry;
	
	
	private RedocsRUIResources() {
		this.registry= Plugin.getDefault().getImageRegistry();
	}
	
	public ImageDescriptor getImageDescriptor(final String id) {
		return this.registry.getDescriptor(id);
	}
	
	public Image getImage(final String id) {
		return this.registry.get(id);
	}
	
}
