/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui.debug;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.TextUtil;

import org.eclipse.statet.r.launching.ICodeSubmitContentHandler;
import org.eclipse.statet.redocs.r.core.source.DocContentSectionsRweaveExtension;


public class RweaveSubmitContentHandler implements ICodeSubmitContentHandler {
	
	
	private final IDocumentSetupParticipant docSetup;
	
	private final DocContentSectionsRweaveExtension docContentSections;
	
	
	public RweaveSubmitContentHandler(final IDocumentSetupParticipant docSetup,
			final DocContentSectionsRweaveExtension docContentSections) {
		this.docSetup= docSetup;
		this.docContentSections= docContentSections;
	}
	
	
	@Override
	public void setup(final IDocument document) {
		this.docSetup.setup(document);
	}
	
	@Override
	public List<String> getCodeLines(final IDocument document) throws BadLocationException, CoreException {
		final ArrayList<String> lines= new ArrayList<>(document.getNumberOfLines() / 2);
		
		final List<? extends TextRegion> rCodeRegions= this.docContentSections.getRChunkCodeRegions(
				document, 0, document.getLength() );
		for (final TextRegion region : rCodeRegions) {
			TextUtil.addLines(document, region.getStartOffset(), region.getLength(), lines);
		}
		
		return lines;
	}
	
	@Override
	public List<String> getCodeLines(final IDocument document, final int offset, final int length)
			throws CoreException, BadLocationException {
		final ArrayList<String> lines= new ArrayList<>(Math.min(
				document.getNumberOfLines(0, length) + 1, 64) );
		
		final List<? extends TextRegion> rCodeRegions= this.docContentSections.getRChunkCodeRegions(
				document, offset, length );
		for (final TextRegion region : rCodeRegions) {
			TextUtil.addLines(document, region.getStartOffset(), region.getLength(), lines);
		}
		
		return lines;
	}
	
}
