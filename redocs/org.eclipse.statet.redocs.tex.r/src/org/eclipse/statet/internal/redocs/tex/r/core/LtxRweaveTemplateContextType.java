/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.core;

import org.eclipse.statet.ltk.ui.templates.IWaTemplateContextTypeExtension1;
import org.eclipse.statet.ltk.ui.templates.SourceEditorContextType;


public class LtxRweaveTemplateContextType extends SourceEditorContextType
		implements IWaTemplateContextTypeExtension1 {
	
	
/*- Context Types -------------------------------------------------------------*/
	
	public static final String NEWDOC_CONTEXTTYPE= "Ltx+Rweave_NewDoc"; //$NON-NLS-1$
	
	public static final String WEAVE_DOCDEFAULT_CONTEXTTYPE= "Ltx+Rweave_Weave:DocDefault"; //$NON-NLS-1$
	
/*- Templates -----------------------------------------------------------------*/
	
	
	public LtxRweaveTemplateContextType(final String id) {
		super(id);
		
		init();
	}
	
	/** Instantiation extension point */
	public LtxRweaveTemplateContextType() {
		super();
	}
	
	
	@Override
	public void init() {
		final String id= getId();
		
		addCommonVariables();
		if (id.equals(NEWDOC_CONTEXTTYPE)) {
			addSourceUnitGenerationVariables();
		}
		else if (id.equals(WEAVE_DOCDEFAULT_CONTEXTTYPE)) {
			addEditorVariables();
		}
	}
	
}
