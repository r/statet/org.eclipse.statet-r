/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.model;

import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.redocs.r.core.model.RChunkReconciler;
import org.eclipse.statet.redocs.tex.r.core.model.LtxRweaveSourceUnit;


@NonNullByDefault
class LtxRChunkReconciler extends RChunkReconciler<
		LtxRweaveSourceUnit, LtxSourceUnitModelInfo, LtxRweaveSuModelContainer> {
	
	
	private static @Nullable LtxRChunkReconciler INSTANCE;
	
	static final LtxRChunkReconciler getInstance() {
		synchronized(LtxRChunkReconciler.class) {
			LtxRChunkReconciler reconciler= INSTANCE;
			if (reconciler == null) {
				reconciler= new LtxRChunkReconciler();
				INSTANCE= reconciler;
			}
			return reconciler;
		}
	}
	
	
//	private static final Pattern CHUNK_START_LINE_PATTERN= Pattern.compile("\\A<<(.*?)(?:>>\\p{all}*)?+\\z"); //$NON-NLS-1$
	/**
	 * Regex: <code>\A<<(.*?)(?:>>\p{all}*)?+\s*\z</code>
	 **/
	private static final Pattern CHUNK_START_LINE_PATTERN= Pattern.compile("\\A<<(.*?)(?:>>\\p{all}*)?+\\s*\\z"); //$NON-NLS-1$
	private static final Pattern CHUNK_REF_LINE_PATTERN= CHUNK_START_LINE_PATTERN;
	private static final Pattern CHUNK_END_LINE_PATTERN= Pattern.compile("\\A@\\p{all}*\\z"); //$NON-NLS-1$
	
	
	public LtxRChunkReconciler() {
		super("Ltx", CHUNK_START_LINE_PATTERN, CHUNK_REF_LINE_PATTERN, CHUNK_END_LINE_PATTERN); //$NON-NLS-1$
	}
	
	
}
