/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.model;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.ast.Embedded;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceElement;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitEmbeddedModelReconciler;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.docmlet.tex.core.project.TexProject;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.redocs.r.core.model.RChunkReconcileConfig;


@NonNullByDefault
public class LtxREmbeddedModelReconciler implements LtxSourceUnitEmbeddedModelReconciler<RChunkReconcileConfig> {
	
	
	private final LtxRChunkReconciler reconciler= new LtxRChunkReconciler();
	
	
	public LtxREmbeddedModelReconciler() {
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public RChunkReconcileConfig createConfig(final @Nullable IProject wsProject,
			final @Nullable TexProject texProject,
			final int level) {
		final RProject rProject= RProjects.getRProject(wsProject);
		final RCoreAccess rCoreAccess= (rProject != null) ? rProject : RCore.getWorkbenchAccess();
		return new RChunkReconcileConfig(rCoreAccess, texProject);
	}
	
	
	@Override
	public void reconcileAst(final SourceContent content, final List<Embedded> list,
			final LtxSourceUnitModelContainer<?> container, final RChunkReconcileConfig config, final int level) {
		if (container instanceof LtxRweaveSuModelContainer) {
			this.reconciler.reconcileAst((LtxRweaveSuModelContainer)container,
					content, list, config.getSourceConfig() );
		}
	}
	
	@Override
	public void reconcileModel(final LtxSourceUnitModelInfo texModel, final SourceContent content,
			final List<? extends EmbeddingForeignReconcileTask<Embedded, TexSourceElement>> list,
			final LtxSourceUnitModelContainer<?> container, final RChunkReconcileConfig config, final int level,
			final SubMonitor m) {
		if (container instanceof LtxRweaveSuModelContainer) {
			this.reconciler.reconcileModel((LtxRweaveSuModelContainer)container,
					content, texModel, list, level, m);
		}
	}
	
	@Override
	public void reportIssues(final LtxSourceUnitModelInfo texModel, final SourceContent content,
			final IssueRequestor issueRequestor,
			final LtxSourceUnitModelContainer<?> container, final RChunkReconcileConfig config, final int level) {
		if (container instanceof LtxRweaveSuModelContainer) {
			this.reconciler.reportEmbeddedIssues((LtxRweaveSuModelContainer)container,
					content, texModel, issueRequestor, config, level );
		}
	}
	
}