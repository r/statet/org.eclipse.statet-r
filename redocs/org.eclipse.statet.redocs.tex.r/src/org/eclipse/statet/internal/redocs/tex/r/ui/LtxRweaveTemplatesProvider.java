/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.persistence.TemplateStore;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.base.ext.templates.ICodeGenerationTemplateCategory;
import org.eclipse.statet.internal.redocs.tex.r.RedocsTexRPlugin;
import org.eclipse.statet.internal.redocs.tex.r.ui.sourceediting.LtxRweaveTemplateViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.redocs.tex.r.core.TexRweaveCore;


/**
 * Integrates the R templates into the common StatET template
 * preference page.
 */
public class LtxRweaveTemplatesProvider implements ICodeGenerationTemplateCategory {
	
	
	public LtxRweaveTemplatesProvider() {
	}
	
	
	@Override
	public String getProjectNatureId() {
		return RProjects.R_NATURE_ID;
	}
	
	@Override
	public TemplateStore getTemplateStore() {
		return RedocsTexRPlugin.getInstance().getCodegenTemplateStore();
	}
	
	@Override
	public ContextTypeRegistry getContextTypeRegistry() {
		return RedocsTexRPlugin.getInstance().getCodegenTemplateContextTypeRegistry();
	}
	
	@Override
	public SourceEditorViewerConfigurator getEditTemplateDialogConfiguator(final TemplateVariableProcessor processor, final IProject project) {
		final RProject rProject= RProjects.getRProject(project);
		return new LtxRweaveTemplateViewerConfigurator(
				TexRweaveCore.TEX_WORKBENCH_ACCESS, (rProject != null) ? rProject : RCore.WORKBENCH_ACCESS,
				processor );
	}
	
}
