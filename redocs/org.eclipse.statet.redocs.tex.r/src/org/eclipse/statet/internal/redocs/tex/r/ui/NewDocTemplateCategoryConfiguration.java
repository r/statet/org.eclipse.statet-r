/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.text.templates.ContextTypeRegistry;
import org.eclipse.text.templates.TemplatePersistenceData;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.internal.redocs.tex.r.RedocsTexRPlugin;
import org.eclipse.statet.internal.redocs.tex.r.core.LtxRweaveTemplateContextType;
import org.eclipse.statet.internal.redocs.tex.r.ui.sourceediting.LtxRweaveTemplateViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.templates.config.ITemplateCategoryConfiguration;
import org.eclipse.statet.ltk.ui.templates.config.ITemplateContribution;
import org.eclipse.statet.ltk.ui.templates.config.TemplateStoreContribution;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.redocs.tex.r.core.TexRweaveCore;
import org.eclipse.statet.redocs.tex.r.ui.TexRweaveUI;


public class NewDocTemplateCategoryConfiguration implements ITemplateCategoryConfiguration {
	
	
	public static final String PREF_QUALIFIER= TexRweaveUI.BUNDLE_ID + "/codegen"; //$NON-NLS-1$
	
	public static final String NEWDOC_DEFAULT_NAME_KEY= "NewDoc.Default.name"; //$NON-NLS-1$
	public static final Preference<String> NEWDOC_DEFAULT_NAME_PREF= new Preference.StringPref(
			PREF_QUALIFIER, NEWDOC_DEFAULT_NAME_KEY);
	
	
	public NewDocTemplateCategoryConfiguration() {
	}
	
	
	@Override
	public ITemplateContribution getTemplates() {
		return new TemplateStoreContribution(RedocsTexRPlugin.getInstance().getDocTemplateStore());
	}
	
	@Override
	public Preference<String> getDefaultPref() {
		return NEWDOC_DEFAULT_NAME_PREF;
	}
	
	@Override
	public ContextTypeRegistry getContextTypeRegistry() {
		return RedocsTexRPlugin.getInstance().getDocTemplateContextTypeRegistry();
	}
	
	@Override
	public String getDefaultContextTypeId() {
		return LtxRweaveTemplateContextType.NEWDOC_CONTEXTTYPE;
	}
	
	@Override
	public String getViewerConfigId(final TemplatePersistenceData data) {
		return LtxRweaveTemplateContextType.NEWDOC_CONTEXTTYPE;
	}
	
	@Override
	public SourceEditorViewerConfigurator createViewerConfiguator(final String viewerConfigId,
			final TemplatePersistenceData data,
			final TemplateVariableProcessor templateProcessor, final IProject project) {
		final RProject rProject= RProjects.getRProject(project);
		return new LtxRweaveTemplateViewerConfigurator(
				TexRweaveCore.TEX_WORKBENCH_ACCESS, (rProject != null) ? rProject : RCore.WORKBENCH_ACCESS,
				templateProcessor );
	}
	
}
