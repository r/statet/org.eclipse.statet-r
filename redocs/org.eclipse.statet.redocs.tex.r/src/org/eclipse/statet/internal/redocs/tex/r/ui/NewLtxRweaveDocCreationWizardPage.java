/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.docmlet.tex.core.project.TexProjects;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizardPage;
import org.eclipse.statet.r.core.project.RProjects;


/**
 * The "New" wizard page allows setting the container for
 * the new file as well as the file name. The page
 * will only accept file name without the extension or
 * with the extension that matches the expected one (r).
 */
public class NewLtxRweaveDocCreationWizardPage extends NewElementWizardPage {
	
	
	private static final String fgDefaultExtension= ".Rnw"; //$NON-NLS-1$
	
	
	ResourceGroup resourceGroup;
	
	
	public NewLtxRweaveDocCreationWizardPage(final IStructuredSelection selection) {
		super("NewLtxRweaveDocCreationWizardPage", selection); //$NON-NLS-1$
		
		setTitle(Messages.NewDocWizardPage_title);
		setDescription(Messages.NewDocWizardPage_description);
		
		this.resourceGroup= new ResourceGroup(fgDefaultExtension, new ProjectNatureContainerFilter(
				ImCollections.newSet(TexProjects.TEX_NATURE_ID, RProjects.R_NATURE_ID) ));
	}
	
	@Override
	protected void createContents(final Composite parent) {
		this.resourceGroup.createGroup(parent);
	}
	
	@Override
	public void setVisible(final boolean visible) {
		super.setVisible(visible);
		if (visible) {
			this.resourceGroup.setFocus();
		}
	}
	
	public void saveSettings() {
		this.resourceGroup.saveSettings();
	}
	
	@Override
	protected void validatePage() {
		updateStatus(this.resourceGroup.validate());
	}
	
}
