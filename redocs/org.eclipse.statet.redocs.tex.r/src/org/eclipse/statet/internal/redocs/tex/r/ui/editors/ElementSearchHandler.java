/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.editors;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;

import org.eclipse.statet.ltk.ui.sourceediting.actions.MultiContentSectionHandler;
import org.eclipse.statet.r.ui.editors.RElementSearchHandler;
import org.eclipse.statet.redocs.tex.r.core.source.doc.LtxRweaveDocumentContentInfo;


public class ElementSearchHandler extends MultiContentSectionHandler implements IExecutableExtension {
	
	
	private String commandId;
	
	
	public ElementSearchHandler() {
		super(LtxRweaveDocumentContentInfo.INSTANCE);
	}
	
	
	@Override
	public void setInitializationData(final IConfigurationElement config,
			final String propertyName, final Object data) throws CoreException {
		if (this.commandId == null) {
			final String s= config.getAttribute("commandId"); //$NON-NLS-1$
			if (s != null && !s.isEmpty()) {
				this.commandId= s.intern();
			}
		}
	}
	
	@Override
	protected IHandler2 createHandler(final String sectionType) {
		switch (sectionType) {
		case LtxRweaveDocumentContentInfo.R:
			return new RElementSearchHandler(this.commandId);
		default:
			return null;
		}
	}
	
}
