/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.processing;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfigIOStepTab;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfigMainTab;
import org.eclipse.statet.docmlet.base.ui.processing.operations.DocProcessingConfigOpenFileSetting;
import org.eclipse.statet.redocs.r.ui.RedocsRUIResources;
import org.eclipse.statet.redocs.r.ui.processing.RWeaveDocProcessingConfig;
import org.eclipse.statet.redocs.r.ui.processing.RunRCmdToolOperationSettings;
import org.eclipse.statet.redocs.r.ui.processing.RunRConsoleSnippetOperation;
import org.eclipse.statet.redocs.r.ui.processing.RunRConsoleSnippetOperationSettings;


@NonNullByDefault
public class TexTab extends DocProcessingConfigIOStepTab {
	
	
	private final DocProcessingConfigOpenFileSetting openResult;
	
	
	public TexTab(final DocProcessingConfigMainTab mainTab) {
		super(mainTab, RWeaveDocProcessingConfig.WEAVE_ATTR_QUALIFIER);
		
		setInput(TexRweaveConfig.SOURCE_FORMAT, null);
		setAvailableOutputFormats(TexRweaveConfig.WEAVE_OUTPUT_FORMATS,
				TexRweaveConfig.EXT_LTX_FORMAT_KEY );
		
		setAvailableOperations(ImCollections.newList(
				new RunRConsoleSnippetOperationSettings(),
				new RunRCmdToolOperationSettings() ));
		
		final Realm realm= getRealm();
		this.openResult= new DocProcessingConfigOpenFileSetting(
				RWeaveDocProcessingConfig.WEAVE_OPEN_RESULT_ATTR_NAME, realm );
	}
	
	
	@Override
	public Image getImage() {
		return RedocsRUIResources.INSTANCE.getImage(RedocsRUIResources.TOOL_RWEAVE_IMAGE_ID);
	}
	
	@Override
	public String getName() {
		return createName(Messages.WeaveTab_name);
	}
	
	@Override
	public String getLabel() {
		return Messages.Weave_label;
	}
	
	
	private void updateInput() {
		setInput(TexRweaveConfig.SOURCE_FORMAT, getMainTab().getSourceFile());
	}
	
	
	@Override
	protected void addControls(final Composite parent) {
		updateInput();
		
		super.addControls(parent);
		
		{	final Composite group= createPostGroup(parent);
			group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
	}
	
	@Override
	protected Composite createPostGroup(final Composite parent) {
		final Composite group= super.createPostGroup(parent);
		group.setLayout(LayoutUtils.newGroupGrid(2));
		
		{	final ComboViewer viewer= this.openResult.createControls(group, Messages.WeaveTab_OpenResult_label);
			viewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		
		return group;
	}
	
	@Override
	protected void addBindings(final DataBindingContext dbc) {
		super.addBindings(dbc);
		
		this.openResult.addBindings(dbc);
	}
	
	
	@Override
	protected String getDefaultOperationId() {
		return RunRConsoleSnippetOperation.ID;
	}
	
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		super.setDefaults(configuration);
	}
	
	@Override
	protected void doInitialize(final ILaunchConfiguration configuration) {
		super.doInitialize(configuration);
		
		try {
			this.openResult.load(configuration);
		}
		catch (final CoreException e) {
			logReadingError(e);
		}
	}
	
	@Override
	protected void doSave(final ILaunchConfigurationWorkingCopy configuration) {
		super.doSave(configuration);
		
		this.openResult.save(configuration);
	}
	
}
