/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.sourceediting;


import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.SearchPattern;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.redocs.tex.r.RedocsTexRPlugin;
import org.eclipse.statet.internal.redocs.tex.r.core.LtxRweaveTemplateContextType;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal.TemplateProposalParameters;
import org.eclipse.statet.redocs.r.ui.RedocsRUIResources;


@NonNullByDefault
public class RChunkTemplateCompletionComputer extends TemplateCompletionComputer {
	
	
	public RChunkTemplateCompletionComputer() {
		super(	RedocsTexRPlugin.getInstance().getCodegenTemplateStore(),
				RedocsTexRPlugin.getInstance().getCodegenTemplateContextTypeRegistry() );
	}
	
	
	@Override
	public void computeCompletionProposals(final AssistInvocationContext context, int mode,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		// Set to specific mode to force to include templates in default mode
		if (mode == COMBINED_MODE) {
			mode= SPECIFIC_MODE;
		}
		
		super.computeCompletionProposals(context, mode, proposals, monitor);
	}
	
	@Override
	protected boolean include(final Template template, final TemplateProposalParameters<?> parameters) {
		parameters.matchRule= SearchPattern.OTHER_MATCH;
		return true;
	}
	
	@Override
	protected @Nullable String extractPrefix(final AssistInvocationContext context) {
		final IDocument document= context.getSourceViewer().getDocument();
		final int offset= context.getOffset();
		try {
			final int lineOffset= document.getLineOffset(document.getLineOfOffset(offset));
			switch (offset - lineOffset) {
			case 2:
				if (document.getChar(lineOffset) == '<' && document.getChar(lineOffset + 1) == '<') {
					return "<<"; //$NON-NLS-1$
				}
				break;
			case 1:
				if (document.getChar(lineOffset) == '<') {
					return "<"; //$NON-NLS-1$
				}
				break;
			case 0:
				return ""; //$NON-NLS-1$
			default:
				break;
			}
			return null;
		}
		catch (final BadLocationException e) {
			return null;
		}
	}
	
	@Override
	protected TemplateContextType getContextType(final AssistInvocationContext context,
			final TextRegion region) {
		return getTypeRegistry().getContextType(LtxRweaveTemplateContextType.WEAVE_DOCDEFAULT_CONTEXTTYPE);
	}
	
	@Override
	protected Image getImage(final Template template) {
		return RedocsRUIResources.INSTANCE.getImage(RedocsRUIResources.OBJ_RCHUNK_IMAGE_ID);
	}
	
}
