/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class TexRweaveModel {
	
	
	/**
	 * Model type id for Sweave (LaTeX+R) documents
	 */
	public static final String LTX_R_MODEL_TYPE_ID= "LtxRweave"; //$NON-NLS-1$
	
}
