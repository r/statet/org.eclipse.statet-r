/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.core.source.doc;

import org.eclipse.jface.text.rules.ICharacterScanner;

import org.eclipse.statet.r.core.source.doc.RPartitionNodeType;
import org.eclipse.statet.redocs.r.core.source.AbstractRChunkPartitionNodeScanner;


/**
 * Partition scanner for R chunks (stops if find '@' at column 0).
 */
class RChunkPartitionNodeScanner extends AbstractRChunkPartitionNodeScanner {
	
	
	public RChunkPartitionNodeScanner() {
	}
	
	
	@Override
	protected void handleNewLine(final RPartitionNodeType type) {
		if (getChunkNode() != null) {
			switch (this.reader.read()) {
			case ICharacterScanner.EOF:
				return;
			case '<':
				if (this.reader.read('<')) {
					exitToChunkBase(START_LINE, this.reader.getOffset() - 2);
					addNode(R_CHUNK_CONTROL_TYPE, this.reader.getOffset());
					this.last= LAST_OTHER;
					return;
				}
				break;
			case '@':
				exitToChunkBase(STOP_LINE, this.reader.getOffset() - 1);
				addNode(R_CHUNK_COMMENT_TYPE, this.reader.getOffset());
				this.last= LAST_OTHER;
				return;
			}
			this.reader.unread();
		}
	}
	
	@Override
	protected void processChunkControlOpen(final byte chunkLink) {
		LOOP: while (true) {
			switch (this.reader.read()) {
			case ICharacterScanner.EOF:
				this.last= LAST_EOF;
				return;
			case '\r':
				this.reader.read('\n');
				//$FALL-THROUGH$
			case '\n':
				exitToChunkBase(getChunkLine(), this.reader.getOffset());
				addNode(getDefaultRootType(), this.reader.getOffset());
				this.last= LAST_NEWLINE;
				return;
			case '>':
				if (this.reader.read('>')) {
					exitToChunkBase(chunkLink, this.reader.getOffset() - 2);
					this.reader.read('=');
					addNode(R_CHUNK_COMMENT_TYPE, this.reader.getOffset());
					this.last= LAST_OTHER;
					return;
				}
				continue LOOP;
			default:
				continue LOOP;
			}
		}
	}
	
}
