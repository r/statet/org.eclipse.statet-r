/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.core.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;
import org.eclipse.statet.docmlet.tex.core.util.TexCoreAccessWrapper;
import org.eclipse.statet.internal.redocs.tex.r.core.SweaveLtxCommands;


/**
 * Enriched TexCoreAccess for TexRweave
 */
@NonNullByDefault
public class TexRweaveCoreAccessWrapper extends TexCoreAccessWrapper {
	
	
	private static final ImList<TexCommand> LTX_SWEAVE_COMMAND_LIST= ImCollections.newList( // ASorted
			SweaveLtxCommands.SWEAVE_Sexpr_COMMANDS,
			SweaveLtxCommands.SWEAVE_SweaveOpts_COMMANDS );
	
	private static final WeakHashMap<TexCommandSet, TexCommandSet> COMMAND_CACHE= new WeakHashMap<>();
	
	
	private TexCommandSet texCommandSetOrg;
	private volatile TexCommandSet texCommandSet;
	
	
	public TexRweaveCoreAccessWrapper(final TexCoreAccess texCoreAccess) {
		super(texCoreAccess);
	}
	
	
	@Override
	public TexCommandSet getTexCommandSet() {
		if (this.texCommandSetOrg != super.getTexCommandSet()) {
			updateTexCommandSet();
		}
		return this.texCommandSet;
	}
	
	private void updateTexCommandSet() {
		synchronized (COMMAND_CACHE) {
			final TexCommandSet org= super.getTexCommandSet();
			if (this.texCommandSetOrg == org) {
				return;
			}
			TexCommandSet set= COMMAND_CACHE.get(org);
			if (set == null) {
				set= new TexCommandSet(addSweaveList(org.getAllLtxCommands()), org.getAllLtxEnvs(),
						addSweaveMap(org.getLtxTextCommandsASorted()), addSweaveListASorted(org.getLtxTextCommandsASorted()),
						org.getLtxTextEnvMap(), org.getLtxTextEnvsASorted(),
						org.getLtxPreambleCommandMap(), org.getLtxPreambleCommandsASorted(),
						addSweaveMap(org.getLtxMathCommandsASorted()), addSweaveListASorted(org.getLtxMathCommandsASorted()),
						org.getLtxMathEnvMap(), org.getLtxMathEnvsASorted(),
						org.getLtxInternEnvMap() );
				COMMAND_CACHE.put(org, set);
			}
			this.texCommandSet= set;
			this.texCommandSetOrg= org;
		}
	}
	
	private Map<String, TexCommand> addSweaveMap(final List<TexCommand> org) {
		final Map<String, TexCommand> map= new HashMap<>(org.size() + 2);
		for (final TexCommand command : org) {
			map.put(command.getControlWord(), command);
		}
		for (final TexCommand command : LTX_SWEAVE_COMMAND_LIST) {
			map.put(command.getControlWord(), command);
		}
		return Collections.unmodifiableMap(map);
	}
	
	private List<TexCommand> addSweaveList(final List<TexCommand> org) {
		return ImCollections.concatList(org, LTX_SWEAVE_COMMAND_LIST);
	}
	
	private ImList<TexCommand> addSweaveListASorted(final List<TexCommand> org) {
		final ImList<TexCommand> list= ImCollections.concatList(org, LTX_SWEAVE_COMMAND_LIST,
				(Comparator<TexCommand>) null );
		return list;
	}
	
}
