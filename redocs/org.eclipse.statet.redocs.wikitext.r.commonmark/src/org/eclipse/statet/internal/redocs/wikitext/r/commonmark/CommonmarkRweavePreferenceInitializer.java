/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.commonmark;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkLanguage;
import org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.NewDocTemplateCategoryConfiguration;


public class CommonmarkRweavePreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public CommonmarkRweavePreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IScopeContext scope= DefaultScope.INSTANCE;
		
		{	final IEclipsePreferences node= scope.getNode(CommonmarkRweavePlugin.BUNDLE_ID + "/markup/" + RCommonmarkLanguage.COMMONMARK_RWEAVE_LANGUAGE_NAME); //$NON-NLS-1$
			node.put("MarkupConfig.Workbench.config", //$NON-NLS-1$
					"Commonmark:yaml_metadata_block;" + //$NON-NLS-1$
					"tex_math_dollars;" + //$NON-NLS-1$
					"tex_math_single_backslash;" + //$NON-NLS-1$
					"Paragraph+Header=Blank;" + //$NON-NLS-1$
					"Paragraph+Blockquote=Blank;" + //$NON-NLS-1$
					"Superscript=^;Subscript=~" ); //$NON-NLS-1$
		}
		{	final IEclipsePreferences node= scope.getNode(CommonmarkRweavePlugin.TEMPLATES_QUALIFIER);
			node.put(NewDocTemplateCategoryConfiguration.NEWDOC_DEFAULT_NAME_KEY,
					CommonmarkRweavePlugin.NEWDOC_TEMPLATE_CATEGORY_ID + ':' + "Article" ); //$NON-NLS-1$
		}
	}
	
}
