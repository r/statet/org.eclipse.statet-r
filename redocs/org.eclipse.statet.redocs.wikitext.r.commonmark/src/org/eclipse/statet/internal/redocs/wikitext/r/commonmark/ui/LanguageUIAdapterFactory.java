/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui;

import org.eclipse.core.runtime.IAdapterFactory;

import org.eclipse.statet.docmlet.wikitext.commonmark.ui.CommonmarkCompletionExtension;
import org.eclipse.statet.docmlet.wikitext.ui.sourceediting.MarkupCompletionExtension;


public class LanguageUIAdapterFactory implements IAdapterFactory {
	
	
	private static final Class<?>[] ADAPTERS= new Class[] {
		MarkupCompletionExtension.class
	};
	
	
	private MarkupCompletionExtension markupCompletion;
	
	
	public LanguageUIAdapterFactory() {
	}
	
	@Override
	public Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == MarkupCompletionExtension.class) {
			synchronized (this) {
				if (this.markupCompletion == null) {
					this.markupCompletion= new CommonmarkCompletionExtension();
				}
				return (T) this.markupCompletion;
			}
		}
		return null;
	}
	
}
