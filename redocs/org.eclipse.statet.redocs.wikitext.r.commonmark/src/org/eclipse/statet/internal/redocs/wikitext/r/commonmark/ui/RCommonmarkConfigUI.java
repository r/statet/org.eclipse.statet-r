/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui;

import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.docmlet.wikitext.commonmark.ui.AbstractCommonmarkConfigDialog;
import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupConfig;
import org.eclipse.statet.docmlet.wikitext.ui.config.MarkupConfigUIAdapter;
import org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkConfigImpl;


public class RCommonmarkConfigUI implements MarkupConfigUIAdapter {
	
	
	static class RCommonmarkConfigDialog extends AbstractCommonmarkConfigDialog<RCommonmarkConfigImpl> {
		
		
		public RCommonmarkConfigDialog(final Shell parent, final String contextLabel,
				final boolean isContextEnabeld, final RCommonmarkConfigImpl config) {
			super(parent, contextLabel, isContextEnabeld, config);
		}
		
		
		@Override
		protected Composite createExtensionGroup(final Composite parent) {
			final Composite composite= super.createExtensionGroup(parent);
			
			addProperty(composite, RCommonmarkConfigImpl.YAML_METADATA_ENABLED_PROP);
			addProperty(composite, RCommonmarkConfigImpl.TEX_MATH_DOLLARS_ENABLED_PROP);
			addProperty(composite, RCommonmarkConfigImpl.TEX_MATH_SBACKSLASH_ENABLED_PROP);
			LayoutUtils.addSmallFiller(composite, false);
			addProperty(composite, RCommonmarkConfigImpl.HEADER_INTERRUPT_PARAGRAPH_DISABLED_PROP);
			addProperty(composite, RCommonmarkConfigImpl.BLOCKQUOTE_INTERRUPT_PARAGRAPH_DISABLED_PROP);
			LayoutUtils.addSmallFiller(composite, false);
			addProperty(composite, RCommonmarkConfigImpl.STRIKEOUT_DTILDE_ENABLED_PROP);
			addProperty(composite, RCommonmarkConfigImpl.SUPERSCRIPT_SCIRCUMFLEX_ENABLED_PROP);
			addProperty(composite, RCommonmarkConfigImpl.SUBSCRIPT_STILDE_ENABLED_PROP);
			
			return composite;
		}
		
	}
	
	
	@Override
	public boolean edit(final String contextLabel, final AtomicBoolean isContextEnabled, final MarkupConfig config, final Shell parent) {
		final RCommonmarkConfigDialog dialog= new RCommonmarkConfigDialog(parent, contextLabel,
				(isContextEnabled != null && isContextEnabled.get()), (RCommonmarkConfigImpl) config );
		if (dialog.open() == Dialog.OK) {
			if (isContextEnabled != null) {
				isContextEnabled.set(dialog.isCustomEnabled());
			}
			return true;
		}
		return false;
	}
	
}
