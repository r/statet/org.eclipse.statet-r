/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.textile.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.AbstractMarkupConfig;
import org.eclipse.statet.redocs.wikitext.r.textile.core.RTextileConfig;


@NonNullByDefault
public class RTextileConfigImpl extends AbstractMarkupConfig<RTextileConfigImpl> implements RTextileConfig {
	
	
	public RTextileConfigImpl() {
	}
	
	public RTextileConfigImpl(final RTextileConfigImpl config) {
		load(config);
	}
	
	
	@Override
	public String getConfigType() {
		return "Textile"; //$NON-NLS-1$
	}
	
	@Override
	public RTextileConfigImpl clone() {
		return new RTextileConfigImpl(this);
	}
	
}
