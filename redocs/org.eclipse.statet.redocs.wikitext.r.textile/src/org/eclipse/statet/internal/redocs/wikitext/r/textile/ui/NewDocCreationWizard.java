/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.textile.ui;

import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.statet.internal.redocs.wikitext.r.textile.Messages;
import org.eclipse.statet.internal.redocs.wikitext.r.textile.TextileRweavePlugin;
import org.eclipse.statet.ltk.ui.templates.NewDocTemplateGenerateWizardPage;
import org.eclipse.statet.redocs.wikitext.r.ui.codegen.NewWikidocRweaveDocCreationWizard;
import org.eclipse.statet.redocs.wikitext.r.ui.codegen.NewWikidocRweaveDocCreationWizardPage;


public class NewDocCreationWizard extends NewWikidocRweaveDocCreationWizard {
	
	
	public NewDocCreationWizard() {
		super(TextileRweavePlugin.DOC_CONTENT_TYPE);
		setWindowTitle(Messages.NewDocWizard_title);
	}
	
	
	@Override
	protected NewWikidocRweaveDocCreationWizardPage createFirstPage(final IStructuredSelection selection) {
		final NewWikidocRweaveDocCreationWizardPage page= new NewWikidocRweaveDocCreationWizardPage(
				selection, ".Rtextile" ); //$NON-NLS-1$
		page.setTitle(Messages.NewDocWizardPage_title);
		page.setDescription(Messages.NewDocWizardPage_description);
		return page;
	}
	
	@Override
	protected NewDocTemplateGenerateWizardPage createTemplatePage() {
		final NewDocTemplateGenerateWizardPage page= new NewDocTemplateGenerateWizardPage(
				new NewDocTemplateCategoryConfiguration(),
				TextileRweavePlugin.NEWDOC_TEMPLATE_CATEGORY_ID );
		return page;
	}
	
}
