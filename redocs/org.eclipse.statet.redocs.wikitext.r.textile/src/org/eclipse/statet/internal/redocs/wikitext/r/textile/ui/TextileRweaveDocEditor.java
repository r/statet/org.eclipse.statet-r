/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.textile.ui;

import org.eclipse.statet.internal.redocs.wikitext.r.textile.TextileRweavePlugin;
import org.eclipse.statet.redocs.wikitext.r.ui.editors.WikidocRweaveDocEditor;


public class TextileRweaveDocEditor extends WikidocRweaveDocEditor {
	
	
	public TextileRweaveDocEditor() {
		super(TextileRweavePlugin.DOC_CONTENT_TYPE, new TextileRweaveDocumentSetupParticipant());
	}
	
	
}
