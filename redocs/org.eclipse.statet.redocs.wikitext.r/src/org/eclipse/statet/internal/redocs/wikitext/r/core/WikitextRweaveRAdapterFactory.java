/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.core;

import org.eclipse.core.runtime.IAdapterFactory;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitEmbeddedModelReconciler;
import org.eclipse.statet.internal.redocs.wikitext.r.core.model.WikidocREmbeddedModelReconciler;


@NonNullByDefault
public class WikitextRweaveRAdapterFactory implements IAdapterFactory {
	
	
	private static final @NonNull Class<?>[] ADAPTERS= new @NonNull Class<?>[] {
		WikidocSourceUnitEmbeddedModelReconciler.class,
	};
	
	
	public WikitextRweaveRAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == WikidocSourceUnitEmbeddedModelReconciler.class) {
			return (T)new WikidocREmbeddedModelReconciler();
		}
		return null;
	}
	
}
