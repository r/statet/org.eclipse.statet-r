/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.core.model;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.ast.Embedded;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstInfo;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitEmbeddedModelReconciler;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.redocs.r.core.model.RChunkReconcileConfig;
import org.eclipse.statet.redocs.wikitext.r.core.source.RweaveMarkupLanguage;


@NonNullByDefault
public class WikidocREmbeddedModelReconciler implements WikidocSourceUnitEmbeddedModelReconciler<RChunkReconcileConfig> {
	
	
	public WikidocREmbeddedModelReconciler() {
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	private @Nullable WikidocRChunkReconciler getReconciler(final WikitextMarkupLanguage markupLanguage) {
		if (markupLanguage instanceof RweaveMarkupLanguage) {
			return WikidocRChunkReconciler.getInstance((RweaveMarkupLanguage)markupLanguage);
		}
		return null;
	}
	
	private @Nullable WikidocRChunkReconciler getReconciler(final WikitextAstInfo astInfo) {
		return getReconciler(astInfo.getMarkupLanguage());
	}
	
	@Override
	public RChunkReconcileConfig createConfig(final @Nullable IProject wsProject,
			final @Nullable WikitextProject wikitextProject,
			final int level) {
		final RProject rProject= RProjects.getRProject(wsProject);
		final RCoreAccess rCoreAccess= (rProject != null) ? rProject : RCore.getWorkbenchAccess();
		return new RChunkReconcileConfig(rCoreAccess, wikitextProject);
	}
	
	
	@Override
	public void reconcileAst(final SourceContent content,
			final List<Embedded> list,
			final WikitextMarkupLanguage markupLanguage,
			final WikidocSourceUnitModelContainer<?> container, final RChunkReconcileConfig config, final int level) {
		final WikidocRChunkReconciler reconciler;
		if (container instanceof WikidocRweaveSuModelContainer
				&& (reconciler= getReconciler(markupLanguage)) != null ) {
			reconciler.reconcileAst((WikidocRweaveSuModelContainer)container,
					content, list, config.getSourceConfig() );
		}
	}
	
	@Override
	public void reconcileModel(final WikidocSourceUnitModelInfo wikitextModel, final SourceContent content,
			final List<? extends EmbeddingForeignReconcileTask<Embedded, WikitextSourceElement>> list,
			final WikidocSourceUnitModelContainer<?> container, final RChunkReconcileConfig config, final int level,
			final SubMonitor m) {
		final WikidocRChunkReconciler reconciler;
		if (container instanceof WikidocRweaveSuModelContainer
				&& (reconciler= getReconciler(wikitextModel.getAst())) != null) {
			reconciler.reconcileModel((WikidocRweaveSuModelContainer)container,
					content, wikitextModel, list, level, m);
		}
	}
	
	@Override
	public void reportIssues(final WikidocSourceUnitModelInfo wikitextModel, final SourceContent content,
			final IssueRequestor issueRequestor,
			final WikidocSourceUnitModelContainer<?> container, final RChunkReconcileConfig config, final int level) {
		final WikidocRChunkReconciler reconciler;
		if (container instanceof WikidocRweaveSuModelContainer
				&& (reconciler= getReconciler(wikitextModel.getAst())) != null) {
			reconciler.reportEmbeddedIssues((WikidocRweaveSuModelContainer)container,
					content, wikitextModel, issueRequestor, config, level );
		}
	}
	
}