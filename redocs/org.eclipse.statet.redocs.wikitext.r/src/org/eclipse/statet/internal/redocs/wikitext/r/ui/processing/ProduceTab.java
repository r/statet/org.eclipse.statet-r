/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.processing;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig.Format;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfigIOStepTab;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfigMainTab;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfigStepTab;
import org.eclipse.statet.docmlet.base.ui.processing.operations.RunExternalProgramOperationSettings;
import org.eclipse.statet.redocs.r.ui.RedocsRUIResources;
import org.eclipse.statet.redocs.r.ui.processing.RunRCmdToolOperationSettings;
import org.eclipse.statet.redocs.r.ui.processing.RunRConsoleSnippetOperation;
import org.eclipse.statet.redocs.r.ui.processing.RunRConsoleSnippetOperationSettings;


@NonNullByDefault
public class ProduceTab extends DocProcessingConfigIOStepTab
		implements DocProcessingConfigStepTab.Listener {
	
	
	private final DocProcessingConfigIOStepTab weaveTab;
	
	
	public ProduceTab(final DocProcessingConfigMainTab mainTab,
			final DocProcessingConfigIOStepTab weaveTab) {
		super(mainTab, WikitextRweaveConfig.PRODUCE_ATTR_QUALIFIER);
		
		this.weaveTab= weaveTab;
		this.weaveTab.addListener(this);
		
		setInput(WikitextRweaveConfig.SOURCE_FORMAT, null);
		setAvailableOutputFormats(WikitextRweaveConfig.PRODUCE_OUTPUT_FORMATS,
				WikitextRweaveConfig.AUTO_YAML_FORMAT_KEY );
		
		setAvailableOperations(ImCollections.newList(
				new RunRConsoleSnippetOperationSettings(),
				new RunRCmdToolOperationSettings(),
				new RunExternalProgramOperationSettings() ));
		
		this.weaveTab.getOutputFileValue().addValueChangeListener(this);
	}
	
	
	@Override
	public Image getImage() {
		return RedocsRUIResources.INSTANCE.getImage(RedocsRUIResources.TOOL_BUILDTEX_IMAGE_ID);
	}
	
	@Override
	public String getName() {
		return createName(Messages.ProduceTab_name);
	}
	
	@Override
	public String getLabel() {
		return Messages.Produce_label;
	}
	
	@Override
	public void changed(final DocProcessingConfigStepTab source) {
		if (source == this.weaveTab) {
			updateInput();
		}
	}
	
	@Override
	public void handleValueChange(final ValueChangeEvent<?> event) {
		if (event.getObservable() == this.weaveTab.getOutputFileValue()
				&& this.weaveTab.isEnabled() ) {
			updateInput();
			return;
		}
		super.handleValueChange(event);
	}
	
	private void updateInput() {
		if (this.weaveTab.isEnabled()) {
			final Format format= this.weaveTab.getOutputFormat();
			setInput((format != null) ?
							WikitextRweaveConfig.createWeaveOutputFormat(format) :
							null,
					this.weaveTab.getOutputFile() );
		}
		else {
			setInput(WikitextRweaveConfig.SOURCE_FORMAT,
					getMainTab().getSourceFile() );
		}
	}
	
	@Override
	protected @Nullable IFile getInputFile(final Format format) throws CoreException {
		if (this.weaveTab.isEnabled()) {
			return this.weaveTab.getOutputFile();
		}
		return null;
	}
	
	@Override
	protected void addBindings(final DataBindingContext dbc) {
		super.addBindings(dbc);
	}
	
	
	@Override
	protected String getDefaultOperationId() {
		return RunRConsoleSnippetOperation.ID;
	}
	
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		super.setDefaults(configuration);
	}
	
	@Override
	protected void doInitialize(final ILaunchConfiguration configuration) {
		super.doInitialize(configuration);
	}
	
	@Override
	protected void doSave(final ILaunchConfigurationWorkingCopy configuration) {
		super.doSave(configuration);
	}
	
}
