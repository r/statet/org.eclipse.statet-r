/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.processing;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.redocs.r.ui.processing.RWeaveDocProcessingConfig;


@NonNullByDefault
public class WikitextRweaveConfig extends RWeaveDocProcessingConfig {
	
	
	public static final String TYPE_ID= "org.eclipse.statet.redocs.launchConfigurations.WikitextRweaveDocProcessing"; //$NON-NLS-1$
	
	
/*[ Attributes ]===============================================================*/
	
	// see RWeaveDocProcessingConfig
	
	
/*[ Formats ]==================================================================*/
	
	
	public static final String AUTO_WIKITEXT_FORMAT_KEY= Format.AUTO_TYPE + ":wikitext"; //$NON-NLS-1$
	
	
	public static final Format SOURCE_FORMAT= createSourceFormat(
			Messages.Format_WikitextRweave_label );
	
	public static final Format AUTO_WIKITEXT_FORMAT= new Format(AUTO_WIKITEXT_FORMAT_KEY,
			Messages.Format_AutoWikitext_label, "md / textile / \u2026") { //$NON-NLS-1$
		
		@Override
		public @Nullable String getExt(final @Nullable String inputExt) {
			if (inputExt != null && inputExt.length() > 1
					&& (inputExt.charAt(0) == 'R' || inputExt.charAt(0) == 'r') ) {
				return inputExt.substring(1);
			}
			return null;
		}
		
	};
	
	public static Format createWeaveOutputFormat(final Format format) {
		return new Format(format.getKey(),
				NLS.bind(Messages.Format_RweaveResult_label, format.getInfoLabel()),
				format.getExt() );
	}
	
	public static final ImList<Format> WEAVE_OUTPUT_FORMATS= ImCollections.newList(
			WikitextRweaveConfig.AUTO_WIKITEXT_FORMAT,
			WikitextRweaveConfig.EXT_LTX_FORMAT,
			WikitextRweaveConfig.EXT_OTHER_FORMAT );
	
	public static final ImList<Format> PRODUCE_OUTPUT_FORMATS= ImCollections.newList(
			WikitextRweaveConfig.AUTO_YAML_FORMAT,
			WikitextRweaveConfig.EXT_PDF_FORMAT,
			WikitextRweaveConfig.EXT_HTML_FORMAT,
			WikitextRweaveConfig.EXT_OTHER_FORMAT );
	
	
}
