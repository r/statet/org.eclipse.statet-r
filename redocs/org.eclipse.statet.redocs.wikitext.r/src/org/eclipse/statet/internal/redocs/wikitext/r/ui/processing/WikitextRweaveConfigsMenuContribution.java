/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.processing;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI.CommonFlags;
import org.eclipse.statet.docmlet.base.ui.processing.actions.RunDocConfigsMenuContribution;
import org.eclipse.statet.redocs.r.ui.RedocsRUI;


@NonNullByDefault
public class WikitextRweaveConfigsMenuContribution extends RunDocConfigsMenuContribution {
	
	
	private static final String PROCESS_TEX_COMMAND_ID= "org.eclipse.statet.docmlet.commands.ProcessTexDefault"; //$NON-NLS-1$
	
	
	private class ThisConfigContribution extends DocConfigContribution {
		
		
		public ThisConfigContribution(final String label, final Image icon,
				final ILaunchConfiguration configuration) {
			super(label, icon, configuration);
		}
		
		
		@Override
		protected void addLaunchItems(final Menu menu) {
			String buildDetail= null;
			String weaveDetail= null;
			String produceDetail= null;
			try {
				final ILaunchConfiguration config= getConfiguration();
				final IFile file= getElement();
				final String sourceInputExt= resolveFormatExt(WikitextRweaveConfig.SOURCE_FORMAT,
						file.getFileExtension() );
				final boolean weaveEnabled= config.getAttribute(WikitextRweaveConfig.WEAVE_ENABLED_ATTR_NAME, false);
				final String weaveInputExt= sourceInputExt;
				final String weaveOutputExt= resolveFormatExt(WikitextRweaveConfig.getFormat(
							WikitextRweaveConfig.WEAVE_OUTPUT_FORMATS, null,
							config.getAttribute(WikitextRweaveConfig.WEAVE_OUTPUT_FORMAT_ATTR_NAME, (String) null) ),
						weaveInputExt );
				final boolean produceEnabled= config.getAttribute(WikitextRweaveConfig.PRODUCE_ENABLED_ATTR_NAME, false);
				final String produceInputExt= (weaveEnabled) ? weaveOutputExt : sourceInputExt;
				final String produceOutputExt= resolveFormatExt(WikitextRweaveConfig.getFormat(
							WikitextRweaveConfig.PRODUCE_OUTPUT_FORMATS, null,
							config.getAttribute(WikitextRweaveConfig.PRODUCE_OUTPUT_FORMAT_ATTR_NAME, (String) null) ),
						produceInputExt );
				
				if (weaveEnabled | produceEnabled) {
					buildDetail= createDetail(
							(weaveEnabled) ? weaveInputExt : produceInputExt,
							(produceEnabled) ? produceOutputExt : weaveOutputExt );
				}
				weaveDetail= createDetail(weaveInputExt, weaveOutputExt);
				produceDetail= createDetail(produceInputExt, produceOutputExt);
			}
			catch (final CoreException e) {
			}
			
			addLaunchItem(menu, CommonFlags.PROCESS_AND_PREVIEW, null, true,
					DocProcessingUI.PROCESS_AND_PREVIEW_DOC_DEFAULT_COMMAND_ID,
					DocProcessingUI.ACTIONS_RUN_CONFIG_HELP_CONTEXT_ID );
			addLaunchItem(menu, CommonFlags.PROCESS, buildDetail, (buildDetail != null),
					DocProcessingUI.PROCESS_DOC_DEFAULT_COMMAND_ID,
					DocProcessingUI.ACTIONS_RUN_CONFIG_PROCESS_HELP_CONTEXT_ID );
			
			new MenuItem(menu, SWT.SEPARATOR);
			
			addLaunchItem(menu, CommonFlags.WEAVE, weaveDetail, (weaveDetail != null),
					RedocsRUI.PROCESS_WEAVE_DEFAULT_COMMAND_ID,
					DocProcessingUI.ACTIONS_RUN_CONFIG_STEP_HELP_CONTEXT_ID );
			addLaunchItem(menu, CommonFlags.PRODUCE_OUTPUT, produceDetail, (produceDetail != null),
					PROCESS_TEX_COMMAND_ID,
					DocProcessingUI.ACTIONS_RUN_CONFIG_STEP_HELP_CONTEXT_ID );
			addLaunchItem(menu, CommonFlags.OPEN_OUTPUT, null, true,
					DocProcessingUI.PREVIEW_DOC_DEFAULT_COMMAND_ID,
					DocProcessingUI.ACTIONS_RUN_CONFIG_PREVIEW_HELP_CONTEXT_ID );
		}
		
	}
	
	
	public WikitextRweaveConfigsMenuContribution() {
		super(); // use dynamically content type of editor
	}
	
	
	@Override
	protected ConfigContribution createConfigContribution(final StringBuilder label,
			final Image icon,
			final ILaunchConfiguration configuration) {
		return new ThisConfigContribution(label.toString(), icon, configuration);
	}
	
}
