/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.sourceediting;


import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.SearchPattern;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguageDocumentSetupParticipant;
import org.eclipse.statet.internal.redocs.wikitext.r.RedocsWikitextRPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal.TemplateProposalParameters;
import org.eclipse.statet.redocs.r.ui.RedocsRUIResources;
import org.eclipse.statet.redocs.wikitext.r.core.source.RweaveMarkupLanguage;
import org.eclipse.statet.redocs.wikitext.r.core.source.WikitextRweaveTemplateContextType;


@NonNullByDefault
public class RChunkTemplateCompletionComputer extends TemplateCompletionComputer {
	
	
	private @Nullable RweaveMarkupLanguage markupLanguage;
	
	
	public RChunkTemplateCompletionComputer() {
		super(	RedocsWikitextRPlugin.getInstance().getCodegenTemplateStore(),
				RedocsWikitextRPlugin.getInstance().getCodegenTemplateContextTypeRegistry() );
	}
	
	
	// sessionStarted not called automatically, because computer is not registered in registry
	@Override
	public void onSessionStarted(final SourceEditor editor, final ContentAssist assist) {
		final WikitextMarkupLanguage markupLanguage= MarkupLanguageDocumentSetupParticipant.getMarkupLanguage(
				editor.getViewer().getDocument(), editor.getDocumentContentInfo().getPartitioning() );
		if (markupLanguage instanceof RweaveMarkupLanguage) {
			this.markupLanguage= (RweaveMarkupLanguage) markupLanguage;
		}
	}
	
	@Override
	public void onSessionEnded() {
		this.markupLanguage= null;
	}
	
	
	@Override
	public void computeCompletionProposals(final AssistInvocationContext context, int mode,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		// Set to specific mode to force to include templates in default mode
		if (mode == COMBINED_MODE) {
			mode= SPECIFIC_MODE;
		}
		
		onSessionStarted(context.getEditor(), null);
		try {
			if (this.markupLanguage == null) {
				return;
			}
			
			super.computeCompletionProposals(context, mode, proposals, monitor);
		}
		finally {
			onSessionEnded();
		}
	}
	
	@Override
	protected boolean include(final Template template, final TemplateProposalParameters<?> parameters) {
		final String templatePattern= template.getPattern();
		final int varIdx= templatePattern.indexOf("${"); //$NON-NLS-1$
		if ((varIdx < 0 || varIdx >= parameters.namePattern.getPattern().length())
				&& templatePattern.startsWith(parameters.namePattern.getPattern()) ) {
			parameters.matchRule= SearchPattern.OTHER_MATCH;
			return true;
		}
		return false;
	}
	
	@Override
	protected @Nullable String extractPrefix(final AssistInvocationContext context) {
		final IDocument document= context.getSourceViewer().getDocument();
		final int offset= context.getOffset();
		try {
			final int lineOffset= document.getLineOffset(document.getLineOfOffset(offset));
			String prefix= document.get(lineOffset, offset - lineOffset);
			final List<String> indentPrefixes= this.markupLanguage.getIndentPrefixes();
			ITER_PREFIX: while (!prefix.isEmpty()) {
				for (final String indentPrefix : indentPrefixes) {
					if (prefix.startsWith(indentPrefix)) {
						prefix= prefix.substring(indentPrefix.length());
						continue ITER_PREFIX;
					}
				}
				return prefix;
			}
			return ""; //$NON-NLS-1$
		}
		catch (final BadLocationException e) {
			return null;
		}
	}
	
	@Override
	protected @Nullable TemplateContextType getContextType(final AssistInvocationContext context,
			final TextRegion region) {
		final RweaveMarkupLanguage markupLanguage= this.markupLanguage;
		return (markupLanguage != null) ?
				getTypeRegistry().getContextType(markupLanguage.getName() +
						WikitextRweaveTemplateContextType.WEAVE_DOCDEFAULT_CONTEXTTYPE_SUFFIX) :
				null;
	}
	
	@Override
	protected TemplateProposal createProposal(final TemplateProposalParameters<?> parameters) {
		if (parameters.matchRule == SearchPattern.OTHER_MATCH) {
			parameters.baseRelevance= 90;
		}
		return super.createProposal(parameters);
	}
	
	@Override
	protected Image getImage(final Template template) {
		return RedocsRUIResources.INSTANCE.getImage(RedocsRUIResources.OBJ_RCHUNK_IMAGE_ID);
	}
	
}
