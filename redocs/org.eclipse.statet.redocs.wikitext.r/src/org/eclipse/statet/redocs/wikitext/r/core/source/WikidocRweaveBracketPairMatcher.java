/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.core.source;

import org.eclipse.statet.ecommons.text.ICharPairMatcher;
import org.eclipse.statet.ecommons.text.MultiContentSectionCharPairMatcher;

import org.eclipse.statet.docmlet.wikitext.core.source.MarkupBracketPairMatcher;
import org.eclipse.statet.docmlet.wikitext.core.source.WikitextHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.RHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.text.r.RBracketPairMatcher;
import org.eclipse.statet.redocs.wikitext.r.core.source.doc.WikidocRweaveDocumentContentInfo;
import org.eclipse.statet.redocs.wikitext.r.core.source.doc.WikitextRweaveDocumentConstants;


public class WikidocRweaveBracketPairMatcher extends MultiContentSectionCharPairMatcher {
	
	
	public static RBracketPairMatcher createRChunkPairMatcher(final RHeuristicTokenScanner scanner) {
		return new RBracketPairMatcher(scanner,
				new String[] {
					RDocumentConstants.R_DEFAULT_CONTENT_TYPE,
					WikitextRweaveDocumentConstants.RCHUNK_CONTROL_CONTENT_TYPE
				} );
	}
	
	
	public WikidocRweaveBracketPairMatcher() {
		super(WikidocRweaveDocumentContentInfo.INSTANCE);
	}
	
	
	@Override
	protected ICharPairMatcher createHandler(final String sectionType) {
		switch (sectionType) {
		case WikidocRweaveDocumentContentInfo.WIKITEXT:
			return new MarkupBracketPairMatcher(
					WikitextHeuristicTokenScanner.create(getSections()) );
		case WikidocRweaveDocumentContentInfo.R:
		case WikidocRweaveDocumentContentInfo.R_CHUNK_CONTROL:
			return createRChunkPairMatcher(
					RHeuristicTokenScanner.create(getSections()) );
		default:
			return null;
		}
	}
	
}
