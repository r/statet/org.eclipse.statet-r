/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.core.source.doc;

import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.BasicDocContentSections;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;

import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.redocs.r.core.source.DocContentSectionsRweaveExtension;
import org.eclipse.statet.redocs.r.core.source.RweaveDocSectionTreePartImpl;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentConstants;


@NonNullByDefault
public class WikidocRweaveDocumentContentInfo extends BasicDocContentSections
		implements DocContentSectionsRweaveExtension {
	
	
	public static final String WIKITEXT=                    WikitextDocumentConstants.WIKIDOC_PARTITIONING;
	public static final String YAML=                        YamlDocumentConstants.YAML_PARTITIONING;
	public static final String LTX=                         TexDocumentConstants.LTX_PARTITIONING;
	public static final String R_CHUNK_CONTROL=             "org.eclipse.statet.WikitextR-ChunkControl"; //$NON-NLS-1$
	public static final String R=                           RDocumentConstants.R_PARTITIONING;
	
	
	public static final DocContentSectionsRweaveExtension INSTANCE= new WikidocRweaveDocumentContentInfo();
	
	
	private final RweaveDocSectionTreePartImpl rweaveImpl;
	
	
	public WikidocRweaveDocumentContentInfo() {
		super(WikitextRweaveDocumentConstants.WIKIDOC_R_PARTITIONING, WIKITEXT,
				ImCollections.newList(WIKITEXT, YAML, LTX, R_CHUNK_CONTROL, R) );
		
		this.rweaveImpl= new RweaveDocSectionTreePartImpl(this);
	}
	
	
	@Override
	public final String getTypeByPartition(final String contentType) {
		if (RDocumentConstants.R_ANY_CONTENT_CONSTRAINT.matches(contentType)) {
			return R;
		}
		if (WikitextRweaveDocumentConstants.RCHUNK_PARTITION_CONSTRAINT.matches(contentType)) {
			return R_CHUNK_CONTROL;
		}
		if (YamlDocumentConstants.YAML_ANY_CONTENT_CONSTRAINT.matches(contentType)) {
			return YAML;
		}
		if (TexDocumentConstants.LTX_ANY_CONTENT_CONSTRAINT.matches(contentType)) {
			return LTX;
		}
		return WIKITEXT;
	}
	
	
	@Override
	public @Nullable TreePartitionNode getRChunkRegion(final IDocument document, final int offset)
			throws BadLocationException {
		return this.rweaveImpl.getRChunkRegion(document, offset);
	}
	
	@Override
	public List<TreePartitionNode> getRChunkRegions(final IDocument document,
			final int offset, final int length)
			throws BadLocationException {
		return this.rweaveImpl.getRChunkCodeRegions(document, offset, length);
	}
	
	@Override
	public @Nullable IRegion getRChunkContentRegion(final IDocument document, final int offset)
			throws BadLocationException {
		return this.rweaveImpl.getRChunkContentRegion(document, offset);
	}
	
	@Override
	public @Nullable TreePartitionNode getRCodeRegion(final IDocument document, final int offset)
			throws BadLocationException {
		return this.rweaveImpl.getRCodeRegion(document, offset);
	}
	
	@Override
	public List<TreePartitionNode> getRChunkCodeRegions(final IDocument document,
			final int offset, final int length)
			throws BadLocationException {
		return this.rweaveImpl.getRChunkCodeRegions(document, offset, length);
	}
	
}
