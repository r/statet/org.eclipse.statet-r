/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.core.source.doc;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguageDocumentSetupParticipant1;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguagePartitioner;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextPartitioner;
import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.ExtdocMarkupLanguage;


/**
 * The document setup participant for Wikitext-R documents.
 */
public class WikidocRweaveDocumentSetupParticipant extends MarkupLanguageDocumentSetupParticipant1 {
	
	
	public WikidocRweaveDocumentSetupParticipant(final ExtdocMarkupLanguage markupLanguage) {
		this(markupLanguage, false);
	}
	
	public WikidocRweaveDocumentSetupParticipant(final ExtdocMarkupLanguage markupLanguage,
			final boolean templateMode) {
		super(markupLanguage, (templateMode) ? WikitextMarkupLanguage.TEMPLATE_MODE : 0);
	}
	
	
	@Override
	public String getPartitioningId() {
		return WikitextRweaveDocumentConstants.WIKIDOC_R_PARTITIONING;
	}
	
	@Override
	protected MarkupLanguagePartitioner createDocumentPartitioner(final WikitextMarkupLanguage markupLanguage) {
		return new WikitextPartitioner(getPartitioningId(),
				new WikidocRweavePartitionNodeScanner(markupLanguage, getMarkupLanguageMode()),
				WikitextRweaveDocumentConstants.WIKIDOC_R_CONTENT_TYPES );
	}
	
}
