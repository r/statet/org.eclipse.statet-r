/*=============================================================================#
 # Copyright (c) 2019, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.rhelp.core.http.HttpHeaderUtils;
import org.eclipse.statet.internal.rhelp.core.http.HttpHeaderUtils.MediaTypeEntry;
import org.eclipse.statet.internal.rhelp.core.http.HttpHeaderUtils.ParseException;


@NonNullByDefault
public class ServerREnvHelpAccessTest {
	
	
	static String getVersionedDS_AccessHeader() {
		return "application/x.org.eclipse.statet.rhelp-ds;q=1.0;ser=12,"
				+ "application/x.org.eclipse.statet.rhelp-ds;q=0.9;ser=11,"
				+ "application/x.org.eclipse.statet.rhelp-ds;q=0.8;ser=10";
	}
	
	static String getModestDS_AccessHeader() {
		return "application/x.org.eclipse.statet.rhelp-ds";
	}
	
	
	@Test
	public void DS_VERSIONED_AcceptHeader_valid() throws ParseException {
		final List<MediaTypeEntry> entries= HttpHeaderUtils.readMediaTypeEntries(
				Arrays.asList(getVersionedDS_AccessHeader()), null);
		
		for (final MediaTypeEntry entry : entries) {
			assertEquals(ServerApi.APPLICATION_MEDIA_TYPE, entry.getType());
			assertEquals(ServerApi.DS_MEDIA_SUBTYPE, entry.getSubtype());
			assertNotNull(entry.getParameterValue(ServerApi.DS_SER_VERSION));
		}
	}
	
	@Test
	public void DS_MODEST_AcceptHeader_valid() throws ParseException {
		final List<MediaTypeEntry> entries= HttpHeaderUtils.readMediaTypeEntries(
				Arrays.asList(getModestDS_AccessHeader()), null);
		
		for (final MediaTypeEntry entry : entries) {
			assertEquals(ServerApi.APPLICATION_MEDIA_TYPE, entry.getType());
			assertEquals(ServerApi.DS_MEDIA_SUBTYPE, entry.getSubtype());
		}
	}
	
	
}
