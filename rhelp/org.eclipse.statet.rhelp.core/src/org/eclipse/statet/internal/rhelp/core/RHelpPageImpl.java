/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rhelp.core.RPkgHelp;


@NonNullByDefault
public final class RHelpPageImpl implements RHelpPage {
	
	
	public static final String INTERNAL_KEYWORD= "internal"; //$NON-NLS-1$
	
	public static final byte INTERNAL_FLAG= 1 << 0;
	
	public static byte createFlags(final boolean isInternal) {
		byte flags= 0;
		if (isInternal) {
			flags |= RHelpPageImpl.INTERNAL_FLAG;
		}
		return flags;
	}
	
	
	private final RPkgHelp pkg;
	
	private final String name;
	
	private final byte flags;
	
	private final ImList<String> topics;
	
	private final String title;
	
	
	public RHelpPageImpl(final RPkgHelp pkgHelp, final String name, final byte flags,
			final ImList<String> topics,
			final String title) {
		this.pkg= pkgHelp;
		this.name= name;
		this.flags= flags;
		this.topics= topics;
		this.title= title;
	}
	
	
	@Override
	public RPkgHelp getPackage() {
		return this.pkg;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	public byte getFlags() {
		return this.flags;
	}
	
	@Override
	public boolean isInternal() {
		return ((this.flags & INTERNAL_FLAG) != 0);
	}
	
	@Override
	public ImList<String> getTopics() {
		return this.topics;
	}
	
	@Override
	public String getTitle() {
		return this.title;
	}
	
	
	@Override
	public int hashCode() {
		return this.pkg.hashCode() + this.name.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RHelpPage) {
			final RHelpPage other= (RHelpPage) obj;
			return (this.name.equals(other.getName())
					&& this.pkg.equals(other.getPackage()) );
		}
		return false;
	}
	
	@Override
	public int compareTo(final RHelpPage o) {
		return RHelpCoreInternals.R_NAMES_COLLATOR.compare(this.name, o.getName());
	}
	
	@Override
	public String toString() {
		return this.pkg.getName() + "::" +  this.name;
	}
	
}
