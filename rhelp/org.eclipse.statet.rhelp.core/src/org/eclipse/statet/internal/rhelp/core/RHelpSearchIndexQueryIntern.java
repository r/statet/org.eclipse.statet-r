/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.rhelp.core.index.REnvIndexSearchQuery;


@NonNullByDefault
public class RHelpSearchIndexQueryIntern {
	
	
	private @Nullable REnvIndexSearchQuery indexQuery;
	
	
	public RHelpSearchIndexQueryIntern() {
	}
	
	
	public boolean isIndexQueryOk() {
		return (this.indexQuery != null);
	}
	
	public void setIndexQuery(final REnvIndexSearchQuery indexQuery) {
		this.indexQuery= indexQuery;
	}
	
	public REnvIndexSearchQuery getIndexQuery() {
		return nonNullAssert(this.indexQuery);
	}
	
}
