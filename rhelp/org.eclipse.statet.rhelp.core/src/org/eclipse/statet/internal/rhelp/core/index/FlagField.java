/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
final class FlagField extends Field {
	// see impl IntField (legacy)
	
	
	public static final FieldType TYPE;
	
	static {
		TYPE= new FieldType(StringField.TYPE_STORED);
		TYPE.freeze();
	}
	
	
	/**
	 * Creates a new field.
	 * 
	 * @param name field name
	 * @param value field value
	 * @throws IllegalArgumentException if the field name is {@code null}
	 */
	public FlagField(final String name, final String value) {
		super(name, value, TYPE);
	}
	
}
