/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.apache.lucene.search.vectorhighlight.BoundaryScanner;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class HighlightBoundaryScanner implements BoundaryScanner {
	
	
	private static final int MAX_SCAN= 10;
	
	
	public HighlightBoundaryScanner() {
	}
	
	
	@Override
	public int findStartOffset(final StringBuilder buffer, final int start) {
		if (start > buffer.length() || start < 1) {
			return start;
		}
		
		final int stop= Math.max(0, start - MAX_SCAN);
		for (int offset= start; offset > stop; offset--) {
			final char c= buffer.charAt(offset - 1);
			if (c < 0x20 || !Character.isLetterOrDigit(c)) {
				return offset;
			}
		}
		if (stop == 0) {
			return 0;
		}
		return start;
	}
	
	@Override
	public int findEndOffset(final StringBuilder buffer, final int start) {
		if (start > buffer.length() || start < 0) {
			return start;
		}
		
		final int stop= Math.min(buffer.length(), start + MAX_SCAN);
		for (int offset= start; offset < stop; offset++) {
			final char c= buffer.charAt(offset);
			if (c < 0x20 || !Character.isLetterOrDigit(c)) {
				return offset;
			}
		}
		if (stop == buffer.length()) {
			return stop;
		}
		return start;
	}
	
}
