/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;

import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Constants;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
final class REnvIndexUtils {
	
	
	public static final BytesRef[] toByteRefTerms(final List<String> stringValues) {
		final BytesRef[] terms= new @NonNull BytesRef[stringValues.size()];
		for (int i= 0; i < terms.length; i++) {
			terms[i]= new BytesRef(stringValues.get(i));
		}
		return terms;
	}
	
	public static final BytesRef toByteRefTerm(final String stringValue) {
		final BytesRef term= new BytesRef(stringValue);
		return term;
	}
	
	public static final String toString(final byte[] stringValue) {
	//	return new BytesRef(stringValue).utf8ToString();
		return new String(stringValue, StandardCharsets.UTF_8);
	}
	
	
	public static final FSDirectory getDirectory(final Path path) throws IOException {
		if (Constants.WINDOWS) {
			return new SimpleFSDirectory(path);
		}
		else {
			return new NIOFSDirectory(path);
		}
	}
	
	
	private REnvIndexUtils() {}
	
}
