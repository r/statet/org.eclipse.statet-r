/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
abstract class StringDataField extends Field {
	
	
	StringDataField(final String name, final FieldType type) {
		super(name, type);
	}
	
	
	@Override
	public final void setStringValue(final String value) {
		if (value == null) {
			throw new IllegalArgumentException("value cannot be null");
		}
		this.fieldsData= value;
	}
	
	
}
