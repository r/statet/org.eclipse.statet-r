/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.server;

import java.net.URI;

import org.eclipse.jetty.client.HttpClient;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rhelp.core.http.jetty.JettyRHelpUtils;


@NonNullByDefault
public class JettyServerClientSupport extends ServerClientSupport {
	
	
	private final HttpClient httpClient;
	
	
	public JettyServerClientSupport() {
		this.httpClient= new HttpClient();
		this.httpClient.setExecutor(JettyRHelpUtils.getExecutor());
		this.httpClient.setAddressResolutionTimeout(10000);
		this.httpClient.setConnectTimeout(10000);
	}
	
	
	public void configureAdditional(final HttpClient additional) {
		additional.setAuthenticationStore(this.httpClient.getAuthenticationStore());
	}
	
	@Override
	public ServerREnvHelpAccess newREnvHelpAccess(final URI url) throws Exception {
		return new JettyREnvHelpAccess(url, this.httpClient);
	}
	
}
