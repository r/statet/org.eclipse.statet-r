/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class DocResource {
	
	
	protected static String toPdf(final String url) {
		final int idx= url.lastIndexOf('.');
		return (url.substring(0, idx) + ".pdf"); //$NON-NLS-1$
	}
	
	
	private final String title;
	
	private final String path;
	
	private final @Nullable String pdfPath;
	
	
	public DocResource(final String title, final String path, final @Nullable String pdfPath) {
		this.title= title;
		this.path= path;
		this.pdfPath= pdfPath;
	}
	
	
	public String getTitle() {
		return this.title;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public @Nullable String getPdfPath() {
		return this.pdfPath;
	}
	
}
