/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public interface REnvHelp {
	
	
	REnv getREnv();
	
	void unlock();
	
	ImList<DocResource> getManuals();
	ImList<DocResource> getMiscResources();
	
	ImList<RHelpKeywordGroup> getKeywords();
	
	ImList<RPkgHelp> getPkgs();
	@Nullable RPkgHelp getPkgHelp(final String pkgName);
	
	/**
	 * Looks for the specified help page in the specified package.
	 * 
	 * @param pkgName the name of the package
	 * @param name the name of the page
	 * @return the page if exists, otherwise <code>null</code>
	 */
	@Nullable RHelpPage getPage(final String pkgName, final String name);
	
	/**
	 * Looks for help pages for the specified topic in all packages.
	 * 
	 * @param topic the topic
	 * @return list with all pages
	 */
	List<RHelpPage> getPagesForTopic(final String topic,
			final @Nullable ProgressMonitor m) throws StatusException;
	
	
}
