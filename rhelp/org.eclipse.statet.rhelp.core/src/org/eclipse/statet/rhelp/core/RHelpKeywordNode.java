/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * A node of the R help keywords tree.
 */
@NonNullByDefault
public interface RHelpKeywordNode {
	
	
	/**
	 * @return the description
	 */
	String getDescription();
	
	/**
	 * @return a list with nested keywords
	 */
	List<RHelpKeyword> getNestedKeywords();
	
	/**
	 * @return the nested keywords if exists
	 */
	@Nullable RHelpKeyword getNestedKeyword(final String keyword);
	
}
