/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rhelp.core.REnvHelpImpl;
import org.eclipse.statet.internal.rhelp.core.RHelpManagerIntern;
import org.eclipse.statet.internal.rhelp.core.server.JettyServerClientSupport;
import org.eclipse.statet.internal.rhelp.core.server.ServerClientSupport;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvManager;


@NonNullByDefault
public class RHelpManager extends RHelpManagerIntern {
	
	
	public static class Event {
		
		
		public static final byte HELP_UPDATED= 2;
		public static final byte HELP_REMOVED= 3;
		
		
		private final byte type;
		
		private final REnv rEnv;
		
		
		public Event(final byte type, final REnv rEnv) {
			this.type= type;
			this.rEnv= rEnv;
		}
		
		
		public byte getType() {
			return this.type;
		}
		
		public REnv getREnv() {
			return this.rEnv;
		}
		
	}
	
	public static interface Listener {
		
		
		public void onRHelpChanged(Event event);
		
	}
	
	
	public RHelpManager(final REnvManager rEnvManager) {
		super(rEnvManager);
		
		super.checkREnvs();
	}
	
	
	protected void onREnvChanged() {
		super.checkREnvs();
	}
	
	public List<REnv> getREnvWithHelp() {
		return super.getREnvWithHelpIntern();
	}
	
	public boolean hasHelp(@Nullable REnv rEnv) {
		if (rEnv != null) {
			rEnv= rEnv.resolve();
			if (rEnv != null) {
				return super.hasHelpIntern(rEnv);
			}
		}
		return false;
	}
	
	/**
	 * Returns the help for the specified R environment.
	 * The help is already locked (read lock). Call {@link REnvHelp#unlock()} to release the lock.
	 * 
	 * @param env
	 * @return the R help
	 */
	public @Nullable REnvHelp getHelp(@Nullable REnv rEnv) {
		if (rEnv != null) {
			rEnv= rEnv.resolve();
			if (rEnv != null) {
				return super.getHelpIntern(rEnv);
			}
		}
		return null;
	}
	
	public REnvHelp getHelpChecked(final @Nullable REnv rEnv) throws StatusException {
		final REnvHelp help= getHelp(rEnv);
		if (help != null) {
			return help;
		}
		final REnvHelpConfiguration rEnvConfig;
		if (rEnv == null || (rEnvConfig= rEnv.get(REnvHelpConfiguration.class)) == null) {
			throw new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID,
					"The specified R environment does not exists.",
					null ));
		}
		if (rEnvConfig.getStateSharedType() == REnvConfiguration.SHARED_SERVER) {
			throw new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID,
					String.format("The remote R help of the specified R environment '%1$s' is not available.",
							rEnv.getName() )));
		}
		else {
			throw new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID,
					String.format("The R help of the specified R environment '%1$s' is not yet indexed. " +
							"Please run the indexer first to enable R help support.",
							rEnv.getName() )));
		}
	}
	
	public void search(final RHelpSearchQuery query, final RHelpSearchRequestor requestor,
			final ProgressMonitor m) throws StatusException {
		if (query == null) {
			throw new NullPointerException("query"); //$NON-NLS-1$
		}
		if (requestor == null) {
			throw new NullPointerException("requestor"); //$NON-NLS-1$
		}
		query.validate();
		final REnvHelpImpl help= (REnvHelpImpl) getHelpChecked(query.getREnv());
		try {
			help.search(query, requestor);
		}
		finally {
			help.unlock();
		}
	}
	
	
	@Override
	public void addListener(final Listener listener) {
		super.addListener(listener);
	}
	
	@Override
	public void removeListener(final Listener listener) {
		super.removeListener(listener);
	}
	
	
	private ScheduledExecutorService serverUpdateExecutor;
	
	protected void initServerSupport(final String type) {
		if (type == "jetty") {
			ServerClientSupport.init(new JettyServerClientSupport());
		}
		else {
			throw new UnsupportedOperationException("type= " + type);
		}
		
		this.serverUpdateExecutor= Executors.newSingleThreadScheduledExecutor();
		this.serverUpdateExecutor.scheduleAtFixedRate(() -> updateServerHelp(),
				5, 60, TimeUnit.SECONDS );
	}
	
}
