/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface RHelpPage extends Comparable<RHelpPage> {
	
	
	/**
	 * Returns the package help this page belongs to.
	 * 
	 * @return the R package help
	 */
	RPkgHelp getPackage();
	
	/**
	 * Returns the name of this page.
	 * 
	 * @return the name
	 */
	String getName();
	
	/**
	 * Returns if this page is internal.
	 * 
	 * A page is marked as internal if it has the keyword <code>internal</code>.
	 * 
	 * @return <code>true</code> if marked as internal, otherwise false
	 */
	boolean isInternal();
	
	ImList<String> getTopics();
	
	String getTitle();
	
}
