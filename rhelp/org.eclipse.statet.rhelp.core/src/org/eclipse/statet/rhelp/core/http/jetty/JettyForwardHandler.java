/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http.jetty;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.WWWAuthenticationProtocolHandler;
import org.eclipse.jetty.proxy.ProxyServlet;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rhelp.core.server.JettyServerClientSupport;
import org.eclipse.statet.internal.rhelp.core.server.ServerClientSupport;
import org.eclipse.statet.rhelp.core.http.HttpForwardHandler;


@NonNullByDefault
public class JettyForwardHandler implements HttpForwardHandler {
	
	
	private static final String ATTR_URI= "forward.uri"; //$NON-NLS-1$
	
	private static final String SERVLET_KEY= "org.eclipse.statet.rhelp.ForwardProxy"; //$NON-NLS-1$
	
	private static class Servlet extends ProxyServlet {
		
		private static final long serialVersionUID= 1L;
		
		public Servlet() {
			super();
		}
		
		@Override
		public void init(final ServletConfig config) throws ServletException {
			JettyRHelpUtils.ensureExecutor(config.getServletContext());
			super.init(config);
			
			try {
				final HttpClient httpClient= nonNullAssert(getHttpClient());
				((JettyServerClientSupport) ServerClientSupport.getInstance())
						.configureAdditional(httpClient);
				httpClient.getProtocolHandlers().put(
						new WWWAuthenticationProtocolHandler(httpClient) );
			}
			catch (final StatusException e) {}
		}
		
		@Override
		protected String rewriteTarget(final HttpServletRequest clientRequest) {
			final URI url= (URI) clientRequest.getAttribute(ATTR_URI);
			return url.toString();
		}
		
	}
	
	
	private volatile @Nullable HttpServlet proxyServlet;
	
	
	public JettyForwardHandler() {
	}
	
	
	private HttpServlet getServlet(final ServletContext servletContext) throws ServletException {
		HttpServlet servlet= this.proxyServlet;
		if (servlet == null) {
			synchronized (Servlet.class) {
				servlet= (HttpServlet) servletContext.getAttribute(SERVLET_KEY);
				if (servlet == null) {
					servlet= new Servlet();
					servlet.init(new ServletConfig() {
						private final ServletContext context= servletContext;
						@Override
						public String getServletName() {
							return "org.eclipse.statet.rhelp.ForwardProxy";
						}
						
						@Override
						public ServletContext getServletContext() {
							return this.context;
						}
						
						@Override
						public Enumeration<String> getInitParameterNames() {
							return Collections.emptyEnumeration();
						}
						
						@Override
						public @Nullable String getInitParameter(final String name) {
							return null;
						}
					});
					servletContext.setAttribute(SERVLET_KEY, servlet);
				}
				this.proxyServlet= servlet;
			}
		}
		return servlet;
	}
	
	
	@Override
	public void forward(final URI url,
			final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute(ATTR_URI, url);
		// forward doesn't work because RequestDispatcher by equinox closes connection
//		req.getRequestDispatcher("/proxy").include(req, resp);
		getServlet(req.getServletContext()).service(req, resp);
	}
	
}
