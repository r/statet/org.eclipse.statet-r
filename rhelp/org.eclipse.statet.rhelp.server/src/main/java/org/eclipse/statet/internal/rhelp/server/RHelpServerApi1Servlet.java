/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.http.RHelpApi1Servlet;
import org.eclipse.statet.rhelp.core.http.jetty.JettyRHelpUtils;


@WebServlet (urlPatterns= "/rhelp/api/v1/*")
@NonNullByDefault
public class RHelpServerApi1Servlet extends RHelpApi1Servlet {
	
	
	private static final long serialVersionUID= 1L;
	
	
	private final RHelpManager rHelpManager;
	
	
	@Autowired
	public RHelpServerApi1Servlet(final RHelpManager rHelpManager) {
		this.rHelpManager= rHelpManager;
	}
	
	
	@Override
	public void init(final ServletConfig config) throws ServletException {
		super.init(config);
		
		init(this.rHelpManager, JettyRHelpUtils.newResourceHandler(config.getServletContext()));
		
//		init(this.appContext.getBean(RHelpManager.class));
	}
	
	
}
