/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server.renv;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rj.renv.core.BasicREnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;


@NonNullByDefault
public class REnvConfigurationImpl extends BasicREnvConfiguration
		implements REnvConfiguration, REnvHelpConfiguration {
	
	
	public REnvConfigurationImpl(final REnv rEnv, final Path stateDataRootDirectoryPath)
			throws IOException {
		super(rEnv, stateDataRootDirectoryPath);
		setFlags((LOCAL | SPEC_SETUP));
		
		final Properties p= new Properties();
		final Path propertiesFile= stateDataRootDirectoryPath.resolve("renv.properties");
		try (final BufferedReader reader= Files.newBufferedReader(propertiesFile,
				StandardCharsets.UTF_8 )) {
			p.load(reader);
		}
		load(p);
		resolvePaths();
	}
	
	
}
