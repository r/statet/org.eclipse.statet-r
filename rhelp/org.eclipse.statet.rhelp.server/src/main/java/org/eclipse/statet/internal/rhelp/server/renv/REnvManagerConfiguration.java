/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server.renv;

import java.nio.file.Path;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.annotation.Configuration;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.server.RHelpServerApplication;


@Configuration
@ConfigurationProperties(prefix= "renvs")
@NonNullByDefault
public class REnvManagerConfiguration {
	
	
	private @Nullable Path rootDir;
	
	
	public void setPath(final @Nullable String path) {
		this.rootDir= (path != null) ? Path.of(path) : null;
	}
	
	public Path getRootDirectory() {
		var dir= this.rootDir;
		if (dir != null) {
			return dir;
		}
		else {
			final ApplicationHome applicationHome= new ApplicationHome(RHelpServerApplication.class);
			dir= applicationHome.getDir().toPath();
			return dir.resolve("renvs"); //$NON-NLS-1$
		}
	}
	
}
