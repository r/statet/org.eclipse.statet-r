/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server.renv;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;

import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rhelp.server.RHelpServerApplication;
import org.eclipse.statet.rj.renv.core.BasicREnvManager;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvManager;


@Component
@NonNullByDefault
public class REnvManagerImpl extends BasicREnvManager implements REnvManager {
	
	
	private static class ActualServerREnv extends ActualREnv {
		
		
		public ActualServerREnv(final String id) {
			super(id);
		}
		
		
		@Override
		@SuppressWarnings("unchecked")
		public <T> @Nullable T get(final Class<T> type) {
			if (type == REnvHelpConfiguration.class) {
				return (T) getConfiguration();
			}
			return super.get(type);
		}
		
	}
	
	
	private final REnvManagerConfiguration properties;
	
	private final Path rEnvsDir;
	
	
	@Autowired
	public REnvManagerImpl(final REnvManagerConfiguration properties) {
		this.properties= properties;
		
		final List<REnvConfiguration> configs= new ArrayList<>();
		try {
			this.rEnvsDir= this.properties.getRootDirectory();
			
			try (final var rEnvDirs= Files.newDirectoryStream(this.rEnvsDir, Files::isDirectory)) {
				for (final var rEnvDir : rEnvDirs) {
					final REnvConfiguration config= readConfig(rEnvDir);
					if (config != null) {
						configs.add(config);
					}
				}
			}
		}
		catch (final Exception e) {
			throw new RuntimeException("Failed to initialize R environments.", e);
		}
		
		synchronized (this) {
			final List<ActualREnv> envs= new ArrayList<>(configs.size());
			for (final REnvConfiguration config : configs) {
				final ActualREnv env= (ActualREnv) config.getREnv();
				updateEnv(env, config);
				envs.add(env);
			}
			setActualEnvs(ImCollections.toList(envs));
		}
	}
	
	private @Nullable REnvConfiguration readConfig(final Path path) {
		final String id= nonNullAssert(path.getFileName()).toString();
		try {
			final ActualREnv env= newEnv(id);
			final REnvConfigurationImpl config= new REnvConfigurationImpl(env, path);
			return config;
		}
		catch (final Exception e) {
			CommonsRuntime.log(new ErrorStatus(RHelpServerApplication.BUNDLE_ID,
					String.format("An error occurred when loading R environment configuration '%1$s'.", id),
					e ));
			return null;
		}
	}
	
	@Override
	protected ActualREnv newEnv(final String id) {
		if (!isValidId(id)) {
			throw new IllegalArgumentException("id=" + id);
		}
		return new ActualServerREnv(id.intern());
	}
	
}
