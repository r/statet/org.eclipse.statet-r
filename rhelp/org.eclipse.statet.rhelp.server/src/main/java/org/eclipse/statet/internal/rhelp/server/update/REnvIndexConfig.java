/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server.update;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@Configuration
@ConfigurationProperties(prefix= "index")
@NonNullByDefault
public class REnvIndexConfig {
	
	
	private int monitorDelay= 5000;
	
	private int periodicDelay= -1;
	
	
	public REnvIndexConfig() {
	}
	
	
	public int getMonitorDelay() {
		return this.monitorDelay;
	}
	
	public void setMonitorDelay(final int delay) {
		this.monitorDelay= delay;
	}
	
	public int getPeriodicDelay() {
		return this.periodicDelay;
	}
	
	public void setPeriodicDelay(final int delay) {
		this.periodicDelay= delay;
	}
	
}
