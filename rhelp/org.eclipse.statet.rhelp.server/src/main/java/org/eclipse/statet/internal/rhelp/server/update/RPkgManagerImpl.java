/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server.update;

import static org.eclipse.statet.rhelp.server.RHelpServerApplication.BUNDLE_ID;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.RPkgBuilt;
import org.eclipse.statet.rj.renv.core.RPkgCompilation;
import org.eclipse.statet.rj.renv.runtime.BasicRPkgManagerData;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;
import org.eclipse.statet.rj.renv.runtime.RPkgManagerDataset;
import org.eclipse.statet.rj.renv.runtime.RuntimeRLibPaths;
import org.eclipse.statet.rj.renv.runtime.RuntimeRLibPathsLoader;
import org.eclipse.statet.rj.renv.runtime.RuntimeRPkgInfoLoader;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public class RPkgManagerImpl implements RPkgManager {
	
	
	private final REnv rEnv;
	
	private int requested;
	
	private @Nullable RPkgManagerDataset dataset;
	
	private @Nullable RuntimeRPkgInfoLoader rPkgInfoLoader;
	
	
	public RPkgManagerImpl(final REnv rEnv) {
		this.rEnv= rEnv;
	}
	
	
	@Override
	public REnv getREnv() {
		return this.rEnv;
	}
	
	
	protected void beginRTask(
			final RService r, final ProgressMonitor m) throws StatusException {
	}
	
	protected void endRTask() {
	}
	
	@Override
	public void check(final int flags,
			final RService r, final ProgressMonitor m) throws StatusException {
		runUpdate(flags, r, m);
	}
	
	protected int checkRequest(int request) {
		request= RPkgManager.expandFlags(request);
		return request;
	}
	
	@Override
	public synchronized int request(int request) {
		request= checkRequest(request);
		this.requested |= request;
		
		final RPkgManagerDataset dataset= getDataset();
		if (dataset != null && (dataset.getProviding() & request) == request) {
			return OK;
		}
		return REQUIRES_UPDATE;
	}
	
	@Override
	public void update(final RService r, final ProgressMonitor m) throws StatusException {
		beginRTask(r, m);
		try {
			final int requested;
			synchronized (this) {
				requested= this.requested;
			}
			
			runUpdate(requested, r, m);
		}
		finally {
			endRTask();
		}
	}
	
	@Override
	public synchronized @Nullable RPkgManagerDataset getDataset() {
		return this.dataset;
	}
	
	
	protected void runUpdate(final int flags,
			final RService r, final ProgressMonitor m) throws StatusException {
		final REnvConfiguration rEnvConfig= this.rEnv.get(REnvConfiguration.class);
		if (rEnvConfig == null) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"R environment configuration is missing."));
		}
		
		final RuntimeRLibPathsLoader loader= new RuntimeRLibPathsLoader(rEnvConfig);
		final RuntimeRLibPaths rLibPaths= loader.load(0, r, m);
		
		RuntimeRPkgInfoLoader rPkgInfoLoader= this.rPkgInfoLoader;
		if (rPkgInfoLoader == null) {
			rPkgInfoLoader= new RuntimeRPkgInfoLoader();
			this.rPkgInfoLoader= rPkgInfoLoader;
		}
		final RPkgCompilation<RPkgBuilt> newInstalled= rPkgInfoLoader.loadInstalled(
				rLibPaths, null, null, r, m );
		
		synchronized (this) {
			this.dataset= new BasicRPkgManagerData<>(this.rEnv, INSTALLED,
					rLibPaths, newInstalled );
			this.requested &= ~REFRESH_INSTALLED_PKGS;
		}
	}
	
}
