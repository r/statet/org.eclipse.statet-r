/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server.update;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.update.REnvIndexUpdater;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;


@NonNullByDefault
class ServerREnvIndexUpdater extends REnvIndexUpdater {
	
	
	private class IndexJobRunnable extends IndexJob implements Runnable {
		
		private final String name;
		
		private final ProgressMonitor monitor= new NullProgressMonitor();
		private final CompletableFuture<?> completable;
		
		
		public IndexJobRunnable(final String name) {
			this.name= name;
			this.completable= CompletableFuture.runAsync(this, getExecutor());
		}
		
		@Override
		public void run() {
			run(this.monitor);
		}
		
		@Override
		public void cancel() {
			this.monitor.setCanceled(true);
		}
		
		@Override
		public void join() {
			this.completable.join();
		}
		
	}
	
	
	private final Executor executor;
	
	
	public ServerREnvIndexUpdater(final REnvHelpConfiguration rEnvConfig,
			final RHelpManager rHelpManager, final RPkgManager rPkgManager,
			final Executor executor) {
		super(rEnvConfig, rHelpManager, rPkgManager);
		this.executor= executor;
	}
	
	
	protected Executor getExecutor() {
		return this.executor;
	}
	
	@Override
	protected IndexJob scheduleIndexJob(final String name) {
		return new IndexJobRunnable(name);
	}
	
}
