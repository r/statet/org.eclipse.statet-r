/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.server;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.BasicAppEnvironment;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.runtime.bundle.Bundles;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.util.ACommonsLoggingStatusLogger;

import org.eclipse.statet.internal.rhelp.server.RHelpServerInternals;
import org.eclipse.statet.internal.rhelp.server.update.REnvIndexController;


@SpringBootApplication
@ComponentScan (basePackageClasses= { RHelpServerInternals.class })
@ServletComponentScan (basePackageClasses= { RHelpServerInternals.class })
@NonNullByDefault
public class RHelpServerApplication extends BasicAppEnvironment {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.rhelp.server"; //$NON-NLS-1$
	
	
	public static void main(final String[] args) {
		final SpringApplication app= new SpringApplication(RHelpServerApplication.class);
		
		if (args != null) {
			final ImList<String> argList= ImCollections.newList(args);
			if (argList.contains("-index-and-exit")) { //$NON-NLS-1$
				app.setWebApplicationType(WebApplicationType.NONE);
				app.setAdditionalProfiles(REnvIndexController.INDEX_AND_EXIT_PROFILE);
			}
		}
		
		app.run(args);
	}
	
	
	@Autowired
	@SuppressWarnings("unused")
	private ApplicationContext appContext;
	
	
	public RHelpServerApplication() throws StatusException {
		super(BUNDLE_ID, new ACommonsLoggingStatusLogger(), Bundles.createResolver());
		CommonsRuntime.init(this);
		
		log(new InfoStatus(BUNDLE_ID, "Application started."));
	}
	
	@PreDestroy
	protected void dispose() {
		onAppStopping();
		
		log(new InfoStatus(BUNDLE_ID, "Application stopped."));
	}
	
}
