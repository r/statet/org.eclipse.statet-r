/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.server;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import org.eclipse.statet.internal.rhelp.server.renv.REnvManagerConfiguration;


@TestConfiguration
public class ContextConfiguration {
	
	
	private final REnvManagerConfiguration rEnvManagerConfiguration= new REnvManagerConfiguration();
	
	
	public ContextConfiguration() {
		final String pathString= System.getenv("STATET_TEST_FILES");
		if (pathString != null && !pathString.isEmpty()) {
			this.rEnvManagerConfiguration.setPath(pathString + "/rhelp.server-data/renvs");
		}
	}
	
	
	@Bean
	@Primary
	public REnvManagerConfiguration rEnvManagerConfiguration() {
		return this.rEnvManagerConfiguration;
	}
	
}
