/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.util;

import org.eclipse.statet.rtm.rtdata.RtDataPackage;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


public class RExprType {
	
	
	public static final RExprType DATAFRAME_TYPE= new RExprType(
			RTypedExpr.R, RtDataPackage.RDATA_FRAME,
			"R e&xpression setting the dataframe" );
	
	public static final RExprType EXPR_VALUE_TYPE= new RExprType(
			RTypedExpr.R, -1,
			"R e&xpression setting the attribute value" );
	
	public static final RExprType EXPR_LABEL_VALUE_TYPE= new RExprType(
			RTypedExpr.R, RtDataPackage.RLABEL,
			"R e&xpression setting the attribute label" );
	
	public static final RExprType EXPR_COLOR_VALUE_TYPE= new RExprType(
			RTypedExpr.R, RtDataPackage.RCOLOR,
			"R e&xpression setting the attribute color value" );
	
	public static final RExprType EXPR_ALPHA_VALUE_TYPE= new RExprType(
			RTypedExpr.R, RtDataPackage.RALPHA,
			"R e&xpression setting the attribute alpha value" );
	
	public static final RExprType EXPR_FONT_FAMILY_VALUE_TYPE= new RExprType(
			RTypedExpr.R, RtDataPackage.RFONT_FAMILY,
			"R e&xpression setting the font family value" );
	
	public static final RExprType TEXT_VALUE_TYPE= new RExprType(
			RTypedExpr.CHAR, RtDataPackage.RTEXT,
			"&Text (automatically quoted for R)" );
	
	public static final RExprType DATAFRAME_COLUMN_TYPE= new RExprType(
			RTypedExpr.MAPPED, -1,
			"Dataframe variable &mapping from data to the attribute" );
	
	public static final RExprType EXPR_FUNCTION_TYPE= new RExprType(
			RTypedExpr.R, RtDataPackage.RFUNCTION,
			"R e&xpression setting the function" );
	
	
	private final String typeKey;
	private final int rtDataTypeID;
	
	private final String label;
	
	
	public RExprType(final String type, final int typeID, final String label) {
		this.typeKey= type;
		this.rtDataTypeID= typeID;
		
		this.label= label;
	}
	
	
	public String getTypeKey() {
		return this.typeKey;
	}
	
	public int getRtDataID() {
		return this.rtDataTypeID;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	
	@Override
	public String toString() {
		return this.typeKey + " / " + this.rtDataTypeID; //$NON-NLS-1$
	}
	
}
