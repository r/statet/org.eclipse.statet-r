/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.rtm.rtdata.types.RExpr;


public class RtItemLabelUtils {
	
	
	public static class LabelBuilder {
		
		
		private final StringBuilder builder;
		private int detail= -1;
		
		public LabelBuilder(final String label) {
			this.builder= new StringBuilder(label);
		}
		
		
		public void appendDetail(final String label, final RExpr value) {
			if (label != null && value != null) {
				if (this.detail < 0) {
					this.detail= 1;
					this.builder.append(" ("); //$NON-NLS-1$
				}
				else {
					this.builder.append(", "); //$NON-NLS-1$
				}
				this.builder.append(label);
				this.builder.append(": "); //$NON-NLS-1$
				this.builder.append(value.getExpr());
			}
		}
		
		public void closeDetail() {
			if (this.detail > 0) {
				this.builder.append(")"); //$NON-NLS-1$
			}
		}
		
		@Override
		public String toString() {
			return this.builder.toString();
		}
		
	}
	
	public static class LabelGenerator {
		
		
		private static class Detail {
			
			private final EStructuralFeature eFeature;
			private final String label;
			
			public Detail(final EStructuralFeature eFeature, final String label) {
				this.eFeature= eFeature;
				this.label= label;
			}
			
		}
		
		
		private final String baseLabel;
		
		private final Detail[] details;
		
		
		public LabelGenerator(final ResourceLocator resourceLocator, final EClass eClass, final String[] names) {
			this.baseLabel= resourceLocator.getString(getLabelKey(eClass), true);
			final List<Detail> details= new ArrayList<>();
			for (final String name : names) {
				final EStructuralFeature eFeature= eClass.getEStructuralFeature(name);
				if (eFeature == null) {
					continue;
				}
				final String label= resourceLocator.getString(getLabelKey(eFeature));
				details.add(new Detail(eFeature, label));
			}
			this.details= details.toArray(new Detail[details.size()]);
		}
		
		
		public String createLabel(final EObject eObj) {
			final LabelBuilder sb= new LabelBuilder(this.baseLabel);
			for (int i= 0; i < this.details.length; i++) {
				final Object value= eObj.eGet(this.details[i].eFeature);
				if (value != null) {
					sb.appendDetail(this.details[i].label, (RExpr) value);
				}
			}
			sb.closeDetail();
			return sb.toString();
		}
		
	}
	
	public static String getLabelKey(final EClass eClass) {
		return "_UI_" + eClass.getName() + "_type"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public static String getLabelKey(final EStructuralFeature eFeature) {
		return "_UI_" + eFeature.getEContainingClass().getName() + //$NON-NLS-1$
				"_" + eFeature.getName() + "_feature"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
