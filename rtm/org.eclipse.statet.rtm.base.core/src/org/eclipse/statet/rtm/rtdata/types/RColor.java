/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.rtdata.types;


public class RColor extends RTypedExpr {
	
	
	public RColor(final String type, final String expr) {
		super(type, expr);
	}
	
	
	@Override
	public int hashCode() {
		return this.expr.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		return (obj instanceof RColor && this.expr.equals(((RColor) obj).expr));
	}
	
}
