/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.rtdata.types;


public class RTypedExpr extends RExpr {
	
	
	public static final String R= "r"; //$NON-NLS-1$
	public static final String MAPPED= "map"; //$NON-NLS-1$
	public static final String CHAR= "chr"; //$NON-NLS-1$
	
	
	private final String typeKey;
	
	
	public RTypedExpr(final String typeKey, final String expr) {
		super(expr);
		
		this.typeKey= typeKey;
	}
	
	
	public String getTypeKey() {
		return this.typeKey;
	}
	
	
	@Override
	public int hashCode() {
		return this.typeKey.hashCode() * 17 + this.expr.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RTypedExpr)) {
			return false;
		}
		final RTypedExpr other= (RTypedExpr) obj;
		return (this.typeKey.equals(other.typeKey) && this.expr.equals(other.expr));
	}
	
	@Override
	public String toString() {
		return this.typeKey + ':' + this.expr;
	}
	
}
