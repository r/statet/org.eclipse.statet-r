/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.databinding.IDataBindingPart;


public abstract class Detail implements IDataBindingPart {
	
	
	private final DetailStack parent;
	
	private final Composite composite;
	
	
	public Detail(final DetailStack parent) {
		this.parent= parent;
		
		this.composite= createComposite(this.parent);
	}
	
	
	protected Composite createComposite(final DetailStack parent) {
		final Composite composite= parent.getPage().getToolkit().createComposite(parent);
		return composite;
	}
	
	
	public IEFFormPage getPage() {
		return this.parent.getPage();
	}
	
	protected Composite getComposite() {
		return this.composite;
	}
	
	protected void createContent() {
		this.composite.setLayout(createContentLayout());
		createContent(this.composite);
	}
	
	protected Layout createContentLayout() {
		return EFLayoutUtil.createCompositePropGridLayout();
	}
	
	protected abstract void createContent(Composite composite);
	
	@Override
	public void addBindings(final IEMFEditContext context) {
	}
	
}
