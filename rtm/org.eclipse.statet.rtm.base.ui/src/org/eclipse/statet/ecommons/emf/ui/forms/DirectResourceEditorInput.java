/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import java.util.HashMap;

import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.PlatformUI;


public class DirectResourceEditorInput extends PlatformObject
		implements IEditorInput {
	
	
	public static URI NO_URI= URI.createURI("", true); //$NON-NLS-1$
	
	private static HashMap<String, Integer> NAME_NUMBERS= new HashMap<>();
	
	public static String createNumberedName(final String prefix) {
		synchronized (NAME_NUMBERS) {
			final Integer previous= NAME_NUMBERS.get(prefix);
			final int number= (previous != null) ? previous.intValue() + 1 : 1;
			NAME_NUMBERS.put(prefix, Integer.valueOf(number));
			return prefix + ' ' + number;
		}
	}
	
	
	private final Resource resource;
	
	private final String name;
	
	
	public DirectResourceEditorInput(final String name, final Resource resource) {
//		Assert.isNotNull(fileStore);
//		Assert.isTrue(EFS.SCHEME_FILE.equals(fileStore.getFileSystem().getScheme()));
//		this.fileStore= fileStore;
		this.name=  name;
		this.resource= resource;
	}
	
	@Override
	public boolean exists() {
		return false;
	}
	
	@Override
	public ImageDescriptor getImageDescriptor() {
		return PlatformUI.getWorkbench().getEditorRegistry().getImageDescriptor(this.name);
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}
	
	@Override
	public String getToolTipText() {
		return this.name;
	}
	
	public Resource getResource() {
		return this.resource;
	}
	
	
	@Override
	public <T> T getAdapter(final Class<T> adapterType) {
		return super.getAdapter(adapterType);
	}
	
	
	@Override
	public int hashCode() {
		return this.resource.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DirectResourceEditorInput)) {
			return false;
		}
		return (this.resource == ((DirectResourceEditorInput) obj).resource);
	}
	
}
