/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import java.util.Collections;
import java.util.List;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.databinding.edit.IEMFEditValueProperty;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.databinding.CustomViewerObservables;
import org.eclipse.statet.ecommons.emf.ui.databinding.DetailContext;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


public class DropDownProperty<TProperty> extends EFProperty {
	
	
	private Composite composite;
	
	private ComboViewer comboViewer;
	
	private DetailStack detailStack;
	
	private IObservableValue<TProperty> modelObservable;
	
	private final List<Object> initialInput;
	
	
	public DropDownProperty(final String label, final String tooltip,
			final EClass eClass, final EStructuralFeature eFeature) {
		super(label, tooltip, eClass, eFeature);
		
		this.initialInput= createInitialInput();
	}
	
	
	protected List<Object> createInitialInput() {
		return Collections.emptyList();
	}
	
	@Override
	public void create(final Composite parent, final IEFFormPage page) {
		final EFToolkit toolkit= page.getToolkit();
		
		toolkit.createPropLabel(parent, getLabel(), getTooltip());
		
		this.composite= new Composite(parent, SWT.NULL);
		toolkit.adapt(this.composite);
		this.composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		this.composite.setLayout(LayoutUtils.newCompositeGrid(3));
		
		{	final ComboViewer viewer= new ComboViewer(this.composite, SWT.READ_ONLY | SWT.DROP_DOWN | SWT.SINGLE);
			this.comboViewer= viewer;
			
			final ILabelProvider labelProvider= createLabelProvider(page);
			
			viewer.setLabelProvider(labelProvider);
			viewer.setContentProvider(ArrayContentProvider.getInstance());
			
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, false, false);
			gd.widthHint= LayoutUtils.hintWidth(getCombo(), this.initialInput, labelProvider);
			viewer.getControl().setLayoutData(gd);
		}
		
		this.detailStack= createDetails(this.composite, page);
		this.detailStack.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
	}
	
	@Override
	public Control getControl() {
		return this.composite;
	}
	
	protected Combo getCombo() {
		return this.comboViewer.getCombo();
	}
	
	protected ILabelProvider createLabelProvider(final IEFFormPage page) {
		return new AdapterFactoryLabelProvider(page.getEditor().getAdapterFactory());
	}
	
	protected DetailStack createDetails(final Composite parent, final IEFFormPage page) {
		return new DetailStack(page, this.composite);
	}
	
	
	@Override
	public void bind(final IEMFEditContext context) {
		super.bind(context);
		
		final IEMFEditValueProperty emfProperty= EMFEditProperties.value(getEditingDomain(),
				getEFeature() );
		this.modelObservable= emfProperty.observeDetail(getBaseObservable());
		
		context.getDataBindingContext().bindValue(
				CustomViewerObservables.observeComboSelection(this.comboViewer, this.initialInput),
				this.modelObservable );
		
		final IEMFEditContext detailContext= new DetailContext<>(context, this.modelObservable);
		this.detailStack.setContext(detailContext);
	}
	
	@Override
	public IObservableValue<TProperty> getPropertyObservable() {
		return this.modelObservable;
	}
	
}
