/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.ui.action.RedoAction;
import org.eclipse.emf.edit.ui.action.UndoAction;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;


public class EFEditorActionBarContributor extends MultiPageEditorActionBarContributor 
		implements IPropertyListener {
	
	
	private IEditorPart activeEditor;
	
	private UndoAction undoAction;
	private RedoAction redoAction;
	
	
	public EFEditorActionBarContributor() {
	}
	
	@Override
	public void init(final IActionBars bars, final IWorkbenchPage page) {
		super.init(bars, page);
		
		final ISharedImages sharedImages= PlatformUI.getWorkbench().getSharedImages();
		
		this.undoAction= new UndoAction();
		this.undoAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO));
		bars.setGlobalActionHandler(ActionFactory.UNDO.getId(), this.undoAction);
		
		this.redoAction= new RedoAction();
		this.redoAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_REDO));
		bars.setGlobalActionHandler(ActionFactory.REDO.getId(), this.redoAction);
	}
	
	
	IEditorPart getActiveEditor() {
		return this.activeEditor;
	}
	
	@Override
	public void setActiveEditor(IEditorPart part) {
		if (!(part instanceof IEditingDomainProvider)) {
			part= null;
		}
		if (this.activeEditor != null) {
			this.activeEditor.removePropertyListener(this);
		}
		this.activeEditor= part;
		if (part != null) {
			part.addPropertyListener(this);
		}
		
		this.undoAction.setActiveWorkbenchPart(part);
		this.redoAction.setActiveWorkbenchPart(part);
		
		update();
	}
	
	@Override
	public void setActivePage(final IEditorPart activeEditor) {
	}
	
	@Override
	public void propertyChanged(final Object source, final int propId) {
		update();
	}
	
	public void update() {
		this.undoAction.update();
		this.redoAction.update();
	}
	
	
	public void shareGlobalActions(final IPage page, final IActionBars actionBars) {
	}
	
}
