/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;


public class EFPropertySet {
	
	
	private final List<EFProperty> list= new ArrayList<>(8);
	
	
	public EFPropertySet() {
	}
	
	
	public void add(final EFProperty property) {
		if (property != null) {
			this.list.add(property);
		}
	}
	
	public void createControls(final Composite parent, final IEFFormPage toolkit) {
		for (final EFProperty property : this.list) {
			property.create(parent, toolkit);
		}
	}
	
	public void bind(final IEMFEditContext context) {
		for (final EFProperty property : this.list) {
			property.bind(context);
		}
	}
	
	public EFProperty get(final EStructuralFeature eFeature) {
		for (final EFProperty property : this.list) {
			if (property.getEFeature() == eFeature) {
				return property;
			}
		}
		return null;
	}
	
	public List<EFProperty> getAll() {
		return this.list;
	}
	
	public List<IObservable> getModelObservables() {
		final List<IObservable> observables= new ArrayList<>();
		for (final EFProperty property : this.list) {
			final IObservable observable= property.getPropertyObservable();
			if (observable != null) {
				observables.add(observable);
			}
		}
		return observables;
	}
	
}
