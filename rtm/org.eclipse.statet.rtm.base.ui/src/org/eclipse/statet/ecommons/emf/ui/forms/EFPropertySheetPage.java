/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.ui.IActionBars;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;


public class EFPropertySheetPage extends TabbedPropertySheetPage {
	
	
	private final EFEditor editor;
	
	
	public EFPropertySheetPage(final EFEditor editor) {
		super(editor);
		
		this.editor= editor;
	}
	
	
	public EFEditor getEditor() {
		return this.editor;
	}
	
	@Override
	public void setActionBars(final IActionBars actionBars) {
		super.setActionBars(actionBars);
		this.editor.getActionBarContributor().shareGlobalActions(this, actionBars);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		this.editor.onPropertySheetDisposed(this);
	}
	
}
