/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.FormColors;
import org.eclipse.ui.forms.IFormColors;
import org.eclipse.ui.forms.widgets.FormToolkit;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils.TableComposite;


public class EFToolkit extends FormToolkit {
	
	
	public EFToolkit(final FormColors colors) {
		super(colors);
	}
	
	
	public Label createPropLabel(final Composite parent,
			final String text, final String tooltip) {
		return createPropLabel(parent, text, tooltip, 1);
	}
	
	public Label createPropLabelFullLine(final Composite parent,
			final String text, final String tooltip) {
		final int colSpan= ((GridLayout) parent.getLayout()).numColumns;
		return createPropLabel(parent, text, tooltip, colSpan);
	}
	
	public Label createPropLabel(final Composite parent,
			final String text, final String tooltip, final int colSpan) {
		final Label label= createLabel(parent, text);
		
		final GridData gd= new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd.horizontalSpan= colSpan;
		label.setLayoutData(gd);
		
		label.setToolTipText(tooltip);
		label.setForeground(getColors().getColor(IFormColors.TITLE));
		
		return label;
	}
	
	public Text createPropTextField(final Composite parent, final int numChars) {
		final Text text= createText(parent, null);
		
		final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		gd.widthHint= LayoutUtils.hintWidth(text, numChars);
		text.setLayoutData(gd);
		
		return text;
	}
	
	public TableComposite createPropSingleColumnTable(final Composite parent, final int numRows, final int numChars) {
		final ViewerUtils.TableComposite tableComposite= new ViewerUtils.TableComposite(
				parent, SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL );
		adapt(tableComposite.table, false, false);
		
		final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd.heightHint= LayoutUtils.hintHeight(tableComposite.table, numRows, false);
		gd.widthHint= LayoutUtils.hintWidth(tableComposite.table, numChars);
		gd.minimumWidth= gd.widthHint / 2;
		
		tableComposite.setLayoutData(gd);
		tableComposite.addColumn(null, SWT.LEFT, new ColumnWeightData(100));
		
		return tableComposite;
	}
	
}
