/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.core.expressions.EvaluationResult;
import org.eclipse.core.expressions.Expression;
import org.eclipse.core.expressions.ExpressionInfo;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.ui.ISources;


public interface IEFPropertyExpressions {
	
	
	String EOBJECT_LIST_ID= "org.eclipse.statet.ecommons.emf.ui.EObjectList"; //$NON-NLS-1$
	
	Expression EOBJECT_LIST_EXPRESSION= new Expression() {
		
		@Override
		public void collectExpressionInfo(final ExpressionInfo info) {
			info.addVariableNameAccess(ISources.ACTIVE_FOCUS_CONTROL_ID_NAME);
		}
		
		@Override
		public EvaluationResult evaluate(final IEvaluationContext context) {
			final Object id= context.getVariable(ISources.ACTIVE_FOCUS_CONTROL_ID_NAME);
			return (id == EOBJECT_LIST_ID) ?
				EvaluationResult.TRUE : EvaluationResult.FALSE;
		}
		
	};
	
	
}
