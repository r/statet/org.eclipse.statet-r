/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.emf.ui.forms;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.EMFEditUIPlugin;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


public class UndoCommandHandler extends AbstractHandler implements IElementUpdater {
	
	
	private final EditingDomain domain;
	
	
	public UndoCommandHandler(final EditingDomain domain) {
		this.domain= domain;
	}
	
	
	@Override
	public void setEnabled(final Object evaluationContext) {
		setBaseEnabled(this.domain.getCommandStack().canUndo());
	}
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		final Command command= this.domain.getCommandStack().getUndoCommand();
		if (command != null) {
			WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
			try {
				final String name= EMFEditUIPlugin.INSTANCE.getString("_UI_Undo_menu_item", true); //$NON-NLS-1$
				element.setText(NLS.bind(name, getLabel(command)));
				element.setTooltip(NLS.bind(name, getTipLabel(command)));
			}
			finally {
				WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
			}
		}
	}
	
	protected String getLabel(final Command command) {
		String label= command.getLabel();
		if (label == null) {
			label= ""; //$NON-NLS-1$
		}
		return label;
	}
	
	protected String getTipLabel(final Command command) {
		String label= command.getDescription();
		if (label == null) {
			label= getLabel(command);
		}
		return label;
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		this.domain.getCommandStack().undo();
		return null;
	}
	
}
