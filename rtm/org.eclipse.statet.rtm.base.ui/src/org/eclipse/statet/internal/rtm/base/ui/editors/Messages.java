/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.base.ui.editors;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String EFEditor_MultiObjectSelected_message;
	public static String EFEditor_error_ProblemsInFile_message;
	public static String EFEditor_FileConflict_title;
	public static String EFEditor_FileConflict_message;
	
	public static String RTaskEditor_FirstPage_label;
	public static String RTaskEditor_RCodePage_label;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
