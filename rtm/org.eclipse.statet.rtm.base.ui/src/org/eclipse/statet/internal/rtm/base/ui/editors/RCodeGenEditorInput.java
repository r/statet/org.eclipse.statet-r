/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.base.ui.editors;

import org.eclipse.jface.resource.ImageDescriptor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.ui.input.BasicSourceFragmentEditorInput;
import org.eclipse.statet.r.ui.RUI;


@NonNullByDefault
public class RCodeGenEditorInput extends BasicSourceFragmentEditorInput<SourceFragment> {
	
	
	public RCodeGenEditorInput(final SourceFragment fragment) {
		super(fragment);
	}
	
	
	@Override
	public @Nullable ImageDescriptor getImageDescriptor() {
		return RUI.getImageDescriptor(RUI.IMG_OBJ_R_SCRIPT);
	}
	
	
}
