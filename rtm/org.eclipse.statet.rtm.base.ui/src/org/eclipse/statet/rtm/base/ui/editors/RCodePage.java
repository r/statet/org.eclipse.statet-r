/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.editors;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;

import org.eclipse.statet.base.ui.IStatetUIMenuIds;
import org.eclipse.statet.internal.r.ui.editors.REditor;
import org.eclipse.statet.internal.rtm.base.ui.editors.Messages;
import org.eclipse.statet.rtm.base.ui.RtModelUIPlugin;


public class RCodePage extends REditor implements IFormPage {
	
	
	public static final String ID= "RCode"; //$NON-NLS-1$
	
	
	private RTaskEditor editor;
	private int index;
	
	private Control control;
	
	
	public RCodePage(final RTaskEditor editor) {
		initialize(editor);
		setPartName(Messages.RTaskEditor_RCodePage_label);
	}
	
	
	@Override
	public void initialize(final FormEditor editor) {
		this.editor= (RTaskEditor) editor;
	}
	
	@Override
	protected void initializeEditor() {
		super.initializeEditor();
		
//		setHelpContextId();
		setEditorContextMenuId("org.eclipse.statet.rtm.menus.RCodePageContextMenu"); //$NON-NLS-1$
	}
	
	@Override
	public FormEditor getEditor() {
		return this.editor;
	}
	
	@Override
	public IManagedForm getManagedForm() {
		return null;
	}
	
	@Override
	public void setActive(final boolean active) {
	}
	
	@Override
	public boolean isActive() {
		return (this == this.editor.getActivePageInstance());
	}
	
	@Override
	public boolean canLeaveThePage() {
		return true;
	}
	
	@Override
	public void createPartControl(final Composite parent) {
		super.createPartControl(parent);
		this.control= getAdapter(Control.class);
	}
	
	@Override
	public Control getPartControl() {
		return this.control;
	}
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public int getIndex() {
		return this.index;
	}
	
	@Override
	public void setIndex(final int index) {
		this.index= index;
	}
	
	@Override
	public boolean isEditor() {
		return true;
	}
	
	@Override
	public boolean selectReveal(final Object object) {
		return false;
	}
	
	@Override
	protected void createUndoRedoActions() {
	}
	
	@Override
	protected void editorContextMenuAboutToShow(final IMenuManager m) {
		final var site= getSite();
		
		super.editorContextMenuAboutToShow(m);
		
		m.appendToGroup(IStatetUIMenuIds.GROUP_SUBMIT_MENU_ID, new CommandContributionItem(
				new CommandContributionItemParameter(site,
						null, RtModelUIPlugin.RUN_R_TASK_COMMAND_ID,
						CommandContributionItem.STYLE_PUSH )));
	}
	
}
