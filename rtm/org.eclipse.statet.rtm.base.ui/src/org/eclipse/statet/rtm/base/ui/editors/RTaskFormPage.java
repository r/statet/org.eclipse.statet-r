/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.editors;

import org.eclipse.statet.ecommons.emf.ui.forms.EFFormPage;

import org.eclipse.statet.internal.rtm.base.ui.editors.Messages;


public abstract class RTaskFormPage extends EFFormPage {
	
	
	public RTaskFormPage(final RTaskEditor editor) {
		super(editor, RTaskEditor.MAIN_PAGE_ID, Messages.RTaskEditor_FirstPage_label);
	}
	
	public RTaskFormPage(final RTaskEditor editor, final String id, final String title) {
		super(editor, id, title);
	}
	
	
}
