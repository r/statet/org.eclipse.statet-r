/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.rexpr;

import java.util.List;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.rtm.rtdata.types.RExpr;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


public class RExprLabelProvider extends CellLabelProvider implements ILabelProvider {
	
	
	private final List<RExprTypeUIAdapter> uiAdapters;
	
	
	public RExprLabelProvider(final List<RExprTypeUIAdapter> uiAdapters) {
		this.uiAdapters= uiAdapters;
	}
	
	
	private RExprTypeUIAdapter getAdapter(final String typeKey) {
		for (final RExprTypeUIAdapter uiAdapter : this.uiAdapters) {
			if (uiAdapter.getType().getTypeKey() == typeKey) {
				return uiAdapter;
			}
		}
		return null;
	}
	
	@Override
	public Image getImage(final Object element) {
		if (element instanceof RTypedExpr) {
			final RExprTypeUIAdapter uiAdapter= getAdapter(((RTypedExpr) element).getTypeKey());
			if (uiAdapter != null) {
				return uiAdapter.getImage();
			}
		}
		return null;
	}
	
	@Override
	public String getText(final Object element) {
		if (element instanceof RExpr) {
			return ((RExpr) element).getExpr();
		}
		return ""; //$NON-NLS-1$
	}
	
	@Override
	public void update(final ViewerCell cell) {
		final Object element= cell.getElement();
		cell.setImage(getImage(element));
		cell.setText(getText(element));
	}
	
}
