/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.rexpr;

import java.util.List;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.databinding.edit.IEMFEditValueProperty;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.databinding.jface.ObjValueObservable;
import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.EFProperty;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;

import org.eclipse.statet.rtm.base.ui.editors.RtFormToolkit;
import org.eclipse.statet.rtm.base.util.RExprType;
import org.eclipse.statet.rtm.base.util.RExprTypes;


public class RExprValueProperty extends EFProperty {
	
	
	private final IRExprTypesUIProvider provider;
	private final RExprTypes types;
	
	private RExprWidget widget;
	
	private IObservableValue<RExprType> modelObservable;
	
	
	public RExprValueProperty(final String label, final String tooltip,
			final EClass eClass, final EStructuralFeature eFeature,
			final IRExprTypesUIProvider provider) {
		super(label, tooltip, eClass, eFeature);
		
		this.provider= provider;
		this.types= provider.getTypes(getEClass(), getEFeature());
	}
	
	
	protected IRExprTypesUIProvider getProvider() {
		return this.provider;
	}
	
	@Override
	public void create(final Composite parent, final IEFFormPage page) {
		create(parent, page, 0);
	}
	
	public void create(final Composite parent, final IEFFormPage page, final int options) {
		final IRExprTypesUIProvider provider= getProvider();
		final List<RExprTypeUIAdapter> uiAdapters= provider.getUIAdapters(this.types,
				getEClass(), getEFeature() );
		final RtFormToolkit toolkit= (RtFormToolkit) page.getToolkit();
		
		toolkit.createPropLabel(parent, getLabel(), getTooltip());
		
		this.widget= toolkit.createPropRTypedExpr(parent, options, this.types, uiAdapters);
	}
	
	@Override
	public RExprWidget getControl() {
		return this.widget;
	}
	
	@Override
	public void bind(final IEMFEditContext context) {
		super.bind(context);
		
		this.widget.setContext(this);
		
		final IEMFEditValueProperty emfProperty= EMFEditProperties.value(getEditingDomain(),
				getEFeature() );
		this.modelObservable= emfProperty.observeDetail(getBaseObservable());
		final IObservableValue swtObservable= new ObjValueObservable<>(getRealm(), this.widget);
		
		getDataBindingContext().bindValue(swtObservable, this.modelObservable);
	}
	
	@Override
	public IObservableValue<RExprType> getPropertyObservable() {
		return this.modelObservable;
	}
	
}
