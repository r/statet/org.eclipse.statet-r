/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.rexpr;

import org.eclipse.core.databinding.conversion.IConverter;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


@NonNullByDefault
public class String2RTypedExprConverter implements IConverter<String, @Nullable RTypedExpr> {
	
	
	private final String type;
	
	
	public String2RTypedExprConverter() {
		this(RTypedExpr.R);
	}
	
	public String2RTypedExprConverter(final String type) {
		this.type= type;
	}
	
	
	@Override
	public Object getFromType() {
		return String.class;
	}
	
	@Override
	public Object getToType() {
		return RTypedExpr.class;
	}
	
	@Override
	public @Nullable RTypedExpr convert(final @Nullable String fromObject) {
		final String s= fromObject;
		if (s == null || s.isEmpty()) {
			return null;
		}
		return new RTypedExpr(this.type, fromObject);
	}
	
}
