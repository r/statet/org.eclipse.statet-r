/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ftable.core;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.rtm.base.util.IRExprTypesProvider;
import org.eclipse.statet.rtm.base.util.RExprType;
import org.eclipse.statet.rtm.base.util.RExprTypes;
import org.eclipse.statet.rtm.ftable.FTablePackage;
import org.eclipse.statet.rtm.rtdata.RtDataPackage;


public class FTableExprTypesProvider implements IRExprTypesProvider {
	
	
	private static final RExprTypes T_DATA= new RExprTypes(
			RExprType.DATAFRAME_TYPE );
	
	private static final RExprTypes T_DATA_VAR= new RExprTypes(
			RExprType.DATAFRAME_COLUMN_TYPE );
	
	private static final RExprTypes T_LABEL= new RExprTypes(ImCollections.newList(
			RExprType.TEXT_VALUE_TYPE,
			RExprType.EXPR_VALUE_TYPE ), 0 );
	
	private static final RExprTypes T_LABEL_VAR= new RExprTypes(ImCollections.newList(
			RExprType.TEXT_VALUE_TYPE,
			RExprType.EXPR_VALUE_TYPE,
			RExprType.DATAFRAME_COLUMN_TYPE), 2 );
	
	private static final RExprTypes T_COLOR_VAR= new RExprTypes(ImCollections.newList(
			RExprType.EXPR_COLOR_VALUE_TYPE,
			RExprType.DATAFRAME_COLUMN_TYPE ), 0 );
	
	private static final RExprTypes T_OTHER_VAR= new RExprTypes(ImCollections.newList(
			RExprType.EXPR_VALUE_TYPE,
			RExprType.DATAFRAME_COLUMN_TYPE ), 0 );
	
	private static final RExprTypes T_OTHER= new RExprTypes(
			RExprType.EXPR_VALUE_TYPE );
	
	
	public static FTableExprTypesProvider INSTANCE= new FTableExprTypesProvider();
	
	
	@Override
	public RExprTypes getTypes(final EClass eClass, final EStructuralFeature eFeature) {
		switch (eClass.getClassifierID()) {
		case FTablePackage.FTABLE:
			switch (eFeature.getFeatureID()) {
			case FTablePackage.FTABLE__COL_VARS:
			case FTablePackage.FTABLE__ROW_VARS:
				return T_DATA_VAR;
			default:
				break;
			}
			break;
		default:
			break;
		}
		
		if (eFeature.getEType() == RtDataPackage.Literals.RDATA_FRAME) {
			return T_DATA;
		}
		if (eFeature.getEType() == RtDataPackage.Literals.RDATA_FILTER) {
			return T_OTHER;
		}
		if (eFeature.getEType() == RtDataPackage.Literals.RTEXT) {
			return T_LABEL;
		}
		if (eFeature.getEType() == RtDataPackage.Literals.RLABEL) {
			return T_LABEL;
		}
		return T_OTHER;
	}
	
}
