/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Geom Point Layer</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.GeomPointLayer#getPositionXJitter <em>Position XJitter</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.GeomPointLayer#getPositionYJitter <em>Position YJitter</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGeomPointLayer()
 * @model
 * @generated
 */
public interface GeomPointLayer extends XYVarLayer, PropShapeProvider, PropSizeProvider, PropColorProvider, PropFillProvider, PropAlphaProvider {
	/**
	 * Returns the value of the '<em><b>Position XJitter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position XJitter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position XJitter</em>' attribute.
	 * @see #setPositionXJitter(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGeomPointLayer_PositionXJitter()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RNum"
	 * @generated
	 */
	RTypedExpr getPositionXJitter();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.GeomPointLayer#getPositionXJitter <em>Position XJitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position XJitter</em>' attribute.
	 * @see #getPositionXJitter()
	 * @generated
	 */
	void setPositionXJitter(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>Position YJitter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position YJitter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position YJitter</em>' attribute.
	 * @see #setPositionYJitter(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGeomPointLayer_PositionYJitter()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RNum"
	 * @generated
	 */
	RTypedExpr getPositionYJitter();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.GeomPointLayer#getPositionYJitter <em>Position YJitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position YJitter</em>' attribute.
	 * @see #getPositionYJitter()
	 * @generated
	 */
	void setPositionYJitter(RTypedExpr value);

} // GeomPointLayer
