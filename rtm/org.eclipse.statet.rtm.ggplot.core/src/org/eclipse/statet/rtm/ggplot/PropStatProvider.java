/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prop Stat Provider</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.PropStatProvider#getStat <em>Stat</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getPropStatProvider()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface PropStatProvider extends EObject {
	/**
	 * Returns the value of the '<em><b>Stat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stat</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stat</em>' containment reference.
	 * @see #setStat(Stat)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getPropStatProvider_Stat()
	 * @model containment="true"
	 * @generated
	 */
	Stat getStat();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.PropStatProvider#getStat <em>Stat</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stat</em>' containment reference.
	 * @see #getStat()
	 * @generated
	 */
	void setStat(Stat value);

} // PropStatProvider
