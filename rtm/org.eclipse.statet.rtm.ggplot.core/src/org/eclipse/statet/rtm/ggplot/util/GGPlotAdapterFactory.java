/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.statet.rtm.ggplot.FacetLayout;
import org.eclipse.statet.rtm.ggplot.GGPlot;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.GeomAblineLayer;
import org.eclipse.statet.rtm.ggplot.GeomBarLayer;
import org.eclipse.statet.rtm.ggplot.GeomBoxplotLayer;
import org.eclipse.statet.rtm.ggplot.GeomHistogramLayer;
import org.eclipse.statet.rtm.ggplot.GeomLineLayer;
import org.eclipse.statet.rtm.ggplot.GeomPointLayer;
import org.eclipse.statet.rtm.ggplot.GeomSmoothLayer;
import org.eclipse.statet.rtm.ggplot.GeomTextLayer;
import org.eclipse.statet.rtm.ggplot.GeomTileLayer;
import org.eclipse.statet.rtm.ggplot.GeomViolinLayer;
import org.eclipse.statet.rtm.ggplot.GridFacetLayout;
import org.eclipse.statet.rtm.ggplot.IdentityStat;
import org.eclipse.statet.rtm.ggplot.Layer;
import org.eclipse.statet.rtm.ggplot.PropAlphaProvider;
import org.eclipse.statet.rtm.ggplot.PropColorProvider;
import org.eclipse.statet.rtm.ggplot.PropDataProvider;
import org.eclipse.statet.rtm.ggplot.PropFillProvider;
import org.eclipse.statet.rtm.ggplot.PropGroupVarProvider;
import org.eclipse.statet.rtm.ggplot.PropLineTypeProvider;
import org.eclipse.statet.rtm.ggplot.PropShapeProvider;
import org.eclipse.statet.rtm.ggplot.PropSizeProvider;
import org.eclipse.statet.rtm.ggplot.PropStatProvider;
import org.eclipse.statet.rtm.ggplot.PropXVarProvider;
import org.eclipse.statet.rtm.ggplot.PropYVarProvider;
import org.eclipse.statet.rtm.ggplot.Stat;
import org.eclipse.statet.rtm.ggplot.SummaryStat;
import org.eclipse.statet.rtm.ggplot.TextStyle;
import org.eclipse.statet.rtm.ggplot.WrapFacetLayout;
import org.eclipse.statet.rtm.ggplot.XVarLayer;
import org.eclipse.statet.rtm.ggplot.XYVarLayer;


/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage
 * @generated
 */
public class GGPlotAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GGPlotPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GGPlotAdapterFactory() {
		if (modelPackage == null) {
			modelPackage= GGPlotPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(final Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GGPlotSwitch<Adapter> modelSwitch =
		new GGPlotSwitch<>() {
			@Override
			public Adapter caseGGPlot(final GGPlot object) {
				return createGGPlotAdapter();
			}
			@Override
			public Adapter caseLayer(final Layer object) {
				return createLayerAdapter();
			}
			@Override
			public Adapter caseXVarLayer(final XVarLayer object) {
				return createXVarLayerAdapter();
			}
			@Override
			public Adapter caseXYVarLayer(final XYVarLayer object) {
				return createXYVarLayerAdapter();
			}
			@Override
			public Adapter caseGeomAblineLayer(final GeomAblineLayer object) {
				return createGeomAblineLayerAdapter();
			}
			@Override
			public Adapter caseGeomBarLayer(final GeomBarLayer object) {
				return createGeomBarLayerAdapter();
			}
			@Override
			public Adapter caseGeomBoxplotLayer(final GeomBoxplotLayer object) {
				return createGeomBoxplotLayerAdapter();
			}
			@Override
			public Adapter caseGeomHistogramLayer(final GeomHistogramLayer object) {
				return createGeomHistogramLayerAdapter();
			}
			@Override
			public Adapter caseGeomLineLayer(final GeomLineLayer object) {
				return createGeomLineLayerAdapter();
			}
			@Override
			public Adapter caseGeomPointLayer(final GeomPointLayer object) {
				return createGeomPointLayerAdapter();
			}
			@Override
			public Adapter caseGeomTextLayer(final GeomTextLayer object) {
				return createGeomTextLayerAdapter();
			}
			@Override
			public Adapter caseGeomSmoothLayer(final GeomSmoothLayer object) {
				return createGeomSmoothLayerAdapter();
			}
			@Override
			public Adapter caseGeomTileLayer(final GeomTileLayer object) {
				return createGeomTileLayerAdapter();
			}
			@Override
			public Adapter caseGeomViolinLayer(final GeomViolinLayer object) {
				return createGeomViolinLayerAdapter();
			}
			@Override
			public Adapter caseFacetLayout(final FacetLayout object) {
				return createFacetLayoutAdapter();
			}
			@Override
			public Adapter caseGridFacetLayout(final GridFacetLayout object) {
				return createGridFacetLayoutAdapter();
			}
			@Override
			public Adapter caseWrapFacetLayout(final WrapFacetLayout object) {
				return createWrapFacetLayoutAdapter();
			}
			@Override
			public Adapter caseStat(final Stat object) {
				return createStatAdapter();
			}
			@Override
			public Adapter caseIdentityStat(final IdentityStat object) {
				return createIdentityStatAdapter();
			}
			@Override
			public Adapter caseSummaryStat(final SummaryStat object) {
				return createSummaryStatAdapter();
			}
			@Override
			public Adapter caseTextStyle(final TextStyle object) {
				return createTextStyleAdapter();
			}
			@Override
			public Adapter casePropDataProvider(final PropDataProvider object) {
				return createPropDataProviderAdapter();
			}
			@Override
			public Adapter casePropXVarProvider(final PropXVarProvider object) {
				return createPropXVarProviderAdapter();
			}
			@Override
			public Adapter casePropYVarProvider(final PropYVarProvider object) {
				return createPropYVarProviderAdapter();
			}
			@Override
			public Adapter casePropStatProvider(final PropStatProvider object) {
				return createPropStatProviderAdapter();
			}
			@Override
			public Adapter casePropGroupVarProvider(final PropGroupVarProvider object) {
				return createPropGroupVarProviderAdapter();
			}
			@Override
			public Adapter casePropShapeProvider(final PropShapeProvider object) {
				return createPropShapeProviderAdapter();
			}
			@Override
			public Adapter casePropLineTypeProvider(final PropLineTypeProvider object) {
				return createPropLineTypeProviderAdapter();
			}
			@Override
			public Adapter casePropSizeProvider(final PropSizeProvider object) {
				return createPropSizeProviderAdapter();
			}
			@Override
			public Adapter casePropColorProvider(final PropColorProvider object) {
				return createPropColorProviderAdapter();
			}
			@Override
			public Adapter casePropFillProvider(final PropFillProvider object) {
				return createPropFillProviderAdapter();
			}
			@Override
			public Adapter casePropAlphaProvider(final PropAlphaProvider object) {
				return createPropAlphaProviderAdapter();
			}
			@Override
			public Adapter defaultCase(final EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(final Notifier target) {
		return this.modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GGPlot <em>GG Plot</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GGPlot
	 * @generated
	 */
	public Adapter createGGPlotAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.Layer <em>Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.Layer
	 * @generated
	 */
	public Adapter createLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.XVarLayer <em>XVar Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.XVarLayer
	 * @generated
	 */
	public Adapter createXVarLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.XYVarLayer <em>XY Var Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.XYVarLayer
	 * @generated
	 */
	public Adapter createXYVarLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomPointLayer <em>Geom Point Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomPointLayer
	 * @generated
	 */
	public Adapter createGeomPointLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomBarLayer <em>Geom Bar Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomBarLayer
	 * @generated
	 */
	public Adapter createGeomBarLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomTextLayer <em>Geom Text Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomTextLayer
	 * @generated
	 */
	public Adapter createGeomTextLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomSmoothLayer <em>Geom Smooth Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomSmoothLayer
	 * @generated
	 */
	public Adapter createGeomSmoothLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomViolinLayer <em>Geom Violin Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomViolinLayer
	 * @generated
	 */
	public Adapter createGeomViolinLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.FacetLayout <em>Facet Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.FacetLayout
	 * @generated
	 */
	public Adapter createFacetLayoutAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GridFacetLayout <em>Grid Facet Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GridFacetLayout
	 * @generated
	 */
	public Adapter createGridFacetLayoutAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.WrapFacetLayout <em>Wrap Facet Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.WrapFacetLayout
	 * @generated
	 */
	public Adapter createWrapFacetLayoutAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.TextStyle <em>Text Style</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.TextStyle
	 * @generated
	 */
	public Adapter createTextStyleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.Stat <em>Stat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.Stat
	 * @generated
	 */
	public Adapter createStatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.IdentityStat <em>Identity Stat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.IdentityStat
	 * @generated
	 */
	public Adapter createIdentityStatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.SummaryStat <em>Summary Stat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.SummaryStat
	 * @generated
	 */
	public Adapter createSummaryStatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropDataProvider <em>Prop Data Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropDataProvider
	 * @generated
	 */
	public Adapter createPropDataProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropXVarProvider <em>Prop XVar Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropXVarProvider
	 * @generated
	 */
	public Adapter createPropXVarProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropYVarProvider <em>Prop YVar Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropYVarProvider
	 * @generated
	 */
	public Adapter createPropYVarProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropStatProvider <em>Prop Stat Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropStatProvider
	 * @generated
	 */
	public Adapter createPropStatProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropGroupVarProvider <em>Prop Group Var Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropGroupVarProvider
	 * @generated
	 */
	public Adapter createPropGroupVarProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropColorProvider <em>Prop Color Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropColorProvider
	 * @generated
	 */
	public Adapter createPropColorProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropFillProvider <em>Prop Fill Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropFillProvider
	 * @generated
	 */
	public Adapter createPropFillProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropAlphaProvider <em>Prop Alpha Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropAlphaProvider
	 * @generated
	 */
	public Adapter createPropAlphaProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropShapeProvider <em>Prop Shape Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropShapeProvider
	 * @generated
	 */
	public Adapter createPropShapeProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropLineTypeProvider <em>Prop Line Type Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropLineTypeProvider
	 * @generated
	 */
	public Adapter createPropLineTypeProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.PropSizeProvider <em>Prop Size Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.PropSizeProvider
	 * @generated
	 */
	public Adapter createPropSizeProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomBoxplotLayer <em>Geom Boxplot Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomBoxplotLayer
	 * @generated
	 */
	public Adapter createGeomBoxplotLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomHistogramLayer <em>Geom Histogram Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomHistogramLayer
	 * @generated
	 */
	public Adapter createGeomHistogramLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomLineLayer <em>Geom Line Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomLineLayer
	 * @generated
	 */
	public Adapter createGeomLineLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomAblineLayer <em>Geom Abline Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomAblineLayer
	 * @generated
	 */
	public Adapter createGeomAblineLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.statet.rtm.ggplot.GeomTileLayer <em>Geom Tile Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.statet.rtm.ggplot.GeomTileLayer
	 * @generated
	 */
	public Adapter createGeomTileLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //GGPlotAdapterFactory
