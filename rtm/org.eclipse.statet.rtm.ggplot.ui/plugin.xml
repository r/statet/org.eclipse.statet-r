<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<!--
 #=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<plugin>

   <extension
         point="org.eclipse.ui.editors">
      <editor
            id="org.eclipse.statet.rtm.ggplot.editors.GGPlot"
            icon="icons/obj_16/ggplot_task.png"
            name="%editors_GGPlot_label"
            class="org.eclipse.statet.internal.rtm.ggplot.ui.editors.GGPlotEditor"
            contributorClass="org.eclipse.statet.ecommons.emf.ui.forms.EFEditorActionBarContributor">
         <contentTypeBinding
               contentTypeId="org.eclipse.statet.rtm.contentTypes.GGPlot">
         </contentTypeBinding>
      </editor>
   </extension>

   <extension
         point="org.eclipse.ui.commands">
      <command
            id="org.eclipse.statet.rtm.ggplot.commands.NewGGPlot"
            categoryId="org.eclipse.statet.rtm.commands.RTasksCategory"
            name="%commands_NewGGPlot_name"
            description="%commands_NewGGPlot_description"
            defaultHandler="org.eclipse.statet.rtm.ggplot.ui.actions.NewGGPlotHandler">
      </command>
   </extension>

   <extension point="org.eclipse.ui.newWizards">
      <wizard
            id="org.eclipse.statet.rtm.ggplot.newWizards.NewGGPlot"
            category="org.eclipse.statet.r.newWizards.RCategory/org.eclipse.statet.rtm.base.newWizards.RTasksCategory"
            icon="icons/obj_16/ggplot_task.png"
            name="%newWizards_NewGGPlot_name"
            class="org.eclipse.statet.rtm.ggplot.ui.actions.NewGGPlotFileWizard">
         <description>%newWizards_NewGGPlot_description</description>
         <selection class="org.eclipse.core.resources.IResource"/>
      </wizard>
   </extension>
   <extension
         point="org.eclipse.ui.menus">
      <menuContribution
            allPopups="false"
            locationURI="menu:org.eclipse.statet.r.menus.RMain?after=r_tasks.graphics">
         <command
               commandId="org.eclipse.statet.rtm.ggplot.commands.NewGGPlot"
               style="push">
         </command>
      </menuContribution>
   </extension>
   <extension
         point="org.eclipse.ui.perspectiveExtensions">
      <perspectiveExtension
            targetID="org.eclipse.statet.rtm.base.perspectives.RGraphics">
         <newWizardShortcut
               id="org.eclipse.statet.rtm.ggplot.newWizards.NewGGPlot">
         </newWizardShortcut>
      </perspectiveExtension>
   </extension>
   <extension
         point="org.eclipse.ui.views.properties.tabbed.propertyContributor">
      <propertyContributor
            contributorId="org.eclipse.statet.rtm.ggplot.editors.GGPlot">
         <propertyCategory
               category="main"></propertyCategory>
         <propertyCategory
               category="layer">
         </propertyCategory>
      </propertyContributor>
   </extension>
   <extension
         point="org.eclipse.ui.views.properties.tabbed.propertyTabs">
      <propertyTabs
            contributorId="org.eclipse.statet.rtm.ggplot.editors.GGPlot">
         <propertyTab
               id="org.eclipse.statet.rtm.ggplot.properties.MainTitleStyleTab"
               category="main"
               label="Main Title"
               indented="true">
         </propertyTab>
         <propertyTab
               id="org.eclipse.statet.rtm.ggplot.properties.AxXLabelStyleTab"
               category="main"
               label="X-Axis Label"
               indented="true"
               afterTab="org.eclipse.statet.rtm.ggplot.properties.MainTitleStyleTab">
         </propertyTab>
         <propertyTab
               id="org.eclipse.statet.rtm.ggplot.properties.AxYLabelStyleTab"
               category="main"
               label="Y-Axis Label"
               indented="true"
               afterTab="org.eclipse.statet.rtm.ggplot.properties.AxXLabelStyleTab">
         </propertyTab>
         <propertyTab
               id="org.eclipse.statet.rtm.ggplot.properties.AxXTextStyleTab"
               category="main"
               label="X-Axis Text"
               indented="true"
               afterTab="org.eclipse.statet.rtm.ggplot.properties.AxYLabelStyleTab">
         </propertyTab>
         <propertyTab
               id="org.eclipse.statet.rtm.ggplot.properties.AxYTextStyleTab"
               category="main"
               label="Y-Axis Text"
               indented="true"
               afterTab="org.eclipse.statet.rtm.ggplot.properties.AxXTextStyleTab">
         </propertyTab>
         <propertyTab
               id="org.eclipse.statet.rtm.ggplot.properties.LayerTextStyleTab"
               category="main"
               label="Layer Text"
               indented="true"
               afterTab="org.eclipse.statet.rtm.ggplot.properties.AxYTextStyleTab">
         </propertyTab>
      </propertyTabs>
   </extension>
   <extension
         point="org.eclipse.ui.views.properties.tabbed.propertySections">
      <propertySections
            contributorId="org.eclipse.statet.rtm.ggplot.editors.GGPlot">
         <propertySection
               id="org.eclipse.statet.rtm.ggplot.properties.MainTitleStyleSection"
               tab="org.eclipse.statet.rtm.ggplot.properties.MainTitleStyleTab"
               class="org.eclipse.statet.internal.rtm.ggplot.ui.editors.TextStylePropertySection$MainTitle"
               filter="org.eclipse.jface.viewers.AcceptAllFilter">
         </propertySection>
         <propertySection
               id="org.eclipse.statet.rtm.ggplot.properties.AxXLabelStyleSection"
               tab="org.eclipse.statet.rtm.ggplot.properties.AxXLabelStyleTab"
               class="org.eclipse.statet.internal.rtm.ggplot.ui.editors.TextStylePropertySection$AxXLabel"
               filter="org.eclipse.jface.viewers.AcceptAllFilter">
         </propertySection>
         <propertySection
               id="org.eclipse.statet.rtm.ggplot.properties.AxYLabelStyleSection"
               tab="org.eclipse.statet.rtm.ggplot.properties.AxYLabelStyleTab"
               class="org.eclipse.statet.internal.rtm.ggplot.ui.editors.TextStylePropertySection$AxYLabel"
               filter="org.eclipse.jface.viewers.AcceptAllFilter">
         </propertySection>
         <propertySection
               id="org.eclipse.statet.rtm.ggplot.properties.AxXTextStyleSection"
               tab="org.eclipse.statet.rtm.ggplot.properties.AxXTextStyleTab"
               class="org.eclipse.statet.internal.rtm.ggplot.ui.editors.TextStylePropertySection$AxXText"
               filter="org.eclipse.jface.viewers.AcceptAllFilter">
         </propertySection>
         <propertySection
               id="org.eclipse.statet.rtm.ggplot.properties.AxYTextStyleSection"
               tab="org.eclipse.statet.rtm.ggplot.properties.AxYTextStyleTab"
               class="org.eclipse.statet.internal.rtm.ggplot.ui.editors.TextStylePropertySection$AxYText"
               filter="org.eclipse.jface.viewers.AcceptAllFilter">
         </propertySection>
         <propertySection
               id="org.eclipse.statet.rtm.ggplot.properties.LayerTextStyleSection"
               tab="org.eclipse.statet.rtm.ggplot.properties.LayerTextStyleTab"
               class="org.eclipse.statet.internal.rtm.ggplot.ui.editors.TextStylePropertySection$LayerText"
               enablesFor="2"
               filter="org.eclipse.statet.internal.rtm.ggplot.ui.editors.LayerTextStyleFilter">
         </propertySection>
      </propertySections>
   </extension>

</plugin>
