/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import java.util.List;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.ui.forms.widgets.ExpandableComposite;

import org.eclipse.statet.ecommons.ui.util.UIAccess;


public class AutoExpandListener implements IValueChangeListener<Object>, DisposeListener {
	
	
	private final ExpandableComposite composite;
	
	private int values;
	
	private List<? extends IObservable> observables;
	
	
	public AutoExpandListener(final ExpandableComposite composite, final List<? extends IObservable> observables) {
		this.composite= composite;
		
		this.observables= observables;
		for (final IObservable observable : observables) {
			if (observable instanceof IObservableValue) {
				final IObservableValue<?> observableValue= (IObservableValue<?>) observable;
				if (observableValue.getValue() != null) {
					this.values++;
				}
				observableValue.addValueChangeListener(this);
			}
		}
		if (this.values > 0) {
			this.composite.setExpanded(true);
		}
		
		this.composite.addDisposeListener(this);
	}
	
	
	@Override
	public void handleValueChange(final ValueChangeEvent<?> event) {
		if (this.values > 0 || this.composite.isExpanded() || this.observables == null) {
			return;
		}
		if (event.diff.getOldValue() != null) {
			if (event.diff.getNewValue() == null) {
				this.values--;
			}
		}
		else {
			if (event.diff.getNewValue() != null) {
				this.values++;
			}
		}
		if (this.values > 0 && UIAccess.isOkToUse(this.composite)) {
			this.composite.setExpanded(true);
		}
	}
	
	@Override
	public void widgetDisposed(final DisposeEvent e) {
		final List<? extends IObservable> observables= this.observables;
		this.observables= null;
		if (observables != null) {
			for (final IObservable observable : observables) {
				if (observable instanceof IObservableValue) {
					final IObservableValue<?> observableValue= (IObservableValue<?>) observable;
					if (!observableValue.isDisposed()) {
						observableValue.removeValueChangeListener(this);
					}
				}
			}
		}
	}
	
}
