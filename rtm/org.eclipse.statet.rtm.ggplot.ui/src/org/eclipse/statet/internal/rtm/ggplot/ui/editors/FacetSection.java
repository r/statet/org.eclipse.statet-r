/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.databinding.CustomViewerObservables;
import org.eclipse.statet.ecommons.emf.ui.databinding.DetailContext;
import org.eclipse.statet.ecommons.emf.ui.forms.Detail;
import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.EFToolkit;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;
import org.eclipse.statet.ecommons.ui.components.WaCombo;
import org.eclipse.statet.ecommons.ui.components.WaComboViewer;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.core.RtGGPlotCorePlugin;


public class FacetSection extends EFFormSection {
	
	
	private static class Details extends DetailStack {
		
		public Details(final IEFFormPage page, final Composite parent) {
			super(page, parent);
		}
		
		
		@Override
		protected Detail createDetail(final EObject value) {
			if (value != null) {
				switch (value.eClass().getClassifierID()) {
				case GGPlotPackage.WRAP_FACET_LAYOUT:
					return new FacetWrapDetail(this);
				case GGPlotPackage.GRID_FACET_LAYOUT:
					return new FacetGridDetail(this);
				default:
					break;
				}
			}
			return super.createDetail(value);
		}
		
	}
	
	
	private WaCombo typeControl;
	private TableViewer typeViewer;
	
	private DetailStack detailStack;
	
	
	public FacetSection(final IEFFormPage page, final Composite parent) {
		super(page, parent,
				"Layout / Facets",
				null );
		
		createClient();
	}
	
	@Override
	protected Layout createClientLayout() {
		final GridLayout layout= (GridLayout) super.createClientLayout();
		
		layout.marginTop= 0;
		
		return layout;
	}
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		final EFToolkit toolkit= page.getToolkit();
		
		this.typeControl= new WaCombo(composite, SWT.BORDER);
		this.typeViewer= new WaComboViewer(this.typeControl);
		toolkit.adapt(this.typeControl);
		this.typeControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
		
		this.typeViewer.setLabelProvider(new AdapterFactoryLabelProvider(
				page.getEditor().getAdapterFactory()) {
			@Override
			public Image getColumnImage(final Object object, final int columnIndex) {
				if (object instanceof String && columnIndex == 0) {
					return getImageFromObject(RtGGPlotCorePlugin.INSTANCE.getImage("full/obj16/NoFacetLayout"));
				}
				return super.getColumnImage(object, columnIndex);
			}
		});
		this.typeViewer.setContentProvider(ArrayContentProvider.getInstance());
		
		this.detailStack= new Details(page, composite);
		this.detailStack.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		final IObservableValue facetObservable= EMFEditProperties.value(context.getEditingDomain(),
						GGPlotPackage.Literals.GG_PLOT__FACET )
				.observeDetail(context.getBaseObservable());
		{	final List<Object> options= new ArrayList<>();
			options.add("Single Plot / No Facets");
			options.add(GGPlotPackage.eINSTANCE.getGGPlotFactory().createWrapFacetLayout());
			options.add(GGPlotPackage.eINSTANCE.getGGPlotFactory().createGridFacetLayout());
			context.getDataBindingContext().bindValue(
					CustomViewerObservables.observeComboSelection(this.typeViewer, options),
					facetObservable );
		}
		
		final IEMFEditContext detailContext= new DetailContext(context, facetObservable);
		this.detailStack.setContext(detailContext);
	}
	
}
