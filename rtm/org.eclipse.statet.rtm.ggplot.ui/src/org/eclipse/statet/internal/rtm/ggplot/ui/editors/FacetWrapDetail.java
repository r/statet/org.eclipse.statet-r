/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;
import org.eclipse.statet.ecommons.emf.ui.forms.EFPropertySet;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.rtm.base.ui.rexpr.RExprListProperty;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;


class FacetWrapDetail extends FacetDetail {
	
	
	private final RExprListProperty colVarsProperty;
	private final EFPropertySet otherProperties;
	
	
	public FacetWrapDetail(final DetailStack parent) {
		super(parent);
		
		final EClass eClass= GGPlotPackage.Literals.WRAP_FACET_LAYOUT;
		
		this.colVarsProperty= (RExprListProperty) GGPlotProperties.createProperty(
				eClass, GGPlotPackage.Literals.WRAP_FACET_LAYOUT__COL_VARS,
				"Variables:", "The variables for facets" );
		
		this.otherProperties= new EFPropertySet();
		this.otherProperties.add(GGPlotProperties.createProperty(
				eClass, GGPlotPackage.Literals.WRAP_FACET_LAYOUT__COL_NUM,
				"Number of Columns:", "The number of facets in a single row" ));
		
		createContent();
	}
	
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		LayoutUtils.addSmallFiller(composite, false);
		
		{	final Composite column= new Composite(composite, SWT.NONE);
			column.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			column.setLayout(LayoutUtils.newCompositeGrid(3));
			
			this.colVarsProperty.create(column, page);
		}
		{	final Composite column= new Composite(composite, SWT.NONE);
			column.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			column.setLayout(LayoutUtils.newCompositeGrid(3));
			
			this.otherProperties.createControls(column, page);
		}
		LayoutUtils.addGDDummy(composite, true);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.colVarsProperty.bind(context);
		
		this.otherProperties.bind(context);
	}
	
}
