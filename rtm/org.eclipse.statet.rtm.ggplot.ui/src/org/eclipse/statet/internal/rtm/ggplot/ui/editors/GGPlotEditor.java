/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ui.PartInitException;

import org.eclipse.statet.rtm.base.ui.editors.RTaskEditor;
import org.eclipse.statet.rtm.base.ui.rexpr.HyperlinkType;
import org.eclipse.statet.rtm.base.ui.rexpr.HyperlinkType.IHyperlinkProvider;
import org.eclipse.statet.rtm.base.ui.rexpr.HyperlinkType.PropertyTabLink;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;
import org.eclipse.statet.rtm.ggplot.ui.RtGGPlotDescriptor;


public class GGPlotEditor extends RTaskEditor {
	
	private static final IHyperlinkProvider LINK_DESCRIPTORS= new IHyperlinkProvider() {
		
		private final PropertyTabLink mainTitle= new PropertyTabLink("Text Style...",
				"org.eclipse.statet.rtm.ggplot.properties.MainTitleStyleTab"); //$NON-NLS-1$
		private final PropertyTabLink axXLabel= new PropertyTabLink("Text Style...",
				"org.eclipse.statet.rtm.ggplot.properties.AxXLabelStyleTab"); //$NON-NLS-1$
		private final PropertyTabLink axYLabel= new PropertyTabLink("Text Style...",
				"org.eclipse.statet.rtm.ggplot.properties.AxYLabelStyleTab"); //$NON-NLS-1$
		private final PropertyTabLink layerTextLabel= new PropertyTabLink("Text Style...",
				"org.eclipse.statet.rtm.ggplot.properties.LayerTextStyleTab"); //$NON-NLS-1$
		
		@Override
		public PropertyTabLink get(final EClass eClass, final EStructuralFeature eFeature) {
			if (eFeature == Literals.GG_PLOT__MAIN_TITLE) {
				return this.mainTitle;
			}
			if (eFeature == Literals.GG_PLOT__AX_XLABEL) {
				return this.axXLabel;
			}
			if (eFeature == Literals.GG_PLOT__AX_YLABEL) {
				return this.axYLabel;
			}
			if (eFeature == Literals.GEOM_TEXT_LAYER__LABEL) {
				return this.layerTextLabel;
			}
			return null;
		}
		
	};
	
	
	public static String FACET_PAGE_ID= "Facet"; //$NON-NLS-1$
	
	
	public GGPlotEditor() {
		super(RtGGPlotDescriptor.INSTANCE);
	}
	
	
	@Override
	protected IAdapterFactory createContextAdapterFactory() {
		return new ContextAdapterFactory() {
			@Override
			@SuppressWarnings("unchecked")
			public <T> T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
				if (adapterType.equals(HyperlinkType.IHyperlinkProvider.class)) {
					return (T) LINK_DESCRIPTORS;
				}
				return super.getAdapter(adaptableObject, adapterType);
			}
		};
	}
	
	@Override
	protected void addFormPages() throws PartInitException {
		addPage(new MainPage(this));
		addPage(new FacetPage(this));
	}
	
}
