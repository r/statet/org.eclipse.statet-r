/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.EFPropertySet;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


class LayerDataSection extends EFFormSection {
	
	
	private final LayerDetail detail;
	
	private final EFPropertySet properties;
	
	
	public LayerDataSection(final LayerDetail detail, final Composite parent) {
		super(detail.getPage(), parent, (TITLE_STYLE | EXPANDABLE_STYLE));
		
		this.detail= detail;
		
		getSection().setText("Layer Data");
		
		final EClass eClass= this.detail.getEClass();
		
		this.properties= new EFPropertySet();
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_DATA_PROVIDER__DATA,
				"Data:", "The data source for this layer" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_XVAR_PROVIDER__XVAR,
				"x:", "The data for x" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_YVAR_PROVIDER__YVAR,
				"y:", "The data for y" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GEOM_ABLINE_LAYER__INTERCEPT_VAR,
				"Intercept:", "The intercept of the line(s)" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GEOM_ABLINE_LAYER__SLOPE_VAR,
				"Slope:", "The slope of the line(s)" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_STAT_PROVIDER__STAT,
				"Stat:", "The explicite statistical transformation" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_GROUP_VAR_PROVIDER__GROUP_VAR,
				"Group:", "The explicite group definition" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GEOM_TEXT_LAYER__LABEL,
				"Label:", "The text to plot" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GEOM_POINT_LAYER__POSITION_XJITTER,
				"Jitter x:", "The degree of jitter in x direction" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GEOM_POINT_LAYER__POSITION_YJITTER,
				"Jitter y:", "The degree of jitter in y direction" ));
		
		createClient();
	}
	
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		this.properties.createControls(composite, page);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.properties.bind(context);
		
		new AutoExpandListener(getSection(), this.properties.getModelObservables());
	}
	
}
