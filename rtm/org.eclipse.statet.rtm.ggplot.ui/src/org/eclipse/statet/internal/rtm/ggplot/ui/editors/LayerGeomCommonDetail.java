/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage;


public class LayerGeomCommonDetail extends LayerDetail {
	
	
	private LayerDataSection dataSection;
	private LayerAestheticsSection aestheticsSection;
	
	
	public LayerGeomCommonDetail(final DetailStack parent, final EClass eClass) {
		super(parent, eClass);
	}
	
	
	@Override
	protected void createContent(final Composite composite) {
		this.dataSection= new LayerDataSection(this, composite);
		this.dataSection.getSection().setLayoutData(createSectionLayoutData());
		registerSection(this.dataSection);
		switch (getEClass().getClassifierID()) {
		case GGPlotPackage.GEOM_ABLINE_LAYER:
		case GGPlotPackage.GEOM_TEXT_LAYER:
			this.dataSection.getSection().setExpanded(true);
			break;
		}
		
		this.aestheticsSection= new LayerAestheticsSection(this, composite);
		this.aestheticsSection.getSection().setLayoutData(createSectionLayoutData());
		registerSection(this.aestheticsSection);
		this.aestheticsSection.getSection().setExpanded(true);
	}
	
}
