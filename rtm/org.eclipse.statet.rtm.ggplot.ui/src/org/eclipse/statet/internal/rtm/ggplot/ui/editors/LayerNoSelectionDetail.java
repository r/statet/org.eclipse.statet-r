/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;


class LayerNoSelectionDetail extends LayerDetail {
	
	
	private static class MessageSection extends EFFormSection {
		
		
		public MessageSection(final IEFFormPage page, final Composite parent) {
			super(page, parent,
					"No Layer Selected",
					"Select a Layer to edit its details.");
			
			createClient();
		}
		
		
		@Override
		protected void createContent(final Composite composite) {
			final Label label= new Label(composite, SWT.NONE);
			final GridData gd= new GridData();
			gd.horizontalSpan= 3;
			gd.grabExcessHorizontalSpace= true;
			gd.widthHint= 10;
			label.setLayoutData(gd);
		}
		
	}
	
	
	public LayerNoSelectionDetail(final DetailStack parent) {
		super(parent, null);
	}
	
	@Override
	protected void createContent(final Composite composite) {
		final MessageSection section= new MessageSection(getPage(), composite);
		section.getSection().setLayoutData(createSectionLayoutData());
	}
	
}
