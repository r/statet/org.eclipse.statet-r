/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.databinding.DetailContext;
import org.eclipse.statet.ecommons.emf.ui.forms.Detail;
import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.EObjectListProperty;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


public class LayersSection extends EFFormSection {
	
	
	private static class Details extends DetailStack {
		
		
		public Details(final IEFFormPage page, final Composite parent) {
			super(page, parent);
		}
		
		
		@Override
		protected Detail createDetail(final EObject value) {
			if (value != null) {
				switch (value.eClass().getClassifierID()) {
				case GGPlotPackage.GEOM_ABLINE_LAYER:
				case GGPlotPackage.GEOM_BAR_LAYER:
				case GGPlotPackage.GEOM_BOXPLOT_LAYER:
				case GGPlotPackage.GEOM_HISTOGRAM_LAYER:
				case GGPlotPackage.GEOM_LINE_LAYER:
				case GGPlotPackage.GEOM_POINT_LAYER:
				case GGPlotPackage.GEOM_SMOOTH_LAYER:
				case GGPlotPackage.GEOM_TEXT_LAYER:
				case GGPlotPackage.GEOM_TILE_LAYER:
				case GGPlotPackage.GEOM_VIOLIN_LAYER:
					return new LayerGeomCommonDetail(this, value.eClass());
				default:
					return super.createDetail(value);
				}
			}
			return new LayerNoSelectionDetail(this);
		}
		
	}
	
	
	private final EObjectListProperty layersProperty;
	
	private Details details;
	
	
	public LayersSection(final IEFFormPage page, final Composite parent) {
		super(page, parent,
				"Layers",
				null ); 
		
		final EClass eClass= Literals.GG_PLOT;
		this.layersProperty= new EObjectListProperty("Layers", null,
				eClass, Literals.GG_PLOT__LAYERS );
		
		createClient();
	}
	
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		this.layersProperty.create(composite, page);
		((GridData) this.layersProperty.getControl().getLayoutData()).verticalAlignment= SWT.TOP;
	}
	
	protected DetailStack createDetails(final Composite parent) {
		this.details= new Details(getPage(), parent);
		return this.details;
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.layersProperty.bind(context);
		
		final IEMFEditContext detailContext= new DetailContext(context,
				this.layersProperty.getSingleSelectionObservable() );
		this.details.setContext(detailContext);
		
		getSection().getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				final IObservableValue selection= LayersSection.this.layersProperty.getSingleSelectionObservable();
				final IObservableList layers= LayersSection.this.layersProperty.getPropertyObservable();
				if (selection.getValue() == null & !layers.isEmpty()) {
					selection.setValue(layers.get(0));
				}
			}
		});
	}
	
	public ISelectionProvider getSelectionProvider() {
		return this.layersProperty.getSelectionProvider();
	}
	
}
