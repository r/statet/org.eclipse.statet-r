/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.ColumnLayoutData;

import org.eclipse.statet.ecommons.emf.ui.forms.EFLayoutUtil;
import org.eclipse.statet.ecommons.emf.ui.forms.MasterDetailComposite;

import org.eclipse.statet.rtm.base.ui.editors.RTaskEditor;
import org.eclipse.statet.rtm.base.ui.editors.RTaskFormPage;


public class MainPage extends RTaskFormPage {
	
	
	private PlotDataSection mainDataSection;
	private PlotLabelSection labelSection;
	
	private LayersSection layersSection;
	
	
	public MainPage(final RTaskEditor editor) {
		super(editor);
	}
	
	
	@Override
	public void createPartControl(final Composite parent) {
		super.createPartControl(parent);
		
		setSelectionProvider(this.layersSection.getSelectionProvider());
	}
	
	@Override
	protected void createFormBodyContent(final Composite body) {
		{	final Composite composite= addBodyComposite();
			composite.setLayout(EFLayoutUtil.createMainColumnLayout());
			
			this.mainDataSection= new PlotDataSection(this, composite);
			this.mainDataSection.getSection().setLayoutData(new ColumnLayoutData(300));
			registerSection(this.mainDataSection);
			
			this.labelSection= new PlotLabelSection(this, composite);
			this.labelSection.getSection().setLayoutData(new ColumnLayoutData(300));
			registerSection(this.labelSection);
		}
		{	final MasterDetailComposite composite= addBodySashComposite();
			
			this.layersSection= new LayersSection(this, composite.getMasterContainer());
			registerSection(this.layersSection);
			
			this.layersSection.createDetails(composite.getDetailContainer());
			
			composite.layout();
		}
	}
	
}
