/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.EFPropertySet;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


public class PlotLabelSection extends EFFormSection {
	
	
	private final EFPropertySet properties;
	
	
	public PlotLabelSection(final IEFFormPage page, final Composite parent) {
		super(page, parent,
				"Labels",
				"Customize the labels of the plot." );
		
		final EClass eClass= Literals.GG_PLOT;
		this.properties= new EFPropertySet();
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GG_PLOT__MAIN_TITLE,
				"Title:", "The overall title for the plot" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GG_PLOT__AX_XLABEL,
				"x Label:", "The title for the x-axis" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.GG_PLOT__AX_YLABEL,
				"y Label:", "The title for the y-axis" ));
		
		createClient();
	}
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		this.properties.createControls(composite, page);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.properties.bind(context);
	}
	
}
